<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('menus', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('label')->nullable();
			$table->string('title')->nullable();
			$table->integer('type')->unsigned();
			$table->string('content')->nullable();
			$table->string('description')->nullable();
			$table->string('icon')->nullable();
			$table->integer('display')->nullable()->default(0);
			$table->integer('access')->nullable()->default(0);
			$table->integer('listorder')->nullable()->default(0);
			$table->integer('modified_by')->nullable()->default(0);
			$table->timestamps('last_modified');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('menus');
	}

}
