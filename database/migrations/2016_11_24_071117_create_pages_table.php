<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title',225)->nullable();
			$table->string('slug',225);
			$table->text('tags')->nullable();
			$table->text('body')->nullable();
			$table->timestamp('published_at');
            $table->timestamp('deleted_at');
            $table->timestamps('updated_at');
             
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pages');
	}

}
