<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassifiedsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('classifieds', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');		
			$table->string('title')->nullable();
			$table->string('slug',225);
			$table->integer('parentid')->unsigned();
			$table->integer('category')->unsigned();
			$table->integer('location_province')->unsigned();
			$table->integer('location_city')->unsigned();
			$table->integer('location_town')->unsigned();
			$table->integer('location_building')->unsigned();
			$table->string('thumb',255)->nullable();
			$table->decimal('price')->nullable();
			$table->text('tags')->nullable();
			$table->text('extra_fields')->nullable();
			$table->integer('status')->unsigned();
            $table->integer('featured')->unsigned();
            $table->integer('visibility')->unsigned();
            $table->integer('publish')->unsigned();
			$table->string('description')->nullable();
			$table->string('approve',5)->nullable();
            $table->string('show_in_homepage',5)->nullable();
            $table->integer('modified_by')->nullable()->default(0);
            $table->integer('display')->nullable()->default(0);
            $table->timestamp('featured_at');
            $table->timestamp('published_at');
            $table->timestamp('deleted_at');
            $table->timestamps('updated_at');


		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('classifieds');
	}

}
