<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostBoostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('post_boosts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->nullable()->default(0);
			$table->integer('classifieds_id')->nullable()->default(0);
			$table->integer('boost_type')->nullable()->default(0);
			$table->integer('duration')->nullable()->default(0);
			$table->decimal('budget')->nullable();
			$table->string('paypal_id')->nullable();
			$table->string('paypal_reference')->nullable();
			$table->decimal('paid_amount')->nullable();
			$table->integer('status')->nullable()->default(0);
            $table->timestamp('featured_at');
            $table->timestamp('deleted_at');
            $table->timestamps('updated_at');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('post_boosts');
	}

}
