<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('locations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('parentid')->nullable()->default(0);
			$table->string('title')->nullable();
			$table->string('slug')->nullable();
			$table->string('type')->nullable();
			$table->string('icon')->nullable();
			$table->integer('display')->nullable()->default(0);
			$table->integer('access')->nullable()->default(0);
			$table->integer('listorder')->nullable()->default(0);
			$table->integer('modified_by')->nullable()->default(0);
			$table->timestamps('last_modified');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('locations');
	}

}
