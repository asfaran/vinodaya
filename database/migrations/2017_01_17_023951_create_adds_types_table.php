<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddsTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('adds_types', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title')->nullable();
			$table->string('position_name')->nullable();
			$table->integer('price_type')->nullable()->default(0);
			$table->decimal('price')->nullable();
			$table->string('thumb',255)->nullable();
			$table->integer('width')->nullable()->default(0);
			$table->integer('height')->nullable()->default(0);	
            $table->text('extra')->nullable();
            $table->integer('status')->nullable()->default(0);
            $table->timestamp('featured_at');
            $table->timestamp('deleted_at');
            $table->timestamps('updated_at');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('adds_types');
	}

}
