<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormFieldsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('form_fields', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title')->nullable();
			$table->string('label')->nullable();
			$table->string('slug')->nullable();
			$table->string('frontend_type')->nullable();
			$table->string('backend_type')->nullable();
			$table->integer('parentid')->nullable()->default(0);
			$table->integer('listorder')->nullable()->default(0);
			$table->string('icon')->nullable();
            $table->text('content')->nullable();
            $table->timestamp('featured_at');
            $table->timestamp('deleted_at');
            $table->timestamps('updated_at');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('form_fields');
	}

}
