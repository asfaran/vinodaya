<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('usertype')->unsigned();
            $table->string('username', 40)->nullable()->unique();
            $table->string('username_slug', 40)->nullable();
            $table->string('name', 20)->nullable();
            $table->string('f_name', 20)->nullable();
            $table->string('l_name', 20)->nullable();
            $table->string('mobile', 20)->nullable();
            $table->string('location', 20)->nullable();
            $table->string('genre', 20);
            $table->string('icon', 200)->nullable();
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->integer('facebook_id')->nullable();
            $table->string('about', 500)->nullable();
            $table->integer('company_status')->nullable()->default(0);
            $table->string('company_name', 50)->nullable();
            $table->string('company_addr', 150)->nullable();
            $table->string('company_contact', 50)->nullable();
            $table->string('facebookurl', 250)->nullable();
            $table->string('twitterurl', 250)->nullable();
            $table->string('weburl', 250)->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
