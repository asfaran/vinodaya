<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('adds', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title')->nullable();
			$table->integer('user_id')->nullable()->default(0);
			$table->integer('adds_type')->nullable()->default(0);
			$table->integer('duration_type')->nullable()->default(0);
			$table->decimal('duration')->nullable();
			$table->string('thumb_real',255)->nullable();
			$table->string('thumb_resize',255)->nullable();
            $table->text('extra')->nullable();
            $table->integer('status')->nullable()->default(0);
            $table->integer('visibility')->nullable()->default(0);
            $table->integer('approve')->nullable()->default(0);
            $table->timestamp('featured_at');
            $table->timestamp('deleted_at');
            $table->timestamps('updated_at');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('adds');
	}

}
