<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('videos', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('user_id');
            $table->string('category_id')->nullable();
            $table->string('type',25);
            $table->string('ordertype',25);
            $table->string('slug',225);
            $table->string('title',225)->nullable();
            $table->string('excerpt',225)->nullable();
            $table->integer('status')->unsigned();
            $table->integer('featured')->unsigned();
            $table->integer('visibility')->unsigned();
            $table->integer('publish')->unsigned();
            $table->string('video_url',255)->nullable();
            $table->text('body')->nullable();
            $table->text('tags')->nullable();
            $table->string('thumb',255)->nullable();
            $table->string('approve',5)->nullable();
            $table->string('show_in_homepage',5)->nullable();
            $table->timestamp('featured_at');
            $table->timestamp('published_at');
            $table->timestamp('deleted_at');
            $table->timestamps('updated_at'); 
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('videos');
	}

}
