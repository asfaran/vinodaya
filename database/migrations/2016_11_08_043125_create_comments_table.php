<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('comments', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('post_type')->unsigned();
            $table->integer('post_id');
            $table->string('author',25);
            $table->string('author_email',225);
            $table->string('author_url',225)->nullable();
            $table->string('author_IP', 45);
            $table->integer('parent')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('approved')->unsigned();
            $table->text('content')->nullable();
            $table->timestamp('featured_at');
            $table->timestamp('published_at');
            $table->timestamp('deleted_at');
            $table->timestamps('updated_at');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('comments');
	}

}
