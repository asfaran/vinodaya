<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassifiedFieldsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('classified_fields', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('category')->nullable()->default(0);
            $table->integer('parentid')->nullable()->default(0);		
            $table->text('fields')->nullable();
            $table->timestamp('featured_at');
            $table->timestamp('deleted_at');
            $table->timestamps('updated_at');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('classified_fields');
	}

}
