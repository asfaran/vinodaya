<?php 
if (!defined('PAGE_PARENT'))
    define('PAGE_PARENT', '');
if (!defined('PAGE_CURRENT'))
    define('PAGE_CURRENT', '');
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" ng-app="inspinia"  data-ng-app="">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>@yield('title') - Vinodaya</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="{{ url('/assets/css/bootstrap.min.css') }}" rel="stylesheet">

	<!-- Font awesome -->
	<link href="{{ url('/assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

	<!-- Main Inspinia CSS files -->
	<link href="{{ url('/assets/css/animate.css') }}" rel="stylesheet">
	<link id="loadBefore" href="{{ url('/assets/css/style.css') }}" rel="stylesheet">
    <!-- END THEME STYLES -->

    <!-- BEGIN Summernote STYLES -->
    <link href="{{ url('/assets/css/plugins/summernote/summernote-bs3.css') }}" rel="stylesheet">
    <link href="{{ url('/assets/css/plugins/summernote/summernote.css') }}" rel="stylesheet">
    <!-- End Summernote STYLES -->

    <!-- BEGIN ngTagsInput STYLES -->
    <link href="{{ url('/assets/css/plugins/ngTagsInput/ng-tags-input.min.css') }}" rel="stylesheet">
    <!-- End ngTagsInput STYLES -->

    <!-- BEGIN dropzone STYLES -->
    <link href="{{ url('/assets/dropzone/dropzone.css') }}" rel="stylesheet">
    <!-- End dropzone STYLES -->


    <!-- BEGIN tag-editor STYLES -->
    <link href="{{ url('/assets/css/jquery.tag-editor.css') }}" rel="stylesheet">
    <!-- End tag-editor STYLES -->

    <!-- BEGIN dataTables STYLES -->
    <link href="{{ url('/assets/css/plugins/dataTables/dataTables.bootstrap.css') }}" rel="stylesheet">
    <!-- End dataTables STYLES -->

    <link rel="shortcut icon" href="{{ url('favicon.ico') }}"/>

    <style>
    .note-editor {
        border: 1px solid rgb(206, 198, 206);
    } 

    .ibox-title h5 .btn-default {

        padding: 0;
    }
    </style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<?php /*
 {{ !--
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
-- }}
*/ ?>
{{----}}

<body ng-controller="MainCtrl as main">

<!-- Main view  -->
<!-- Wrapper-->
<div id="wrapper">

    <!-- Navigation -->
    <!-- Navigation -->
    <div>
        @include('sidebar')
    </div>

    <!-- Page wraper -->
    <!-- ng-class with current state name give you the ability to extended customization your view -->
    <div id="page-wrapper" class="gray-bg" >

        <!-- Page wrapper -->
        <div>
             @include('topnavbar')
        </div>

        <!-- Main view  -->
        <div ui-view>
            @yield('content')
            @include('footer')
        </div>

    </div>
    <!-- End page wrapper-->

</div>
<!-- End wrapper-->

<!-- <div class="clearfix"></div>


<input class="hidden" type="text" data-ng-model="angular_loaded" id="angular_loaded" value=""/> -->
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- jQuery and Bootstrap -->
<script src="{{ url('/assets/js/jquery/jquery-2.1.1.min.js') }}"></script>
<script src="{{ url('/assets/js/plugins/jquery-ui/jquery-ui.js') }}"></script>
<script src="{{ url('/assets/js/bootstrap/bootstrap.min.js') }}"></script>

<!-- MetsiMenu -->
<script src="{{ url('/assets/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>

<!-- SlimScroll -->
<script src="{{ url('/assets/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

<!-- Peace JS -->
<script src="{{ url('/assets/js/plugins/pace/pace.min.js') }}"></script>

<!-- Custom and plugin javascript -->
<script src="{{ url('/assets/js/inspinia.js') }}"></script>

<!-- Main Angular scripts-->
<script src="{{ url('/assets/js/angular/angular.min.js') }}"></script>
<script src="{{ url('/assets/js/angular/angular-sanitize.js') }}"></script>
<script src="{{ url('/assets/js/plugins/oclazyload/dist/ocLazyLoad.min.js') }}"></script>
<script src="{{ url('/assets/js/angular-translate/angular-translate.min.js') }}"></script>
<script src="{{ url('/assets/js/ui-router/angular-ui-router.min.js') }}"></script>
<script src="{{ url('/assets/js/bootstrap/ui-bootstrap-tpls-0.12.0.min.js') }}"></script>
<script src="{{ url('/assets/js/plugins/angular-idle/angular-idle.js') }}"></script>

<!--
 You need to include this script on any page that has a Google Map.
 When using Google Maps on your own site you MUST signup for your own API key at:
 https://developers.google.com/maps/documentation/javascript/tutorial#api_key
 After your sign up replace the key in the URL below..
-->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQTpXj82d8UpCi97wzo_nKXL7nYrd4G70"></script>

<!-- Anglar App Script -->
<script src="{{ url('/assets/js/app.js') }}"></script>
<script src="{{ url('/assets/js/config.js') }}"></script>
<script src="{{ url('/assets/js/translations.js') }}"></script>
<script src="{{ url('/assets/js/directives.js') }}"></script>
<script src="{{ url('/assets/js/controllers.js') }}"></script>
<script src="{{ url('/assets/js/vinodaya.js') }}"></script>

<!-- Begin Anglar summernote Script -->
<script src="{{ url('/assets/js/plugins/summernote/summernote.min.js') }}"></script>
<script src="{{ url('/assets/js/plugins/summernote/angular-summernote.min.js') }}"></script>
<!--End Anglar summernote Script -->

<!-- Begin Anglar ngTagsInput Script -->
<script src="{{ url('/assets/js/plugins/ngTagsInput/ng-tags-input.min.js') }}"></script>
<!--End Anglar ngTagsInput Script -->


    <!-- BEGIN dropzone Script -->
    <link href="{{ url('/assets/dropzone/dropzone.js') }}" rel="stylesheet">
    <!-- End dropzone Script -->

    <!-- BEGIN tag-editor Script -->
    <script src="{{ url('/assets/js/jquery.tag-editor.js') }}"></script>
    <script src="{{ url('/assets/js/jquery.caret.min.js') }}"></script>
    <!-- End tag-editor Script -->

    <!-- BEGIN dataTables Script -->
    <script src="{{ url('/assets/js/plugins/dataTables/jquery.dataTables.js') }}"></script>
    <script src="{{ url('/assets/js/plugins/dataTables/dataTables.bootstrap.js') }}"></script>
    <script src="{{ url('/assets/js/plugins/dataTables/angular-datatables.min.js') }}"></script>
    <!-- End dataTables Script -->
<script>

    var base_url = "{!! url() !!}";
    
    jQuery(document).ready(function() {
        angular.module('inspinia', [
        'ui.router',                    // Routing
        'oc.lazyLoad',                  // ocLazyLoad
        'ui.bootstrap',                 // Ui Bootstrap
        'pascalprecht.translate',       // Angular Translate
        'ngIdle',                       // Idle timer
        'ngSanitize',                    // ngSanitize
        'dataTables'
    ])

        var ngTags_app = angular.module('inspinia', ['ngTagsInput']);
        ngTags_app.controller('MainCtrl', function($scope, $http) {
          $scope.tags = [
            { text: 'Tag1' },
            { text: 'Tag2' },
            { text: 'Tag3' }
          ];
        });



        $('#summernote').summernote({
            height: "500px"
        });

        /*$('#summernote').summernote({
            height: 200,
            onImageUpload: function(files, editor, welEditable) {
                sendFile(files[0], editor, welEditable);
            }
        });*/
        function sendFile(file, editor, welEditable) {
            data = new FormData();
            data.append("file", file);//You can append as many data as you want. Check mozilla docs for this
            $.ajax({
                data: data,
                type: "POST",
                url: base_url+'/admin/general/image_upload',
                cache: false,
                contentType: false,
                processData: false,
                success: function(url) {
                    editor.insertImage(welEditable, url);
                }
            });
        }


        /*angular.module('modalTest',['ui.bootstrap','dialogs'])*/
        $("#angular_loaded").val('true');
        $("#angular_loaded").trigger('input');

    });
    @yield('scripts')
</script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- END JAVASCRIPTS -->
</body>

<!-- END BODY -->
</html>
