<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="qal-bc brdr" >
            <div class="gal-fr-6">
              @if(isset($quick_category_array[4]) && count($quick_category_array[4]) > 0)
              <div class="gal-bak">
                <span class="videos"></span>videos
              </div>
              <div class="gal-bak">
                  <?php $video_ql = 1;?>
                  @foreach($quick_category_array[4]  as $video_key => $video_category)
                    @if($video_ql <= 6)
                    <a href="{{ URL::to('/video-lp/category/'.$video_key) }}">{{ $video_category }}</a>
                    @endif
                    <?php $video_ql++;?>
                  @endforeach              
              </div>
              @endif
              @if(isset($quick_category_array[1]) && count($quick_category_array[1]) > 0)
              <div class="gal-bak">
                <span class="news"></span>News
              </div>
              <div class="gal-bak">
                  <?php $news_ql = 1;?>
                  @foreach($quick_category_array[1]  as $news_key => $news_category)
                    @if($news_ql <= 7)
                    <a href="{{ URL::to('/news-category/'.$news_key) }}">{{ $news_category }}</a>
                    @endif
                    <?php $news_ql++;?>
                  @endforeach               
              </div>
              @endif
            </div>
            <div class="gal-fr-6">
              @if(isset($quick_category_array[5]) && count($quick_category_array[5]) > 0)
              <div class="gal-bak">
                <span class="budi-pg"></span>Products
              </div>
              <div class="gal-bak">              
                <?php $prod_ql = 1;?>
                  @foreach($quick_category_array[5]  as $prod_key => $prod_category)
                    @if($prod_ql <= 4)
                    <a href="{{ URL::to('/classifieds/category/'.$prod_key) }}">{{ $prod_category }}</a>
                    @endif
                    <?php $prod_ql++;?>
                  @endforeach
              </div>
              @endif

              @if(isset($quick_category_array[2]) && count($quick_category_array[2]) > 0)
              <div class="gal-bak">
                <span class="ape-lipi"></span>ape lipi
              </div>
              <div class="gal-bak">              
                <?php $lipi_ql = 1;?>
                  @foreach($quick_category_array[2]  as $lipi_key => $lipi_category)
                    @if($lipi_ql <= 3)
                    <a href="{{ URL::to('/articles-lp/category/'.$lipi_key) }}">{{ $lipi_category }}</a>
                    @endif
                    <?php $lipi_ql++;?>
                  @endforeach
              </div>
              @endif
              @if(isset($quick_category_array[3]) && count($quick_category_array[3]) > 0)
              <div class="gal-bak">
                <span class="sindu"></span>Pintoora pituva
              </div>
              <div class="gal-bak">              
                <?php $Pintoora = 1;?>
                  @foreach($quick_category_array[3]  as $Pintoora_key => $Pintoora_category)
                    @if($Pintoora <= 3)
                    <a href="{{ URL::to('/images-lp/category/'.$Pintoora_key) }}">{{ $Pintoora_category }}</a>
                    @endif
                    <?php $Pintoora++;?>
                  @endforeach
              </div>
              @endif
            </div>
      </div>
    </div>
    <!-- Modal content-->
    <button type="button" class="btn btn-default cols" data-dismiss="modal">X</button>
  </div>
</div>
<!-- Modal -->