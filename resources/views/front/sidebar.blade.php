<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul side-navigation class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element" dropdown>
                    <img alt="image" class="img" width="180px" src="{{ asset('/assets/img/vinodaya_logo.png') }}"/>
                </div>

               
                <div class="logo-element">
                    <img alt="image" class="img-circle" width="70px" src="{{ asset('/assets/img/vinodaya_logo_small.png') }}"/>
                </div>

            </li>

            <li ui-sref-active="<?php echo (PAGE_PARENT == 'dashboard') ? 'active':''; ?>"  class="<?php echo (PAGE_PARENT == 'dashboard') ? 'active':''; ?>" >
                <a href="{{ url('/admin/dashboard') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span> </a>
            </li>
            <li ui-sref-active="@if(PAGE_PARENT == 'general') active @endif"  class="@if(PAGE_PARENT == 'general') active @endif" >
                <a href="">
                    <i class="fa fa-cogs"></i> 
                    <span class="nav-label">General Configuration</span><span class="fa arrow"></span>
                    <ul class="nav nav-second-level collapse">
                            <li ui-sref-active="@if(PAGE_CURRENT == 'category_list') active @endif"
                            class="@if(PAGE_CURRENT == 'category_list') active @endif">
                                <a href="{{ url('/admin/general/category/list') }}">Category manager</a>
                            </li>
                            <li ui-sref-active="@if(PAGE_CURRENT == 'tags_list') active @endif"
                            class="@if(PAGE_CURRENT == 'tags_list') active @endif">
                                <a href="{{ url('/admin/general/tags/list') }}">Tags manager</a>
                            </li>
                            <li ui-sref-active="@if(PAGE_CURRENT == 'menu_list') active @endif"
                            class="@if(PAGE_CURRENT == 'menu_list') active @endif">
                                <a href="{{ url('/admin/general/menu/list') }}">Menu manager</a>
                            </li>
                            <li ui-sref-active="@if(PAGE_CURRENT == 'page_list') active @endif"
                            class="@if(PAGE_CURRENT == 'page_list') active @endif">
                                <a href="{{ url('/admin/general/page/list') }}">Pages</a>
                            </li>
                            <li ui-sref-active="@if(PAGE_CURRENT == 'comments_list') active @endif"
                            class="@if(PAGE_CURRENT == 'comments_list') active @endif">
                                <a href="{{ url('/admin/comments/list/') }}">Manage Comments</a>
                            </li>

                        </ul>
                </a>
            </li>
            <li ui-sref-active="@if(PAGE_PARENT == 'settings') active @endif"  class="@if(PAGE_PARENT == 'settings') active @endif" >
                <a href="">
                    <i class="fa fa-wrench"></i> 
                    <span class="nav-label">Settings</span><span class="fa arrow"></span>
                    <ul class="nav nav-second-level collapse">
                            <li ui-sref-active="@if(PAGE_CURRENT == 'language_list') active @endif"
                            class="@if(PAGE_CURRENT == 'language_list') active @endif">
                                <a href="{{ url('/admin/settings/language') }}">Language</a>
                            </li>
                            <li ui-sref-active="@if(PAGE_CURRENT == 'social_list') active @endif"
                            class="@if(PAGE_CURRENT == 'social_list') active @endif">
                                <a href="{{ url('/admin/settings/social/list') }}">social Links</a>
                            </li>

                        </ul>
                </a>
            </li>
            <li ui-sref-active="@if(PAGE_PARENT == 'news') active @endif"  class="@if(PAGE_PARENT == 'news') active @endif" >
                <a href="">
                    <i class="fa fa-newspaper-o"></i> 
                    <span class="nav-label">News Controller</span><span class="fa arrow"></span>
                    <ul class="nav nav-second-level collapse">
                            <li 
                                ui-sref-active="@if(PAGE_CURRENT == 'news_list') active @endif"  
                                class="@if(PAGE_CURRENT == 'news_list') active @endif" >
                                <a href="{{ url('/admin/news/list') }}">All News List</a>
                            </li>
                            <li ui-sref-active="@if(PAGE_CURRENT == 'news_add_new') active @endif"  
                                class="@if(PAGE_CURRENT == 'news_add_new') active @endif">
                                <a href="{{ url('/admin/news/add_new') }}">New News post</a>
                            </li>
                            <!-- <li>
                                <a href="{{ url('/admin/news/configuration') }}">Configuration</a>
                            </li> -->
                            <li ui-sref-active="@if(PAGE_CURRENT == 'news_display') active @endif"  
                                class="@if(PAGE_CURRENT == 'news_display') active @endif">
                                <a href="{{ url('/admin/news/display') }}">Display Setting</a>
                            </li>
                            <li>
                                <a href="{{ url('/admin/comments/display/1') }}">Manage Comments</a>
                            </li>

                        </ul>
                </a>
            </li>
            <li ui-sref-active="@if(PAGE_PARENT == 'article') active @endif"  class="@if(PAGE_PARENT == 'article') active @endif" >
                <a href="">
                    <i class="fa fa-twitch"></i> 
                    <span class="nav-label">Article Controller</span><span class="fa arrow"></span>
                    <ul class="nav nav-second-level collapse">
                            <li 
                                ui-sref-active="@if(PAGE_CURRENT == 'article_list') active @endif"  
                                class="@if(PAGE_CURRENT == 'article_list') active @endif" >
                                <a href="{{ url('/admin/article/list') }}">All Article List</a>
                            </li>
                             <li 
                                ui-sref-active="@if(PAGE_CURRENT == 'article_add_new') active @endif"  
                                class="@if(PAGE_CURRENT == 'article_add_new') active @endif" >
                                <a href="{{ url('/admin/article/add_new') }}">New Article post</a>
                            </li>
                           <!--  <li 
                                ui-sref-active="@if(PAGE_CURRENT == 'article') active @endif"  
                                class="@if(PAGE_CURRENT == 'article') active @endif" >
                                <a href="{{ url('/admin/article/configuration') }}">Configuration</a>
                            </li> -->
                            <li 
                                ui-sref-active="@if(PAGE_CURRENT == 'article_display') active @endif"  
                                class="@if(PAGE_CURRENT == 'article_display') active @endif" >
                                <a href="{{ url('/admin/article/display') }}">Display Setting</a>
                            </li>
                            <li>
                                <a href="{{ url('/admin/comments/display/2') }}">Manage Comments</a>
                            </li>

                        </ul>
                </a>
            </li>
            <li ui-sref-active="@if(PAGE_PARENT == 'image') active @endif"  class="@if(PAGE_PARENT == 'image') active @endif" >
                <a href="">
                    <i class="fa fa-file-image-o"></i> 
                    <span class="nav-label">Image Controller</span><span class="fa arrow"></span>
                    <ul class="nav nav-second-level collapse">
                            <li 
                                ui-sref-active="@if(PAGE_CURRENT == 'image_list') active @endif"  
                                class="@if(PAGE_CURRENT == 'image_list') active @endif" >
                                <a href="{{ url('/admin/image/list') }}">All Image Posts</a>
                            </li>
                            <li 
                                ui-sref-active="@if(PAGE_CURRENT == 'image_add_new') active @endif"  
                                class="@if(PAGE_CURRENT == 'image_add_new') active @endif">
                                <a href="{{ url('/admin/image/add_new') }}">New Image post</a>
                            </li>
                            <!-- <li>
                                <a href="{{ url('/admin/image/configuration') }}">Configuration</a>
                            </li> -->
                            <li ui-sref-active="@if(PAGE_CURRENT == 'image_display') active @endif"  
                                class="@if(PAGE_CURRENT == 'image_display') active @endif">
                                <a href="{{ url('/admin/image/display') }}">Display Setting</a>
                            </li>

                        </ul>
                </a>
            </li>
            <li ui-sref-active="@if(PAGE_PARENT == 'video') active @endif"  class="@if(PAGE_PARENT == 'video') active @endif" >
                <a href="">
                    <i class="fa fa-file-video-o"></i> 
                    <span class="nav-label">Video Controller</span><span class="fa arrow"></span>
                    <ul class="nav nav-second-level collapse">
                            <li 
                                ui-sref-active="@if(PAGE_CURRENT == 'video_list') active @endif"  
                                class="@if(PAGE_CURRENT == 'video_list') active @endif" >
                                <a href="{{ url('/admin/video/list') }}">All Video Posts</a>
                            </li>
                            <li 
                                ui-sref-active="@if(PAGE_CURRENT == 'video_add_new') active @endif"  
                                class="@if(PAGE_CURRENT == 'video_add_new') active @endif" >
                                <a href="{{ url('/admin/video/add_new') }}">New Video post</a>
                            </li>
                            <!-- <li>
                                <a href="{{ url('/admin/video/configuration') }}">Configuration</a>
                            </li> -->
                            <li ui-sref-active="@if(PAGE_CURRENT == 'video_display') active @endif"  
                                class="@if(PAGE_CURRENT == 'video_display') active @endif">
                                <a href="{{ url('/admin/video/display') }}">Display Setting</a>
                            </li>

                        </ul>
                </a>
            </li>
            <li ui-sref-active="@if(PAGE_PARENT == 'classified') active @endif"  class="@if(PAGE_PARENT == 'classified') active @endif" >
                <a href="">
                    <i class="fa fa-shopping-cart"></i> 
                    <span class="nav-label">Classified Controller</span><span class="fa arrow"></span>
                    <ul class="nav nav-second-level collapse">
                            <li ui-sref-active="@if(PAGE_CURRENT == 'classified_list') active @endif"  
                                class="@if(PAGE_CURRENT == 'classified_list') active @endif">
                                <a href="{{ url('/admin/classified/list') }}">Approve Classified List</a>
                            </li>
                            <li ui-sref-active="@if(PAGE_CURRENT == 'classified_add_new') active @endif"  
                                class="@if(PAGE_CURRENT == 'classified_add_new') active @endif">
                                <a href="{{ url('/admin/classified/add_new') }}">Add New Classified</a>
                            </li>
                            <li ui-sref-active="@if(PAGE_CURRENT == 'location_list') active @endif"  
                                class="@if(PAGE_CURRENT == 'location_list') active @endif">
                                <a href="{{ url('/admin/classified/location') }}">Location Setting</a>
                            </li>
                            <li ui-sref-active="@if(PAGE_CURRENT == 'adds_manager') active @endif"  
                                class="@if(PAGE_CURRENT == 'adds_manager') active @endif">
                                <a href="{{ url('/admin/classified/adds/list') }}">Adds manager</a>
                            </li>
                            <li ui-sref-active="@if(PAGE_CURRENT == 'emails') active @endif"  
                                class="@if(PAGE_CURRENT == 'emails') active @endif">
                                <a href="{{ url('/admin/classified/mails') }}">Mail Configuration</a>
                            </li>
                            <li ui-sref-active="@if(PAGE_CURRENT == 'classified_display') active @endif"  
                                class="@if(PAGE_CURRENT == 'classified_display') active @endif">
                                <a href="{{ url('/admin/classified/display') }}">Display Setting</a>
                            </li>
                            <li ui-sref-active="@if(PAGE_CURRENT == 'form_fields_list') active @endif"  
                                class="@if(PAGE_CURRENT == 'form_fields_list') active @endif">
                                <a href="{{ url('/admin/general/form/list') }}">Form Fields Manager</a>
                            </li>
                            <li ui-sref-active="@if(PAGE_CURRENT == 'classified_fields_list') active @endif"  
                                class="@if(PAGE_CURRENT == 'classified_fields_list') active @endif">
                                <a href="{{ url('/admin/general/form/classified_fields') }}">Classified Fields Manager</a>
                            </li>

                        </ul>
                </a>
            </li>
            <li ui-sref-active="@if(PAGE_PARENT == 'adminuser') active @endif"  class="@if(PAGE_PARENT == 'adminuser') active @endif" >
                <a href="">
                    <i class="fa fa-user"></i> 
                    <span class="nav-label">Manage User's</span><span class="fa arrow"></span>
                    <ul class="nav nav-second-level collapse">
                            <li ui-sref-active="@if(PAGE_CURRENT == 'users_list') active @endif"  
                                class="@if(PAGE_CURRENT == 'users_list') active @endif">
                                <a href="{{ url('/admin/users/list') }}">Users List</a>
                            </li>
                            <li ui-sref-active="@if(PAGE_CURRENT == 'add_users') active @endif"  
                                class="@if(PAGE_CURRENT == 'add_users') active @endif">
                                <a href="{{ url('/admin/users/add_new') }}">Add New Users</a>
                            </li>

                        </ul>
                </a>
            </li>
            <!-- <li>
                <a href=""><i class="fa fa-sitemap"></i> <span class="nav-label">{{ 'MENULEVELS' }}</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li>
                        <a href="">Third Level <span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="">Third Level Item</a>
                            </li>
                            <li>
                                <a href="">Third Level Item</a>
                            </li>
                            <li>
                                <a href="">Third Level Item</a>
                            </li>

                        </ul>
                    </li>
                    <li><a href="">Second Level Item</a></li>
                    <li>
                        <a href="">Second Level Item</a></li>
                    <li>
                        <a href="">Second Level Item</a></li>
                </ul>
            </li> -->
        </ul>

    </div>
</nav>