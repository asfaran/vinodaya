<section>
    <div class="top-f">
        <div class="container">
            <div class="">
                <div class="col-top-2">
                    <div class="hot-news">
                        <a href="{{URL::route('news-lp')}}">Hot news</a>
                    </div>
                </div>
                <div class="col-top-7">
                    <div class="top-text">
                        <div class="ticker" data-duration="20" data-loop="2" data-effect="linear">
                            <span>
                                @if(isset($news_list) && count($news_list) > 0)
                                @foreach($news_list  as $news)
                                    @if($news->title !== "" && $news->title !== null)
                                        <a href="{{ URL::to('/news/'.$news->id) }}" target="_self">{{ $news->title }}</a>&#09;&#09;&#09;&#09;
                                    @endif
                                @endforeach                            
                                @endif  
                            </span>
                        </div>
                        
                    </div>
                </div>
                <div class="col-top-3">
                    <div class="top-onli">
                    <div class="sio-m">
                        <ul>
                        @if(isset($social_links) && count($social_links) > 0)
                            @foreach($social_links  as $social)
                                @if($social->url !== "" && $social->url !== null)
                                <li>
                                    <a class="{{ strpos($social->slug, 'fa')? 'fa':''  }} {{ $social->slug }}" 
                                    href="{{ $social->url }}" target="_blank"></a>
                                </li>
                                @endif
                            @endforeach                            
                        @endif
                        </ul>
                    </div>
                    <div class="dropdown">
                      <img src="{{ url('/assets/img/flag.png') }}"><button class="flg-btn dropdown-toggle" type="button" data-toggle="dropdown">සිංහල
                      <span class="caret"></span></button>
                      <ul class="dropdown-menu">
                        <li><a href="#">tamil</a></li>
                        <li><a href="#">English</a></li>
                      </ul>
                    </div>
                    <div class="dropdown">
                      <i class="fa fa-user" style="color: white;"></i><button class="flg-btn dropdown-toggle" type="button" data-toggle="dropdown">My Account
                      <span class="caret"></span></button>
                      <ul class="dropdown-menu">
                        @if(Auth::check())
                            <li><a href="{{ url('/user/myads') }}">My Ads</a></li>
                            <li><a href="{{ url('/user/myposts') }}">My Classified</a></li>
                            <li><a href="{{ url('/user/postads') }}">Post Ads</a></li>
                            <li><a href="{{ url('/user/postclassified') }}">Post Classified</a></li>
                            <li><a href="{{ url('/user/logout') }}">Logout</a></li>
                        @else
                            <li><a href="{{ url('/user/login') }}">Login</a></li>
                            <li><a href="{{ url('/user/register') }}">Register</a></li>
                            
                        @endif
                      </ul>
                    </div>
                    <!-- <div class="dropdown">
                      <button class="flg-btn dropdown-toggle" type="button" data-toggle="dropdown">සිංහල
                      <span class="caret"></span></button>
                      <ul class="dropdown-menu">
                        <li><a href="#">tamil</a></li>
                        <li><a href="#">English</a></li>
                        <li><a href="#">Malayalam</a></li>
                        <li><a href="#">japan</a></li>
                        <li><a href="#">Korian</a></li>
                      </ul>
                    </div> -->
                    </div> 
                </div>
            </div>
        </div>
    </div>

    <div class="logo-se">
        <div class="container">
            <div class="">
                <div class="top-logo">
                    <a href="{{URL::route('home')}}">
                        <img src="{{ url('/assets/img/logo.png') }}">
                    </a>
                </div>
                <div class="lef-con">
                    <div class="top-cont">
                        <p>contact us</p>
                    </div>
                    <div class="top-email">
                        <p>info@vinodaya.com</p>
                    </div>
                    <div>
                        <div id="sear-in">
                        {!! Form::open(array('url'=>'/search','role'=>'form', 'id' => 'home_search_form')) !!}
                            <input type="text" id="home_search_input" name="search_text" class="sear-in" value="">
                            <img src="{{ url('/assets/img/src.png') }}" id="sear-ico">
                        {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="menu">
        <div class="container">
            <nav id='cssmenu'>
                <div id="head-mobile"></div>
                <div class="button"></div>
                <ul>
                @if(isset($menu_list) && count($menu_list) > 0)
                    @foreach($menu_list  as $menu)
                    <?php $active_class= ($menu->title === PAGE_PARENT)? "active": "";  ?>

                        @if($menu->title !== "" && $menu->title !== null)
                            @if($menu->type == 2)
                                <li class="{{ $active_class }}">
                                    <a href="{{URL::route($menu->content)}}">{{ $menu->label }}</a>
                                </li>
                            @elseif($menu->type == 3)
                                <li class="{{ $active_class }}">
                                    <a href="{{ url('/informative/'.$menu->content) }}">{{ $menu->     label }}</a>
                                </li>
                            @endif
                        @endif
                    @endforeach                            
                @endif
                    <!-- <li><a href='#'>Contact</a></li> -->
                </ul>
            </nav>
            <div class="hot-l">
                <a href=""><img src="{{ url('/assets/img/call.png') }}"></a>
                <p>Advertising hotline: 071 186 6410</p>   
            </div>
        </div>
    </div>

</section>