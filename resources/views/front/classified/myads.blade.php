<?php
define('PAGE_PARENT', 'classified', true);
define('PAGE_CURRENT', 'myads', true);
?>
@extends('front.app')

@section('title', 'My Advertisments')

@section('content')
<style>
  .ri-ico {
    float: right;
    margin-top: 0;
    padding-left: 3px;
    width: 66px;
    text-align: center;
}

.ri-ico span {
    display: inline-block;
    height: 23px;
    margin-right: 22px;
    width: 55px;
    text-decoration: none;
    font-size: 13px;
}

a .status_success {
    color: green;
}
a .status_danger{
    color: red;
}
</style>
<dir class="container">
  <dir class="row row-15in no-pad ">
    <div class="bredcm">
      <a href="">Home</a><span>></span><a href="">My Advertisments</a>
      <h2 class="lan-hid">My Advertisments</h2>
    </div>
    
  </dir>

  <dir class="row row-15in no-padi">
    <div class="col-30 max-vid">
      <div class="panel-group">
        <div class="panel panel-default">
          <div class="panel-heading my-acus">
              <a data-toggle="collapse" href="#collapse009">
                <div class="my-acu">my account menu</div> 
              </a>
          </div>
          <div id="collapse009" class="panel-collapse collapse">
            <div class="my-ad-rig white">
              <a class="activ" href="{!! url('/user/myads') !!}">My Advertisments</a>
              <a href="{!! url('/user/myposts') !!}">My Classified</a>
              <a   href="{!! url('/user/reset') !!}">Reset Password </a>
              @if(Auth::user()->company_status !== 1)
              <a   href="{!! url('/user/upgrade') !!}">Upgrade to Business Profile</a>
              @endif
            </div>
            <div class="pos-ad">
              <a href="{!! url('/user/postads') !!}">Post Advertisment</a>
              <a href="{!! url('/user/postclassified') !!}">Post Classified</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-70 max-vid">
    @if ( Session::has('flash_message') )
        <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                <button class="close" data-dismiss="alert"></button>
                {!! Session::get('flash_message') !!}
        </div>
    @endif
    @if ( Session::has('flash_success') )
        <div class="alert alert-success  {{ Session::get('flash_type') }}">
            <button class="close" data-dismiss="alert"></button>
            {!! Session::get('flash_success') !!}
        </div>
    @endif
    <div class="myad-ri white">
      <div class="myad-h4">
        <h4>My Advertisments</h4>
        <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet</p>
      </div>

      <!-- Advertisment -->
      <div class="myad-min">
        <div>
        @if(isset($my_ads) && count($my_ads) > 0)
          <div class="my-add">
            <div class="myad-l">
            </div>
            
            
            <div class="ri-ico">
              <span >Edit</span>
            </div>
            <div class="ri-ico">
              <span >Delete</span>
            </div>
            <div class="ri-ico">
              <span>Visibility</span>
            </div>
            <!-- <div class="ri-ico">
              <span > Views</span>
            </div> -->
          </div>
        
            @foreach($my_ads  as $ads)
                <div class="my-add">
                  <div class="myad-l">
                    @if(isset($ads->thumb_real))
                    <img src="{{ url('/uploads/classifieds/adds/real_image/'.$ads->thumb_real) }}" 
                      width="75px" height="60px">
                    @endif
                    <div class="my-dis">
                      <h5>{{ substr(strip_tags($ads->title),0,30) }}</h5>
                      <p>{{ $adds_types_array[$ads->adds_type] }}</p>
                      <p >Admin Approval:
                          @if($ads->approve == 0)
                          <a >
                            <i class="status_danger fa fa-times-circle"></i>
                          </a>
                          @else
                          <a >
                            <i class="status_success fa fa-check-circle"></i>
                          </a > 
                          @endif</p>
                       @if($ads->status == 0)
                       <p >Payment Status:
                          <a >
                            <i class="status_danger fa fa-times-circle"></i>
                          </a>
                        </p>
                        @endif
                    </div>
                  </div>
                  
                  <div class="ri-ico">
                    <a class="edi" href="{{ URL::to('/user/editads/'.$ads->id) }}"><i class="fa fa-pencil-square"></i></a>
                  </div>
                  <div class="ri-ico">
                    <a class="dele" href="{{ URL::to('/user/deleteads/'.$ads->id) }}"><i class="fa fa-trash"></i></a>
                  </div>
                  <div class="ri-ico">
                    @if($ads->visibility == 0)
                    <a class="status_danger" href="{{ URL::to('/user/adds_status/'.$ads->id) }}">
                      <i class="fa fa-times-circle"></i>
                    </a> 
                    @else
                    <a class="status_success" href="{{ URL::to('/user/adds_status/'.$ads->id) }}">
                      <i class="fa fa-check-circle"></i>
                    </a> 
                    @endif
                  </div>
                  <!-- <div class="ri-ico">
                    <span><center>{{ Counter::show('adds_count', $ads->id) }}</center><span>
                  </div> -->
                </div>
            @endforeach
        @else
        <div class="my-add">
          No Addvertisment to Display.
        </div>
        @endif
          
          <!-- <div class="my-add">
            <div class="myad-l">
              <img src="{{ url('/assets/img/my-add.png') }}">
              <div class="my-dis">
                <h5>Advertisment name</h5>
                <p>liber tempor cum soluta</p>
              </div>
            </div>
            <div class="ri-ico">
              <a class="viw" href=""></a>
              <a class="edi" href=""></a>
              <a class="cls" href=""></a>
              <a class="dele" href=""></a>
            </div>
          </div>
          <div class="my-add">
            <div class="myad-l">
              <img src="{{ url('/assets/img/my-add.png') }}">
              <div class="my-dis">
                <h5>Advertisment name</h5>
                <p>liber tempor cum soluta</p>
              </div>
            </div>
            <div class="ri-ico">
              <a class="viw" href=""></a>
              <a class="edi" href=""></a>
              <a class="cls" href=""></a>
              <a class="dele" href=""></a>
            </div>
          </div> -->
        </div>
      </div>

      @if(isset($my_ads) && count($my_ads) > 0)
      <div class="pagi-my pagi">
          {!!  $my_ads->appends(array('Search' => Input::get('Search'),'sorting' => Input::get('sorting')))->render() !!}
      </div>
      @endif

    </div>
    </div>
  </dir>
</dir>


@endsection