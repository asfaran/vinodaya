﻿<?php
define('PAGE_PARENT', 'classified', true);
define('PAGE_CURRENT', 'updateads', true);
?>
@extends('front.app')

@section('title', 'Edit Ads')

@section('content')
<dir class="container">
    <dir class="row row-15in no-padi">
        <div class="bredcm">
            <a href="">Home</a><span>></span><a href="">Edit Advertisment</a>
            <h2>Edit Advertisment</h2>
        </div>

    </dir>
    <dir class="row row-15in no-padi">
        <div class="col-30 max-vid">
            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading my-acus">
                        <a data-toggle="collapse" href="#collapse009">
                            <div class="my-acu">my account menu</div>
                        </a>
                    </div>
                    <div id="collapse009" class="panel-collapse collapse">
                        <div class="my-ad-rig white">
                            <a href="{!! url('/user/myads') !!}">My Advertisments</a>
                            <a href="{!! url('/user/myposts') !!}">My Classified</a>
                            <a  href="{!! url('/user/reset') !!}">Reset Password </a>
                            @if(Auth::user()->company_status !== 1)
                            <a   href="{!! url('/user/upgrade') !!}">Upgrade to Business Profile</a>
                            @endif
                        </div>
                        <div class="pos-ad">         
                          <a href="{!! url('/user/postads') !!}">Post Advertisment</a>
                          <a class="active" href="{!! url('/user/postclassified') !!}">Post Classified</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-70 max-vid">
            <div class="myad-ri white">
                <div class="myad-h4">
                    <h4>Advertisment Details</h4>
                    <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet</p>
                </div>

                <!-- Advertisment -->
                {!! Form::open(array('url'=>'/user/postclassified','role'=>'form', 'files' => true, 'class'=>'form-horizontal', 'id' => 'form_post_classified')) !!}
                {!! Form::hidden('id', $classified_one->id, array('id' => 'invisible_id')) !!}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                @if ( Session::has('flash_message') )
                    <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {!! Session::get('flash_message') !!}
                    </div>
                @endif
                @if ( Session::has('flash_success') )
                    <div class="alert alert-success  {{ Session::get('flash_type') }}">
                        <button class="close" data-dismiss="alert"></button>
                        {!! Session::get('flash_success') !!}
                    </div>
                @endif
                <div class="myad-min">
                    <div>
                        <div class="row flex">
                            <div class="col-md-8 col-xs-8 a">
                                <form>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="re-imp">
                                                <label>Name of the Product or service</label>
                                                <input type="text" placeholder="Enter Title" class="form-control"  value="{{ $classified_one->title }}" id="title" name="title">
                                                @if ($errors->has('title'))
                                                    <span class="alert-danger">
                                                        {{ $errors->first('title') }}
                                                    </span><br>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-9 col-xs-9 ph-100">
                                            <div class="re-imp">
                                                <label>Province</label>
                                                {!! Form::select('location_province', $location['province'], $classified_one->location_province , ['id' => 'location_province', 'class' => 'classified_location_selection form-control']) !!}
                                                @if ($errors->has('location_province'))
                                                    <span class="alert-danger">
                                                        {{ $errors->first('location_province') }}
                                                    </span><br>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-9 col-xs-9 ph-100">
                                            <div class="re-imp">
                                                <label>City</label>
                                                <div id="div_location_city">
                                                    {!! Form::select('location_city', $location['city'], $classified_one->location_city, ['id' => 'location_city', 'class' => 'classified_location_selection form-control']) !!}
                                                </div>
                                                @if($errors->has('location_city'))
                                                    <span class="alert-danger">
                                                        {{ $errors->first('location_city') }}
                                                    </span><br>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-9 col-xs-9 ph-100">
                                            <div class="re-imp">
                                                <label>Town</label>
                                                <div id="div_location_town">
                                                    {!! Form::select('location_town', $location['town'], $classified_one->location_town, ['id' => 'location_town', 'class' => 'classified_location_selection form-control']) !!}
                                                </div>
                                                @if ($errors->has('location_town'))
                                                    <span class="alert-danger">
                                                        {{ $errors->first('location_town') }}
                                                    </span><br>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-9 col-xs-9 ph-100">
                                            <div class="re-imp">
                                                <label>Main Category</label>
                                                {!! Form::select('parentid', $category_list, $classified_one->parentid, ['id' => 'classified_parentid', 'class' => 'classified_add_form_selection form-control']) !!}
                                                @if ($errors->has('parentid'))
                                                    <span class="alert-danger">
                                                        {{ $errors->first('parentid') }}
                                                    </span><br>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-9 col-xs-9 ph-100">
                                            <div class="re-imp">
                                                <label>Sub Category</label>
                                                <div id="classified_category">
                                                {!! Form::select('category', $subcategory_array, $classified_one->category, ['id' => 'category', 'class' => 'classified_add_form_selection form-control']) !!}
                                                </div>
                                                @if ($errors->has('category'))
                                                    <span class="alert-danger">
                                                        {{ $errors->first('category') }}
                                                    </span><br>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-9 col-xs-9 ph-100">
                                            <div class="re-imp">
                                                <div id="classified_add_form_fields_rows"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- <div class="row">
                                        <div class="col-md-9 col-xs-9 ph-100">
                                            <div class="re-imp">
                                                <label>Condition</label>
                                                <input type="text" name="">
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="re-imp">
                                                <label>Description</label>
                                                <textarea class="pos-text" placeholder="Enter description"id="description" name="description">{{ $classified_one->description }}</textarea>
                                                @if ($errors->has('description'))
                                                <br>
                                                    <span class="alert-danger">
                                                        {{ $errors->first('description') }}
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-7 col-xs-7">
                                            <div class="re-imp">
                                                <label>Price </label>
                                                <input type="text" placeholder="Enter Price" class="form-control"  value="{{ $classified_one->price }}" id="price" name="price">
                                                @if ($errors->has('price'))
                                                    <span class="alert-danger">
                                                        {{ $errors->first('price') }}
                                                    </span><br>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8 col-xs-8 col-xs-8">
                                            <h4>Boost this AD ? </h4>
                                        </div>
                                        <div class="col-md-4 col-xs-4 incenter">
                                            <input id="boost_selection" name="featured" data-toggle="toggle" type="checkbox"  @if(isset($classified_one->featured)) checked @endif>
                                                
                                            @if($errors->has('featured'))
                                            <br>
                                                <span class="alert-danger">
                                                    {{ $errors->first('featured') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row" id="boost_type_selection_row" style='display: @if(isset($classified_one->featured)) block @else none @endif'>

                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-8 ">
                                                <div class="re-imp">
                                                    <label>Boost Duration(Days)</label>
                                                    @if(isset($post_boost_row->duration))
                                                    {!! Form::select('duration_display', $duration_type , $post_boost_row->duration, ['id' => 'duration_display', 'class' => 'form-control', 'disabled' => 'disabled']) !!}
                                                    <input type="hidden" name="duration" value="{{ $post_boost_row->duration }}">
                                                    @else
                                                    {!! Form::select('duration', $duration_type, null, ['id' => 'duration', 'class' => 'form-control']) !!}
                                                    @endif

                                                    @if ($errors->has('duration'))
                                                        <span class="alert-danger">
                                                            {{ $errors->first('duration') }}
                                                        </span><br>
                                                    @endif
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-8 ">
                                                <div class="re-imp">
                                                    <label>Budget(Rs)</label>
                                                    @if(isset($post_boost_row->budget))
                                                    {!! Form::text('budget_display', $post_boost_row->budget, ['id' => 'budget_display', 'class' => 'form-control', 'disabled' => 'disabled']) !!}
                                                    <input type="hidden" name="budget" value="{{ $post_boost_row->budget }}">
                                                    @else
                                                    <input type="text" placeholder="Enter your Budget" class="form-control" id="budget" name="budget">
                                                    @endif

                                                    
                                                    @if ($errors->has('budget'))
                                                        <span class="alert-danger">
                                                            {{ $errors->first('budget') }}
                                                        </span><br>
                                                    @endif
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                                            <div class="col-md-6 col-sm-6 col-xs-6" >
                                                <div class="widget topad-bg p-lg text-center">
                                                    <div class="m-b-md">
                                                        <div class="radio">
                                                            <label style="font-size: 2.5em">
                                                                
                                                                @if($post_boost_row->boost_type !== 0)
                                                                <input type="radio" name="boost_type_display" value=""  @if($post_boost_row->boost_type === 1 ) checked @endif  disabled>
                                                                @else
                                                                <input type="radio" name="boost_type" value="top_ads">
                                                                @endif
                                                                <span class="cr"><i class="cr-icon fa fa-circle"></i></span>
                                                            </label>
                                                        </div>

                                                        <h3 class="font-bold no-margins">
                                                            <center>Top Ads</center>
                                                        </h3>
                                                        <div><small>#sample Detail Text</small></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <di class="col-md-6 col-xs-6 col-xs-6" >
                                                <div class="widget featuredad-bg p-lg text-center">
                                                    <div class="m-b-md">
                                                        <div class="radio">
                                                            <label style="font-size: 2.5em">
                                                                @if($post_boost_row->boost_type !== 0)
                                                                <input type="radio" name="boost_type_display" value=""  @if($post_boost_row->boost_type === 2 ) checked @endif  disabled>
                                                                @else
                                                                <input type="radio" name="boost_type" value="featured_ads" >
                                                                @endif


                                                                <span class="cr"><i class="cr-icon fa fa-circle"></i></span>
                                                            </label>
                                                        </div>
                                                        <h3 class="font-bold no-margins">
                                                            <center>Featured Ads</center>
                                                        </h3>
                                                        <div><small>#sample Detail Text</small></div>
                                                    </div>
                                                </div>
                                            </di>
                                            
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="b7">Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis.</p>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <div class="col-md-4 col-xs-4 b">
                                <div class="uplod-img">
                                    @if(isset($classified_one->thumb))
                                    <?php $img_url = url('/uploads/classifieds/'.$classified_one->parentid.'/'.$classified_one->thumb) ; ?>
                                    <label for="fileToUpload" style="background-image: url('{{ $img_url }}')"  id="pre_image"></label>
                                    @else
                                    <label for="fileToUpload" id="pre_image"></label>
                                    @endif
                                    <input type="file" name="thumb" id="fileToUpload" onchange="readURL(this)">
                                </div>
                                <p class="img-uplabl">
                                @if ($errors->has('thumb'))
                                    <span class="alert-danger">
                                        {{ $errors->first('thumb') }}
                                    </span>
                                @else
                                    Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis.

                                @endif
                                </p>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <hr>
                                <input type="checkbox" class="mid-chk"  checked name="mid_chk">
                                <p class="b7">I have read and understood the terms and conditions </p>
                                <br>
                                @if ($errors->has('mid_chk'))
                                    <span class="alert-danger">
                                        {{ $errors->first('mid_chk') }}
                                    </span><br>
                                @endif
                                <input type="submit" class="let-post" value="Let’s Post the  AD" name="submit">
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </dir>
</dir>


@endsection

@section('scripts')
    <script src="{!! asset('assets/plugins/select2/select2.min.js') !!}"></script>
    <script src="{!! asset('assets/plugins/select2/select2.implementation.js') !!}"></script>
    <script src="{!! asset('assets/front/js/jquery.validate.js') !!}"></script>
    <script>
        $(document).ready(function () {
            $.ajaxSetup(
            {
                headers:
                {
                    'X-CSRF-Token': $('input[name="_token"]').val()
                }
            });

        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    document.getElementById("pre_image").style.backgroundImage = "url('"+e.target.result+"')";
                   /* $('#faux').attr('value', $('#image').val());
*/
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@stop