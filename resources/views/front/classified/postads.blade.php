<?php
define('PAGE_PARENT', 'classified', true);
define('PAGE_CURRENT', 'postads', true);
?>
@extends('front.app')

@section('title', 'Post Ads')

@section('content')
<dir class="container">
    <dir class="row row-15in no-padi">
        <div class="bredcm">
            <a href="">Home</a><span>></span><a href="">Post Advertisment</a>
            <h2>Post Advertisment</h2>
        </div>

    </dir>
    <dir class="row row-15in no-padi">
        <div class="col-30 max-vid">
            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading my-acus">
                        <a data-toggle="collapse" href="#collapse009">
                            <div class="my-acu">my account menu</div>
                        </a>
                    </div>
                    <div id="collapse009" class="panel-collapse collapse">
                        <div class="my-ad-rig white">
                            <a href="{!! url('/user/myads') !!}">My Advertisments</a>
                            <a href="{!! url('/user/myposts') !!}">My Classified</a>
                            <a  href="{!! url('/user/reset') !!}">Reset Password </a>
                            @if(Auth::user()->company_status !== 1)
                            <a   href="{!! url('/user/upgrade') !!}">Upgrade to Business Profile</a>
                            @endif
                        </div>
                        <div class="pos-ad">  
                          <a class="active" href="{!! url('/user/postads') !!}">Post Advertisment</a>
                          <a href="{!! url('/user/postclassified') !!}">Post Classified</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-70 max-vid">
            <div class="myad-ri white">
                <div class="myad-h4">
                    <h4>Advertisment Details</h4>
                    <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet</p>
                </div>

                <!-- Advertisment -->
                {!! Form::open(array('url'=>'/user/postads','role'=>'form', 'files' => true, 'class'=>'form-horizontal')) !!}
                <input type="hidden" name="duration_type" value="2">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                @if ( Session::has('flash_message') )
                    <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {!! Session::get('flash_message') !!}
                    </div>
                @endif
                @if ( Session::has('flash_success') )
                    <div class="alert alert-success  {{ Session::get('flash_type') }}">
                        <button class="close" data-dismiss="alert"></button>
                        {!! Session::get('flash_success') !!}
                    </div>
                @endif
                <div class="myad-min">
                    <div>
                        <div class="row flex">
                            <div class="col-md-8 col-xs-8 a">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="re-imp">
                                                <label>Title of Advertisment</label>
                                                <input type="text" placeholder="Enter Title" class="form-control"  value="{{ Input::old('title')}}" id="title" name="title">
                                                @if ($errors->has('title'))
                                                    <span class="alert-danger">
                                                        {{ $errors->first('title') }}
                                                    </span><br>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-9 col-xs-9 ph-100">
                                            <div class="re-imp">
                                                <label>Advertisment Type</label>
                                                {!! Form::select('adds_type', $adds_types, null, ['id' => 'postadds_type', 'class' => 'form-control']) !!}
                                                @if ($errors->has('adds_type'))
                                                    <span class="alert-danger">
                                                        {{ $errors->first('adds_type') }}
                                                    </span><br>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-9 col-xs-9 ph-100">
                                            <div class="re-imp">
                                                <label>Duration</label>
                                                <div id="div_location_city">
                                                    {!! Form::select('duration', $duration_type , null, ['id' => 'duration', 'class' => 'form-control']) !!}
                                                </div>
                                                @if($errors->has('duration'))
                                                    <span class="alert-danger">
                                                        {{ $errors->first('duration') }}
                                                    </span><br>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="re-imp">
                                                <label>Url Link</label>
                                                <input type="text" placeholder="Enter valid Url Link" class="form-control"  value="{{ Input::old('extra')}}" id="extra" name="extra">
                                                    <span class="help-block m-b-none">
                                                       Please add valid url link with "http://" or "https://"
                                                    </span>
                                                @if ($errors->has('extra'))
                                                <br>
                                                    <span class="alert-danger">
                                                        {{ $errors->first('extra') }}
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <!-- <div class="row">
                                        <div class="col-md-12">
                                            <p class="b7">Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis.</p>
                                        </div>
                                    </div> -->
                            </div>
                            <div class="col-md-4 col-xs-4 b">
                                <div class="uplod-img">
                                    <label for="fileToUpload" id="pre_image"></label>
                                    <input type="file" name="thumb_real" id="fileToUpload" onchange="readURL(this)" value="{{ Input::old('thumb') }}">
                                    <?php /*<input type="hidden" name="image_url_return" id="image_url_return" value="{{ Input::old('image_url_return') }}">*/ ?>
                                </div>
                                <p class="img-uplabl">
                                @if ($errors->has('thumb_real'))
                                    <span class="alert-danger">
                                        {{ $errors->first('thumb_real') }}
                                    </span>
                                @else
                                    Image Will Resize According to Selected Addvertiment.
                                    <div id="image_records"></div>

                                @endif
                                </p>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <hr>
                                <input type="checkbox" class="mid-chk" name="mid_chk">
                                <p class="b7">I have read and understood the terms and conditions </p>
                                <br>
                                @if ($errors->has('mid_chk'))
                                    <span class="alert-danger">
                                        {{ $errors->first('mid_chk') }}
                                    </span><br>
                                @endif
                                <input type="submit" class="let-post" value="Let’s Post the  AD" name="submit">
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </dir>
</dir>


@endsection

@section('scripts')
    <script src="{!! asset('assets/plugins/select2/select2.min.js') !!}"></script>
    <script src="{!! asset('assets/plugins/select2/select2.implementation.js') !!}"></script>
    <script>
        $(document).ready(function () {

            
            $.ajaxSetup(
            {
                headers:
                {
                    'X-CSRF-Token': $('input[name="_token"]').val()
                }
            });

            /*var image_url_return = $('#image_url_return').val();

            if(image_url_return){
                $('#pre_image').css('background-image', 'url(' + image_url_return + ')');

            }*/

        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                console.log(input.files[0]);
                reader.onload = function (e) {
                    document.getElementById("pre_image").style.backgroundImage = "url('"+e.target.result+"')";
                    /*document.getElementById("image_url_return").value = e.target.result;*/
                   /* $('#faux').attr('value', $('#image').val());
*/
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@stop