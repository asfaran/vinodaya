<?php
define('PAGE_PARENT', 'classified', true);
define('PAGE_CURRENT', 'updateclassified', true);
?>
@extends('front.app')

@section('title', 'Edit Classified')

@section('content')
<dir class="container">
    <dir class="row row-15in no-padi">
        <div class="bredcm">
            <a href="">Home</a><span>></span><a href="">Edit Classified</a>
            <h2>Edit Classified</h2>
        </div>

    </dir>
    <dir class="row row-15in no-padi">
        <div class="col-30 max-vid">
            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading my-acus">
                        <a data-toggle="collapse" href="#collapse009">
                            <div class="my-acu">my account menu</div>
                        </a>
                    </div>
                    <div id="collapse009" class="panel-collapse collapse">
                        <div class="my-ad-rig white">
                            <a href="{!! url('/user/myads') !!}">My Advertisments</a>
                            <a href="{!! url('/user/myposts') !!}">My Classified</a>
                            <a  href="{!! url('/user/reset') !!}">Reset Password </a>
                            @if(Auth::user()->company_status !== 1)
                            <a   href="{!! url('/user/upgrade') !!}">Upgrade to Business Profile</a>
                            @endif
                        </div>
                        <div class="pos-ad">
                            <a class="active" href="{!! url('/user/postads') !!}">Post Advertisment</a>
                            <a href="{!! url('/user/postclassified') !!}">Post Classified</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-70 max-vid">
            <div class="myad-ri white">
                <div class="myad-h4">
                    <h4>Classified Details</h4>
                    <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet</p>
                </div>

                <!-- Advertisment -->
                {!! Form::open(array('url'=>'/user/postads','role'=>'form', 'files' => true, 'class'=>'form-horizontal')) !!}
                {!! Form::hidden('id', $advert_one->id, array('id' => 'invisible_id')) !!}
                <input type="hidden" name="duration_type" value="2">
                <input type="hidden" name="adds_type" id="postadds_type_display" value="{{ $advert_one->adds_type }}">
                <input type="hidden" name="duration" value="{{ (int)$advert_one->duration }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                @if ( Session::has('flash_message') )
                    <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {!! Session::get('flash_message') !!}
                    </div>
                @endif
                @if ( Session::has('flash_success') )
                    <div class="alert alert-success  {{ Session::get('flash_type') }}">
                        <button class="close" data-dismiss="alert"></button>
                        {!! Session::get('flash_success') !!}
                    </div>
                @endif
                <div class="myad-min">
                    <div>
                        <div class="row flex">
                            <div class="col-md-8 col-xs-8 a">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="re-imp">
                                                <label>Title of Advertisment</label>
                                                <input type="text" placeholder="Enter Title" class="form-control"  value="{{ $advert_one->title }}" id="title" name="title">
                                                @if ($errors->has('title'))
                                                    <span class="alert-danger">
                                                        {{ $errors->first('title') }}
                                                    </span><br>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-9 col-xs-9 ph-100">
                                            <div class="re-imp">
                                                <label>Advertisment Type</label>
                                                {!! Form::select('adds_type_display', $adds_types, $advert_one->adds_type, ['id' => 'adds_type_display', 'class' => 'form-control', 'disabled' => 'disabled']) !!}
                                                @if ($errors->has('adds_type'))
                                                    <span class="alert-danger">
                                                        {{ $errors->first('adds_type') }}
                                                    </span><br>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-9 col-xs-9 ph-100">
                                            <div class="re-imp">
                                                <label>Duration</label>
                                                <div id="div_location_city">
                                                    {!! Form::select('duration_display', $duration_type , $advert_one->duration, ['id' => 'duration_display', 'class' => 'form-control', 'disabled' => 'disabled']) !!}
                                                </div>
                                                @if($errors->has('duration'))
                                                    <span class="alert-danger">
                                                        {{ $errors->first('duration') }}
                                                    </span><br>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="re-imp">
                                                <label>Url Link</label>
                                                <input type="text" placeholder="Enter Url Link" class="form-control"  value="{{ $advert_one->extra }}" id="extra" name="extra">
                                                <span class="help-block m-b-none">
                                                   Please add valid url link with "http://" or "https://"
                                                </span>
                                                @if ($errors->has('extra'))
                                                <br>
                                                    <span class="alert-danger">
                                                        {{ $errors->first('extra') }}
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <!-- <div class="row">
                                        <div class="col-md-12">
                                            <p class="b7">Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis.</p>
                                        </div>
                                    </div> -->
                            </div>
                            <div class="col-md-4 col-xs-4 b">
                                <div class="uplod-img">
                                    @if(isset($advert_one->thumb_real))
                                    <?php $img_url = url('/uploads/classifieds/adds/real_image/'.$advert_one->thumb_real) ; ?>
                                    <label for="fileToUpload" style="background-image: url('{{ $img_url }}')"  id="pre_image"></label>
                                    @else
                                    <label for="fileToUpload" id="pre_image"></label>
                                    @endif
                                    <input type="file" name="thumb_real" id="fileToUpload" onchange="readURL(this)">
                                </div>
                                <p class="img-uplabl">
                                @if ($errors->has('thumb_real'))
                                    <span class="alert-danger">
                                        {{ $errors->first('thumb_real') }}
                                    </span>
                                @else
                                    Image Will Resize According to Selected Addvertiment.
                                    <div id="image_records"></div>

                                @endif
                                </p>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <hr>
                                <input type="checkbox" class="mid-chk" checked name="mid_chk">
                                <p class="b7">I have read and understood the terms and conditions </p>
                                <br>
                                @if ($errors->has('mid_chk'))
                                    <span class="alert-danger">
                                        {{ $errors->first('mid_chk') }}
                                    </span><br>
                                @endif
                                <input type="submit" class="let-post" value="Let’s Update the  AD" name="submit">
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </dir>
</dir>


@endsection

@section('scripts')
    <script src="{!! asset('assets/plugins/select2/select2.min.js') !!}"></script>
    <script src="{!! asset('assets/plugins/select2/select2.implementation.js') !!}"></script>
    <script>
        $(document).ready(function () {
            $.ajaxSetup(
            {
                headers:
                {
                    'X-CSRF-Token': $('input[name="_token"]').val()
                }
            });

            var adds_type_display = $('#postadds_type_display').val();

            if(adds_type_display){

                $.ajax({
                    type: "POST",
                    url: base_url+"/user/postads/image_records",
                    data: {'adds_type' : adds_type_display},
                    dataType: 'HTML',
                    success: function (data) {

                        $("#image_records").html(data);

                        console.log(data);
                    },
                    error: function(e) {
                        console.log(e.responseText);
                    }
                });

            }

        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    document.getElementById("pre_image").style.backgroundImage = "url('"+e.target.result+"')";
                   /* $('#faux').attr('value', $('#image').val());
*/
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@stop