<?php
define('PAGE_PARENT', $view_data['page_parent'], true);
define('PAGE_CURRENT', $view_data['current_page'], true);
?>
@extends('front.app')

@section('title', $view_data['page_title'])

@section('content')

<dir class="container">
    <dir class="row row-15in no-pad ">
        <div class="bredcm">
          <h2 class="lan-hid">{{ $view_data['section_title'] }}</h2>
        </div>
        
    </dir>
    <dir class="row row-70in no-padi">
        <div class="myad-min">
            <div class="my-add">
                <div class="ibox-content">
                    <br />
                    <div class="note note-danger">
                        <h4 class="block">{{ $view_data['confirm_title'] }}</h4>
                        <p>
                            {{ $view_data['confirm_message'] }}
                        </p>
                    </div>

                    {!! Form::open() !!}
                    <input type="submit" class="btn btn-danger" name="submit_confirm" value="{{ $view_data['confirm_button'] or 'Confirm' }}" />
                    <a href="{{ $view_data['cancel_url'] }}" class="btn btn-default">{{ $view_data['cancel_button'] or 'Cancel' }}</a>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection