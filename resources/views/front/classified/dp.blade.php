<?php
define('PAGE_PARENT', 'classified', true);
define('PAGE_CURRENT', 'classified', true);
?>
@extends('front.app')

@section('title', $classified_one->title)

@section('content')
<div class="container">

    <div class="clear nomal"></div>
    <div class="reletive">
         <div class="ql vi-ld">
                <span  data-toggle="modal" data-target="#myModal">
                    <img src="{{ url('/assets/img/ql.png') }}">
                </span>
            </div>
    </div>
    <div class="row row-15in">

        <div class="col-75 max-vid">

            <div class="bred-cm">
                <p>Home <span>></span> Classified</p>
            </div>

            <div class="d-man top-imgdp">
              
              <div class="d-di img-dti">
                  <h3>{{ $classified_one->title }}</h3>
                  <div class="ove-hid">
                    <div class="d-tm"> 
                      <img src="{{ url('/assets/img/b-time.png') }}">
                      <p>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($classified_one->created_at))->diffForHumans() }}</p>
                    </div>                    
                  </div>
                  <div class="cal-dt">
                      <h4><center>Rs {{ $classified_one->price }}<center></h4>
                  </div>
                 <div>

                   <div class="col-70 land-60 art-50">
                     <div class="flexslider">
                      <ul class="slides">
                        <li data-thumb="{{ url('/uploads/classifieds/'.$classified_one->parentid.'/'.$classified_one->thumb) }}">
                          <img  src="{{ url('/uploads/classifieds/'.$classified_one->parentid.'/'.$classified_one->thumb) }}">
                        </li>


                        <!-- <li data-thumb="{{ url('/assets/img/cal_slider/cl-sli2.jpg') }}">
                          <img src="{{ url('/assets/img/cal_slider/cl-sli2.jpg') }}" />
                        </li>
                        <li data-thumb="{{ url('/assets/img/cal_slider/cl-sli3.jpg') }}"> 
                          <img src="{{ url('/assets/img/cal_slider/cl-sli3.jpg') }}" />
                        </li>
                        <li data-thumb="{{ url('/assets/img/cal_slider/cl-sli4.jpg') }}">
                          <img src="{{ url('/assets/img/cal_slider/cl-sli4.jpg') }}" />
                        </li> -->
                      </ul>
                    </div>
                   </div>
 
                   <div class="col-30 land-40 art-50">
                     <div class="cli-dr">
                       <div>
                         <p>Condition:<span>New</span></p>
                       </div>
                       <div>
                         <p>Item type: <span>Other Accessory</span></p>
                       </div>
                        <div>
                         <p>Status <a href="">Available</a></p>
                       </div>
                     </div>

                     <div class="cls-dis">
                       <hr>

                       <h5>Description</h5>
                       <div>
                         <p>{{ $classified_one->description }}</p>
                       </div>

                       <div class="col-ms">
                       <input type="hidden" name="cldp_cl_value" id="cldp_cl_value" value="{{ (isset($product_owner->mobile))? $product_owner->mobile : "No Records"  }}">
                       <input type="hidden" name="cldp_mail_value" id="cldp_mail_value" value="{{ (isset($product_owner->email))? $product_owner->email : "No Records"  }}">
                         <div>
                           <p class="cl"  id="cldp_clbutton"></p>
                           <span></span>
                           <p class="ms" id="cldp_mailbutton"></p>
                         </div>
                       </div>

                       <div class="clas-mot">
                         <!-- <img src="{{ url('/assets/img/clis-v.jpg') }}"> -->
                         <icon class="fa fa-user" style="font-size:50px"></icon>
                         <div>
                           <h4>{{ (isset($product_owner->name))? $product_owner->name : "" }}</h4>
                           {{ (isset($product_owner->location))? $product_owner->location : "" }}
                         </div>
                       </div>

                       <!-- <div class="by-nw">
                         <a href="">Buy Now</a>
                       </div> -->
                     </div>
                   </div>

                   <div class="clear"></div>
                 </div>
                 <hr>
                  <div class="d-sio">
                    <!-- <img src="{{ url('/assets/img/sio-d.jpg') }}"> -->
                    <div class="share-items" data-title="{{ $classified_one->title }}" data-hash="{{ $classified_one->title }}" data-url="{{ URL::to('/classified/'.$classified_one->slug) }}" >
                      <ul class="share-links">
                        <li>
                          <a class="twitterBtn" data-dir="left" href="" >
                              <i class="fa fa-twitter"></i>&nbsp;
                              <span>Twitter</span>
                              <span class="twitter-count"></span>
                                </a>
                        </li>
                        <li>
                          <a class="facebookBtn" href="">
                              <i class="fa fa-facebook"></i>&nbsp;
                              <span>Facebook</span>
                              <span class="facebook-count"></span>
                                </a>
                        </li>
                        <li>
                          <a class="linkedinBtn" href="">
                              <i class="fa fa-linkedin"></i>&nbsp;
                              <span>LinkedIn</span>
                              <span class="linkedin-count"></span>
                                </a>
                        </li>
                        <li>
                          <a class="googleBtn" href="">
                              <i class="fa fa-google-plus"></i>&nbsp;
                              <span>Google</span>
                              <span class="google-count"></span>
                                </a>
                        </li>
                      </ul>
                    </div>

                  </div>
                  @if($page_adds && isset($page_adds['Add-5']))
                  <div class="d-adln">
                      <a  target="_blank"  href="{{ $page_adds['Add-5']->extra }}">
                          @if(isset($page_adds['Add-5']->thumb_resize))
                          <img src="{{ url('/uploads/classifieds/adds/resize_image/'.$page_adds['Add-5']->thumb_resize) }}">
                          {{ Counter::count('adds_count', $page_adds['Add-5']->id) }}
                          @else
                          <img src="{{ url('/assets/img/d-ad.jpg') }}">
                          @endif
                      </a>
                  </div>
                  @endif
              </div>              
            </div>

          @if(isset($related_products) && count($related_products) > 0)
          <div class="mr-res-n">
            <h4>Related Products</h4>
          </div>
          <div class="rele-nws">
                  @foreach($related_products  as $key => $related)
                  <div class="clid-d">
                      <a href="{{ URL::to('/classified/'.$related->slug) }}">
                        <img width="140px" height="155px"  src="{{ url('/uploads/classifieds/'.$related->parentid.'/'.$related->thumb) }}">
                      </a>
                      <h4>
                      <a href="{{ URL::to('/classified/'.$related->slug) }}">
                      <?php $s_related = substr(strip_tags($related->title), 0, 45);?>
                      {!! (strrpos($s_related, ' '))? substr($s_related, 0, strrpos($s_related, ' ')) : $s_related !!}
                      </a></h4>
                      <?php /*<p>{{ substr(strip_tags($related->description),0,80) }}...</p>*/ ?>
                      <a href="{{ URL::to('/classified/'.$related->slug) }}">Rs {{ $related->price }}</a>
                  </div>
                  @endforeach
          </div>
          @endif
          
          <div>

            </div>

        </div>
  
        <!-- Featured products -->
        <div class="col-25 max-vid">
            @if(isset($classified_featured) && count($classified_featured) > 0)
            <div class="ln-div top-cas">
                <div class="it-re"></div>
                <div class="it-rs">Featured Items</div>
            </div>

            <div class="wit">
                @foreach($classified_featured  as $key => $classified)
                {{ Counter::count('classified_featured', $classified->id) }}
                <div class="nlis-featured">
                    <div class="">
                        <div class="fic-i">
                            <img width="140px" height="155px"  src="{{ url('/uploads/classifieds/'.$classified->parentid.'/'.$classified->thumb) }}">
                            <div>
                                <p>{{ $classified->title }}</p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="rs-div">
                            <div class="rs-re"></div>
                            <div class="rs-rs"> Rs {{ $classified->price }}</div>
                            <a href="{{ URL::to('/classified/'.$classified->slug) }}">More</a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @endif


            @if($page_adds && isset($page_adds['Add-9']))
            <div class="sid-im">
                <a  target="_blank"  href="{{ $page_adds['Add-9']->extra }}">
                    @if(isset($page_adds['Add-9']->thumb_resize))
                    <img src="{{ url('/uploads/classifieds/adds/resize_image/'.$page_adds['Add-9']->thumb_resize) }}">
                    {{ Counter::count('adds_count', $page_adds['Add-9']->id) }}
                    @else
                    <img src="{{ url('/assets/img/add-4.jpg') }}">
                    @endif
                </a>
            </div>
            @endif
            
        </div>
        <!-- end Featured products-->

    </div>

</div>

@include('front.quick_links')

@endsection