<?php
define('PAGE_PARENT', 'classified', true);
define('PAGE_CURRENT', 'classified', true);
?>
@extends('front.app')

@section('title', 'Classified Listing')

@section('content')
<div class="container">

    <div class="clear nomal"></div>
    <div class="reletive">
         <div class="ql vi-ld">
                <span  data-toggle="modal" data-target="#myModal">
                    <img src="{{ url('/assets/img/ql.png') }}">
                </span>
            </div>
    </div>
    <div class="row row-15in">

        <div class="col-75 max-vid">

            <div class="bred-cm">
                <p>Home <span>></span> Classified</p>
            </div>

             <!-- Heading -->
            <div class="top-hed">
                Classified
            </div>
            <!-- end Heading -->

            <!-- Search Keyword-->
            <div class="srch">
             {!! Form::open(array('url'=>'/classifieds/search','role'=>'form', 'id' => 'classified_search')) !!}
                <input type="text" name="search_text" id="search_text" placeholder="Search Keyword" value="{{ Input::old('search_text') }}">
                <input type="button" value="Search Now" id="search_submit" name="search_submit">
            {!! Form::close() !!}
            </div>
            <!-- end Search Keyword-->

            <!-- show add numbers-->
            <div class="catman-dv">
                <div class="itm-sw">
                    <!-- Showing 1-25 of 71,280 ads -->
                </div>
               <!--  <div class="stro-b">
                    <p>Sort By</p>
                    <div class="dropdown">
                      <button class="dropdown-toggle" type="button" data-toggle="dropdown">By Date
                      <span class="caret"></span></button>
                      <ul class="dropdown-menu">
                        <li><a href="#">huu</a></li>
                        <li><a href="#">English</a></li>
                      </ul>
                    </div>
                </div> -->

            </div>
            <!--end show add numbers-->

            <div class="clear"></div>

            <div>

                <div class="col-30 lan-max">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default cat-man">
                            <div class="panel-heading">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                    <div class="cech-cat">
                                        <p><img class="res-clsi" src="{{ url('/assets/img/bok.png') }}"><span>Search By Categories</span></p>
                                        <img class="nar-clsi" src="{{ url('/assets/img/bok.png') }}">
                                        <img class="ar-clsi" src="{{ url('/assets/img/dwn.png') }}">
                                    </div>
                                    <div class="seplin"></div>
                                    <div class="clear"></div>
                                </a>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="cat-h">
                                    @if(isset($list_category) && count($list_category) > 0)
                                        <ul>
                                            <li class="@if($selected_category === 0) active @endif">
                                            <a href="{{ URL::to('/classified-lp') }}">
                                            All Categories
                                            </a>
                                            </li>
                                        @foreach($list_category  as $category)
                                            <li class="@if($selected_category === $category->id) active @endif">
                                                @if($category->post_count !== 0)
                                                <a href="{{ URL::to('/classifieds/category/'.$category->id) }}">
                                                @endif
                                                    {{ $category->title }} ({{ $category->post_count }})
                                                @if($category->post_count !== 0)
                                                </a>
                                                @endif
                                            </li>
                                        @endforeach
                                        </ul>
                                    @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default cat-man">
                            <div class="panel-heading">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                                    <div class="cech-cat">
                                        <p><img class="res-clsi" src="{{ url('/assets/img/pin.png') }}">Search By Location</p>
                                        <img class="nar-clsi" src="{{ url('/assets/img/pin.png') }}">
                                        <img class="ar-clsi" src="{{ url('/assets/img/dwn.png') }}">
                                    </div>
                                    <div class="seplin"></div>
                                    <div class="clear"></div>
                                </a>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="cat-h">
                                    @if(isset($list_location) && count($list_location) > 0)
                                        <ul>
                                            <li class="@if($selected_location === 0) active @endif">
                                                <a href="{{ URL::to('/classified-lp') }}"> All</a>
                                            </li>
                                        @foreach($list_location  as $location)
                                            <li class="@if($selected_location === $location->id) active @endif">
                                                @if($location->post_count !== 0)
                                                <a href="{{ URL::to('/classifieds/location/'.$location->id) }}">
                                                @endif
                                                    {{ $location->title }} ({{ $location->post_count }})
                                                @if($location->post_count !== 0)
                                                </a>
                                                @endif
                                            </li>
                                        @endforeach
                                        </ul>
                                    @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="cli-no">
                        @if($page_adds && count($page_adds) > 0)
                        <div class="sid-im">
                            <a  target="_blank"  href="{{ $page_adds[0]->extra }}">
                                @if(isset($page_adds[0]->thumb_resize))
                                <img src="{{ url('/uploads/classifieds/adds/resize_image/'.$page_adds[0]->thumb_resize) }}">
                                {{ Counter::count('adds_count', $page_adds[0]->id) }}
                                @else
                                <img src="{{ url('/assets/img/add-4.jpg') }}">
                                @endif
                            </a>
                        </div>
                        @endif
                        @if($page_adds && count($page_adds) > 2)
                        <div class="sid-im">
                            <a  target="_blank"  href="{{ $page_adds[2]->extra }}">
                                @if(isset($page_adds[2]->thumb_resize))
                                <img src="{{ url('/uploads/classifieds/adds/resize_image/'.$page_adds[2]->thumb_resize) }}">
                                {{ Counter::count('adds_count', $page_adds[2]->id) }}
                                @else
                                <img src="{{ url('/assets/img/in-2.jpg') }}">
                                @endif
                            </a>
                        </div>
                        @endif
                    </div>
                </div>

                <div class="col-70 lan-max">
                    <div class="casimin-div">

                    @if(isset($classified_list) && count($classified_list) > 0)
                        @foreach($classified_list  as $classified_for_top)
                            @if($classified_for_top->featured == 1 && $classified_for_top->boost_status == 1 && $classified_for_top->boost_type == 1)
                            {{ Counter::count('classified_featured', $classified_for_top->id) }}
                            <!-- classified add top add-->
                            <div class="casi casi-top">
                                <div class="casi-img">
                                    @if($classified_for_top->thumb)
                                    <a href="{{ URL::to('/classified/'.$classified_for_top->slug) }}">
                                     <img width="90px" height="75px"  src="{{ url('/uploads/classifieds/'.$classified_for_top->parentid.'/'.$classified_for_top->thumb) }}">
                                    </a>
                                    @else
                                     <img width="90px" height="75px"  src="{{ url('/assets/img/dum.jpg') }}">
                                    @endif
                                </div>
                                <div class="casi-dis">
                                    <div>
                                        <h3><a href="{{ URL::to('/classified/'.$classified_for_top->slug) }}">{{ $classified_for_top->title }}</a></h3>
                                        <p>{{ substr(strip_tags($classified_for_top->description),0,80) }}...</p>
                                       <!--  <p>Colombo, Motorbikes & Scooters</p> -->
                                    </div>
                                    <div class="casi-tim">
                                        <div class="ln-tim">
                                            <img src="{{ url('/assets/img/time.png') }}">
                                            <p>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($classified_for_top->created_at))->diffForHumans() }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="casi-pri">
                                    <img src="{{ url('/assets/img/top ad.png') }}">                                
                                    <h3>Rs {{ $classified_for_top->price }}</h3>
                                </div>
                            </div>
                            <!-- end classified add top add-->
                            @endif
                        @endforeach

                        @foreach($classified_list  as $classified)
                            @if(!($classified->featured == 1 && $classified->boost_status == 1 && $classified->boost_type == 1))
                            <!-- classified add top add-->
                            <div class="casi casi-top">
                                <div class="casi-img">
                                    @if($classified->thumb)
                                    <a href="{{ URL::to('/classified/'.$classified->slug) }}">
                                     <img width="90px" height="75px"  src="{{ url('/uploads/classifieds/'.$classified->parentid.'/'.$classified->thumb) }}">
                                    </a>
                                    @else
                                     <img width="90px" height="75px"  src="{{ url('/assets/img/dum.jpg') }}">
                                    @endif
                                </div>
                                <div class="casi-dis">
                                    <div>
                                        <h3><a href="{{ URL::to('/classified/'.$classified->slug) }}">{{ $classified->title }}</a></h3>
                                        <p>{{ substr(strip_tags($classified->description),0,80) }}...</p>
                                       <!--  <p>Colombo, Motorbikes & Scooters</p> -->
                                    </div>
                                    <div class="casi-tim">
                                        <div class="ln-tim">
                                            <img src="{{ url('/assets/img/time.png') }}">
                                            <p>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($classified->created_at))->diffForHumans() }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="casi-pri">
                                    @if($classified->featured == 1 && $classified->boost_status == 1 && $classified->boost_type == 1)
                                    <img src="{{ url('/assets/img/top ad.png') }}">
                                    {{ Counter::count('classified_featured', $classified->id) }}
                                    @endif
                                    <h3>Rs {{ $classified->price }}</h3>
                                </div>
                            </div>
                            <!-- end classified add top add-->
                            @endif
                        @endforeach
                    @else
                        <div class="casi casi-top">No Records for search result.</div>
                    @endif                    

                        

                        <div class="clear"></div>
                       <?php /*<div class="row">
                            <div class="pull-left">
                                {!!  $subscriptions->appends(array('Search' => Input::get('Search'),'sorting' => Input::get('sorting')))->render() !!}
                            </div>
                            <div class="pull-right" style="margin-top: 20px;margin-bottom: 20px;">
                                {!! Form::select('paginationDD', array('15' => '15','30' => '30','50' => '50', '100' => '100', '500' => '500', '1000' => '1000', '100000' => 'ALL'), $per_page ,array('id' => 'paginationDD','class'=> 'form-control declarative-s2')) !!}
                            </div>
                        </div>*/ ?>

                        <div class="pagi casi-pgi">
                            {!!  $classified_list->appends(array('Search' => Input::get('Search'),'sorting' => Input::get('sorting')))->render() !!}
                        </div>
                    </div>
                </div>

            </div>

            
        </div>


        <div class="col-25 max-vid">

           
            <!-- Latest News -->                                             
            @if(isset($latest_news) && count($latest_news) > 0)
            <div class="ln-main2 vid-toppad">
                <div class="n-top">
                    <img src="{{ url('/assets/img/n-top.png') }}">
                    <p>Latest News</p>
                </div>
                @foreach($latest_news  as $latest)
                <div class="ni-pad">
                    <div class="ln-pic li-s">
                        <a href="{{ URL::to('/news/'.$latest->id) }}">
                            <img src="{{ url('/uploads/news/'.$latest->thumb) }}">
                        </a>
                    </div>
                    <div class="in-con">
                        <div class="ln-hp lis-txt">

                            <p>{{ $latest->title }}...</p>
                        </div>
                        <div class="ln-min-com">
                            <div class="ln-tim">
                                <img src="{{ url('/assets/img/time.png') }}">
                                <p>{{ $latest->updated_at }}</p>
                            </div>

                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @endif
            <!-- end Latest News -->

            <!-- Gossip -->
            @if(isset($gossip_zone) && count($gossip_zone) > 0)         
            <div class="ln-main2 vid-gsp">
                <div class="niws-gosip-bc">

                </div>
                @foreach($gossip_zone  as $gossip)
                <div class="ni-pad">
                    <div class="ln-pic li-s">
                        <a href="{{ URL::to('/news/'.$gossip->id) }}">
                            <img src="{{ url('/uploads/news/'.$gossip->thumb) }}">
                        </a>
                    </div>
                    <div class="in-con">
                        <div class="ln-hp lis-txt">

                            <p>{{ $gossip->title }}...</p>
                        </div>
                        <div class="ln-min-com">
                            <div class="ln-tim">
                                <img src="{{ url('/assets/img/time.png') }}">
                                <p>{{ $gossip->updated_at }}</p>
                            </div>

                        </div>
                    </div>
                </div>
                @endforeach

            </div>
            @endif
            <!-- end Gossip -->
            
            @if($page_adds && count($page_adds) > 1)
            <div  class="cli-no">
                <div class="sid-im">
                    <a  target="_blank"   href="{{ $page_adds[1]->extra }}">
                        @if(isset($page_adds[1]->thumb_resize))
                        <img src="{{ url('/uploads/classifieds/adds/resize_image/'.$page_adds[1]->thumb_resize) }}">
                        {{ Counter::count('adds_count', $page_adds[1]->id) }}
                        @else
                        <img src="{{ url('/assets/img/add-2.jpg') }}">
                        @endif
                    </a>
                </div>
            </div>
            @endif
            
        </div>
       

    </div>

</div>


@include('front.quick_links')

@endsection