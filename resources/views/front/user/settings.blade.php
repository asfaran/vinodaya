<?php
define('PAGE_PARENT', 'home', true);
define('PAGE_CURRENT', 'settings', true);
?>
@extends('front.app')

@section('title', 'User Settings')

@section('content')
<dir class="container">
  <dir class="row row-15in no-padi">
    <div class="bredcm">
      <a href="">Home</a><span>></span><a href="">About us</a>
      <h2>My Advertisments</h2>
    </div>
    
  </dir>
  <dir class="row row-15in no-padi">
    <div class="col-30 max-vid">
      <div class="panel-group">
        <div class="panel panel-default">
          <div class="panel-heading my-acus">
              <a data-toggle="collapse" href="#collapse009">
                <div class="my-acu">my account menu</div> 
              </a>
          </div>
          <div id="collapse009" class="panel-collapse collapse">
            <div class="my-ad-rig white">
            <a href="">My Advertisments</a>
            <a href="">Reset Password </a>
            </div>
            <div class="pos-ad">
              <a href="">Post Advertisment</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-70 max-vid">
    <div class="myad-ri white">
      <div class="myad-h4">
        <h4>Personal Accounts Settings</h4>
        <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet</p>
      </div>

      <!-- Advertisment -->
      <div class="myad-min">
        <div>
          <form>
            <div class="row">
              <div class="col-md-7">
                <div class="re-imp">
                  <label>First/Last name</label>
                  <input type="text" name="">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-7">
                <div class="re-imp">
                  <label>Location </label>
                  <input type="text" name="">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-7">
                <div class="re-imp">
                  <label>Email Address</label>
                  <input type="text" name="">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-7">
                <div class="re-imp">
                  <label>Phone number</label>
                  <input type="text" name="">
                </div>
              </div>
            </div>
            
            <div class="row">
              <div class="col-md-12">
                <div class="regied">
                  <h3>Business Account Related Settings</h3>
                </div>              
              </div>
            </div>

            <div class="row">
              <div class="col-md-7">
                <div class="re-imp">
                  <label>Company name </label>
                  <input type="text" name="">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-7">
                <div class="re-imp">
                  <label>Address </label>
                  <input type="text" name="">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-7">
                <div class="re-imp">
                  <label>Contact person </label>
                  <input type="text" name="">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-7">
                <div class="re-imp">
                  <input type="submit" value="Save Settings" name="">
                </div>
              </div>
            </div>
          </form>
         
        </div>
      </div>

    </div>
    </div>
  </dir>
</dir>


@endsection