<?php
define('PAGE_PARENT', 'home', true);
define('PAGE_CURRENT', 'register', true);
?>
@extends('front.app')

@section('title', 'Register')

@section('content')
<dir class="container">
  <dir class="row row-15in no-padi">
    <div class="hregi-top">
      Upgrade to Business Profile
    </div>
    <div class="regi-w">
      <div class="row">
        <div class="col-md-6 col-xs-6 col-md-offset-3 col-xs-offset-3 no-padi pot-100">
          <div class="tex-alcen">
            <div class="fr-1">
              <span class="regi reg-don"></span>         
            </div>
            <div class="fr-1 reg-don-t">
              <h3>Personal Profile</h3>
              <p>quis nostrud exerci tation ullamcorper</p>
            </div>
          </div>          
        </div>
      
      </div>

      @if ( Session::has('flash_message') )
          <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                  <button class="close" data-dismiss="alert"></button>
                  {{ Session::get('flash_message') }}
          </div>
      @endif
      @if ( Session::has('flash_success') )
          <div class="alert alert-success  {{ Session::get('flash_type') }}">
              <button class="close" data-dismiss="alert"></button>
              {{ Session::get('flash_success') }}
          </div>
      @endif

      <div class="">
        {!! Form::open(array('url'=>'/user/upgrade','role'=>'form', 'class'=>'form-horizontal')) !!}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="id" value="{{ $user_data->id }}">


          <div class="form-group">
            <label class="col-lg-3 reg-max control-label">First name</label>

            <div class="col-lg-9 reg-max">

                {!! Form::text('f_name',$user_data->f_name, ['id' => 'f_name', 'disabled' => 'disabled', 'class' => 'form-control']) !!}
                <span class="alert-danger m-b-none">
                  {!! ($errors->has('f_name') ? $errors->first('f_name') : '') !!}
                </span>
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 reg-max control-label">Last name</label>

            <div class="col-lg-9 reg-max">

                {!! Form::text('l_name',$user_data->l_name, ['id' => 'l_name', 'disabled' => 'disabled', 'class' => 'form-control']) !!}
                <span class="alert-danger m-b-none">
                  {!! ($errors->has('l_name') ? $errors->first('l_name') : '') !!}
                </span>
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 reg-max control-label">Email address</label>

            <div class="col-lg-9 reg-max">
                {!! Form::text('email',$user_data->email, ['id' => 'email', 'disabled' => 'disabled', 'class' => 'form-control']) !!}
                <span class="alert-danger m-b-none">
                  {!! ($errors->has('email') ? $errors->first('email') : '') !!}
                </span>
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 reg-max control-label">Phone Number</label>

            <div class="col-lg-9 reg-max">
                {!! Form::text('mobile',$user_data->mobile, ['id' => 'mobile', 'class' => 'form-control']) !!}
                <span class="alert-danger m-b-none">
                  {!! ($errors->has('mobile') ? $errors->first('mobile') : '') !!}
                </span>
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 reg-max control-label">Location</label>

            <div class="col-lg-9 reg-max">
                {!! Form::text('location',$user_data->location, ['id' => 'user_register_location', 'disabled' => 'disabled', 'class' => 'form-control']) !!}
                <p id="user_register_location_detail"></p>
                <span class="alert-danger m-b-none">
                  {!! ($errors->has('location') ? $errors->first('location') : '') !!}
                </span>
            </div>
          </div>
          <br>
          <hr>
          <div class="row">
            <div class="col-md-6 col-xs-6 col-md-offset-3 col-xs-offset-3 no-padi pot-100">
                <div class="tex-alcen">
                  <div class="fr-1">
                    <span id="company_detail_selection" class="regi"></span>        
                  </div>
                  <div class="fr-1">
                    <h3>Company Profile</h3>
                    <p>quis nostrud exerci tation ullamcorper</p>
                  </div>
                </div>
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-8 col-md-8  col-xs-8 col-md-offset-2 col-xs-offset-2  control-label">
              <input id="company_selection" name="company_selection" data-toggle="toggle" type="checkbox">&#9;<label>I Agree To Pay Monthly Rs.500/= for display my company details.</label>
              <span class="alert-danger m-b-none">
                {!! ($errors->has('company_selection') ? $errors->first('company_selection') : '') !!}
              </span>
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 reg-max control-label">Company Name</label>

            <div class="col-lg-9 reg-max">
                {!! Form::text('company_name',Input::old('company_name'), ['id' => 'register_company_name', 'disabled' => 'disabled', 'class' => 'form-control']) !!}
                <span class="alert-danger m-b-none">
                {!! ($errors->has('company_name') ? $errors->first('company_name') : '') !!}
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 reg-max control-label">Address</label>

            <div class="col-lg-9 reg-max">
                {!! Form::text('company_addr',Input::old('company_addr'), ['id' => 'register_company_addr', 'disabled' => 'disabled', 'class' => 'form-control']) !!}
                <span class="alert-danger m-b-none">
                {!! ($errors->has('company_addr') ? $errors->first('company_addr') : '') !!}
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 reg-max control-label">Contact person</label>

            <div class="col-lg-9 reg-max">
                {!! Form::text('company_contact',Input::old('company_contact'), ['id' => 'register_company_contact', 'disabled' => 'disabled', 'class' => 'form-control']) !!}
                <span class="alert-danger m-b-none">
                {!! ($errors->has('company_contact') ? $errors->first('company_contact') : '') !!}
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 col-xs-6 col-md-offset-3 col-xs-offset-3">
              <div class="regi-sub">
                <input type="submit" value="Upgrade Profile " name="btn_submit">
              </div>
            </div>
          </div>

        {!! Form::close() !!}
      </div>
    </div>
  </dir>
</dir>

@endsection
@section('scripts')
    @parent
    
    <script>
    var x = document.getElementById("user_register_location_detail");

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else { 
            x.innerHTML = "Geolocation is not supported by this browser.";
        }
    }

    function showPosition(position) {
        /*x.innerHTML = "Your Latitude: " + position.coords.latitude + 
        "<br>Your Longitude: " + position.coords.longitude + "<br>";*/
        
        
        var GEOCODING = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + position.coords.latitude + '%2C' + position.coords.longitude + '&language=en&sensor=true&key=AIzaSyBYvJWewblBmyPAkm0v5ZOJdE7l3jSkoKw';

                $.getJSON(GEOCODING).done(function(location) {
                   console.log(location);
                  $('#user_register_location').val(location.results[0].formatted_address);

                  /*$('#demo').append( "<br>country:<p>"+location.results[0].address_components[5].long_name+"</p><br>" );
                  $('#demo').append( "<p>state:"+location.results[0].address_components[4].long_name+"</p><br>" );
                  $('#demo').append( "<p>city:"+location.results[0].address_components[2].long_name+"</p><br>" );
                  $('#location').append( "<p>address:"+location.results[0].formatted_address+"</p>" );
                  $('#demo').append( "<p>latitude:"+position.coords.latitude+"</p><br>" );
                  $('#demo').append( "<p>longitude:"+position.coords.longitude+"</p>" );*/
                })
    }


    


</script>
    
@endsection



