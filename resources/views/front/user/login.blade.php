<?php
define('PAGE_PARENT', 'home', true);
define('PAGE_CURRENT', 'login', true);
?>
@extends('front.app')

@section('title', 'User Login')

@section('content')
<dir class="container">
  <div class="log-mndiv">
    <div>
      <img class="img-responsive" src="{{ url('/assets/img/login.png') }}">
    </div>
    <div class="log-mian">
      <h6>Enter your accounts Details</h6>
      @if ( Session::has('flash_message') )
          <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                  <button class="close" data-dismiss="alert"></button>
                  {!! Session::get('flash_message') !!}
          </div>
      @endif
      @if ( Session::has('flash_success') )
          <div class="alert alert-success  {{ Session::get('flash_type') }}">
              <button class="close" data-dismiss="alert"></button>
              {!! Session::get('flash_success') !!}
          </div>
      @endif
      @if ($errors->has('password'))
        <span class="alert-danger">{{ $errors->first('password') }}</span><br>
      @endif 
      {!! Form::open(array('url'=>'/user/login','role'=>'form', 'class'=>'form-horizontal')) !!}
       <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="text" name="email" placeholder="Email Address" value="{{ Input::old('email') }}">
        @if ($errors->has('email'))
          <span class="alert-danger">{{ $errors->first('email') }}</span><br>
        @endif
        <input type="Password" name="password" placeholder="Password">

        <a href="{!! url('/user/forgot') !!}">I forgot my password </a>
        <input type="submit" value="Login" name="">
      {!! Form::close() !!}
    </div>
    <div class="dwn-log">
      <p>Don’t have a Login ? </p>
      <a href="{!! url('/user/register') !!}">Register Now</a>
    </div>
  </div>
</dir>

@endsection