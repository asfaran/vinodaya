<?php
define('PAGE_PARENT', 'home', true);
define('PAGE_CURRENT', 'register', true);
?>
@extends('front.app')

@section('title', 'Register')

@section('content')
<dir class="container">
  <dir class="row row-15in no-padi">
    <div class="hregi-top">
      Register with Vinodaya
    </div>
    <div class="regi-w">
      <div class="row">
        <div class="col-md-6 col-xs-6 no-padi pot-100">
          <div class="tex-alcen">
            <div class="fr-1">
              <span class="regi reg-don"></span>         
            </div>
            <div class="fr-1 reg-don-t">
              <h3>Personal Profile</h3>
              <p>quis nostrud exerci tation ullamcorper</p>
            </div>
          </div>          
        </div>
        <div class="col-md-6 col-xs-6 no-padi pot-100"> 
          <div class="tex-alcen">
            <div class="fr-1">
              <span id="company_detail_selection" class="regi"></span>        
            </div>
            <div class="fr-1">
              <h3>Company Profile</h3>
              <p>quis nostrud exerci tation ullamcorper</p>
            </div>
          </div>
        </div>
      
      </div>

      <div class="row">
        <div class="col-md-12 col-xs-12">
          <h3 class="regi-h3">About your Details</h3>
        </div>
      </div>

      @if ( Session::has('flash_message') )
          <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                  <button class="close" data-dismiss="alert"></button>
                  {{ Session::get('flash_message') }}
          </div>
      @endif
      @if ( Session::has('flash_success') )
          <div class="alert alert-success  {{ Session::get('flash_type') }}">
              <button class="close" data-dismiss="alert"></button>
              {{ Session::get('flash_success') }}
          </div>
      @endif

      <div class="">
        {!! Form::open(array('url'=>'/user/register','role'=>'form', 'class'=>'form-horizontal')) !!}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="row">
            <div class="col-md-6 col-xs-6 pot-100">
              <div class="reg-max">
                <label>First name</label>
                {!! Form::text('f_name',Input::old('f_name'), ['id' => 'f_name']) !!}
                <span class="alert-danger m-b-none">
                  {!! ($errors->has('f_name') ? $errors->first('f_name') : '') !!}
                </span>

              </div>
            </div>
            <div class="col-md-6 col-xs-6 pot-100">
              <div class="reg-max">
                <label>Last name</label>
                {!! Form::text('l_name',Input::old('l_name'), ['id' => 'l_name']) !!}
                <span class="alert-danger m-b-none">
                  {!! ($errors->has('l_name') ? $errors->first('l_name') : '') !!}
                </span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 col-xs-6 pot-100">
              <div class="reg-max">
                <label>Email address</label>
                {!! Form::text('email',Input::old('email'), ['id' => 'email']) !!}
                <span class="alert-danger m-b-none">
                  {!! ($errors->has('email') ? $errors->first('email') : '') !!}
                </span>
              </div>
            </div>
            <div class="col-md-6 col-xs-6 pot-100">
              <div class="reg-max">
                <label>Phone Number</label>
                {!! Form::text('mobile',Input::old('mobile'), ['id' => 'mobile']) !!}
                <span class="alert-danger m-b-none">
                  {!! ($errors->has('mobile') ? $errors->first('mobile') : '') !!}
                </span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-9 col-xs-9 pot-100">
              <div class="reg-max">
                <label>Location</label>
                {!! Form::text('location',Input::old('location'), ['id' => 'user_register_location']) !!}
                <p id="user_register_location_detail"></p>
                <span class="alert-danger m-b-none">
                  {!! ($errors->has('location') ? $errors->first('location') : '') !!}
                </span>
              </div>
            </div>
            <div class="col-md-3 col-xs-3 pot-100">
              <span class="find-regi"  onclick="getLocation()">Find Location</span>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 col-xs-6 pot-100">
              <div class="reg-max">
                <label>Password</label>
                {!! Form::password('password',['id' => 'password']) !!}
                <span class="alert-danger m-b-none">
                  {!! ($errors->has('password') ? $errors->first('password') : '') !!}
                </span>
              </div>
            </div>
            <div class="col-md-6 col-xs-6 pot-100">
              <div class="reg-max">
                <label>Confirm Password</label>
                {!! Form::password('password_confirmation',['id' => 'password_confirmation']) !!}
                <span class="alert-danger m-b-none">
                  {!! ($errors->has('password_confirmation') ? $errors->first('password_confirmation') : '') !!}
                </span>
              </div>
            </div>
          </div>
          <br>
          <hr>
          <div class="row">
            <div class="col-md-12 col-xs-12 pot-100">
              <h3 class="regi-h3">Company Details</h3>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 col-xs-12 pot-100">
              <input id="company_selection" name="company_selection" data-toggle="toggle" type="checkbox">&#9;<label>I Agree To Pay Monthly Rs.500/= for display my company details.</label>
            </div>
          </div>

          <div class="row user_register_padding" >
            <div class="col-md-6 col-xs-6 pot-100">
              <div class="reg-max">
                <label>Company Name</label>
                {!! Form::text('company_name',Input::old('company_name'), ['id' => 'register_company_name', 'disabled' => 'disabled']) !!}
                <span class="alert-danger m-b-none">
                  {!! ($errors->has('company_name') ? $errors->first('company_name') : '') !!}
                </span>
              </div>
            </div>
            <div class="col-md-6 col-xs-6 pot-100">
              <div class="reg-max">
                <label>Address</label>
                {!! Form::text('company_addr',Input::old('company_addr'), ['id' => 'register_company_addr', 'disabled' => 'disabled']) !!}
                <span class="alert-danger m-b-none">
                  {!! ($errors->has('company_addr') ? $errors->first('company_addr') : '') !!}
                </span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 col-xs-6 pot-100">
              <div class="reg-max">
                <label>Contact person</label>
                {!! Form::text('company_contact',Input::old('company_contact'), ['id' => 'register_company_contact', 'disabled' => 'disabled']) !!}
                <span class="alert-danger m-b-none">
                  {!! ($errors->has('company_contact') ? $errors->first('company_contact') : '') !!}
                </span>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12 col-xs-12">
              <div class="regi-sub">
                <input type="submit" value="Register Now" name="btn_submit">
              </div>
            </div>
          </div>

        {!! Form::close() !!}
      </div>
    </div>
  </dir>
</dir>

@endsection
@section('scripts')
    @parent
    
    <script>
    var x = document.getElementById("user_register_location_detail");

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else { 
            x.innerHTML = "Geolocation is not supported by this browser.";
        }
    }

    function showPosition(position) {
        /*x.innerHTML = "Your Latitude: " + position.coords.latitude + 
        "<br>Your Longitude: " + position.coords.longitude + "<br>";*/
        
        
        var GEOCODING = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + position.coords.latitude + '%2C' + position.coords.longitude + '&language=en&sensor=true&key=AIzaSyBYvJWewblBmyPAkm0v5ZOJdE7l3jSkoKw';

                $.getJSON(GEOCODING).done(function(location) {
                   console.log(location);
                  $('#user_register_location').val(location.results[0].formatted_address);

                  /*$('#demo').append( "<br>country:<p>"+location.results[0].address_components[5].long_name+"</p><br>" );
                  $('#demo').append( "<p>state:"+location.results[0].address_components[4].long_name+"</p><br>" );
                  $('#demo').append( "<p>city:"+location.results[0].address_components[2].long_name+"</p><br>" );
                  $('#location').append( "<p>address:"+location.results[0].formatted_address+"</p>" );
                  $('#demo').append( "<p>latitude:"+position.coords.latitude+"</p><br>" );
                  $('#demo').append( "<p>longitude:"+position.coords.longitude+"</p>" );*/
                })
    }


    


</script>
    
@endsection



