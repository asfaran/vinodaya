<?php
define('PAGE_PARENT', 'home', true);
define('PAGE_CURRENT', 'login', true);
?>
@extends('front.app')

@section('title', 'User Login')

@section('content')
<dir class="container">
  <div class="log-mndiv">
    <div>
      <img class="img-responsive" src="{{ url('/assets/img/login.png') }}">
    </div>
    <div class="log-mian">
      <h6>Enter your accounts Details</h6>
      {!! Form::open(array('url'=>'/user/forgot','role'=>'form', 'class'=>'form-horizontal')) !!}
       <input type="hidden" name="_token" value="{{ csrf_token() }}">
        Email Address<br>
        <input type="text" name="email" placeholder="Email Address">
        <input type="submit" value="Get Reset Link" name="">
      {!! Form::close() !!}
    </div>
    <div class="dwn-log">
      <a href="{!! url('/user/login') !!}">Login</a>
    </div>
  </div>
</dir>

@endsection