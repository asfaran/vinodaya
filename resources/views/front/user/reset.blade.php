<?php
define('PAGE_PARENT', 'home', true);
define('PAGE_CURRENT', 'reset', true);
?>
@extends('front.app')

@section('title', 'reset Password')

@section('content')
<dir class="container">
  <dir class="row row-15in no-padi">
    <div class="bredcm">
      <a href="">Home</a><span>></span><a href="">Reset Password</a>
      <h2>Reset Password</h2>
    </div>
    
  </dir>
  <dir class="row row-15in no-padi">
    <div class="col-30 max-vid">
      <div class="panel-group">
        <div class="panel panel-default">
          <div class="panel-heading my-acus">
              <a data-toggle="collapse" href="#collapse009">
                <div class="my-acu">my account menu</div> 
              </a>
          </div>

          <div id="collapse009" class="panel-collapse collapse">
              <div class="my-ad-rig white">
                  <a href="{!! url('/user/myads') !!}">My Advertisments</a>
                  <a href="{!! url('/user/myposts') !!}">My Classified</a>
                  <a  class="activ" href="{!! url('/user/reset') !!}">Reset Password </a>

              </div>
              <div class="pos-ad">
                  <a href="{!! url('/user/postclassified') !!}">Post Classified</a>
                  <a href="{!! url('/user/postads') !!}">Post Advertisment</a>
              </div>
          </div>
          
        </div>
      </div>
    </div>
    <div class="col-70 max-vid">
    <div class="myad-ri white">
      <div class="myad-h4">
        <h4>Reset Password</h4>
        <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet</p>
      </div>
      @if ( Session::has('flash_message') )
          <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                  <button class="close" data-dismiss="alert"></button>
                  {{ Session::get('flash_message') }}
          </div>
      @endif
      @if ( Session::has('flash_success') )
          <div class="alert alert-success  {{ Session::get('flash_type') }}">
              <button class="close" data-dismiss="alert"></button>
              {{ Session::get('flash_success') }}
          </div>
      @endif

      <!-- Advertisment -->
      <div class="myad-min">
        <div>
          <form class="form-horizontal" role="form" method="POST" action="{!! route('user.reset') !!}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="row">
              <div class="col-md-7">
                <div class="re-imp">
                  <label>Current Password</label>
                  <input type="password" class="form-control" name="current_password">
                  @if ($errors->has('current_password'))
                      <span class="alert-danger">
                          {{ $errors->first('current_password') }}
                      </span><br>
                  @endif
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-7">
                <div class="re-imp">
                  <label>New Password</label>
                  <input type="password" class="form-control" name="password">
                  @if ($errors->has('password'))
                      <span class="alert-danger">
                          {{ $errors->first('password') }}
                      </span><br>
                  @endif
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-7">
                <div class="re-imp">
                  <label>Repeat Password</label>
                  <input type="password" class="form-control" name="password_confirmation">
                  @if ($errors->has('password_confirmation'))
                      <span class="alert-danger">
                          {{ $errors->first('password_confirmation') }}
                      </span><br>
                  @endif
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-7">
                <div class="re-imp">
                  <input type="submit" value="Reset Password Now" name="">
                </div>
              </div>
            </div>
          </form>
         
        </div>
      </div>

    </div>
    </div>
  </dir>
</dir>

@endsection