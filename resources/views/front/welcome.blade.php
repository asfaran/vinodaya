<?php
define('PAGE_PARENT', 'home', true);
define('PAGE_CURRENT', 'home', true);
?>
@extends('front.app')

@section('title', 'Home Page')

@section('content')
<link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
<div class="container">

<div class="row rpad">
  <div class="top-pad">
    <div class="les-news">
      @if(isset($latest_news) && count($latest_news) > 0)
      <div class="les-slider">
        <div id="slider" class="nivoSlider">
        @foreach($latest_news  as $key=>$latest)
          <div class="slide slide_{{ $key }}">
              <img class="slide-image" src="{{ url('/uploads/news/'.$latest->thumb) }}" 
                    width="100%" height="370px" title="#htmlcaption_{{ $key }}">
          </div>
        @endforeach
        </div>
        @foreach($latest_news  as $key_one => $latest_one)
          <div id="htmlcaption_{{ $key_one }}" class="nivo-html-caption">
             <h3 class="slide-h1"><a href="{{ URL::to('/news/'.$latest_one->id) }}">{{ $latest_one->title }}</a></h3>
             <div class="slide-h2"><img src="{{ url('/assets/img/cl-s.png') }}"> {{ \Carbon\Carbon::createFromTimeStamp(strtotime($latest_one->created_at))->diffForHumans() }}</div>
            
          </div>
        @endforeach

        <a href="#">
          <div class="ls-btn">
            Latest News
          </div>
        </a>
      </div>
      @endif

      <div class="fet-news">
      <?php $news_by_category_col = 1; ?>
      @if(isset($news_by_category) && count($news_by_category) > 0)
        @foreach($news_by_category  as $category_key => $category)
          @if($category['slug'] === "sports")
          <div class="col-33">    
            <div class="sports">
              <!-- <img src="{{ url('/assets/img/sports.png') }}"> -->
              <img height="145px" width="195px"  src="{{ url('/uploads/news/'.$category['news_list'][0]->thumb) }}">
              <a href="{{ URL::to('/news-category/'.$category_key) }}" class="sports-a">sports</a>
              <div class="s-in-div">
                <h6><a href="{{ URL::to('/news/'.$category['news_list'][0]->id) }}">{{ $category['news_list'][0]->title }}</a></h6>
                <div class="st-time">
                   <img src="{{ url('/assets/img/cl-sm.png') }}">
                   <p>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($category['news_list'][0]->created_at))->diffForHumans() }}</p>
                </div>       
              </div>
            </div>
          </div>
          @endif
          @if($category['slug'] == "international")
          <div class="col-32">
            <div class="sports margin-0-auto">
              <img height="145px" width="195px" src="{{ url('/uploads/news/'.$category['news_list'][0]->thumb) }}">
              <a href="{{ URL::to('/news-category/'.$category_key) }}" class="International-a">International</a>
              <div class="s-in-div">
                <h6><a href="{{ URL::to('/news/'.$category['news_list'][0]->id) }}">{{ $category['news_list'][0]->title }}</a></h6>
                <div class="st-time">
                   <img src="{{ url('/assets/img/cl-sm.png') }}">
                   <p>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($category['news_list'][0]->created_at))->diffForHumans() }}</p>
                </div>       
              </div>
            </div>
          </div>
          @endif
          @if($category['slug'] == "health")
          <div class="col-32">
            <div class="sports float-r">
              <img height="145px" width="195px" src="{{ url('/uploads/news/'.$category['news_list'][0]->thumb) }}">
              <a href="{{ URL::to('/news-category/'.$category_key) }}" class="Health-a">Health</a>
              <div class="s-in-div">
                <h6><a href="{{ URL::to('/news/'.$category['news_list'][0]->id) }}">{{ $category['news_list'][0]->title }}</a></h6>
                <div class="st-time">
                   <img src="{{ url('/assets/img/cl-sm.png') }}">
                   <p>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($category['news_list'][0]->created_at))->diffForHumans() }}</p>
                </div>       
              </div>
            </div>
          </div>
          @endif
        
        @endforeach
      @endif   
        
      </div>

    </div>

        <div class="qal">
          <div class="gal-top">
             Quick Access Links
          </div>
          <div class="qal-bc">
            <div class="gal-fr-6">
              @if(isset($quick_category_array[4]) && count($quick_category_array[4]) > 0)
              <div class="gal-bak">
                <span class="videos"></span>videos
              </div>
              <div class="gal-bak">
                  <?php $video_ql = 1;?>
                  @foreach($quick_category_array[4]  as $video_key => $video_category)
                    @if($video_ql <= 6)
                    <a href="{{ URL::to('/video-lp/category/'.$video_key) }}">{{ $video_category }}</a>
                    @endif
                    <?php $video_ql++;?>
                  @endforeach              
              </div>
              @endif
              @if(isset($quick_category_array[1]) && count($quick_category_array[1]) > 0)
              <div class="gal-bak">
                <span class="news"></span>News
              </div>
              <div class="gal-bak">
                  <?php $news_ql = 1;?>
                  @foreach($quick_category_array[1]  as $news_key => $news_category)
                    @if($news_ql <= 7)
                    <a href="{{ URL::to('/news-category/'.$news_key) }}">{{ $news_category }}</a>
                    @endif
                    <?php $news_ql++;?>
                  @endforeach               
              </div>
              @endif
            </div>
            <div class="gal-fr-6">
              @if(isset($quick_category_array[5]) && count($quick_category_array[5]) > 0)
              <div class="gal-bak">
                <span class="budi-pg"></span>Products
              </div>
              <div class="gal-bak">              
                <?php $prod_ql = 1;?>
                  @foreach($quick_category_array[5]  as $prod_key => $prod_category)
                    @if($prod_ql <= 4)
                    <a href="{{ URL::to('/classifieds/category/'.$prod_key) }}">{{ $prod_category }}</a>
                    @endif
                    <?php $prod_ql++;?>
                  @endforeach
              </div>
              @endif

              @if(isset($quick_category_array[2]) && count($quick_category_array[2]) > 0)
              <div class="gal-bak">
                <span class="ape-lipi"></span>ape lipi
              </div>
              <div class="gal-bak">              
                <?php $lipi_ql = 1;?>
                  @foreach($quick_category_array[2]  as $lipi_key => $lipi_category)
                    @if($lipi_ql <= 3)
                    <a href="{{ URL::to('/articles-lp/category/'.$lipi_key) }}">{{ $lipi_category }}</a>
                    @endif
                    <?php $lipi_ql++;?>
                  @endforeach
              </div>
              @endif
              @if(isset($quick_category_array[3]) && count($quick_category_array[3]) > 0)
              <div class="gal-bak">
                <span class="sindu"></span>Pintoora pituva
              </div>
              <div class="gal-bak">              
                <?php $Pintoora = 1;?>
                  @foreach($quick_category_array[3]  as $Pintoora_key => $Pintoora_category)
                    @if($Pintoora <= 3)
                    <a href="{{ URL::to('/images-lp/category/'.$Pintoora_key) }}">{{ $Pintoora_category }}</a>
                    @endif
                    <?php $Pintoora++;?>
                  @endforeach
              </div>
              @endif
            </div>
          </div>
          
        </div> 
    </div>
</div>




    <div class="row rpad first-r">
      <div class="sec-row-lef">
        <!-- Latest News -->
        @if(isset($front_latest) && count($front_latest) > 0)
        <?php 
          $adds_row = ceil(count($front_latest)/2);
          $latest_row = 1;
        ?>
        <div class="ln-main" id="{{ $adds_row }}">

          <div class="ln-div">
            <div class="ln-angal"></div>
            <div class="ln-hed">Latest News </div>
          </div>

          @foreach($front_latest  as $latest)
          <div class="ln"  id="{{ $latest_row }}">
            <div class="ln-pic">
               <a href="{{ URL::to('/news/'.$latest->id) }}">
               <img height="100px"  width="120px" src="{{ url('/uploads/news/'.$latest->thumb) }}">
              </a>
            </div>
            <div class="in-con">
              <div class="ln-hp">
                <h6><a href="{{ URL::to('/news/'.$latest->id) }}">{{ $latest->title }}</a></h6>
                <p>{!! substr(strip_tags($latest->body),0,80)  !!}...</p>
              </div>
              <div class="ln-min-com">
                <div class="ln-tim">
                  <img src="{{ url('/assets/img/time.png') }}">
                  <p>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($latest->created_at))->diffForHumans() }}</p>
                </div>
                <div class="ln-com">
                  <img src="{{ url('/assets/img/coment.png') }}">
                  <p>Comments</p>
                </div>
              </div>
            </div>
          </div>
          @if($latest_row == $adds_row)
            @if($home_adds['Add-1'])
            <div class="addis">
              <a  target="_blank"  href="{{ $home_adds['Add-1']->extra }}"> 
                @if(isset($home_adds['Add-1']->thumb_resize))
                <img src="{{ url('/uploads/classifieds/adds/resize_image/'.$home_adds['Add-1']->thumb_resize) }}">
                {{ Counter::count('adds_count', $home_adds['Add-1']->id) }}
                @else
                <img src="{{ url('/assets/img/add-1.jpg') }}">
                @endif
              </a>
            </div>
            @endif
          @endif
          <?php 
            $latest_row++;
          ?>
          @endforeach
        </div>
        @endif
        <!-- end Latest News -->

        <!-- MODEL OF THE DAY -->
        @if($model_ofthe_day)
        <div class="mof">
          <div class="mof-top">
            <img src="{{ url('/assets/img/mod-top.png') }}">
            <p>MODEL OF THE DAY</p>
          </div>
          <div class="mof-model">
            <a href="{{ URL::to('/images/'.$model_ofthe_day->id) }}"><img src="{{ url('/uploads/images/'.$model_ofthe_day->thumb) }}"></a>
          </div>
          <div class="mof-btn">
            <a href="{{ URL::to('/images/'.$model_ofthe_day->id) }}">{{ $model_ofthe_day->title }}</a>
          </div>  
        </div>
        @endif
        <!-- end MODEL OF THE DAY -->

        <!-- cartoon of the day -->
        @if($cartoon_ofthe_day)
        <div class="car-f-day">
          <div class="car-hed">
            <div class="angal-div"></div>
            <div class="carton-hed">cartoon of the day</div>
          </div>
          <div>
            <a href="{{ URL::to('/images/'.$cartoon_ofthe_day->id) }}">
              <img src="{{ url('/uploads/images/'.$cartoon_ofthe_day->thumb) }}">
            </a>
          </div>
        </div>
        @endif
        <!-- end cartoon of the day -->

        <!-- addtisment -->
        @if($home_adds['Add-2'])
        <div class="addt-2">
          <a  target="_blank"  href="{{ $home_adds['Add-2']->extra }}">
            @if(isset($home_adds['Add-2']->thumb_resize))
            <img src="{{ url('/uploads/classifieds/adds/resize_image/'.$home_adds['Add-2']->thumb_resize) }}">
            {{ Counter::count('adds_count', $home_adds['Add-2']->id) }}
            @else
            <img src="{{ url('/assets/img/add-2.jpg') }}">
            @endif
          </a>
        </div>
        @endif
        <!-- end addtisment -->
      </div>

      <div class="sec-row-rig">

      <!-- patta video -->
        @if(isset($patta_videos) && count($patta_videos) > 0)
        <div class="patta-one">
          <div class="patta-img">
            <img src="{{ url('/assets/img/pata vidi.png') }}">
          </div>
          <div>
            <iframe width="100%" height="300"  
                src="{{ $patta_videos[0]->embed_url }}" frameborder="0" allowfullscreen>
            </iframe>
          </div>
          <div class="rel-vi">
            @foreach($patta_videos  as $video_key => $video)
            @if($video_key !== 0)
            <div>
              <a href="">
              @if($video->thumbnail_type == "image")
                  <a target="_blank" href="{{ URL::to('/video-dp/'.$video->id) }}">
                      <img alt="image" class="img" width="125px" height="100px" 
                          src="{{ url('/uploads/video/'.$video->thumb) }}"/>
                  </a>
              @else
                  <a target="_blank" href="{{ URL::to('/video-dp/'.$video->id) }}">
                      <img alt="image" class="img" width="125px" height="100px" 
                              src="{{ url($video->thumbnail) }}"/>
                  </a>
              @endif
                <!-- <img src="{{ url('/assets/img/venasa.jpg') }}"> -->
                <?php $s_patta_videos = substr(strip_tags($video->title), 0, 35);?>
                <p><a href="{{ URL::to('/video-dp/'.$video->id) }}">
                {!! (strrpos($s_patta_videos, ' '))? substr($s_patta_videos, 0, strrpos($s_patta_videos, ' ')) : $s_patta_videos !!}
                </a></p>
              </a>
            </div>
            @endif
            @endforeach

          </div>
          <a href="{{ URL::to('/video-lp/') }}">
            <div class="see-mor">
             See More Videos
            </div>
          </a>
        </div>
        @endif
        <div class="clear"></div>
        <!-- end patta video -->

        <!-- recent video -->
        @if(isset($latest_videos) && count($latest_videos) > 0)
        <div class="resent-v">
          <div class="res-top-br">
            <div class="rec-f">
              Recent Videos
            </div>
            <div class="rec-s">
              <a href="{{ URL::to('/video-lp/') }}">
                <img src="{{ url('/assets/img/brows.png') }}"><br>
                Browse
              </a>
            </div>
          </div>
         
          <div>
            <iframe width="100%" height="300" src="{{ $latest_videos[0]->embed_url }}" frameborder="0" allowfullscreen></iframe>
          </div>
          <div class="rel-vi">
            @foreach($latest_videos  as $latest_key => $latest_video)
              @if($latest_key !== 0)
              <div>
                <a href="">
                  @if($latest_video->thumbnail_type == "image")
                      <a target="_blank" href="{{ URL::to('/video-dp/'.$latest_video->id) }}">
                          <img alt="image" class="img" width="125px" height="100px" 
                              src="{{ url('/uploads/video/'.$latest_video->thumb) }}"/>
                      </a>
                  @else
                      <a target="_blank" href="{{ URL::to('/video-dp/'.$latest_video->id) }}">
                          <img alt="image" class="img" width="125px" height="100px" 
                                  src="{{ url($latest_video->thumbnail) }}"/>
                      </a>
                  @endif
                   <?php $s_recent_video = substr(strip_tags($latest_video->title), 0, 35);?>
                  
                  <p><a href="{{ URL::to('/video-dp/'.$latest_video->id) }}"> {!! (strrpos($s_recent_video, ' '))? substr($s_recent_video, 0, strrpos($s_recent_video, ' ')) : $s_recent_video !!}</a></p>
                </a>
              </div>
              @endif
            @endforeach
            
          </div>
          <a href="{{ URL::to('/video-lp/') }}">
            <div class="see-mor">
             See More Videos
            </div>
          </a>
        </div>
        @endif
        <div class="clear"></div>
        <!-- end recent video -->

        <!-- Gossip -->
        @if(isset($gossip_zone) && count($gossip_zone) > 0) 
        <div>
          <div class="gosip-bc">
          
          </div>
          <div class="gosip-con">
            <div class="col-60">
              <div class="relative-gs">
                <div class="gos-img">              
                  <a href="{{ URL::to('/news/'.$gossip_zone[0]->id) }}">
                    <img src="{{ url('/uploads/news/'.$gossip_zone[0]->thumb) }}">
                  </a>
                </div>
                <div class="gos-cmt">              
                    <div class="l-red">
                      <a href="#">
                        <img src="{{ url('/assets/img/cmt.png') }}">
                      </a>
                    </div>
                 
                    <div class="d-red">
                       <a href="{{ URL::to('/news/'.$gossip_zone[0]->id.'/#cmnet_display_box') }}">{{ $gossip_zone[0]->comments_count }}</a>
                    </div>
                </div>
                <div class="goisp-con">
                  <h4>
                    <a href="{{ URL::to('/news/'.$gossip_zone[0]->id) }}">
                    {{ $gossip_zone[0]->title }}
                    </a>
                  </h4>
                  <?php $s_gossip_zone = substr(strip_tags($gossip_zone[0]->body), 0, 120);?>
                                   
                  <p>{!! (strrpos($s_gossip_zone, ' '))? substr($s_gossip_zone, 0, strrpos($s_gossip_zone, ' ')) : $s_gossip_zone !!}</p>
                  <div class="gsip-t">
                    <img src="{{ url('/assets/img/cl-s.png') }}">
                    <p>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($gossip_zone[0]->created_at))->diffForHumans() }}</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-40">
              <div class="mor-gosip">
                More Gossips
              </div>
              @foreach($gossip_zone  as $key => $gossip)
              @if($key !== 0)
                <div class="g-ln">
                  <div class="g-ln-pic">
                     <a href="{{ URL::to('/news/'.$gossip->id) }}">
                            <img src="{{ url('/uploads/news/'.$gossip->thumb) }}">
                      </a>
                  </div>
                  <div class="in-con">
                    <div class="g-ln-hp">
                     <?php $s_gossip_title = substr(strip_tags($gossip->title), 0, 60);?>
                      <h6><a href="{{ URL::to('/news/'.$gossip->id) }}">
                      {!! (strrpos($s_gossip_title, ' '))? substr($s_gossip_title, 0, strrpos($s_gossip_title, ' ')) : $s_gossip_zone !!}
                      </a></h6>
                    </div>
                    <div class="ln-min-com">
                      <div class="g-ln-tim">
                        <img src="{{ url('/assets/img/cl-s.png') }}">
                        <p>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($gossip->created_at))->diffForHumans() }}</p>
                      </div>
                    </div>
                  </div>
                </div>
              @endif
              @endforeach

              <!-- <div class="g-ln">
                <div class="g-ln-pic">
                   <a href="">
                   <img src="{{ url('/assets/img/face.jpg') }}">
                  </a>
                </div>
                <div class="in-con">
                  <div class="g-ln-hp">
                    <h6>Duis aute irure dolor in reprehen</h6>
                  </div>
                  <div class="ln-min-com">
                    <div class="g-ln-tim">
                      <img src="{{ url('/assets/img/cl-s.png') }}">
                      <p>times difference</p>
                    </div>
                  </div>
                </div>
              </div> -->
            </div>
          </div>
        </div>
        @endif
        <!-- end Gossip -->


      </div>
    </div> <!-- end row -->

    <div class="row rpad addtisment-r">

        <div class="col-40 max-ad">
          @if(isset($home_adds['Add-3']) && count($home_adds['Add-3']) > 0)
          <div class="add-3">
            <a  target="_blank"  href="{{ $home_adds['Add-3'][0]->extra }}">
              @if(isset($home_adds['Add-3'][0]->thumb_resize))
              <img src="{{ url('/uploads/classifieds/adds/resize_image/'.$home_adds['Add-3'][0]->thumb_resize) }}">
              {{ Counter::count('adds_count', $home_adds['Add-3'][0]->id) }}
              @else
              <img src="{{ url('/assets/img/add-3.jpg') }}">
              @endif
            </a>
          </div>
          @endif
        </div>

        <div class="col-60 max-ad">
          <!-- Pathtara mailli -->
          @if(isset($home_adds['Add-4']) && count($home_adds['Add-4']) > 0)
          <div class="pathtara">
            <a  target="_blank"  href="{{ $home_adds['Add-4'][0]->extra }}">
              @if(isset($home_adds['Add-4'][0]->thumb_resize))
              <img src="{{ url('/uploads/classifieds/adds/resize_image/'.$home_adds['Add-4'][0]->thumb_resize) }}">
               {{ Counter::count('adds_count', $home_adds['Add-4'][0]->id) }}
              @else
              <img src="{{ url('/assets/img/pathtara-malli.jpg') }}">
              @endif
            </a>
          </div>
          @endif
        <!-- Pathtara mailli -->
        </div>

    </div>

    <!-- Featured Product -->
    @if(isset($classified_featured) && count($classified_featured) > 0)
    <div class="row rpad fe-row">
      @foreach($classified_featured  as $key => $classified)
      {{ Counter::count('classified_featured', $classified->id) }}
        <div class="featured">
        <div class="">
          <div class="ln-div">
            <div class="it-re"></div>
            <div class="it-rs">Featured  Items</div>
          </div>
          <div class="fic-i">
              <a href="{{ URL::to('/classified/'.$classified->slug) }}">
                <img width="140px" height="115px"  src="{{ url('/uploads/classifieds/'.$classified->parentid.'/'.$classified->thumb) }}">
              </a>
            <div>
              <p><a href="{{ URL::to('/classified/'.$classified->slug) }}">{{ $classified->title }}</a></p>
            </div>
          </div>
        </div>
        <div>
          <div class="rs-div">
            <div class="rs-re"></div>
            <div class="rs-rs"> Rs {{ $classified->price }}</div>
            <a href="{{ URL::to('/classified/'.$classified->slug) }}">More</a>
          </div>
        </div>
      </div>
      @endforeach
    </div>
    @endif
    <!-- Featured Product -->

    <div class="row rpad addtisment-r">
     
        <div class="col-40 max-ad">
          @if(isset($home_adds['Add-3']) && count($home_adds['Add-3']) > 1)
          <div class="add-3">
            <a  target="_blank"  href="{{ $home_adds['Add-3'][1]->extra }}">
              @if(isset($home_adds['Add-3'][1]->thumb_resize))
              <img src="{{ url('/uploads/classifieds/adds/resize_image/'.$home_adds['Add-3'][1]->thumb_resize) }}">
              {{ Counter::count('adds_count', $home_adds['Add-3'][1]->id) }}
              @else
              <img src="{{ url('/assets/img/add-3.jpg') }}">
              @endif
            </a>
          </div>
          @endif
        </div>

        <div class="col-60 max-ad">
          <!-- Pathtara mailli -->
          @if(isset($home_adds['Add-4']) && count($home_adds['Add-4']) > 1)
          <div class="pathtara">
            <a  target="_blank"  href="{{ $home_adds['Add-4'][1]->extra }}">
              @if(isset($home_adds['Add-4'][1]->thumb_resize))
              <img src="{{ url('/uploads/classifieds/adds/resize_image/'.$home_adds['Add-4'][1]->thumb_resize) }}">
              {{ Counter::count('adds_count', $home_adds['Add-4'][1]->id) }}
              @else
              <img src="{{ url('/assets/img/pathtara-malli.jpg') }}">
              @endif
            </a>
          </div>
          @endif
        <!-- Pathtara mailli -->
        </div>

    </div>
</div>
 <div class="container">
 <div class="panel-group" id="accordion">
 <?php  $panel_values = array_rand($news_by_category, 3); ?>
  @if(isset($news_by_category) && count($news_by_category) > 0)
    <div class="panel panel-default col-32 pa-l">
      <div class="panel-heading">
          <a class="pusm collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse1">
          
        <div class="ln-div">
         <div class="bt-re"></div>
         <div class="bt-rs he">{{ $news_by_category[$panel_values[0]]['title'] }}</div>
         <div class="he-lin pus"></div>
        </div></a>
      </div>
      <div id="collapse1" class="panel-collapse collapse in">
        @foreach($news_by_category[$panel_values[0]]['news_list']  as $news_arrcol_one)
        <div class="ln">
              <div class="he-pic">
                 <a href="{{ URL::to('/news/'.$news_arrcol_one->id) }}">
                 <img width="96px" height="73px" src="{{ url('/uploads/news/'.$news_arrcol_one->thumb) }}">
                </a>
              </div>
              <div class="in-con">
                <div class="he-hp">            
                  <p><a href="{{ URL::to('/news/'.$news_arrcol_one->id) }}">{{ $news_arrcol_one->title }}</a></p>
                  <h6>{{ Carbon\Carbon::parse($news_arrcol_one->created_at)->format('F d Y') }}</h6>
                </div>
              </div>
        </div>
        @endforeach
        
        
      </div>
    </div>
    <div class="panel panel-default col-32 pa-b">
      <div class="panel-heading">
          <a class="pusm collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse2">
          <div class="ln-div">
        <div class="bt-re in-w"></div>
         <div class="bt-rs in">{{ $news_by_category[$panel_values[1]]['title'] }}</div>
         <div class="in-lin pus"></div>
         </div></a>
      </div>
      <div id="collapse2" class="panel-collapse collapse">
        @foreach($news_by_category[$panel_values[1]]['news_list']  as $news_arrcol_second)
        <div class="ln">
              <div class="he-pic">
                 <a href="{{ URL::to('/news/'.$news_arrcol_second->id) }}">
                 <img width="96px" height="73px" src="{{ url('/uploads/news/'.$news_arrcol_second->thumb) }}">
                </a>
              </div>
              <div class="in-con">
                <div class="he-hp">            
                  <p><a href="{{ URL::to('/news/'.$news_arrcol_second->id) }}">{{ $news_arrcol_second->title }}</a></p>
                  <h6>{{ Carbon\Carbon::parse($news_arrcol_second->created_at)->format('F d Y') }}</h6>
                </div>
              </div>
        </div>
        @endforeach
      </div>
    </div>
    <div class="panel panel-default col-32 pa-r">
      <div class="panel-heading">
          <a class="pusm collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse3">
          <div class="ln-div">
          <div class="bt-re in-w"></div>
         <div class="bt-rs spo">{{ $news_by_category[$panel_values[2]]['title'] }}</div>
         <div class="spo-lin pus"></div>
         </div></a>
      </div>
      <div id="collapse3" class="panel-collapse collapse">
        @foreach($news_by_category[$panel_values[2]]['news_list']  as $news_arrcol_third)
        <div class="ln">
              <div class="he-pic">
                 <a href="{{ URL::to('/news/'.$news_arrcol_third->id) }}">
                 <img width="96px" height="73px" src="{{ url('/uploads/news/'.$news_arrcol_third->thumb) }}">
                </a>
              </div>
              <div class="in-con">
                <div class="he-hp">            
                  <p><a href="{{ URL::to('/news/'.$news_arrcol_third->id) }}">{{ $news_arrcol_third->title }}</a></p>
                  <h6>{{ Carbon\Carbon::parse($news_arrcol_third->created_at)->format('F d Y') }}</h6>
                </div>
              </div>
        </div>
        @endforeach
      </div>
    </div>
  @endif
</div> 
</div>

@endsection

