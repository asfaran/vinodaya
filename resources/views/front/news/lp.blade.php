<?php
define('PAGE_PARENT', 'news', true);
define('PAGE_CURRENT', 'news_list', true);
?>
@extends('front.app')

@section('title', 'News List')

@section('content')
<div class="container">

    <div class="clear nomal"></div>
    <div class="reletive">
        <div class="ql vi-ld">
                <span  data-toggle="modal" data-target="#myModal">
                    <img src="{{ url('/assets/img/ql.png') }}">
                </span>
        </div>
    </div>
    <div class="row row-15in">

        <div class="col-75 max-vid">

            <div class="bred-cm">
                <p>Home <span>></span> News</p>
            </div>
            <div class="top-hed">
                News
            </div>

            <div class="top-pad">
                <div class="f-news">
                    <div class="les-slider">
                        @if(isset($news_featured) && count($news_featured) > 0)
                        <?php $slider_count = 0; $slider_content_count = 0; ?>
                          <div id="slider" class="nivoSlider" style="height: 415px;width: 554px;">
                          @foreach($news_featured  as $slide_news)                     
                                @if(isset($slide_news->thumb))
                                <div class="slide slide_{{ $slider_count }}">
                                  <img class="slide-image" src="{{ url('/uploads/news/'.$slide_news->thumb) }}" height="100%" width="100%" title="#htmlcaption_{{ $slider_count }}">
                                </div>
                                @else
                                <div class="slide slide_{{ $slider_count }}">
                                  <img class="slide-image" src="{{ url('/assets/img/picture-not-available.jpg') }}" height="100%" width="100%" title="#htmlcaption_{{ $slider_count }}">
                                </div>
                                @endif
                          <?php $slider_count++; ?>
                          @endforeach
                          </div>
                          @foreach($news_featured  as $slide_content)
                              <div id="htmlcaption_{{ $slider_content_count }}" class="nivo-html-caption">
                                  <h1 class="slide-h1">
                                    <a href="{{ URL::to('/news/'.$slide_content->id) }}">
                                    <?php $s_slider = substr(strip_tags($slide_content->title), 0, 45);?>
                                    {!! (strrpos($s_slider, ' '))? substr($s_slider, 0, strrpos($s_slider, ' ')) : $s_slider !!}
                                    </a>
                                  </h1>
                                  <div class="slide-h2"><img src="{{ url('/assets/img/cl-s.png') }}"> 
                                  {{ \Carbon\Carbon::createFromTimeStamp(strtotime($slide_content->created_at))->diffForHumans() }}
                                  </div>

                              </div>
                          <?php $slider_content_count++; ?>
                          @endforeach

                          <a href="#">
                              <div class="ls-btn">
                                  Featured
                              </div>
                          </a>
                        @endif
                    </div>

                </div>

                <div class="col-33-5">
                    <?php /**/ ?>
                    @if($page_adds &&  isset($page_adds['Add-11']) && $page_adds['Add-12'] !== null)
                    <div class="lis-add">
                        <a href="{{ $page_adds['Add-11'][$adds_row]->extra }}">
                          @if(isset($page_adds['Add-11'][$adds_row]->thumb_resize))
                          <img src="{{ url('/uploads/classifieds/adds/resize_image/'.$page_adds['Add-11'][$adds_row]->thumb_resize) }}">
                          @else
                          <img src="{{ url('/assets/img/add-4.jpg') }}">
                          @endif
                        </a>
                    </div>
                    @else
                    <div class="add-im">
                        <a href="">
                            <img src="{{ url('/assets/img/add-4.jpg') }}">
                        </a>
                    </div>
                    @endif
                    @if($page_adds &&  isset($page_adds['Add-12']) && $page_adds['Add-12'] !== null)
                    <div class="lis-add">
                        <a href="{{ $page_adds['Add-12'][$adds_row]->extra }}">
                          @if(isset($page_adds['Add-12'][$adds_row]->thumb_resize))
                          <img src="{{ url('/uploads/classifieds/adds/resize_image/'.$page_adds['Add-12'][$adds_row]->thumb_resize) }}">
                          @else
                          <img src="{{ url('/assets/img/add-2.jpg') }}">
                          @endif
                        </a>
                    </div>
                    @else
                    <div class="add-im">
                        <a href="">
                            <img src="{{ url('/assets/img/add-2.jpg') }}">
                        </a>
                    </div>
                    @endif
                
                </div>           
            </div>

            <div class="clear"></div>

            <div class="top-lis">
                <div class="las-tp">latest</div>
                <div class="las-tp-lin"></div>
            </div>

            @if(isset($news_list) && count($news_list) > 0)
            <?php $adds_count = ceil((count($news_list))/2); $adds_row = 0;?>
                @foreach($news_list  as $news_row)

            <div class="lis-news">
                <div class="lis-ad w28">
                    <a href="{{ URL::to('/news/'.$news_row->id) }}">
                        <img height="200px" width="242px" src="{{ url('/uploads/news/'.$news_row->thumb) }}">
                    </a>
                </div>
                <div class="lis-ad w72">
                    <div class="lis-tx">
                        <h3>
                            <a href="{{ URL::to('/news/'.$news_row->id) }}">
                            <?php $s_title = substr(strip_tags($news_row->title), 0, 45);?>
                            {!! (strrpos($s_title, ' '))? substr($s_title, 0, strrpos($s_title, ' ')) : $s_title !!}
                            </a>
                        </h3>
                        <p>
                            <?php $s_body = substr(strip_tags($news_row->body), 0, 165);?>
                            {!! (strrpos($s_body, ' '))? substr($s_body, 0, strrpos($s_body, ' ')) : $s_body !!}..
                        </p>
                    </div>
                    <div class="lis-rw">
                        <div class="ln-min-com pull-left">
                            <div class="ln-tim mr-p">
                                <img src="{{ url('/assets/img/time.png') }}">
                                <p>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($news_row->created_at))->diffForHumans() }}</p>
                            </div>
                            <div class="ln-com mr-p">
                                <img src="{{ url('/assets/img/coment.png') }}">
                                <p>Comments</p>
                            </div>
                        </div>
                        <div class="lis-views pull-right">
                            <span>{{ Counter::show('news', $news_row->id) }}</span>views
                        </div>
                    </div>
                </div>
            </div>

            @if($page_adds &&  isset($page_adds['Add-8']))
            @if(count($page_adds['Add-8']) > $adds_row )
            <div class="lis-add">
              <a  target="_blank"  href="{{ $page_adds['Add-8'][$adds_row]->extra }}">
                  @if(isset($page_adds['Add-8'][$adds_row]->thumb_resize))
                  <img src="{{ url('/uploads/classifieds/adds/resize_image/'.$page_adds['Add-8'][$adds_row]->thumb_resize) }}">
                  {{ Counter::count('adds_count', $page_adds['Add-8'][$adds_row]->id) }}
                  @else
                  <img src="{{ url('/assets/img/banner.jpg') }}">
                  @endif
              </a>
            </div>
            @endif
            @endif
            <?php $adds_row++;?>
            @endforeach
            @endif
            <div class="pagi casi-pgi">
                {!!  $news_list->appends(array('Search' => Input::get('Search'),'sorting' => Input::get('sorting')))->render() !!}
            </div>
        </div>


        <div class="col-25 max-vid">

           
            <!-- Latest News -->                                             
            @if(isset($latest_news) && count($latest_news) > 0)
            <div class="ln-main2 vid-toppad">
                <div class="n-top">
                    <img src="{{ url('/assets/img/n-top.png') }}">
                    <p>Latest News</p>
                </div>
                @foreach($latest_news  as $latest)
                <div class="ni-pad">
                    <div class="ln-pic li-s">
                        <a href="{{ URL::to('/news/'.$latest->id) }}">
                            <img src="{{ url('/uploads/news/'.$latest->thumb) }}">
                        </a>
                    </div>
                    <div class="in-con">
                        <div class="ln-hp lis-txt">

                            <p>{{ $latest->title }}...</p>
                        </div>
                        <div class="ln-min-com">
                            <div class="ln-tim">
                                <img src="{{ url('/assets/img/time.png') }}">
                                <p>{{ $latest->updated_at }}</p>
                            </div>

                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @endif
            <!-- end Latest News -->

            <!-- Gossip -->
            @if(isset($gossip_zone) && count($gossip_zone) > 0)         
            <div class="ln-main2 vid-gsp">
                <div class="niws-gosip-bc">

                </div>
                @foreach($gossip_zone  as $gossip)
                <div class="ni-pad">
                    <div class="ln-pic li-s">
                        <a href="{{ URL::to('/news/'.$gossip->id) }}">
                            <img src="{{ url('/uploads/news/'.$gossip->thumb) }}">
                        </a>
                    </div>
                    <div class="in-con">
                        <div class="ln-hp lis-txt">

                            <p>{{ $gossip->title }}...</p>
                        </div>
                        <div class="ln-min-com">
                            <div class="ln-tim">
                                <img src="{{ url('/assets/img/time.png') }}">
                                <p>{{ $gossip->updated_at }}</p>
                            </div>

                        </div>
                    </div>
                </div>
                @endforeach

            </div>
            @endif
            <!-- end Gossip -->
            
            <!-- Featured Items -->
            @if(isset($classified_featured) && count($classified_featured) > 0)
            <div class="iapd-vidhaf">
                <div class="ln-div mar-6">
                    <div class="it-re"></div>
                    <div class="it-rs">Featured Items</div>
                </div>

                <div class="wit mar-6">
                    @foreach($classified_featured  as $key => $classified)
                    {{ Counter::count('classified_featured', $classified->id) }}
                    <div class="nlis-featured">
                        <div class="">
                            <div class="fic-i">
                                <img width="140px" height="155px"  src="{{ url('/uploads/classifieds/'.$classified->parentid.'/'.$classified->thumb) }}">
                                <div>
                                    <p>{{ $classified->title }}</p>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="rs-div">
                                <div class="rs-re"></div>
                                <div class="rs-rs"> Rs {{ $classified->price }}</div>
                                <a href="{{ URL::to('/classified/'.$classified->slug) }}">More</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            @endif
            <!-- end Featured Items -->

        </div>
        <!-- end Latest News -->

    </div>

</div>
@include('front.quick_links')

@endsection