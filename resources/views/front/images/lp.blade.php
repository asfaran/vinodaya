<?php
define('PAGE_PARENT', 'images', true);
define('PAGE_CURRENT', 'images_list', true);
?>
@extends('front.app')

@section('title', 'Images List')

@section('content')
<div class="container">

    <div class="clear nomal"></div>
    <div class="reletive">
         <div class="ql vi-ld">
                <span  data-toggle="modal" data-target="#myModal">
                    <img src="{{ url('/assets/img/ql.png') }}">
                </span>
            </div>
    </div>
    <div class="row row-15in">

        <div class="col-75 max-vid">

            <div class="bred-cm">
                <p>Home <span>></span> Images</p>
            </div>
            <div class="top-hed">
                Images
            </div>
        <div>
              
                <!-- artical listing -->
                @if(isset($image_list) && count($image_list) > 0)
                <?php $listing_count = 0; $adds_count = ceil((count($image_list))/2); $adds_row = 0;?>
                @foreach($image_list  as $image_row)
                @if($listing_count ==0 || $listing_count % 2 == 0)
                <div class="ove-hid">
                @endif
                    <div class="art-50">
                        <div class="art_one">
                            <a href="{{ URL::to('/images/'.$image_row->id) }}">
                            <img height="250px" width="400px"  src="{{ url('/uploads/images/'.$image_row->thumb) }}">
                            </a>
                            <div class="art-inn">
                                <div class="img-lis">
                                     <h4>
                                         <a href="{{ URL::to('/images/'.$image_row->id) }}">
                                         {{ $image_row->title }}
                                         </a>
                                     </h4> 
                                     <div>
                                         <h4>{{ Counter::show('images', $image_row->id) }}</h4>
                                         <p>views</p>
                                     </div>
                                </div>
                                <div class="ln-min-com">
                                    <div class="ln-tim">
                                        <img src="{{ url('/assets/img/time.png') }}">
                                        <p>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($image_row->created_at))->diffForHumans() }}</p>
                                    </div>                                        
                                </div>
                            </div>
                        </div>
                    </div>
                @if(($listing_count !=0  && $listing_count % 2 !== 0 ) || 
                ($listing_count +1 === count($image_list)) )
                </div>
                <!-- banner image listing -->
                @if($page_adds && count($page_adds) > $adds_row )
                <div class="art-banner">
                  <a  target="_blank"  href="{{ $page_adds[$adds_row]->extra }}">
                      @if(isset($page_adds[$adds_row]->thumb_resize))
                      <img src="{{ url('/uploads/classifieds/adds/resize_image/'.$page_adds[$adds_row]->thumb_resize) }}">
                      {{ Counter::count('adds_count', $page_adds[$adds_row]->id) }}
                      @else
                      <img src="{{ url('/assets/img/banner.jpg') }}">
                      @endif
                  </a>
                </div>
                @endif
                <!--end banner image listing -->
                @endif
                
                
                
                <?php $listing_count++; $adds_row++;?>
                @endforeach
                @endif
                <!-- end image listing -->
            </div>
            <div class="pagi casi-pgi">
                {!!  $image_list->appends(array('Search' => Input::get('Search'),'sorting' => Input::get('sorting')))->render() !!}
            </div>
        </div>


        <div class="col-25 max-vid">

           
            <!-- Latest News -->                                             
            @if(isset($latest_news) && count($latest_news) > 0)
            <div class="ln-main2 vid-toppad">
                <div class="n-top">
                    <img src="{{ url('/assets/img/n-top.png') }}">
                    <p>Latest News</p>
                </div>
                @foreach($latest_news  as $latest)
                <div class="ni-pad">
                    <div class="ln-pic li-s">
                        <a href="{{ URL::to('/news/'.$latest->id) }}">
                            <img src="{{ url('/uploads/news/'.$latest->thumb) }}">
                        </a>
                    </div>
                    <div class="in-con">
                        <div class="ln-hp lis-txt">

                            <p>{{ $latest->title }}...</p>
                        </div>
                        <div class="ln-min-com">
                            <div class="ln-tim">
                                <img src="{{ url('/assets/img/time.png') }}">
                                <p>{{ $latest->updated_at }}</p>
                            </div>

                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @endif
            <!-- end Latest News -->

            <!-- Gossip -->
            @if(isset($gossip_zone) && count($gossip_zone) > 0)         
            <div class="ln-main2 vid-gsp">
                <div class="niws-gosip-bc">

                </div>
                @foreach($gossip_zone  as $gossip)
                <div class="ni-pad">
                    <div class="ln-pic li-s">
                        <a href="{{ URL::to('/news/'.$gossip->id) }}">
                            <img src="{{ url('/uploads/news/'.$gossip->thumb) }}">
                        </a>
                    </div>
                    <div class="in-con">
                        <div class="ln-hp lis-txt">

                            <p>{{ $gossip->title }}...</p>
                        </div>
                        <div class="ln-min-com">
                            <div class="ln-tim">
                                <img src="{{ url('/assets/img/time.png') }}">
                                <p>{{ $gossip->updated_at }}</p>
                            </div>

                        </div>
                    </div>
                </div>
                @endforeach

            </div>
            @endif
            <!-- end Gossip -->
            
            <!-- Featured Items -->
            @if(isset($classified_featured) && count($classified_featured) > 0)
            <div class="iapd-vidhaf">
                <div class="ln-div mar-6">
                    <div class="it-re"></div>
                    <div class="it-rs">Featured Items</div>
                </div>

                <div class="wit mar-6">
                    @foreach($classified_featured  as $key => $classified)
                    {{ Counter::count('classified_featured', $classified->id) }}
                    <div class="nlis-featured">
                        <div class="">
                            <div class="fic-i">
                                <img width="140px" height="155px"  src="{{ url('/uploads/classifieds/'.$classified->parentid.'/'.$classified->thumb) }}">
                                <div>
                                    <p>{{ $classified->title }}</p>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="rs-div">
                                <div class="rs-re"></div>
                                <div class="rs-rs"> Rs {{ $classified->price }}</div>
                                <a href="{{ URL::to('/classified/'.$classified->slug) }}">More</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            @endif
            <!-- end Featured Items -->

        </div>
        <!-- end Latest News -->

    </div>

</div>

@include('front.quick_links')

@endsection