<?php
define('PAGE_PARENT', 'home', true);
define('PAGE_CURRENT', 'home', true);
?>
@extends('front.app')

@section('title', 'Search Listing')

@section('content')
<div class="container">

    <div class="clear nomal"></div>
    <div class="reletive">
        <div class="ql vi-ld">
                <span  data-toggle="modal" data-target="#myModal">
                    <img src="{{ url('/assets/img/ql.png') }}">
                </span>
        </div>
    </div>
    <div class="row row-15in">

        <div class="col-75 max-vid">

            <div class="bred-cm">
                <p>Home <span>
            </div>
            <div class="top-hed">
                Search Listing
            </div>
            <div class="clear"></div>

            <div class="top-lis">
                <div class="las-tp">Search result</div>
                <div class="las-tp-lin"></div>
            </div>

            @if(isset($search_list) && count($search_list) > 0)
            <?php $adds_count = ceil((count($search_list))/2); $adds_row = 0;?>
                @foreach($search_list  as $key => $search_row)

                @if($search_row->return_type === "classified")
                <div class="lis-news">
                    <div class="lis-ad w28">
                        @if($search_row->thumb)
                        <a href="{{ URL::to('/classified/'.$search_row->slug) }}">
                         <img height="200px" width="242px"  src="{{ url('/uploads/classifieds/'.$search_row->parentid.'/'.$search_row->thumb) }}">
                        </a>
                        @else
                         <img height="200px" width="242px"  src="{{ url('/assets/img/dum.jpg') }}">
                        @endif
                    </div>
                    <div class="lis-ad w72">
                        <div class="lis-tx">
                            <h3>
                                <a href="{{ URL::to('/classified/'.$search_row->slug) }}">
                                {{ $search_row->title }}
                                </a>
                            </h3>
                            <p>{{ substr(strip_tags($search_row->description),0,300)  }}..</p>
                        </div>
                        <div class="lis-rw">
                            <div class="ln-min-com pull-left">
                                <div class="ln-tim mr-p">
                                    <img src="{{ url('/assets/img/time.png') }}">
                                    <p>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($search_row->created_at))->diffForHumans() }}</p>
                                </div>
                                <div class="ln-com mr-p">
                                    <img src="{{ url('/assets/img/coment.png') }}">
                                    <p>Comments</p>
                                </div>
                            </div>
                            <div class="lis-views pull-right">
                                <!-- <span>5,000</span>views -->
                            </div>
                        </div>
                    </div>
                </div>

                @elseif($search_row->return_type === "news")
                <div class="lis-news">
                    <div class="lis-ad w28">
                        <a href="{{ URL::to('/news/'.$search_row->id) }}">
                            <img height="200px" width="242px" src="{{ url('/uploads/news/'.$search_row->thumb) }}">
                        </a>
                    </div>
                    <div class="lis-ad w72">
                        <div class="lis-tx">
                            <h3>
                                <a href="{{ URL::to('/news/'.$search_row->id) }}">
                                {{ $search_row->title }}
                                </a>
                            </h3>
                            <p>{{ substr(strip_tags($search_row->body),0,300)  }}..</p>
                        </div>
                        <div class="lis-rw">
                            <div class="ln-min-com pull-left">
                                <div class="ln-tim mr-p">
                                    <img src="{{ url('/assets/img/time.png') }}">
                                    <p>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($search_row->created_at))->diffForHumans() }}</p>
                                </div>
                                <div class="ln-com mr-p">
                                    <img src="{{ url('/assets/img/coment.png') }}">
                                    <p>Comments</p>
                                </div>
                            </div>
                            <div class="lis-views pull-right">
                                <span>{{ Counter::show('news', $search_row->id) }}</span>views
                            </div>
                        </div>
                    </div>
                </div>
                @elseif($search_row->return_type === "article")
                <div class="lis-news">
                    <div class="lis-ad w28">
                        <a href="{{ URL::to('/article/'.$search_row->id) }}">
                            <img height="200px" width="242px" src="{{ url('/uploads/article/'.$search_row->thumb) }}">
                        </a>
                    </div>
                    <div class="lis-ad w72">
                        <div class="lis-tx">
                            <h3>
                                <a href="{{ URL::to('/article/'.$search_row->id) }}">
                                {{ $search_row->title }}
                                </a>
                            </h3>
                            <p>{{ substr(strip_tags($search_row->body),0,300)  }}..</p>
                        </div>
                        <div class="lis-rw">
                            <div class="ln-min-com pull-left">
                                <div class="ln-tim mr-p">
                                    <img src="{{ url('/assets/img/time.png') }}">
                                    <p>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($search_row->created_at))->diffForHumans() }}</p>
                                </div>
                                <div class="ln-com mr-p">
                                    <img src="{{ url('/assets/img/coment.png') }}">
                                    <p>Comments</p>
                                </div>
                            </div>
                            <div class="lis-views pull-right">
                                <span>{{ Counter::show('article', $search_row->id) }}</span>views
                            </div>
                        </div>
                    </div>
                </div>

                @elseif($search_row->return_type === "image")
                <div class="lis-news">
                    <div class="lis-ad w28">
                        <a href="{{ URL::to('/images/'.$search_row->id) }}">
                            <img height="200px" width="242px" src="{{ url('/uploads/images/'.$search_row->thumb) }}">
                        </a>
                    </div>
                    <div class="lis-ad w72">
                        <div class="lis-tx">
                            <h3>
                                <a href="{{ URL::to('/images/'.$search_row->id) }}">
                                {{ $search_row->title }}
                                </a>
                            </h3>
                            <p>{{ substr(strip_tags($search_row->body),0,300)  }}..</p>
                        </div>
                        <div class="lis-rw">
                            <div class="ln-min-com pull-left">
                                <div class="ln-tim mr-p">
                                    <img src="{{ url('/assets/img/time.png') }}">
                                    <p>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($search_row->created_at))->diffForHumans() }}</p>
                                </div>
                                <div class="ln-com mr-p">
                                    <img src="{{ url('/assets/img/coment.png') }}">
                                    <p>Comments</p>
                                </div>
                            </div>
                            <div class="lis-views pull-right">
                                <span>{{ Counter::show('images', $search_row->id) }}</span>views
                            </div>
                        </div>
                    </div>
                </div>
                @elseif($search_row->return_type === "video")
                <div class="lis-news">
                    <div class="lis-ad w28">
                        <a href="{{ URL::to('/video-dp/'.$search_row->id) }}">
                            <img height="200px" width="242px" src="{{ url('/uploads/video/'.$search_row->thumb) }}">
                        </a>
                    </div>
                    <div class="lis-ad w72">
                        <div class="lis-tx">
                            <h3>
                                <a href="{{ URL::to('/video-dp/'.$search_row->id) }}">
                                {{ $search_row->title }}
                                </a>
                            </h3>
                            <p>{{ substr(strip_tags($search_row->body),0,300)  }}..</p>
                        </div>
                        <div class="lis-rw">
                            <div class="ln-min-com pull-left">
                                <div class="ln-tim mr-p">
                                    <img src="{{ url('/assets/img/time.png') }}">
                                    <p>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($search_row->created_at))->diffForHumans() }}</p>
                                </div>
                                <div class="ln-com mr-p">
                                    <img src="{{ url('/assets/img/coment.png') }}">
                                    <p>Comments</p>
                                </div>
                            </div>
                            <div class="lis-views pull-right">
                                <span>{{ Counter::show('video', $search_row->id) }}</span>views
                            </div>
                        </div>
                    </div>
                </div>
                @endif

            @if($page_adds &&  isset($page_adds['Add-8']))
            @if(count($page_adds['Add-8']) > $adds_row )
            <div class="lis-add">
              <a  target="_blank"  href="{{ $page_adds['Add-8'][$adds_row]->extra }}">
                  @if(isset($page_adds['Add-8'][$adds_row]->thumb_resize))
                  <img src="{{ url('/uploads/classifieds/adds/resize_image/'.$page_adds['Add-8'][$adds_row]->thumb_resize) }}">
                  {{ Counter::count('adds_count', $page_adds['Add-8'][$adds_row]->id) }}
                  @else
                  <img src="{{ url('/assets/img/banner.jpg') }}">
                  @endif
              </a>
            </div>
            @endif
            @endif
            <?php $adds_row++;?>
            @endforeach
            @else
            <div class="lis-news">
              No Records for search result.
            </div>
            @endif
            <?php /*<div class="pagi casi-pgi">
                {!!  $search_list->appends(array('Search' => Input::get('Search'),'sorting' => Input::get('sorting')))->render() !!}
            </div>*/ ?>
        </div>


        <div class="col-25 max-vid">

           
            <!-- Latest News -->                                             
            @if(isset($latest_news) && count($latest_news) > 0)
            <div class="ln-main2 vid-toppad">
                <div class="n-top">
                    <img src="{{ url('/assets/img/n-top.png') }}">
                    <p>Latest News</p>
                </div>
                @foreach($latest_news  as $latest)
                <div class="ni-pad">
                    <div class="ln-pic li-s">
                        <a href="{{ URL::to('/news/'.$latest->id) }}">
                            <img src="{{ url('/uploads/news/'.$latest->thumb) }}">
                        </a>
                    </div>
                    <div class="in-con">
                        <div class="ln-hp lis-txt">

                            <p>{{ $latest->title }}...</p>
                        </div>
                        <div class="ln-min-com">
                            <div class="ln-tim">
                                <img src="{{ url('/assets/img/time.png') }}">
                                <p>{{ $latest->updated_at }}</p>
                            </div>

                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @endif
            <!-- end Latest News -->

            <!-- Gossip -->
            @if(isset($gossip_zone) && count($gossip_zone) > 0)         
            <div class="ln-main2 vid-gsp">
                <div class="niws-gosip-bc">

                </div>
                @foreach($gossip_zone  as $gossip)
                <div class="ni-pad">
                    <div class="ln-pic li-s">
                        <a href="{{ URL::to('/news/'.$gossip->id) }}">
                            <img src="{{ url('/uploads/news/'.$gossip->thumb) }}">
                        </a>
                    </div>
                    <div class="in-con">
                        <div class="ln-hp lis-txt">

                            <p>{{ $gossip->title }}...</p>
                        </div>
                        <div class="ln-min-com">
                            <div class="ln-tim">
                                <img src="{{ url('/assets/img/time.png') }}">
                                <p>{{ $gossip->updated_at }}</p>
                            </div>

                        </div>
                    </div>
                </div>
                @endforeach

            </div>
            @endif
            <!-- end Gossip -->
            
            <!-- Featured Items -->
            @if(isset($classified_featured) && count($classified_featured) > 0)
            <div class="iapd-vidhaf">
                <div class="ln-div mar-6">
                    <div class="it-re"></div>
                    <div class="it-rs">Featured Items</div>
                </div>

                <div class="wit mar-6">
                    @foreach($classified_featured  as $key => $classified)
                    {{ Counter::count('classified_featured', $classified->id) }}
                    <div class="nlis-featured">
                        <div class="">
                            <div class="fic-i">
                                <img width="140px" height="155px"  src="{{ url('/uploads/classifieds/'.$classified->parentid.'/'.$classified->thumb) }}">
                                <div>
                                    <p>{{ $classified->title }}</p>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="rs-div">
                                <div class="rs-re"></div>
                                <div class="rs-rs"> Rs {{ $classified->price }}</div>
                                <a href="{{ URL::to('/classified/'.$classified->slug) }}">More</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            @endif
            <!-- end Featured Items -->

        </div>
        <!-- end Latest News -->

    </div>

</div>

@include('front.quick_links')

@endsection