<?php
define('PAGE_PARENT', 'articles', true);
define('PAGE_CURRENT', 'articles_list', true);
?>
@extends('front.app')

@section('title', $article_one->title)

@section('content')
<div class="container">

    <div class="clear nomal"></div>
    <div class="reletive">
         <div class="ql vi-ld">
                <span  data-toggle="modal" data-target="#myModal">
                    <img src="{{ url('/assets/img/ql.png') }}">
                </span>
            </div>
    </div>

    <div class="row row-15in">

        <div class="col-75 max-vid">

            <div class="bred-cm">
                <p>Home <span>></span> Articles</p>
            </div>

            <div class="d-man">
              <div class="d-img">
                @if($article_one->thumb)
                  <img width="100%"  src="{{ url('/uploads/article/'.$article_one->thumb) }}">
                @else
                  <img src="{{ url('/assets/img/new-1.jpg') }}">
                @endif
              </div>
              <div class="d-di">
                  <h3>{{ $article_one->title }}</h3>
                  <div class="ove-hid">
                    <div class="mobi-dt">
                      <div class="d-tm"> 
                        <img src="{{ url('/assets/img/b-time.png') }}">
                        <p>{{ Carbon\Carbon::parse($article_one->created_at)->format('d F Y') }}</p>
                      </div>
                      <div class="d-tm">
                        <img src="{{ url('/assets/img/anc.png') }}">
                        <p>{!! $category_names !!}</p>
                      </div>
                    </div>
                    <div class="d-cat float-r">
                      <p>Views</p><span>{{ Counter::showAndCount('article', $article_one->id) }}</span>
                    </div>
                  </div>
                  <div class="d-d">
                  <p>{!! $article_one->body !!}

                  </p>
                  </div>
                  <div class="d-sio">
                    <!-- <img src="{{ url('/assets/img/sio-d.jpg') }}"> -->
                    <div class="share-items" data-title="{{ $article_one->title }}" data-hash="{{ $article_one->title }}" data-url="{{ URL::to('/article/'.$article_one->id) }}" >
                      <ul class="share-links">
                        <li>
                          <a class="twitterBtn" data-dir="left" href="" >
                              <i class="fa fa-twitter"></i>&nbsp;
                              <span>Twitter</span>
                              <span class="twitter-count"></span>
                                </a>
                        </li>
                        <li>
                          <a class="facebookBtn" href="">
                              <i class="fa fa-facebook"></i>&nbsp;
                              <span>Facebook</span>
                              <span class="facebook-count"></span>
                                </a>
                        </li>
                        <li>
                          <a class="linkedinBtn" href="">
                              <i class="fa fa-linkedin"></i>&nbsp;
                              <span>LinkedIn</span>
                              <span class="linkedin-count"></span>
                                </a>
                        </li>
                        <li>
                          <a class="googleBtn" href="">
                              <i class="fa fa-google-plus"></i>&nbsp;
                              <span>Google</span>
                              <span class="google-count"></span>
                                </a>
                        </li>
                      </ul>
                    </div>

                  </div>
                  @if($page_adds && isset($page_adds['Add-5']))
                  <div class="d-adln">
                      <a  target="_blank"  href="{{ $page_adds['Add-5']->extra }}">
                          @if(isset($page_adds['Add-5']->thumb_resize))
                          <img src="{{ url('/uploads/classifieds/adds/resize_image/'.$page_adds['Add-5']->thumb_resize) }}">
                          {{ Counter::count('adds_count', $page_adds['Add-5']->id) }}
                          @else
                          <img src="{{ url('/assets/img/d-ad.jpg') }}">
                          @endif
                      </a>
                  </div>
                  @endif

                  @if(isset($Comments_array) && count($Comments_array) > 0)
                  <div class="cmnet" id="cmnet_display_box">
                      <h3>Comments ({{ count($Comments_array) }})</h3>
                      @if(count($Comments_array) > 5)
                      <div class="shw-cmt">
                          <a href="">Show Comments</a>
                      </div>
                      @endif
                      @foreach($Comments_array  as $Comments)
                      <div class="po-cmnt">
                          <div>
                              <h4>{{ $Comments->author }}</h4>
                              <span>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($Comments->created_at))->diffForHumans() }}</span>
                          </div>
                          <p>{{ $Comments->content }}</p>
                      </div>
                      @endforeach
                  </div>
                  @endif

                  <div class="pos-new"   id="post_comments_div">
                      <h3>Post a new comment</h3>
                      @if ( Session::has('flash_message') )
                          <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                                  <button class="close" data-dismiss="alert"></button>
                                  {!! Session::get('flash_message') !!}
                          </div>
                      @endif
                      @if ( Session::has('flash_success') )
                          <div class="alert alert-success  {{ Session::get('flash_type') }}">
                              <button class="close" data-dismiss="alert"></button>
                              {!! Session::get('flash_success') !!}
                          </div>
                      @endif
                      <p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
                    <div class="row">
                        {!! Form::open(array('url'=>'/user/postcomments','role'=>'form', 'files' => true, 'class'=>'form-horizontal', 'id' => 'form_post_comments')) !!}
                         {!! Form::hidden('post_type', 2, array('id' => 'post_type')) !!}
                         {!! Form::hidden('post_id', $article_one->id, array('id' => 'post_id')) !!}
                            <div class="col-md-6 ">
                                <div class="form-grup" >
                                    <label for="author">Name</label>
                                    <input class="form-control" type="text" name="author" id="author" value="{{ Input::old('author')}}">
                                    @if ($errors->has('author'))
                                      <span class="alert-danger">{{ $errors->first('author') }}</span><br>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 ">
                                <div class="form-grup" >
                                    <label for="author_email">Email Address</label>
                                    <input class="form-control" type="text" name="author_email" id="author_email" value="{{ Input::old('author_email')}}">
                                    @if ($errors->has('author_email'))
                                      <span class="alert-danger">{{ $errors->first('author_email') }}</span><br>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12 ">
                                <div class="form-grup" >
                                    <label for="content">Your Comment</label>
                                    <textarea class="form-control" type="text" name="content" id="content">{{ Input::old('content')}}</textarea>
                                    @if ($errors->has('content'))
                                      <span class="alert-danger">{{ $errors->first('content') }}</span><br>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12 ">
                                <div class="sub">
                                    <input class="form-control" type="submit" value="submit Comment" name="">
                                </div>
                            </div>

                        {!! Form::close() !!}
                    </div>
                  </div>
              </div>              
            </div>

            <div class="mr-res-n">
              <h4>More Related Articles</h4>
            </div>
            <div class="rele-nws">
              @if(isset($related_articles) && count($related_articles) > 0)
                @foreach($related_articles  as $articles)
                  <div class="re-s">
                       <a href="{{ URL::to('/article/'.$articles->id) }}">
                        <img width="175px" height="140px" src="{{ url('/uploads/article/'.$articles->thumb) }}">
                        </a>
                      <p><a href="{{ URL::to('/article/'.$articles->id) }}">
                      <?php $s_related = substr(strip_tags($articles->title), 0, 45);?>
                      {!! (strrpos($s_related, ' '))? substr($s_related, 0, strrpos($s_related, ' ')) : $s_related !!}
                      </a></p>
                      <div class="st-time">
                          <img src="{{ url('/assets/img/time.png') }}">
                          <p> {{ $articles->created_at }}</p>
                      </div>
                  </div>
                @endforeach
              @endif
              </div>
            <div>

            </div>

        </div>

        <div class="col-25 max-vid">

            <!-- Featured Items -->
            @if(isset($classified_featured) && count($classified_featured) > 0)
            <div class="iapd-vidhaf">
                <div class="ln-div mar-6">
                    <div class="it-re"></div>
                    <div class="it-rs">Featured Items</div>
                </div>

                <div class="wit mar-6">
                    @foreach($classified_featured  as $key => $classified)
                    {{ Counter::count('classified_featured', $classified->id) }}
                    <div class="nlis-featured">
                        <div class="">
                            <div class="fic-i">
                                <img width="140px" height="155px"  src="{{ url('/uploads/classifieds/'.$classified->parentid.'/'.$classified->thumb) }}">
                                <div>
                                    <p>{{ $classified->title }}</p>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="rs-div">
                                <div class="rs-re"></div>
                                <div class="rs-rs"> Rs {{ $classified->price }}</div>
                                <a href="{{ URL::to('/classified/'.$classified->slug) }}">More</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            @endif
            <!-- end Featured Items -->

            <div class="di-ad">
              @if($page_adds && isset($page_adds['Add-6']))
              <div class="sid-im">
                  <a  target="_blank"  href="{{ $page_adds['Add-6']->extra }}">
                      @if(isset($page_adds['Add-6']->thumb_resize))
                      <img src="{{ url('/uploads/classifieds/adds/resize_image/'.$page_adds['Add-6']->thumb_resize) }}">
                      {{ Counter::count('adds_count', $page_adds['Add-6']->id) }}
                      @else
                      <img src="{{ url('/assets/img/add-4.jpg') }}">
                      @endif
                  </a>
              </div>
              @endif
              @if($page_adds && isset($page_adds['Add-7']))
              <div class="sid-im">
                  <a  target="_blank"  href="{{ $page_adds['Add-7']->extra }}">
                      @if(isset($page_adds['Add-7']->thumb_resize))
                      <img src="{{ url('/uploads/classifieds/adds/resize_image/'.$page_adds['Add-7']->thumb_resize) }}">
                      {{ Counter::count('adds_count', $page_adds['Add-7']->id) }}
                      @else
                      <img src="{{ url('/assets/img/add-2.jpg') }}">
                      @endif
                  </a>
              </div>
              @endif
            </div>

            
        </div>
        <!-- end Latest News -->

    </div>
</div>

@include('front.quick_links')
@endsection