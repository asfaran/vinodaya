<?php
define('PAGE_PARENT', 'articles', true);
define('PAGE_CURRENT', 'articles_list', true);
?>
@extends('front.app')

@section('title', 'Article Listing')

@section('content')
<link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
<div class="container">

    <div class="clear nomal"></div>
    <div class="reletive">
         <div class="ql vi-ld">
                <span  data-toggle="modal" data-target="#myModal">
                    <img src="{{ url('/assets/img/ql.png') }}">
                </span>
          </div>
    </div>
    
    <div class="row row-15in">

        <div class="col-75 max-vid">

            <div class="bred-cm">
                <p>Home <span>></span> Articles</p>
            </div>
            <div class="top-hed">
                Articles
            </div>
            <div>
              <div class="ban-ti">
                  <div>
                      <div class="les-slider">
                      @if(isset($article_featured) && count($article_featured) > 0)
                      <?php $slider_count = 0; $slider_content_count = 0; ?>
                          <div id="slider" class="nivoSlider">
                          @foreach($article_featured  as $slide_article)
                                @if(isset($slide_article->thumb))
                                <div class="slide slide_{{ $slider_count }}">
                                    <img class="slide-image" src="{{ url('/uploads/article/'.$slide_article->thumb) }}" height="375px" width="825px" title="#htmlcaption_{{ $slider_count }}">
                                </div>
                                @else
                                <div class="slide slide_{{ $slider_count }}">
                                    <img class="slide-image" src="{{ url('/assets/img/picture-not-available.jpg') }}" height="375px" width="825px" title="#htmlcaption_{{ $slider_count }}">
                                </div>
                                @endif                        
                          <?php $slider_count++; ?>
                          @endforeach
                          </div>
                          @foreach($article_featured  as $slide_content)
                              <div id="htmlcaption_{{ $slider_content_count }}" class="nivo-html-caption">
                                  <h1 class="slide-h1">
                                    <a href="{{ URL::to('/article/'.$slide_content->id) }}">
                                    <?php $s = substr(strip_tags($slide_content->title), 0, 45);?>
                                    {!! (strrpos($s, ' '))? substr($s, 0, strrpos($s, ' ')) : $s !!}
                                    </a>
                                  </h1>
                                  <div class="slide-h2"><img src="{{ url('/assets/img/cl-s.png') }}"> 
                                  {{ \Carbon\Carbon::createFromTimeStamp(strtotime($slide_content->created_at))->diffForHumans() }}
                                  </div>

                              </div>
                          <?php $slider_content_count++; ?>
                          @endforeach

                          <a href="#">
                              <div class="ls-btn">
                                  Featured
                              </div>
                          </a>
                        @endif
                        </div>
                  </div>

                  <div class="top-lis">
                      <div class="las-tp">latest</div>
                      <div class="las-tp-lin"></div>
                  </div>
              </div>

                <!-- artical listing -->
                @if(isset($article_list) && count($article_list) > 0)
                <?php $listing_count = 0; $adds_count = ceil((count($article_list))/2); $adds_row = 0;?>
                @foreach($article_list  as $article_row)
                @if($listing_count ==0 || $listing_count % 2 == 0)
                <div class="ove-hid">
                @endif
                    <div class="art-50">
                        <div class="art_one">
                            <a href="{{ URL::to('/article/'.$article_row->id) }}">
                                <img height="250px" width="400px" src="{{ url('/uploads/article/'.$article_row->thumb) }}">
                            </a>
                            <div class="art-inn">
                                <h4><a href="{{ URL::to('/article/'.$article_row->id) }}">
                                    <?php $s_list = substr(strip_tags($article_row->title), 0, 45);?>
                                    {!! (strrpos($s_list, ' '))? substr($s_list, 0, strrpos($s_list, ' ')) : $s_list !!}
                                </a></h4>
                                <?php $s_body = substr(strip_tags($article_row->body), 0, 110);?>
                                <p>{!! (strrpos($s_body, ' '))? substr($s_body, 0, strrpos($s_body, ' ')) : $s_body !!}...</p>
                                <div class="ln-min-com">
                                    <div class="ln-tim">
                                        <img src="{{ url('/assets/img/time.png') }}">
                                        <p>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($article_row->created_at))->diffForHumans() }}</p>
                                    </div>
                                    <div class="ln-com">
                                        <img src="{{ url('/assets/img/coment.png') }}">
                                        <p>Comments</p>
                                    </div>
                                    <div class="art-we">
                                        {{ Counter::show('article', $article_row->id) }} <span>views</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @if(count($article_list) === 1 || 
                ($listing_count !== 0  && $listing_count % 2 !== 0) || 
                ($listing_count +1 === count($article_list)) )
                </div>

                @if($page_adds && count($page_adds) > $adds_row )
                <div class="art-banner">
                  <a  target="_blank"  href="{{ $page_adds[$adds_row]->extra }}">
                      @if(isset($page_adds[$adds_row]->thumb_resize))
                      <img src="{{ url('/uploads/classifieds/adds/resize_image/'.$page_adds[$adds_row]->thumb_resize) }}">
                      {{ Counter::count('adds_count', $page_adds[$adds_row]->id) }}
                      @else
                      <img src="{{ url('/assets/img/banner.jpg') }}">
                      @endif
                  </a>
                </div>
                @endif
                
                @endif
                <!-- banenr artical listing -->
                
                <!--end banenr artical listing -->
                <?php $listing_count++; $adds_row++;?>
                @endforeach
                @else
                    <div class="ove-hid">No Records for Selection.</div>
                @endif 
                <!-- end artical listing -->

            </div>
            <div class="pagi casi-pgi">
                {!!  $article_list->appends(array('Search' => Input::get('Search'),'sorting' => Input::get('sorting')))->render() !!}
            </div>
        </div>
        <!-- </div> -->


        <div class="col-25 max-vid">

           
            <!-- Latest News -->                                             
            @if(isset($latest_news) && count($latest_news) > 0)
            <div class="ln-main2 vid-toppad">
                <div class="n-top">
                    <img src="{{ url('/assets/img/n-top.png') }}">
                    <p>Latest News</p>
                </div>
                @foreach($latest_news  as $latest)
                <div class="ni-pad">
                    <div class="ln-pic li-s">
                        <a href="{{ URL::to('/news/'.$latest->id) }}">
                            <img src="{{ url('/uploads/news/'.$latest->thumb) }}">
                        </a>
                    </div>
                    <div class="in-con">
                        <div class="ln-hp lis-txt">

                            <p>{{ $latest->title }}...</p>
                        </div>
                        <div class="ln-min-com">
                            <div class="ln-tim">
                                <img src="{{ url('/assets/img/time.png') }}">
                                <p>{{ $latest->updated_at }}</p>
                            </div>

                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @endif
            <!-- end Latest News -->

            <!-- Gossip -->
            @if(isset($gossip_zone) && count($gossip_zone) > 0)         
            <div class="ln-main2 vid-gsp">
                <div class="niws-gosip-bc">

                </div>
                @foreach($gossip_zone  as $gossip)
                <div class="ni-pad">
                    <div class="ln-pic li-s">
                        <a href="{{ URL::to('/news/'.$gossip->id) }}">
                            <img src="{{ url('/uploads/news/'.$gossip->thumb) }}">
                        </a>
                    </div>
                    <div class="in-con">
                        <div class="ln-hp lis-txt">

                            <p>{{ $gossip->title }}...</p>
                        </div>
                        <div class="ln-min-com">
                            <div class="ln-tim">
                                <img src="{{ url('/assets/img/time.png') }}">
                                <p>{{ $gossip->updated_at }}</p>
                            </div>

                        </div>
                    </div>
                </div>
                @endforeach

            </div>
            @endif
            <!-- end Gossip -->
            
            <!-- Featured Items -->
            @if(isset($classified_featured) && count($classified_featured) > 0)
            <div class="iapd-vidhaf">
                <div class="ln-div mar-6">
                    <div class="it-re"></div>
                    <div class="it-rs">Featured Items</div>
                </div>

                <div class="wit mar-6">
                    @foreach($classified_featured  as $key => $classified)
                    {{ Counter::count('classified_featured', $classified->id) }}
                    <div class="nlis-featured">
                        <div class="">
                            <div class="fic-i">
                                <img width="140px" height="155px"  src="{{ url('/uploads/classifieds/'.$classified->parentid.'/'.$classified->thumb) }}">
                                <div>
                                    <p>{{ $classified->title }}</p>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="rs-div">
                                <div class="rs-re"></div>
                                <div class="rs-rs"> Rs {{ $classified->price }}</div>
                                <a href="{{ URL::to('/classified/'.$classified->slug) }}">More</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            @endif
            <!-- end Featured Items -->

        </div>
        <!-- end Latest News -->





        

    </div>

</div>

@include('front.quick_links')

@endsection

