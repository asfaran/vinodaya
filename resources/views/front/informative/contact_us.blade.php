<?php
define('PAGE_PARENT', 'contact-us'  , true);
define('PAGE_CURRENT', 'contact-us', true);
?>
@extends('front.app')

@section('title','Contact Us')

@section('styles')
    @parent
   <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
   <style>
    #gmap_canvas img{max-width:none!important;background:none!important}
  </style>
@endsection

@section('content')
<div class="wrapper about">

        <!-- Info Contact -->
        <div class="contact-page">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="contact-form">
                            <h3>Contact Us</h3>
                            @if ( Session::has('flash_message') )
                                  <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                                          <button class="close" data-dismiss="alert"></button>
                                          {!! Session::get('flash_message') !!}
                                  </div>
                              @endif
                              @if ( Session::has('flash_success') )
                                  <div class="alert alert-success  {{ Session::get('flash_type') }}">
                                      <button class="close" data-dismiss="alert"></button>
                                      {!! Session::get('flash_success') !!}
                                  </div>
                              @endif
                            <p>{!! (isset($option_list['contact_page_description']))? $option_list['contact_page_description'] : '' !!}</p>
                            {!! Form::open(array('url'=>'/user/messages','role'=>'form', 'class'=>'form-horizontal')) !!}
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-page-contact">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" placeholder="First Name *" name="name" value="{{ Input::old('name')}}">
                                        @if ($errors->has('name'))
                                          <span class="alert-danger">{{ $errors->first('name') }}</span><br>
                                        @endif            
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" placeholder="Your Email *" name="email" value="{{ Input::old('email')}}">
                                        @if ($errors->has('email'))
                                          <span class="alert-danger">{{ $errors->first('email') }}</span><br>
                                        @endif
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" placeholder="Your Phone Number *" name="mobile" value="{{ Input::old('mobile')}}">
                                        @if ($errors->has('mobile'))
                                          <span class="alert-danger">{{ $errors->first('mobile') }}</span><br>
                                        @endif
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" placeholder="Subject *" name="subject" value="{{ Input::old('subject')}}">
                                        @if ($errors->has('subject'))
                                          <span class="alert-danger">{{ $errors->first('subject') }}</span><br>
                                        @endif
                                    </div>
                                    <div class="col-md-12">
                                        <textarea class="message"  name="message" placeholder="Message">{{ Input::old('message')}}</textarea>
                                        @if ($errors->has('message'))
                                          <span class="alert-danger">{{ $errors->first('message') }}</span><br>
                                        @endif
                                        <button class="let-post" type="submit">Post Comment</button>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="contact-map">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="contact-info">
                                        <span><i class="fa fa-map-marker"></i> </span>
                                        <div class="info-title">
                                            <h4>Mailing Address</h4>
                                            <span>{!! (isset($option_list['contact_page_address']))? $option_list['contact_page_address'] : '' !!}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="contact-info">
                                        <span><i class="fa fa-phone"></i> </span>
                                        <div class="info-title">
                                            <h4>Contact Info</h4>
                                            {!! (isset($option_list['contact_page_info']))? $option_list['contact_page_info'] : '' !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="info-map">
                                    <input value="{{(isset($option_list['contact_page_latitude']))? $option_list['contact_page_latitude'] : '6.9271' }}" type="hidden" name="latitude" id="latitude" />
                                    <input value="{{ (isset($option_list['contact_page_longtitude']))? $option_list['contact_page_longtitude'] : '79.8612' }}" type="hidden"  name="longitude" id="longitude" />
                                      <div style='overflow:hidden;height:355px;width:570px;'>
                                        <div id='gmap_canvas' style='height:355px;width:570px;'></div>
                                      </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Info Contact -->

        <!-- End Main -->
    </div>

@endsection
@section('scripts')
    @parent
    <script>
    

    var latlon = latitude + "," + longitude;

    


    /*var status = showPosition();*/

    function initMap() {
        var latitude = document.getElementById("latitude").value;
        var longitude = document.getElementById("longitude").value;
        /*var myLatLng = {lat: latitude, lng: longitude};*/
        var myLatLng = {lat:  parseFloat(document.getElementById("latitude").value), lng: parseFloat(document.getElementById("longitude").value) };


        var map = new google.maps.Map(document.getElementById('gmap_canvas'), {
          zoom: 15,
          center: myLatLng
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'Hello World!'
        });
      }



       /* function myMap() {
        var mapProp= {
            center:new google.maps.LatLng( latitude, longitude),
            zoom:8,
            pointer:true;
        };

        

        var map=new google.maps.Map(document.getElementById(""),mapProp);
        }*/
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?callback=initMap&key=AIzaSyDfqZ_-nv3MWupuJXYkd60YCNgswcGIFTA"></script>
    <script>

    
  /*  var status = showPosition();*/

    /*function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition, showError);
        } else {
            x.innerHTML = "Geolocation is not supported by this browser.";
        }
    }*/

    /*function showPosition() {
        

        var img_url = "https://maps.googleapis.com/maps/api/staticmap?center="+latlon+"&zoom=14&size=400x300&sensor=false";
        document.getElementById("").innerHTML = "<img src='"+img_url+"'>";
    }*/

    /*function showError(error) {
        switch(error.code) {
            case error.PERMISSION_DENIED:
                x.innerHTML = "User denied the request for Geolocation."
                break;
            case error.POSITION_UNAVAILABLE:
                x.innerHTML = "Location information is unavailable."
                break;
            case error.TIMEOUT:
                x.innerHTML = "The request to get user location timed out."
                break;
            case error.UNKNOWN_ERROR:
                x.innerHTML = "An unknown error occurred."
                break;
        }
    }*/
    </script>
    
@endsection