<?php
define('PAGE_PARENT', (isset($menu_data->title))? $menu_data->title : 'informative'  , true);
define('PAGE_CURRENT', (isset($menu_data->title))? $menu_data->title : 'informative', true);
?>
@extends('front.app')

@section('title', (isset($menu_data->label))? $menu_data->label : 'informative')

@section('content')
<dir class="container">
  <dir class="row abou-rw">
    <div class="bredcm">
      <a href="">Home</a><span>></span><a href="">{{ (isset($menu_data->label))? $menu_data->label : 'informative' }}</a>
      <h2>{{ $informativePage->title }}</h2>
    </div>
    
  </dir>
  <dir class="row abou-rw">
    <div class="col-70 max-w white">
      <div class="con-ab">
             <p>{!! $informativePage->body !!}</p>
        <div class="clear"></div>
        <!-- <div class="col-33 abu-d-img">
          <img src="{{ url('/assets/img/in-1.png') }}">
          <div class="abu-i">
            <h4>minimum senserit expete</h4>
            <p>usto assueverit persequeris </p>
          </div>
        </div>
        <div class="col-33 abu-d-img">
          <img src="{{ url('/assets/img/in-2.png') }}">
          <div class="abu-i">
            <h4>minimum senserit expete</h4>
            <p>usto assueverit persequeris </p>
          </div>
        </div>
        <div class="col-33 abu-d-img">
          <img src="{{ url('/assets/img/in-3.png') }}">
          <div class="abu-i">
            <h4>minimum senserit expete</h4>
            <p>usto assueverit persequeris </p>
          </div>
        </div> -->
      </div>
    </div>
    <div class="col-30 max-ad">
        @if($page_adds)

          @foreach($page_adds  as $key => $adds_row)
          @if($adds_row)

          
            <div class="ab-img">
              <a target="_blank"  href="{{ $adds_row->extra }}">
                @if(isset($adds_row->thumb_resize))
                <img src="{{ url('/uploads/classifieds/adds/resize_image/'.$adds_row->thumb_resize) }}">
                {{ Counter::count('adds_count', $adds_row->id) }}
                @else
                <img src="{{ url('/assets/img/in-1.jpg') }}">
                @endif
              </a>
            </div>
          @endif
          @endforeach
        @endif
      </div>
    </dir>
</dir>

<div class="container">
 <div class="panel-group" id="accordion">
 <?php  $panel_values = array_rand($news_by_category, 3); ?>
  @if(isset($news_by_category) && count($news_by_category) > 0)
    <div class="panel panel-default col-32 pa-l">
      <div class="panel-heading">
          <a class="pusm collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse1">
          
        <div class="ln-div">
         <div class="bt-re"></div>
         <div class="bt-rs he">{{ $news_by_category[$panel_values[0]]['title'] }}</div>
         <div class="he-lin pus"></div>
        </div></a>
      </div>
      <div id="collapse1" class="panel-collapse collapse in">
        @foreach($news_by_category[$panel_values[0]]['news_list']  as $news_arrcol_one)
        <div class="ln">
              <div class="he-pic">
                 <a href="">
                 <img width="96px" height="73px" src="{{ url('/uploads/news/'.$news_arrcol_one->thumb) }}">
                </a>
              </div>
              <div class="in-con">
                <div class="he-hp">            
                  <p>{{ $news_arrcol_one->title }}</p>
                  <h6>{{ $news_arrcol_one->created_at }}</h6>
                </div>
              </div>
        </div>
        @endforeach
        
        
      </div>
    </div>
    <div class="panel panel-default col-32 pa-b">
      <div class="panel-heading">
          <a class="pusm collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse2">
          <div class="ln-div">
        <div class="bt-re in-w"></div>
         <div class="bt-rs in">{{ $news_by_category[$panel_values[1]]['title'] }}</div>
         <div class="in-lin pus"></div>
         </div></a>
      </div>
      <div id="collapse2" class="panel-collapse collapse">
        @foreach($news_by_category[$panel_values[1]]['news_list']  as $news_arrcol_second)
        <div class="ln">
              <div class="he-pic">
                 <a href="">
                 <img width="96px" height="73px" src="{{ url('/uploads/news/'.$news_arrcol_second->thumb) }}">
                </a>
              </div>
              <div class="in-con">
                <div class="he-hp">            
                  <p>{{ $news_arrcol_second->title }}</p>
                  <h6>{{ $news_arrcol_second->created_at }}</h6>
                </div>
              </div>
        </div>
        @endforeach
      </div>
    </div>
    <div class="panel panel-default col-32 pa-r">
      <div class="panel-heading">
          <a class="pusm collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse3">
          <div class="ln-div">
          <div class="bt-re in-w"></div>
         <div class="bt-rs spo">{{ $news_by_category[$panel_values[2]]['title'] }}</div>
         <div class="spo-lin pus"></div>
         </div></a>
      </div>
      <div id="collapse3" class="panel-collapse collapse">
        @foreach($news_by_category[$panel_values[2]]['news_list']  as $news_arrcol_third)
        <div class="ln">
              <div class="he-pic">
                 <a href="">
                 <img width="96px" height="73px" src="{{ url('/uploads/news/'.$news_arrcol_third->thumb) }}">
                </a>
              </div>
              <div class="in-con">
                <div class="he-hp">            
                  <p>{{ $news_arrcol_third->title }}</p>
                  <h6>{{ $news_arrcol_third->created_at }}</h6>
                </div>
              </div>
        </div>
        @endforeach
      </div>
    </div>
  @endif
</div> 
</div>

@endsection