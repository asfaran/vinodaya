<?php 
if (!defined('PAGE_PARENT'))
    define('PAGE_PARENT', '');
if (!defined('PAGE_CURRENT'))
    define('PAGE_CURRENT', '');
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" ng-app="inspinia"  data-ng-app="">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <?php

    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
    header('Access-Control-Allow-Headers: Content-Type, X-Auth-Token, Origin, x-xsrf-token, x-csrf-token, DNT,X-Mx-ReqToken,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control');

    ?>
    <meta charset="utf-8"/>
    <title>@yield('title') - Vinodaya</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->

    <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,500,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link href="{{ url('/assets/css/bootstrap.min.css') }}" rel="stylesheet"> 
    

    <link href="{{ url('/assets/front/css/animate.css') }}" rel="stylesheet">
    <link href="{{ url('/assets/front/css/owl.carousel.css') }}" rel="stylesheet">   
    <link href="{{ url('/assets/front/css/bootstrap.css') }}" rel="stylesheet">   
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/front/css/nivo-slider.css') }}">
    <link rel="stylesheet" href="{{ url('assets/front/css/style.css') }}">
    <link href="{{ url('/assets/front/css/ipad.css') }}" rel="stylesheet">
    <link href="{{ url('/assets/front/css/landscap.css') }}" rel="stylesheet">
    <link href="{{ url('/assets/front/css/potreit.css') }}" rel="stylesheet">
    <link href="{{ url('/assets/front/css/flexslider.css') }}" rel="stylesheet" media="screen">

  <!-- Font awesome -->
  <link href="{{ url('/assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <style type="text/css">
      .ticker {
        color:#fff;
        padding: 5px;
      }

     .po-cmnt { display:none; }
    </style>
    @yield('styles')

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<?php /*
 {{ !--
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
-- }}
*/ ?>
{{----}}

<body>
<!-- Navigation -->
@include('front.topnavbar')
<!-- Navigation -->

<!-- Main view  -->
@yield('content')
<!-- Main view  -->

<!-- START FOOTER -->
@include('front.footer')
<!-- END FOOTER -->


<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- jQuery and Bootstrap -->
  <script src="{{ url('/assets/js/jquery/jquery-2.1.1.min.js') }}"></script>
  <script src="{{ url('/assets/front/js/jquery.min.js') }}"></script>
  <script src="{{ url('/assets/js/plugins/jquery-ui/jquery-ui.js') }}"></script>
  <script src="{{ url('/assets/front/js/html5gallery.js') }}"></script>
  <script src="{{ url('/assets/front/js/owl.carousel.js') }}"></script> 
  <!-- <script src="{{ url('/assets/front/js/bootstrap.js') }}"></script>  -->
  <script src="{{ url('/assets/js/bootstrap/bootstrap.min.js') }}"></script>
  <script src="{{ url('/assets/front/js/wow.min.js') }}"></script> 
  <script src="{{ url('/assets/front/js/jquery.nivo.slider.js') }}"></script>
  <script src="{{ url('/assets/front/js/custom.js') }}"></script> 
  <script src="{{ url('/assets/front/js/jquery.flexslider.js') }}"></script>
  <script src="{{ url('/assets/front/js/jquery.jticker.min.js') }}"></script>

  <script src="{{ url('/assets/front/js/bootstrap-checkbox.min.js') }}"></script>
  <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
  <script src="{{ url('/assets/front/js/vinodaya.js') }}"></script>
<!--   <script src="https://code.jquery.com/jquery-1.12.3.min.js" integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ=" crossorigin="anonymous"></script> -->
    <script src="{!! asset('/assets/front/js/customShareCounts.min.js') !!}"></script>
    <script>
        $('.share-items').customShareCount({
          // in order to get twitter counts & totals you must sign up for a free account @ https://opensharecount.com/
          twitter: true,

          facebook: true,
          linkedin: true,
          google: true,

          // the twitter username you want to append to your share, leave blank for none;
          twitterUsername: 'Vinodayasl',

          // shows the counts on buttons
          showCounts: true,

          // shows the total of all the shares
          showTotal: false
        });
    </script>
    <script>
      new WOW().init();
    </script>
    <script>

    var base_url = "{!! url() !!}";
    $(window).load(function() {
    $('#slider').nivoSlider({
        effect:'random', //Specify sets like: 'fold,fade,sliceDown'
        slices:15,
        animSpeed:500, //Slide transition speed
        pauseTime:300000,
        startSlide:0, //Set starting Slide (0 index)
    });
    });

    
    
    jQuery(function($) {


      $('#accordion .panel-heading').on('click', function (e) {
        if ($(window).width() > 665) {
          return false;
        }
          
          /*e.preventDefault();*/
      });
  


        $('body').on('click', '#sear-ico', function() {
            var search_text = $("#home_search_input").val();
            if(search_text !== "" && search_text !== 0 && search_text !== null && search_text !== undefined){
                $( "#home_search_form" ).submit();
            }

          
        });


        $('body').on('click', '#search_submit', function() {
            var search_text = $("#search_text").val();
            if(search_text !== "" && search_text !== 0 && search_text !== null && search_text !== undefined){
                $( "#classified_search" ).submit();
            }

          
        });


        $('#company_selection').checkboxpicker({
            html: true,
            offLabel: '<span class="fa fa-thumbs-down">',
            onLabel: '<span class="fa fa-thumbs-up">'
          });

        $('body').on('change', '#company_selection', function() {
            if ($(this).is(':checked')) {
               $("#register_company_name").removeAttr('disabled');
               $("#register_company_addr").removeAttr('disabled');
               $("#register_company_contact").removeAttr('disabled');

            }else{
              $("#register_company_name").attr('disabled','disabled');
               $("#register_company_addr").attr('disabled','disabled');
               $("#register_company_contact").attr('disabled','disabled');
            }
        });


        $('.ticker').jTicker();
        
        $('body').on('click', '.onoffswitch-inner', function() {
          $( ".boost_status" ).toggle();
        });
        $('#boost_selection').checkboxpicker();

        $('body').on('change', '#boost_selection', function() {
            if ($(this).is(':checked')) {
               $("#boost_type_selection_row").show();
            }else{
              $("#boost_type_selection_row").hide();
            }
        });

        $('.dropdown-toggle').dropdown();
    });

     
    </script>
    @yield('scripts')
</script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
