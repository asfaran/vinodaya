<?php
define('PAGE_PARENT', 'video', true);
define('PAGE_CURRENT', 'video_add_new', true);
?>
@extends('app')

@section('title', 'Post New Video')

@section('content')
<style type="text/css">
.ibox {
    padding: 10px;
}
</style>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    {!! Form::open(array('url'=>'/admin/video/store','role'=>'form', 'files' => true, 'class'=>'form-horizontal')) !!}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
       @if ( Session::has('flash_message') )
            <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                    <button class="close" data-dismiss="alert"></button>
                    {{ Session::get('flash_message') }}
            </div>
        @endif
        @if ( Session::has('flash_success') )
            <div class="alert alert-success  {{ Session::get('flash_type') }}">
                <button class="close" data-dismiss="alert"></button>
                {{ Session::get('flash_success') }}
            </div>
        @endif
        <div class="col-lg-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Post New Video</h5>
                </div>
                <div class="ibox-content col-sm-12">

                    <!-- <form role="form" class="form-inline"> -->
                        <div class="form-group ">
                            <input type="text" placeholder="Enter title here"  value="{{ Input::old('title') }}"  name="title" id="title" class="form-control">
                        </div>
                        @if ($errors->has('title'))
                                        <span class="alert-danger">{{ $errors->first('title') }}</span><br>
                        @endif
                   <!--  </form> -->
                </div>
            </div>

            <div class="ibox">
                <div class="ibox-content col-sm-12">
                    <div class="ibox-content no-padding">
                        <input type="text" placeholder="Enter video url here"  value="{{ Input::old('video_url') }}" name="video_url" id="video_url" class="form-control">
                        @if ($errors->has('video_url'))
                                        <span class="alert-danger">{{ $errors->first('video_url') }}</span><br>
                        @endif

                    </div>
                </div>
            </div>

            <div class="ibox">
                <div class="ibox-content col-sm-12">
                    <div class="ibox-content no-padding">
                    @if ($errors->has('body'))
                                        <span class="alert-danger">{{ $errors->first('body') }}</span><br>
                    @endif
                    <textarea class="input-block-level" id="summernote" name="body" rows="18">{{ Input::old('body') }}</textarea>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Publish & Config</h5>
                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                     <input type="hidden" name="post_status" id="post_status_value" value="Draft" >
                     <input type="hidden" name="post_featured" id="post_featured_value" value="Yes" >
                     <input type="hidden" name="post_visibility" id="post_visibility_value" value="Public" >
                     <input type="hidden" name="post_publish" id="post_publish_value" value="Immediately" >

                        <div class="form-group">
                            <div class="col-lg-6">
                                <input class="btn  btn-sm btn-outline btn-default"  name="submit_draft" type="submit" value="Save Draft">
                            </div>
                            <!--<div class="col-lg-6">
                                <button class="btn btn-sm btn-outline btn-default" type="button" 
                                alt="Button Will Work Once Front End Done">Preview</button>
                            </div>-->
                        </div>
                        <div class="form-group">
                            <div class="col-lg-2">
                                <i class="fa fa-key"></i>
                            </div>
                            <div class="col-lg-10">
                                Status: &nbsp;<span  style="font-weight: bold;">Draft</span> &nbsp;
                                <a href=""  id="post_status" class="post_edit">Edit</a>
                            </div>
                        </div>
                        <div class="form-group" id="div_post_status" style="display: none;">
                            <div class="col-lg-2">
                            </div>
                            <div class="col-lg-10" data-id="select">
                                <select class=" form-control  ">
                                    <option value="Draft">Draft</option>
                                    <option value="Pending Review">Pending Review</option>
                                </select>
                                &nbsp;
                                <button class="btn  btn-sm  btn-default post_selection" type="button">OK</button>
                                &nbsp;<a href="" class="post_edit_close">Cancel</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-2">
                                <i class="fa fa-star"></i>
                            </div>
                            <div class="col-lg-10">
                                Featured: &nbsp;<span  style="font-weight: bold;">Yes</span> &nbsp;
                                <a href="" id="post_featured" class="post_edit">Edit</a>
                            </div>
                        </div>
                        <div class="form-group" id="div_post_featured" style="display: none;">
                            <div class="col-lg-2">
                            </div>
                            <div class="col-lg-10"  data-id="select">
                                <select class=" form-control ">
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                                &nbsp;
                                <button class="btn  btn-sm  btn-default post_selection" type="button">OK</button>
                                &nbsp;<a href="" class="post_edit_close">Cancel</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-2">
                                <i class="fa fa-eye"></i>
                            </div>
                            <div class="col-lg-10">
                                Visibility: &nbsp;<span  style="font-weight: bold;">Public</span> &nbsp;
                                <a href="" id="post_visibility"  class="post_edit">Edit</a>
                            </div>
                        </div>
                        <div class="form-group"  style="display: none;">
                            <div class="col-lg-2">
                            </div>
                            <div class="col-lg-10 radio" id="div_post_visibility" >
                                <div class="radio" data-id="radio">
                                            <input type="radio" name="radio1" id="radio1" value="Public" checked="">
                                            <label for="radio1">
                                                Public
                                            </label>
                                            <br>
                                            <input type="radio" name="radio1"   id="radio2" value="Private">
                                            <label for="radio2">
                                                Private
                                            </label>
                                            <br>
                                            &nbsp;
                                            <button class="btn  btn-sm  btn-default post_selection" type="button">OK</button>
                                            &nbsp;<a href="" class="post_edit_close">Cancel</a>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <div class="col-lg-2">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <div class="col-lg-10">
                                Publish: &nbsp;<span  style="font-weight: bold;">Immediately</span> &nbsp;
                                <a href="" id="post_publish" class="post_edit">Edit</a>
                            </div>
                        </div> -->
                        <div class="form-group" style="display: none;">
                            <div class="col-lg-2">
                            </div>
                            <div class="col-lg-10"  id="div_post_publish">
                                <div class="input-group date" data-id="input">
                                    <input type="datetime" class="form-control " date-time ng-model="sampleDate" view="date" auto-close="true">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <br>
                                    &nbsp;
                                    <button class="btn  btn-sm  btn-default post_selection" type="button">OK</button>
                                    &nbsp;<a href=""  class="post_edit_close">Cancel</a>
                                </div>
                                
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-lg-6">
                            </div>
                            <div class="col-lg-6">
                                <input class="btn btn-sm btn-w-m btn-success"  name="submit_button" type="submit" value="Publish">
                            </div>
                        </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Categories</h5>
                    <div ibox-tools></div>
                </div>
                <div class="ibox-content" >
                    @if ($errors->has('category'))
                                        <span class="alert-danger">{{ $errors->first('category') }}</span><br>
                    @endif
                    @if($category_list)

                        @foreach($category_list  as $category)
                        <div class="form-group">
                            <div class="col-lg-2">
                               <input type="checkbox" name="category[]"  value="{{ $category->id }}" ng-model="main.check{{ $category->id }}">
                            </div>
                            <div class="col-lg-10">
                                {{ $category->title }}
                            </div>
                        </div>
                        @endforeach
                    @else
                    <div class="form-group">
                            <div class="col-lg-12">
                                No categories added.
                            </div>
                    </div>
                    <div class="form-group">
                            <div class="col-lg-2">
                               <i class="fa fa-eyedropper"></i>
                            </div>
                            <div class="col-lg-10">
                                <a href="{{ URL::to('/admin/general/category/list') }}">Create New Category</a>
                            </div>
                    </div>
                    @endif   
                          
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Tags</h5>
                    <div ibox-tools></div>
                </div>
                <div class="ibox-content" >

                        <div class="form-group">
                            <div class="col-lg-12">
                             <textarea id="hero-demo" name="tags" placeholder="Type Tags Here"></textarea>                                
                            </div>
                        </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Featured Image</h5>
                    <div ibox-tools></div>
                </div>
                <div class="ibox-content" >
                    <form class="form-horizontal">

                        <div class="form-group">
                            <div class="col-lg-12">
                            <div class="fallback">
                                <input name="thumb" type="file" multiple />
                            </div>

                            <!-- <div class="dropzone-previews" id="dropzonePreview"></div>
                            <h4 style="text-align: center;color:#428bca;">Drop images in this area  <span class="glyphicon glyphicon-hand-down"></span></h4>
                                <a href="">Set featured image</a> -->
                                @if ($errors->has('thumb'))
                                        <span class="alert-danger">{{ $errors->first('title') }}</span><br>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
    <!-- form end -->

    <!-- Dropzone Preview Template -->
    <div id="preview-template" style="display: none;">

        <div class="dz-preview dz-file-preview">
            <div class="dz-image"><img data-dz-thumbnail=""></div>

            <div class="dz-details">
                <div class="dz-size"><span data-dz-size=""></span></div>
                <div class="dz-filename"><span data-dz-name=""></span></div>
            </div>
            <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress=""></span></div>
            <div class="dz-error-message"><span data-dz-errormessage=""></span></div>

            <div class="dz-success-mark">
                <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                    <!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
                    <title>Check</title>
                    <desc>Created with Sketch.</desc>
                    <defs></defs>
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                        <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>
                    </g>
                </svg>
            </div>

            <div class="dz-error-mark">
                <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                    <!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
                    <title>error</title>
                    <desc>Created with Sketch.</desc>
                    <defs></defs>
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                        <g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475">
                            <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" sketch:type="MSShapeGroup"></path>
                        </g>
                    </g>
                </svg>
            </div>

        </div>
    </div>
    <!-- End Dropzone Preview Template -->
    </div>
</div>
@endsection
