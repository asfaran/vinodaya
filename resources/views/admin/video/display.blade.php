<?php
define('PAGE_PARENT', 'video', true);
define('PAGE_CURRENT', 'video_display', true);
?>
@extends('app')

@section('title', 'Front View Configuration')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
        {!! Form::open(array('url'=>'/admin/video/display_store','role'=>'form', 'class'=>'form-horizontal')) !!}
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Video Front View Configuration</h5>

                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                    

                    @if ( Session::has('flash_message') )
                        <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                                <button class="close" data-dismiss="alert"></button>
                                {{ Session::get('flash_message') }}
                        </div>
                    @endif
                    @if ( Session::has('flash_success') )
                        <div class="alert alert-success  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {{ Session::get('flash_success') }}
                        </div>
                    @endif          
                </div>
            </div>



            <div class="ibox float-e-margins">
                <div class="ibox-content col-sm-12">
                     <div class="form-group">
                            <label class="col-lg-3 control-label">Number of Video in listing page</label>
                            <div class="col-lg-9">
                                <input type="number"  
                                value="{{ (isset($option_list['video_count_in_listing']))? $option_list['video_count_in_listing'] : '' }}" placeholder="Enter Count number"  class="form-control numeric"  name="video_count_in_listing">
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-lg-3 control-label">Number of Video in Front page</label>
                            <div class="col-lg-9">
                                <input type="text"  value="{{ (isset($option_list['video_count_in_front']))? $option_list['video_count_in_front'] : ''  }}" 
                                placeholder="Enter Count number" class="form-control numeric" name="video_count_in_front">
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-lg-3 control-label">Featured Video Count</label>
                            <div class="col-lg-9">
                                <input type="text"  value="{{ (isset($option_list['featured_video_count']))? $option_list['featured_video_count'] : ''  }}" placeholder="Enter amount Here" class="form-control numeric" name="featured_video_count">
                            </div>
                    </div>
                </div>
            </div>


            <div class="ibox">
                <div class="ibox-content col-sm-12">
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-8">
                            <input class="btn btn-sm btn-primary" type="submit" value="Update View Configuration">
                        </div>
                    </div>
                </div>
            </div>


            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
