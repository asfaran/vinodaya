<?php
define('PAGE_PARENT', 'video', true);
define('PAGE_CURRENT', 'video_list', true);
?>
@extends('app')

@section('title', 'All Video List')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title">
    <h5>All Video List</h5>
    <!-- <div ibox-tools></div> -->
    <div class="pull-right add_new_link">
        <a class="btn btn-sm btn-w-m btn-default" href="{{ URL::to('/admin/video/add_new/') }}">Post New Video</a>
    </div>
</div>
<div class="ibox-content">
@if ( Session::has('flash_message') )
    <div class="alert alert-danger  {{ Session::get('flash_type') }}">
            <button class="close" data-dismiss="alert"></button>
            {{ Session::get('flash_message') }}
    </div>
@endif
@if ( Session::has('flash_success') )
    <div class="alert alert-success  {{ Session::get('flash_type') }}">
        <button class="close" data-dismiss="alert"></button>
        {{ Session::get('flash_success') }}
    </div>
@endif
<table datatable="" class="table table-striped table-bordered table-hover dataTables-example">
<thead>

<tr>
    <th>Title</th>
    <th>Video Url</th>
    <th>Author</th>
    <th>Categories</th>
    <th>status</th>
    <th>Comments</th>
    <th>Click Count</th>
    <th>Date</th>
    <th colspan="2">Action</th>
</tr>
</thead>
<tbody>
@if(count($video_list) > 0 )
    @foreach($video_list  as $video)
     <tr class="gradeX">
        <td><a href="{{ URL::to('/admin/video/edit/'.$video->id) }}">{{ $video->title }}</a></td>
        <td>
        @if($video->thumbnail_type == "image")
            <a target="_blank" href="{{ URL::to($video->video_url) }}">
                <img alt="image" class="img" width="150px" src="{{ url('/uploads/video/'.$video->thumb) }}"/>
            </a>
        @else
            <a target="_blank" href="{{ URL::to($video->video_url) }}">
                <img alt="image" class="img" width="150px" src="{{ url($video->thumbnail) }}"/>
            </a>
        @endif
        </td>
        <td>{{ $video->author }}</td>
        <td>{{ $video->category_id }}</td>
        <td>{{ $status_array[$video->status]   }}</td>
        <td>{!! ($video->comments_count == 0 )? '0 Comments' : "<a href='#'>$video->comments_count</a>"    !!}</td>
        <td>{{ Counter::show('video', $video->id) }}
        <?php  /*{{ ($video->click_count == '0' )? '0 Clicks' : '<a href="#">$video->click_count</a>'    }}*/ ?></td>
        <td>{{ $video->created_at }}</td>
        <td class="center"><a href="{{ URL::to('/admin/video/edit/'.$video->id) }}" class="btn btn-sm btn-warning" type="submit">Edit</a></td>
        <td class="center"><a href="{{ URL::to('/admin/video/delete/'.$video->id) }}" class="btn btn-sm btn-danger" type="submit">Remove</a></td>
    </tr>
    @endforeach 

@else
    <tr>
        <th colspan="8">No Records In database</th>
    </tr>
@endif
  
<!-- <tr class="gradeX">
    <td>Trident</td>
    <td>Internet</td>
    <td>Win 95+</td>
    <td class="center">4</td>
    <td class="center">X</td>
</tr> -->

</tbody>
<tfoot>
<!-- <tr>
    <th>Rendering engine</th>
    <th>Browser</th>
    <th>Platform(s)</th>
    <th>Engine version</th>
    <th>CSS grade</th>
</tr> -->
</tfoot>
</table>

</div>
</div>
</div>
</div>
</div>
@endsection
