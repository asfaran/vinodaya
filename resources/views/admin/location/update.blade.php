<?php
define('PAGE_PARENT', 'classified', true);
define('PAGE_CURRENT', 'location_list', true);
?>
@extends('app')

@section('title', 'Location List')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-5">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Add New Location</h5>

                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                    {!! Form::open(array('url'=>'/admin/classified/location/store','role'=>'form', 'class'=>'form-horizontal')) !!}
                    {!! Form::hidden('id', $location_one->id, array('id' => 'invisible_id')) !!}

                    @if ( Session::has('flash_message') )
                        <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                                <button class="close" data-dismiss="alert"></button>
                                {{ Session::get('flash_message') }}
                        </div>
                    @endif
                    @if ( Session::has('flash_success') )
                        <div class="alert alert-success  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {{ Session::get('flash_success') }}
                        </div>
                    @endif

                        <div class="form-group"><label class="col-lg-3 control-label">Title</label>

                            <div class="col-lg-9">
                               {!! Form::text('title',$location_one->title,array('id'=>'form_title','class'=>'form-control','placeholder'=>'Title')) !!}
                                @if ($errors->has('title'))
                                        <span class="alert-danger">{{ $errors->first('title') }}</span><br>
                                @endif
                                <span class="help-block m-b-none">The name is how it appears on your site.</span>

                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Slug</label>

                            <div class="col-lg-9">
                                {!! Form::text('slug',$location_one->slug,array('id'=>'form_slug','class'=>'form-control','placeholder'=>'Slug')) !!}
                                @if ($errors->has('slug'))
                                        <span class="alert-danger">{{ $errors->first('slug') }}</span><br>
                                @endif
                                <span class="help-block m-b-none">The “slug” is the URL-friendly version of the name.</span>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Location Type</label>

                            <div class="col-lg-9">
                            {!! Form::select('type', array('0'=>'Select One', '1'=>'Province', '2'=>'City', '3'=>'Town', '4'=>'Building' ), $location_one->type, ['id' => 'type', 'class' => 'form-control']) !!}
                            @if ($errors->has('type'))
                                        <span class="alert-danger">{{ $errors->first('type') }}</span><br>
                            @endif
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Parent</label>

                            <div class="col-lg-9">
                            {!! Form::select('parentid', $locations_list, $location_one->parentid, ['id' => 'parentid', 'class' => 'form-control']) !!}
                            @if ($errors->has('parentid'))
                                        <span class="alert-danger">{{ $errors->first('parentid') }}</span><br>
                            @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-8">
                                <input class="btn btn-sm btn-primary" type="submit" value="Update Location">
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-lg-7">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Location List</h5>
                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-9 m-b-xs">
                            <select  id="select_location_type" class=" form-control ">
                                <option value="">[Select]</option>
                                <option value="1">Province</option>
                                <option value="2">City</option>
                                <option value="3">Town</option>
                                <option value="4">Building</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <table datatable="" class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Location</th>
                                <th>Parent</th>
                                <th colspan="2"><center>Action</center></th>
                            </tr>
                            </thead>
                            <tbody   id="table_body_location_list" >
                             @foreach($location_full  as $location)
                             <tr class="gradeX">
                                <td>{{ $location->id }}</td>
                                <td><a href="{{ URL::to('/admin/classified/location/edit/'.$location->id) }}">{{ $location->title }}</a></td>
                                <td>{{ $location->parent_title }}</td>
                                <td class="center"><a href="{{ URL::to('/admin/classified/location/edit/'.$location->id) }}" class="btn btn-sm btn-warning" type="submit">Edit</a></td>
                                <td class="center"><a href="{{ URL::to('/admin/classified/location/delete/'.$location->id) }}" class="btn btn-sm btn-danger" type="submit">Remove</a></td>
                            </tr>
                            @endforeach                          
                            </tbody>
                           <!--  <tfoot>
                            <tr>
                                <th>Rendering engine</th>
                                <th>Browser</th>
                                <th>Platform(s)</th>
                                <th>Engine version</th>
                                <th>CSS grade</th>
                            </tr>
                            </tfoot> -->
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
