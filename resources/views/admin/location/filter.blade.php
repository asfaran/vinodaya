@if(count($location_list) > 0 )
     @foreach($location_list  as $location)
     <tr class="gradeX">
        <td>{{ $location->id }}</td>
        <td><a href="{{ URL::to('/admin/classified/location/edit/'.$location->id) }}">{{ $location->title }}</a></td>
        <td>{{ $location->parent_title }}</td>
        <td class="center"><a href="{{ URL::to('/admin/classified/location/edit/'.$location->id) }}" class="btn btn-sm btn-warning" type="submit">Edit</a></td>
        <td class="center"><a href="{{ URL::to('/admin/classified/location/delete/'.$location->id) }}" class="btn btn-sm btn-danger" type="submit">Remove</a></td>
    </tr>
    @endforeach     
@else
<tr class="gradeX">
    <td colspan="5">No Records For Selected Location Type</td>
</tr>
@endif 