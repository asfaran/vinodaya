<?php
define('PAGE_PARENT', 'settings', true);
define('PAGE_CURRENT', 'social_list', true);
?>
@extends('app')

@section('title', 'Social Links')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Social Media Links</h5>

                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                    {!! Form::open(array('url'=>'/admin/settings/social/store','role'=>'form', 'class'=>'form-horizontal')) !!}

                    @if ( Session::has('flash_message') )
                        <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                                <button class="close" data-dismiss="alert"></button>
                                {{ Session::get('flash_message') }}
                        </div>
                    @endif
                    @if ( Session::has('flash_success') )
                        <div class="alert alert-success  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {{ Session::get('flash_success') }}
                        </div>
                    @endif

                    @foreach($social_list  as $social)
                        @if($social->status === 1)
                        <div class="form-group">
                            <label class="col-lg-3 control-label"><i class="fa {{ $social->icon }}"></i> &nbsp;{{ $social->title }}</label>
                            <div class="col-lg-9">
                                <input type="text"  value="{{ $social->url }}" placeholder="Type {{ $social->title }} Url Here" class="form-control" name="social[{{ $social->slug }}]">
                            </div>
                        </div>
                        @endif
                    @endforeach

                        
                        
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-8">
                                <input class="btn btn-sm btn-primary" type="submit" value="Update Social Links">
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
