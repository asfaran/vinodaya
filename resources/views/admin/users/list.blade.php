<?php
define('PAGE_PARENT', 'adminuser', true);
define('PAGE_CURRENT', 'users_list', true);
?>
@extends('app')

@section('title', 'Users List')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title">
    <h5>All Users List</h5>
    <!-- <div ibox-tools></div> -->
    <div class="pull-right add_new_link">
        <a class="btn btn-sm btn-w-m btn-default" href="{{ URL::to('/admin/users/add_new/') }}">Add New User</a>
    </div>
</div>
<div class="ibox-content">
@if ( Session::has('flash_message') )
    <div class="alert alert-danger  {{ Session::get('flash_type') }}">
            <button class="close" data-dismiss="alert"></button>
            {{ Session::get('flash_message') }}
    </div>
@endif
@if ( Session::has('flash_success') )
    <div class="alert alert-success  {{ Session::get('flash_type') }}">
        <button class="close" data-dismiss="alert"></button>
        {{ Session::get('flash_success') }}
    </div>
@endif
<table datatable="" class="table table-striped table-bordered table-hover dataTables-example">
<thead>

<tr>
    <th>Name</th>
    <th>Email</th>
    <th>User Type</th>
    <th>Date</th>
    <th colspan="2">Action</th>
</tr>
</thead>
<tbody>
@if(count($users_list) > 0 )
<?php  $user_type_array = array('0'=>'Select One', '1'=>'Super Admin', '2'=>'Admin', '3'=>'End User' ); ?>
    @foreach($users_list  as $users)
     <tr class="gradeX">
        <td><a href="{{ URL::to('/admin/users/edit/'.$users->id) }}">{{ $users->name }}</a></td>
        <td>{{ $users->email }}</td>
        <td>{{ $user_type_array[$users->usertype]    }}</td>
        <td>{{ $users->created_at }}</td>
        <td class="center"><a href="{{ URL::to('/admin/users/edit/'.$users->id) }}" class="btn btn-sm btn-warning" type="submit">Edit</a></td>
        <td class="center"><a href="{{ URL::to('/admin/users/delete/'.$users->id) }}" class="btn btn-sm btn-danger" type="submit">Remove</a></td>
    </tr>
    @endforeach 

@else
    <tr>
        <th colspan="8">No Records In database</th>
    </tr>
@endif

</tbody>
<tfoot>
</tfoot>
</table>

</div>
</div>
</div>
</div>
</div>
@endsection
