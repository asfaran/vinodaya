<?php
define('PAGE_PARENT', 'adminuser', true);
define('PAGE_CURRENT', 'add_users', true);
?>
@extends('app')

@section('title', 'Update Users')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Update Selected User</h5>

                <div ibox-tools></div>
            </div>
            <div class="ibox-content">
                {!! Form::open(array('url'=>'/auth/register','role'=>'form', 'class'=>'form-horizontal')) !!}
                 {!! Form::hidden('id', $users_one->id, array('id' => 'invisible_id')) !!}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                @if ( Session::has('flash_message') )
                    <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {{ Session::get('flash_message') }}
                    </div>
                @endif
                @if ( Session::has('flash_success') )
                    <div class="alert alert-success  {{ Session::get('flash_type') }}">
                        <button class="close" data-dismiss="alert"></button>
                        {{ Session::get('flash_success') }}
                    </div>
                @endif

                   <!--  <div class="form-group"><label class="col-lg-2 control-label">User Name</label>

                        <div class="col-lg-8 col-lg-offset-1">
                        {!! Form::text('username',$users_one->username,array('id'=>'username','class'=>'form-control','placeholder'=>'User Name')) !!}
                            <input type="text" placeholder="User Name" class="form-control" id="username" name="username">
                            @if ($errors->has('username'))
                                    <span class="alert-danger">{{ $errors->first('username') }}</span><br>
                            @endif
                        </div>
                    </div> -->
                    <div class="form-group"><label class="col-lg-2 control-label">Name</label>

                        <div class="col-lg-8 col-lg-offset-1">
                            {!! Form::text('name',$users_one->name,array('id'=>'name','class'=>'form-control','placeholder'=>'Name')) !!}
                            @if ($errors->has('name'))
                                    <span class="alert-danger">{{ $errors->first('name') }}</span><br>
                            @endif
                        </div>
                    </div>
                    <div class="form-group"><label class="col-lg-2 control-label">E-Mail Address</label>

                        <div class="col-lg-8 col-lg-offset-1">
                        {!! Form::email('email',$users_one->email,array('id'=>'email','class'=>'form-control','placeholder'=>'Email')) !!}
                            @if ($errors->has('email'))
                                    <span class="alert-danger">{{ $errors->first('email') }}</span><br>
                            @endif
                        </div>
                    </div>
                    <div class="form-group"><label class="col-lg-2 control-label">Password</label>

                        <div class="col-lg-8 col-lg-offset-1">
                        {!! Form::password('password',array('id'=>'password','class'=>'form-control','placeholder'=>'Password')) !!}
                            @if ($errors->has('password'))
                                    <span class="alert-danger">{{ $errors->first('password') }}</span><br>
                            @endif
                        </div>
                    </div>
                    <div class="form-group"><label class="col-lg-2 control-label">Confirm Password</label>

                        <div class="col-lg-8 col-lg-offset-1">
                        {!! Form::password('password_confirmation',array('id'=>'password_confirmation','class'=>'form-control','placeholder'=>'Confirm Password')) !!}
                            @if ($errors->has('password_confirmation'))
                                    <span class="alert-danger">{{ $errors->first('password_confirmation') }}</span><br>
                            @endif
                        </div>
                    </div>
                    <div class="form-group"><label class="col-lg-2 control-label">User Type</label>

                        <div class="col-lg-8 col-lg-offset-1">
                            {!! Form::select('usertype', array('0'=>'Select One', '1'=>'Super Admin', '2'=>'Admin', '3'=>'End User'  ),$users_one->usertype, ['id' => 'usertype', 'class' => 'form-control']) !!}
                            @if ($errors->has('usertype'))
                                        <span class="alert-danger">{{ $errors->first('usertype') }}</span><br>
                            @endif
                        </div>
                    </div>
                   <!--  <div class="form-group"><label class="col-lg-3 control-label">Qucik Link</label>

                        <div class="col-lg-8 col-lg-offset-1">
                        {!! Form::checkbox('quciklink', null, ['id' => 'quciklink', 'class' => 'form-control']) !!}
                        @if ($errors->has('display'))
                                    <span class="alert-danger">{{ $errors->first('display') }}</span><br>
                        @endif
                        </div>
                    </div> -->
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-8">
                            <input class="btn btn-sm btn-primary" type="submit" value="Update Users">
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
