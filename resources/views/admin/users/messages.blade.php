<?php
define('PAGE_PARENT', 'general', true);
define('PAGE_CURRENT', 'messages', true);
?>
@extends('app')

@section('title', 'Messages Lists')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title">
    <h5>All Messages Lists</h5>
    <!-- <div ibox-tools></div> -->
</div>
<div class="ibox-content">
@if ( Session::has('flash_message') )
    <div class="alert alert-danger  {{ Session::get('flash_type') }}">
            <button class="close" data-dismiss="alert"></button>
            {{ Session::get('flash_message') }}
    </div>
@endif
@if ( Session::has('flash_success') )
    <div class="alert alert-success  {{ Session::get('flash_type') }}">
        <button class="close" data-dismiss="alert"></button>
        {{ Session::get('flash_success') }}
    </div>
@endif
<table datatable="" class="table table-striped table-bordered table-hover dataTables-example">
<thead>

<tr>
    <th>Name</th>
    <th>Email</th>
    <th>Mobile</th>
    <th>Subject</th>
    <th>Message</th>
    <th>Created At</th>
    <!-- <th colspan="2">Action</th> -->
</tr>
</thead>
<tbody>


@if(count($message_list) > 0 )
    @foreach($message_list  as $message)
     <tr class="gradeX">
        <td>{{ $message->name }}</td>
        <td>{{ $message->email }}</td>
        <td>{{ $message->mobile }}</td>
        <td>{!! $message->subject   !!}</td>
        <td>{!! $message->message   !!}</td>
        <td>{{ $message->created_at }}</td>
    </tr>
    @endforeach 

@else
    <tr>
        <th colspan="10">No Message found, Once Message recieved listing will shows</th>
    </tr>
@endif
  
<!-- <tr class="gradeX">
    <td>Trident</td>
    <td>Internet</td>
    <td>Win 95+</td>
    <td class="center">4</td>
    <td class="center">X</td>
</tr> -->

</tbody>
<tfoot>
<!-- <tr>
    <th>Rendering engine</th>
    <th>Browser</th>
    <th>Platform(s)</th>
    <th>Engine version</th>
    <th>CSS grade</th>
</tr> -->
</tfoot>
</table>

</div>
</div>
</div>
</div>
</div>
@endsection
