<?php
define('PAGE_PARENT', 'adminuser', true);
define('PAGE_CURRENT', 'add_users', true);
?>
@extends('app')

@section('title', 'Add New Users')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Add New Users</h5>

                <div ibox-tools></div>
            </div>
            <div class="ibox-content">
                {!! Form::open(array('url'=>'/auth/register','role'=>'form', 'class'=>'form-horizontal')) !!}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                @if ( Session::has('flash_message') )
                    <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {{ Session::get('flash_message') }}
                    </div>
                @endif
                @if ( Session::has('flash_success') )
                    <div class="alert alert-success  {{ Session::get('flash_type') }}">
                        <button class="close" data-dismiss="alert"></button>
                        {{ Session::get('flash_success') }}
                    </div>
                @endif

                    <!-- <div class="form-group"><label class="col-lg-2 control-label">User Name</label>

                        <div class="col-lg-8 col-lg-offset-1">
                            <input type="text" placeholder="User Name" class="form-control" id="username" name="username">
                            @if ($errors->has('username'))
                                    <span class="alert-danger">{{ $errors->first('username') }}</span><br>
                            @endif
                        </div>
                    </div> -->
                    <div class="form-group"><label class="col-lg-2 control-label">Name</label>

                        <div class="col-lg-8 col-lg-offset-1">
                            <input type="text" placeholder="Name" class="form-control" id="name"  value="{{ Input::old('name')}}"  name="name">
                            @if ($errors->has('name'))
                                    <span class="alert-danger">{{ $errors->first('name') }}</span><br>
                            @endif
                        </div>
                    </div>
                    <div class="form-group"><label class="col-lg-2 control-label">E-Mail Address</label>

                        <div class="col-lg-8 col-lg-offset-1">
                        <input type="email" placeholder="Email" class="form-control" id="email"  value="{{ Input::old('email') }}" name="email">
                            @if ($errors->has('email'))
                                    <span class="alert-danger">{{ $errors->first('email') }}</span><br>
                            @endif
                        </div>
                    </div>
                    <div class="form-group"><label class="col-lg-2 control-label">Password</label>

                        <div class="col-lg-8 col-lg-offset-1">
                            <input type="password" placeholder="Password" class="form-control" id="password"  name="password">
                            @if ($errors->has('password'))
                                    <span class="alert-danger">{{ $errors->first('password') }}</span><br>
                            @endif
                        </div>
                    </div>
                    <div class="form-group"><label class="col-lg-2 control-label">Confirm Password</label>

                        <div class="col-lg-8 col-lg-offset-1">
                            <input type="password" placeholder="Password" class="form-control" id="password_confirmation"  name="password_confirmation">
                            @if ($errors->has('password_confirmation'))
                                    <span class="alert-danger">{{ $errors->first('password_confirmation') }}</span><br>
                            @endif
                        </div>
                    </div>
                    <div class="form-group"><label class="col-lg-2 control-label">User Type</label>

                        <div class="col-lg-8 col-lg-offset-1">
                            {!! Form::select('usertype', array('0'=>'Select One', '1'=>'Super Admin', '2'=>'Admin', '3'=>'End User' ), Input::old('usertype'), ['id' => 'usertype', 'class' => 'form-control']) !!}
                            @if ($errors->has('usertype'))
                                        <span class="alert-danger">{{ $errors->first('usertype') }}</span><br>
                            @endif
                        </div>
                    </div>
                   <!--  <div class="form-group"><label class="col-lg-3 control-label">Qucik Link</label>

                        <div class="col-lg-8 col-lg-offset-1">
                        {!! Form::checkbox('quciklink', null, ['id' => 'quciklink', 'class' => 'form-control']) !!}
                        @if ($errors->has('display'))
                                    <span class="alert-danger">{{ $errors->first('display') }}</span><br>
                        @endif
                        </div>
                    </div> -->
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-8">
                            <input class="btn btn-sm btn-primary" type="submit" value="Add New Users">
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
