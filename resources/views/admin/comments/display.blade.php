<?php
define('PAGE_PARENT', '{!! strtolower($post_type_name) !!}' , true);
define('PAGE_CURRENT', '{{ $post_type_name }}_comments_list', true);

$title = "Manage  " . $post_type_name . " Comments";
?>
@extends('app')

@section('title', $title)

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title">
    <h5>{{ $post_type_name }} Comments </h5>
    <div ibox-tools></div>
</div>
<div class="ibox-content">

<table datatable="" class="table table-striped table-bordered table-hover dataTables-example">
<thead>

<tr>    

    <th>Author</th>
    <th>status</th>
    <th>Comment</th>
    <th>In Response To</th>
    <th>Submitted On</th>
    <th colspan="2">Action</th>
</tr>
</thead>
<tbody>
@if(count($comments_list) > 0 )
<input type="hidden" name="_token" value="{{ csrf_token() }}">
    @foreach($comments_list  as $comments)
     <tr class="gradeX">
        <td>
            @if($comments->author) 
            <button class="btn btn-default btn-primary" type="button">
                <i class="fa fa-user"></i>&nbsp;{{ $comments->author }}
            </button><br>
            @endif
            @if($comments->author_url)
            <button class="btn btn-default btn-primary" type="button">
                <i class="fa fa-globe"></i>&nbsp;{{ $comments->author_url }}
            </button><br>
            @endif
            @if($comments->author_email)
            <button class="btn btn-default btn-primarye" type="button">
                <i class="fa fa-at"></i>&nbsp;{{ $comments->author_email }}
            </button><br>
            @endif
            @if($comments->author_IP)
            <button class="btn btn-default btn-primary" type="button">
                <i class="fa fa-binoculars"></i>&nbsp;{{ $comments->author_IP }}
            </button><br>
            @endif
        </td>
        <td id="comments_status_div{{ $comments->id }}">
            @if($comments->approved == 1)
            <span class="btn btn-primary dim comments_status" data-value="{{ $comments->id }}" 
                        data-status="comments_approved" >
                <i class="fa fa-check"></i>
            </span>
            @else
            <span class="btn btn-warning dim comments_status" data-value="{{ $comments->id }}" 
                    data-status="comments_not_approved" >
                <i class="fa fa-times"></i>
            </span>
            @endif
        </td>
        <td>{!! $comments->content !!}</td>
        <td>{{ $comments->created_at }}</td>
        <td class="center"><a href="{{ URL::to('/admin/comments/edit/'.$comments->id) }}" class="btn btn-sm btn-warning" type="submit">Edit</a></td>
        <td class="center"><a href="{{ URL::to('/admin/comments/delete/'.$comments->id) }}" class="btn btn-sm btn-danger" type="submit">Remove</a></td>
    </tr>
    @endforeach 

@else
    <tr>
        <th colspan="8">No Records In database</th>
    </tr>
@endif
  
<!-- <tr class="gradeX">
    <td>Trident</td>
    <td>Internet</td>
    <td>Win 95+</td>
    <td class="center">4</td>
    <td class="center">X</td>
</tr> -->

</tbody>
<tfoot>
<!-- <tr>
    <th>Rendering engine</th>
    <th>Browser</th>
    <th>Platform(s)</th>
    <th>Engine version</th>
    <th>CSS grade</th>
</tr> -->
</tfoot>
</table>

</div>
</div>
</div>
</div>
</div>
@endsection
