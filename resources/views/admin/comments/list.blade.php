<?php
define('PAGE_PARENT', 'general', true);
define('PAGE_CURRENT', 'comments_list', true);
?>
@extends('app')

@section('title', 'Manage Comments')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title">
    <h5>All Comments</h5>
    <div ibox-tools></div>
</div>
<div class="ibox-content">
@if ( Session::has('flash_message') )
    <div class="alert alert-danger  {{ Session::get('flash_type') }}">
            <button class="close" data-dismiss="alert"></button>
            {{ Session::get('flash_message') }}
    </div>
@endif
@if ( Session::has('flash_success') )
    <div class="alert alert-success  {{ Session::get('flash_type') }}">
        <button class="close" data-dismiss="alert"></button>
        {{ Session::get('flash_success') }}
    </div>
@endif

<table datatable="" class="table table-striped table-bordered table-hover dataTables-example">
<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
<thead>

<tr>    

    <th>Author</th>
    <th>Place of Comment</th>
    <th>status</th>
    <th>Comment</th>
    <th>In Response To</th>
    <th>Submitted On</th>
    <th colspan="2">Action</th>
</tr>
</thead>
<tbody>
@if(count($comments_list) > 0 )
 <input type="hidden" name="_token" value="{{ csrf_token() }}">
    @foreach($comments_list  as $comments)
     <tr class="gradeX">
        <td>
            @if($comments->author) 
            <button class="btn btn-default btn-primary" type="button">
                <i class="fa fa-user"></i>{{ $comments->author }}
            </button><br>
            @endif
            @if($comments->author_url)
            <button class="btn btn-default btn-primary" type="button">
                <i class="fa fa-globe"></i>{{ $comments->author_url }}
            </button><br>
            @endif
            @if($comments->author_email)
            <button class="btn btn-default btn-primarye" type="button">
                <i class="fa fa-at"></i>{{ $comments->author_email }}
            </button><br>
            @endif
            @if($comments->author_IP)
            <button class="btn btn-default btn-primary" type="button">
                <i class="fa fa-binoculars"></i>{{ $comments->author_IP }}
            </button><br>
            @endif
        </td>
        <td>
            <a href="{{ URL::to('/admin/comments/display/'.$comments->id) }}">
                {!! $type_array[$comments->post_type] !!}
            </a>
        </td>
        <td id="comments_status_div{{ $comments->id }}">
            @if($comments->approved == 1)
            <span class="btn btn-primary dim comments_status" data-value="{{ $comments->id }}" 
                        data-status="comments_approved" >
                <i class="fa fa-check"></i>
            </span>
            @else
            <span class="btn btn-warning dim comments_status" data-value="{{ $comments->id }}" 
                    data-status="comments_not_approved" >
                <i class="fa fa-times"></i>
            </span>
            @endif
        </td>
        <td>{!! $comments->content !!}</td>
        <td>{{ $comments->created_at }}</td>
        <td class="center"><a href="{{ URL::to('/admin/comments/edit/'.$comments->id) }}" class="btn btn-sm btn-warning" type="submit">Edit</a></td>
        <td class="center"><a href="{{ URL::to('/admin/comments/delete/'.$comments->id) }}" class="btn btn-sm btn-danger" type="submit">Remove</a></td>
    </tr>
    @endforeach 

@else
    <tr>
        <th colspan="8">No Records In database</th>
    </tr>
@endif
  
<!-- <tr class="gradeX">
    <td>Trident</td>
    <td>Internet</td>
    <td>Win 95+</td>
    <td class="center">4</td>
    <td class="center">X</td>
</tr> -->

</tbody>
<tfoot>
<!-- <tr>
    <th>Rendering engine</th>
    <th>Browser</th>
    <th>Platform(s)</th>
    <th>Engine version</th>
    <th>CSS grade</th>
</tr> -->
</tfoot>
</table>

</div>
</div>
</div>
</div>
</div>
@endsection
