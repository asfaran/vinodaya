<?php
define('PAGE_PARENT', 'general', true);
define('PAGE_CURRENT', 'comments_list', true);

?>
@extends('app')

@section('title', 'Update Comments')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-9">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Update Comments</h5>

                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                    {!! Form::open(array('url'=>'/admin/comments/store','role'=>'form', 'class'=>'form-horizontal')) !!}
                    {!! Form::hidden('id', $comment_one->id, array('id' => 'invisible_id')) !!}
                    
                    @if ( Session::has('flash_message') )
                        <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                                <button class="close" data-dismiss="alert"></button>
                                {{ Session::get('flash_message') }}
                        </div>
                    @endif
                    @if ( Session::has('flash_success') )
                        <div class="alert alert-success  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {{ Session::get('flash_success') }}
                        </div>
                    @endif

                        <div class="form-group"><label class="col-lg-3 control-label">Post Type</label>

                            <div class="col-lg-9">{{ $type_array[$comment_one->post_type] }}
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Post ID</label>

                            <div class="col-lg-9">
                                <a href="#" target="_blank" >{{ $comment_one->post_id }}</a>
                            </div>
                            <span class="help-block m-b-none">Will Link to Front end post view after front views done!</span>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Name</label>

                            <div class="col-lg-9">
                                 {!! Form::text('author', $comment_one->author, array('id'=>'author','class'=>'form-control','placeholder'=>'Name')) !!}
                                @if ($errors->has('author'))
                                        <span class="alert-danger">{{ $errors->first('author') }}</span><br>
                                @endif
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Email</label>

                            <div class="col-lg-9">
                                 {!! Form::email('author_email', $comment_one->author_email, array('id'=>'author_email','class'=>'form-control','placeholder'=>'Email')) !!}
                                @if ($errors->has('author_email'))
                                        <span class="alert-danger">{{ $errors->first('author_email') }}</span><br>
                                @endif
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Web Address</label>

                            <div class="col-lg-9">
                                 {!! Form::text('author_url', $comment_one->author_url, array('id'=>'author_url','class'=>'form-control','placeholder'=>'Web Address')) !!}
                                @if ($errors->has('author_url'))
                                        <span class="alert-danger">{{ $errors->first('author_url') }}</span><br>
                                @endif
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Comments</label>

                            <div class="col-lg-9">
                                {!! Form::textarea('content', $comment_one->content, array('id'=>'content','class'=>'form-control','placeholder'=>'Type Comment')) !!}
                                @if ($errors->has('content'))
                                        <span class="alert-danger">{{ $errors->first('content') }}</span><br>
                                @endif
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Status Approved</label>

                            <div class="col-lg-9">
                            {!! Form::checkbox('approved', $comment_one->approved, ($comment_one->approved == 1)? true : false , ['id' => 'approved', 'class' => 'form-control']) !!}
                            @if ($errors->has('approved'))
                                        <span class="alert-danger">{{ $errors->first('approved') }}</span><br>
                            @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-8">
                                <input class="btn btn-sm btn-primary" type="submit" value="Update Comment">
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
