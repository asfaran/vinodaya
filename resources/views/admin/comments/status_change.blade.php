@if($selected_span == "comments_approved")

<span class="btn btn-warning dim comments_status" data-value="{{ $comment_id }}" 
        data-status="comments_not_approved">
    <i class="fa fa-times"></i>
</span>

@else

<span class="btn btn-primary dim comments_status" data-value="{{ $comment_id }}" 
            data-status="comments_approved">
    <i class="fa fa-check"></i>
</span>


@endif