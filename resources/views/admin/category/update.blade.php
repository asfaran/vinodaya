<?php
define('PAGE_PARENT', 'general', true);
define('PAGE_CURRENT', 'category_edit', true);
?>
@extends('app')

@section('title', 'Category Update')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-5">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Update Category</h5>

                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                    {!! Form::open(array('url'=>'/admin/general/category/store','role'=>'form', 'class'=>'form-horizontal')) !!}
                    {!! Form::hidden('id', $category->id, array('id' => 'invisible_id')) !!}
                    
                    @if ( Session::has('flash_message') )
                        <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                                <button class="close" data-dismiss="alert"></button>
                                {{ Session::get('flash_message') }}
                        </div>
                    @endif
                    @if ( Session::has('flash_success') )
                        <div class="alert alert-success  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {{ Session::get('flash_success') }}
                        </div>
                    @endif

                        <div class="form-group"><label class="col-lg-3 control-label">Title</label>

                            <div class="col-lg-9">
                                {!! Form::text('title',$category->title,array('id'=>'form_title','class'=>'form-control','placeholder'=>'Title')) !!}
                                @if ($errors->has('title'))
                                        <span class="alert-danger">{{ $errors->first('title') }}</span><br>
                                @endif
                                <span class="help-block m-b-none">The name is how it appears on your site.</span>

                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Slug</label>

                            <div class="col-lg-9">
                            @if($category->access !== 1) 
                                 {!! Form::text('slug',$category->slug,array('id'=>'form_slug','class'=>'form-control','placeholder'=>'Slug')) !!}
                            @else
                                {!! Form::text('display_slug',$category->slug,array('id'=>'display_slug','class'=>'form-control','placeholder'=>'Slug', 'disabled' => 'disabled')) !!}
                                {!! Form::hidden('slug', $category->slug, array('id' => 'slug')) !!}
                            @endif
                                @if ($errors->has('slug'))
                                        <span class="alert-danger">{{ $errors->first('slug') }}</span><br>
                                @endif
                                <span class="help-block m-b-none">The “slug” is the URL-friendly version of the name.</span>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Category Type</label>

                            <div class="col-lg-9">
                            {!! Form::select('type', array('0'=>'Select One', '1'=>'News', '2'=>'Article', '3'=>'Image', '4'=>'Video', '5'=>'Classified' ), $category->type, ['id' => 'type', 'class' => 'form-control']) !!}
                            @if ($errors->has('type'))
                                        <span class="alert-danger">{{ $errors->first('type') }}</span><br>
                            @endif
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Parent</label>

                            <div class="col-lg-9">
                             {!! Form::select('parentid', $category_list, $category->parentid, ['id' => 'parentid', 'class' => 'form-control']) !!}
                            @if ($errors->has('parentid'))
                                        <span class="alert-danger">{{ $errors->first('parentid') }}</span><br>
                            @endif
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Description</label>

                            <div class="col-lg-9">
                                {!! Form::textarea('description',$category->description,array('id'=>'description','class'=>'form-control','placeholder'=>'Type Description')) !!}
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Qucik Link</label>

                            <div class="col-lg-9">
                            {!! Form::checkbox('quciklink', $category->display, ($category->display == 1)? true : false , ['id' => 'quciklink', 'class' => 'form-control']) !!}
                            @if ($errors->has('display'))
                                        <span class="alert-danger">{{ $errors->first('display') }}</span><br>
                            @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-8">
                                <input class="btn btn-sm btn-primary" type="submit" value="Update Category">
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-lg-7">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Category List</h5>
                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-9 m-b-xs">
                            <select id="select_category_type"  class="form-control">
                                <option value="1">News</option>
                                <option value="2">Article</option>
                                <option value="3">Image</option>
                                <option value="4">Video</option>
                                <option value="5">Classified</option>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <div class="input-group">
                             <!-- <span class="input-group-btn">
                                <button type="button" class="btn btn-sm btn-primary"> Go!</button>
                                </span> -->
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <table datatable="" class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>title</th>
                                <th>Parent</th>
                                <th colspan="2"><center>Action</center></th>
                            </tr>
                            </thead>
                            <tbody id="table_body_category_type" >
                            <tr class="gradeX">
                              <td colspan="5"><span class="alert-warning">You don't have permission to  Delete Primary Data's.</span></td>
                            </tr>
                            @foreach($category_full  as $category)
                             <tr class="gradeX">
                                <td>{{ $category->id }}</td>
                                <td>{{ $category->title }}</td>
                                <td>{{ $category->parent_title }}</td>
                                <td class="center" ><a href="{{ URL::to('/admin/general/category/edit/'.$category->id) }}" class="btn btn-sm btn-warning" type="submit">Edit</a></td>
                                @if($category->access !== 1) 
                                <td class="center"><a href="{{ URL::to('/admin/general/category/delete/'.$category->id) }}" class="btn btn-sm btn-danger" type="submit">Remove</a></td>
                                @else
                                <td class="center"><span class="btn btn-sm btn-default">Remove</span></td>
                                @endif
                            </tr>
                            @endforeach                        
                            </tbody>
                           <!--  <tfoot>
                            <tr>
                                <th>Rendering engine</th>
                                <th>Browser</th>
                                <th>Platform(s)</th>
                                <th>Engine version</th>
                                <th>CSS grade</th>
                            </tr>
                            </tfoot> -->
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
