@if(count($category_list) > 0 )
<tr class="gradeX">
  <td colspan="5"><span class="alert-warning">You don't have permission to  Delete Primary Data's.</span></td>
</tr>
     @foreach($category_list  as $category)
     <tr class="gradeX">
        <td>{{ $category->id }}</td>
        <td><a href="{{ URL::to('/admin/general/category/edit/'.$category->id) }}">{{ $category->title }}</a></td>
        <td>{{ $category->parent_title }}</td>
        <td class="center" ><a href="{{ URL::to('/admin/general/category/edit/'.$category->id) }}" class="btn btn-sm btn-warning" type="submit">Edit</a></td>
        @if($category->access !== 1)
        <td class="center"><a href="{{ URL::to('/admin/general/category/delete/'.$category->id) }}" class="btn btn-sm btn-danger" type="submit">Remove</a></td>
        @else
        <td class="center"><span class="btn btn-sm btn-default">Remove</span></td>
        @endif
     </tr>
     @endforeach 
@else
<tr class="gradeX">
    <td colspan="5">No Records For Selected Category</td>
</tr>
@endif 