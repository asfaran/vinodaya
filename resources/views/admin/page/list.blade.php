<?php
define('PAGE_PARENT', 'general', true);
define('PAGE_CURRENT', 'page_list', true);
?>
@extends('app')

@section('title', 'Pages List')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title">
    <h5>All Pages</h5>
    <!-- <div ibox-tools></div> -->
    <div class="pull-right add_new_link">
        <a class="btn btn-sm btn-w-m btn-default" href="{{ URL::to('/admin/general/page/add_new/') }}">Create New Page</a>
    </div>
</div>
<div class="ibox-content">
@if ( Session::has('flash_message') )
    <div class="alert alert-danger  {{ Session::get('flash_type') }}">
            <button class="close" data-dismiss="alert"></button>
            {{ Session::get('flash_message') }}
    </div>
@endif
@if ( Session::has('flash_success') )
    <div class="alert alert-success  {{ Session::get('flash_type') }}">
        <button class="close" data-dismiss="alert"></button>
        {{ Session::get('flash_success') }}
    </div>
@endif
<table datatable="" class="table table-striped table-bordered table-hover dataTables-example">
<thead>

<tr>
    <th>Title</th>
    <th>Tags</th>
    <th>Published Date</th>
    <th colspan="2">Action</th>
</tr>
</thead>
<tbody>
@if(count($page_list) > 0 )
    @foreach($page_list  as $page)
     <tr class="gradeX">
        <td><a href="{{ URL::to('/admin/general/page/edit/'.$page->id) }}">{{ $page->title }}</a></td>
        <td>{{ $page->tags }}</td>
        <td>{{ $page->updated_at }}</td>
        <td class="center"><a href="{{ URL::to('/admin/general/page/edit/'.$page->id) }}" class="btn btn-sm btn-warning" type="submit">Edit</a></td>
        <td class="center"><a href="{{ URL::to('/admin/general/page/delete/'.$page->id) }}" class="btn btn-sm btn-danger" type="submit">Remove</a></td>
    </tr>
    @endforeach 

@else
    <tr>
        <th colspan="8">Page's Not Created Still</th>
    </tr>
@endif
  


</tbody>
<tfoot>

</tfoot>
</table>

</div>
</div>
</div>
</div>
</div>
@endsection
