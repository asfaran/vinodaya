<?php
define('PAGE_PARENT', 'general', true);
define('PAGE_CURRENT', 'page_list', true);
?>
@extends('app')

@section('title', 'Create New Page')

@section('content')
<style type="text/css">
.ibox {
    padding: 10px;
}
</style>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    {!! Form::open(array('url'=>'/admin/general/page/store','role'=>'form', 'class'=>'form-horizontal')) !!}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
       @if ( Session::has('flash_message') )
            <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                    <button class="close" data-dismiss="alert"></button>
                    {{ Session::get('flash_message') }}
            </div>
        @endif
        @if ( Session::has('flash_success') )
            <div class="alert alert-success  {{ Session::get('flash_type') }}">
                <button class="close" data-dismiss="alert"></button>
                {{ Session::get('flash_success') }}
            </div>
        @endif
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Create New Page</h5>
                </div>
                <div class="ibox-content col-sm-12">

                    <!-- <form role="form" class="form-inline"> -->
                        <div class="form-group ">
                            <input type="text" placeholder="Enter title here" value="{{ Input::old('title')}}"  name="title" id="title" class="form-control">
                            @if ($errors->has('title'))
                                        <span class="alert-danger">{{ $errors->first('title') }}</span><br>
                            @endif
                        </div>
                   <!--  </form> -->
                </div>
            </div>

            <div class="ibox">
                <div class="ibox-content col-sm-12">
                    <div class="ibox-content no-padding">
                    @if ($errors->has('body'))
                                        <span class="alert-danger">{{ $errors->first('body') }}</span><br>
                    @endif
                  
                    <textarea class="input-block-level" id="summernote" name="body" rows="18">{{ Input::old('body')}}</textarea>

                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-content col-sm-12" >

                    <div class="form-group">
                        <div class="col-lg-12">
                            <textarea id="hero-demo" name="tags" class="form-control" placeholder="Type Tags Here">{{ Input::old('tags')}}</textarea>   
                                                     
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <center><input class="btn btn-sm btn-w-m btn-success"  name="submit_button" type="submit" value="Publish The Page"></center>
                            
                        </div>
                    </div>
                </div>

            </div>

        </div>
    {!! Form::close() !!}
    <!-- form end -->
    </div>
</div>
@endsection
