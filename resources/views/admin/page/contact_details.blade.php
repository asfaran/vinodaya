<?php
define('PAGE_PARENT', 'general', true);
define('PAGE_CURRENT', 'page_contact', true);
?>
@extends('app')

@section('title', 'Contact Page Configuration')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
         {!! Form::open(array('url'=>'/admin/general/page/contact_store','role'=>'form', 'class'=>'form-horizontal')) !!}
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Contact Page Configuration</h5>

                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                    

                    @if ( Session::has('flash_message') )
                        <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                                <button class="close" data-dismiss="alert"></button>
                                {{ Session::get('flash_message') }}
                        </div>
                    @endif
                    @if ( Session::has('flash_success') )
                        <div class="alert alert-success  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {{ Session::get('flash_success') }}
                        </div>
                    @endif          
                </div>
            </div>



            <div class="ibox float-e-margins">
                <div class="ibox-content col-sm-12">
                     <div class="form-group">
                            <label class="col-lg-3 control-label">Contact Page Description</label>
                            <div class="col-lg-9">
                                <textarea placeholder="Contact Page Description"  class="form-control numeric"  name="contact_page_description">{!! (isset($option_list['contact_page_description']))? $option_list['contact_page_description'] : '' !!}</textarea>
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-lg-3 control-label">Contact Email</label>
                            <div class="col-lg-9">
                                <input type="text"  value="{{ (isset($option_list['contact_page_mail']))? $option_list['contact_page_mail'] : ''  }}" placeholder="Mailing Address" class="form-control" name="contact_page_mail">
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-lg-3 control-label">Mailing Address</label>
                            <div class="col-lg-9">
                                <textarea placeholder="Mailing Address"  class="form-control"  name="contact_page_address">{!! (isset($option_list['contact_page_address']))? $option_list['contact_page_address'] : '' !!}</textarea>
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-lg-3 control-label">Contact Info</label>
                            <div class="col-lg-9">
                                <textarea placeholder="Contact Info"  class="form-control"  name="contact_page_info">{!! (isset($option_list['contact_page_info']))? $option_list['contact_page_info'] : '' !!}</textarea>
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-lg-3 control-label">Map Latitude</label>
                            <div class="col-lg-9">
                                <input type="text"  value="{{ (isset($option_list['contact_page_latitude']))? $option_list['contact_page_latitude'] : ''  }}" placeholder="Map Latitude" class="form-control" name="contact_page_latitude">
                            </div>
                    </div>

                    <div class="form-group">
                            <label class="col-lg-3 control-label">Map Longitude</label>
                            <div class="col-lg-9">
                                <input type="text"  value="{{ (isset($option_list['contact_page_longtitude']))? $option_list['contact_page_longtitude'] : ''  }}" placeholder="Map Longitude" class="form-control" name="contact_page_longtitude">
                            </div>
                    </div>
                </div>
            </div>


            <div class="ibox">
                <div class="ibox-content col-sm-12">
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-8">
                            <input class="btn btn-sm btn-primary" type="submit" value="Update Page Configuration">
                        </div>
                    </div>
                </div>
            </div>


            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
