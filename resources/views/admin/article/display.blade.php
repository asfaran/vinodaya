<?php
define('PAGE_PARENT', 'article', true);
define('PAGE_CURRENT', 'article_display', true);
?>
@extends('app')

@section('title', 'Front View Configuration')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
        {!! Form::open(array('url'=>'/admin/article/display_store','role'=>'form', 'class'=>'form-horizontal')) !!}
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Article Front View Configuration</h5>

                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                    

                    @if ( Session::has('flash_message') )
                        <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                                <button class="close" data-dismiss="alert"></button>
                                {{ Session::get('flash_message') }}
                        </div>
                    @endif
                    @if ( Session::has('flash_success') )
                        <div class="alert alert-success  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {{ Session::get('flash_success') }}
                        </div>
                    @endif          
                </div>
            </div>



            <div class="ibox float-e-margins">
                <div class="ibox-content col-sm-12">
                     <div class="form-group">
                            <label class="col-lg-3 control-label">Number of Articles in listing page</label>
                            <div class="col-lg-9">
                                <input type="text"  
                                value="{{ (isset($option_list['article_count_in_listing']))? $option_list['article_count_in_listing'] : '' }}" placeholder="Enter Count number"  class="form-control numeric"  name="article_count_in_listing">
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-lg-3 control-label">Number of Articles in Front page</label>
                            <div class="col-lg-9">
                                <input type="text"  value="{{ (isset($option_list['article_count_in_front']))? $option_list['article_count_in_front'] : ''  }}" 
                                placeholder="Enter Count number" class="form-control  numeric" name="article_count_in_front">
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-lg-3 control-label">Featured Article Count</label>
                            <div class="col-lg-9">
                                <input type="text"  value="{{ (isset($option_list['featured_article_count']))? $option_list['featured_article_count'] : ''  }}" placeholder="Enter amount Here" class="form-control numeric" name="featured_article_count">
                            </div>
                    </div>
                </div>
            </div>


            <div class="ibox">
                <div class="ibox-content col-sm-12">
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-8">
                            <input class="btn btn-sm btn-primary" type="submit" value="Update View Configuration">
                        </div>
                    </div>
                </div>
            </div>


            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
