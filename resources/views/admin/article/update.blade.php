<?php
define('PAGE_PARENT', 'article', true);
define('PAGE_CURRENT', 'article_add_new', true);
?>
@extends('app')

@section('title', 'Update a Article')

@section('content')
<style type="text/css">
.ibox {
    padding: 10px;
}
</style>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    {!! Form::open(array('url'=>'/admin/article/store', 'files' => true, 'role'=>'form', 'class'=>'form-horizontal')) !!}
    {!! Form::hidden('id', $article_one->id, array('id' => 'invisible_id')) !!}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
       @if ( Session::has('flash_message') )
            <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                    <button class="close" data-dismiss="alert"></button>
                    {{ Session::get('flash_message') }}
            </div>
        @endif
        @if ( Session::has('flash_success') )
            <div class="alert alert-success  {{ Session::get('flash_type') }}">
                <button class="close" data-dismiss="alert"></button>
                {{ Session::get('flash_success') }}
            </div>
        @endif
        <div class="col-lg-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Post New Article</h5>
                </div>
                <div class="ibox-content col-sm-12 " >

                    <!-- <form role="form" class="form-inline"> -->
                        <div class="form-group">
                            &nbsp;{!! Form::text('title',$article_one->title,array('id'=>'title','class'=>'form-control','placeholder'=>'Enter title here')) !!}
                            <div class="hr-line-dashed"></div>

                            <div class="col-lg-10">
                                   &nbsp;Permalink: {{ url('/') }}/article/<span id="post_slug_span" style="font-weight: bold;">{{ $article_one->slug }}</span><div id="post_slug_div" style="display: none;"><input type="text" name="post_slug" id="post_slug_value" value="{{ $article_one->slug }}" ></div> &nbsp;
                                    <a href=""  id="post_slug" class="post_slug_edit">Edit</a>
                            </div>

                            <br>

                            <div class="form-group" id="div_post_slug" style="display: none;">
                                <div data-id="input">
                                    
                                    &nbsp;
                                    <button class="btn  btn-sm  btn-default post_slug_selection" type="button">OK</button>
                                    &nbsp;<a href="" class="post_slug_close">Cancel</a>
                                </div>
                            </div>
                           
                        </div>
                   <!--  </form> -->
                </div>
            </div>

            <div class="ibox">
                <div class="ibox-content col-sm-12">
                    <div class="ibox-content no-padding">

                  {!! Form::textarea('body',$article_one->body,array('id'=>'summernote', 'class'=>'input-block-level', 'placeholder'=>'Your Content Here', 'rows'=>'18')) !!}

                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Publish & Config</h5>
                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                     <input type="hidden" name="post_status" id="post_status_value" value="{{  $status_array[$article_one->status]  }}" >
                     <input type="hidden" name="post_featured" id="post_featured_value" value="{{ ($article_one->featured == 1)? 'Yes' : 'No'  }}" >
                     <input type="hidden" name="post_visibility" id="post_visibility_value" value="{{ ($article_one->visibility == 1)? 'Public' : 'Private'  }}" >
                     <input type="hidden" name="post_publish" id="post_publish_value" value="Immediately" >

                        <div class="form-group">

                            <div class="col-lg-6">
                             @if($article_one->status == 0 || $article_one->status == 1)

                                <input class="btn  btn-sm btn-outline btn-default"  name="submit_draft" type="submit" value="Save Draft">
                             @endif
                            </div>
                            <div class="col-lg-6">
                                <button class="btn btn-sm btn-outline btn-default" 
                                        type="button" 
                                        alt="Button Will Work Once Front End Done">Preview
                                </button>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-2">
                                <i class="fa fa-key"></i>
                            </div>
                            <div class="col-lg-10">
                                Status: &nbsp;<span  style="font-weight: bold;">{{   $status_array[$article_one->status]  }}</span> &nbsp;
                                <a href=""  id="post_status" class="post_edit">Edit</a>
                            </div>
                        </div>
                        <div class="form-group" id="div_post_status" style="display: none;">
                            <div class="col-lg-2">
                            </div>
                            <div class="col-lg-10" data-id="select">
                                <select class=" form-control  ">
                                    <option value="Draft">Draft</option>
                                    <option value="Pending Review">Pending Review</option>
                                    @if($article_one->status == 2 )
                                        <option  selected="selected" value="Published">Published</option>
                                    @endif
                                </select>
                                &nbsp;
                                <button class="btn  btn-sm  btn-default post_selection" type="button">OK</button>
                                &nbsp;<a href="" class="post_edit_close">Cancel</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-2">
                                <i class="fa fa-star"></i>
                            </div>
                            <div class="col-lg-10">
                                Featured: &nbsp;<span  style="font-weight: bold;">{{ ($article_one->featured == 1)? 'Yes' : 'No'  }}</span> &nbsp;
                                <a href="" id="post_featured" class="post_edit">Edit</a>
                            </div>
                        </div>
                        <div class="form-group" id="div_post_featured" style="display: none;">
                            <div class="col-lg-2">
                            </div>
                            <div class="col-lg-10" data-id="select">
                                <select class=" form-control ">
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                                &nbsp;
                                <button class="btn  btn-sm  btn-default post_selection" type="button">OK</button>
                                &nbsp;<a href="" class="post_edit_close">Cancel</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-2">
                                <i class="fa fa-eye"></i>
                            </div>
                            <div class="col-lg-10">
                                Visibility: &nbsp;<span  style="font-weight: bold;">{{ ($article_one->visibility == 1)? 'Public' : 'Private'  }}</span> &nbsp;
                                <a href="" id="post_visibility"  class="post_edit">Edit</a>
                            </div>
                        </div>
                        <div class="form-group"  style="display: none;">
                            <div class="col-lg-2">
                            </div>
                            <div class="col-lg-10 radio" id="div_post_visibility" >
                                <div class="radio" data-id="radio">
                                            <input type="radio" name="radio1" id="radio1" value="Public" checked="">
                                            <label for="radio1">
                                                Public
                                            </label>
                                            <br>
                                            <input type="radio" name="radio1"   id="radio2" value="Private">
                                            <label for="radio2">
                                                Private
                                            </label>
                                            <br>
                                            &nbsp;
                                            <button class="btn  btn-sm  btn-default post_selection" type="button">OK</button>
                                            &nbsp;<a href="" class="post_edit_close">Cancel</a>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <div class="col-lg-2">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <div class="col-lg-10">
                                Publish: &nbsp;<span  style="font-weight: bold;">Immediately</span> &nbsp;
                                <a href="" id="post_publish" class="post_edit">Edit</a>
                            </div>
                        </div> -->
                        <div class="form-group" style="display: none;">
                            <div class="col-lg-2">
                            </div>
                            <div class="col-lg-10"  id="div_post_publish">
                                <div class="input-group date" data-id="input">
                                    <input type="datetime" class="form-control " date-time ng-model="sampleDate" view="date" auto-close="true">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <br>
                                    &nbsp;
                                    <button class="btn  btn-sm  btn-default post_selection" type="button">OK</button>
                                    &nbsp;<a href=""  class="post_edit_close">Cancel</a>
                                </div>
                                
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-lg-6">
                                <a href="{{ URL::to('/admin/article/delete/'.$article_one->id) }}">Move to Trash</a>
                            </div>
                            <div class="col-lg-6">
                                <input class="btn btn-sm btn-w-m btn-success"  name="submit_button" type="submit" value="Publish">
                            </div>
                        </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Categories</h5>
                    <div ibox-tools></div>
                </div>
                <div class="ibox-content" >
                    @if($category_list)

                        @foreach($category_list  as $category)
                        <div class="form-group">
                            <div class="col-lg-2">
                               <input
                               
                               @if(in_array($category->id, array_values($selected_category)))
                                checked="checked"
                                @endif
                               type="checkbox" 
                               name="category[]"  
                               value="{{ $category->id }}" >
                            </div>
                            <div class="col-lg-10">
                                {{ $category->title }}
                            </div>
                        </div>
                        @endforeach
                    @else
                    <div class="form-group">
                            <div class="col-lg-12">
                                No categories added.
                            </div>
                    </div>
                    <div class="form-group">
                            <div class="col-lg-2">
                               <i class="fa fa-eyedropper"></i>
                            </div>
                            <div class="col-lg-10">
                                <a href="{{ URL::to('/admin/general/category/list') }}">Create New Category</a>
                            </div>
                    </div>
                    @endif   
                          
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Tags</h5>
                    <div ibox-tools></div>
                </div>
                <div class="ibox-content" >

                        <div class="form-group">
                            <div class="col-lg-12" ng-controller="MainCtrl">
                                <textarea id="hero-demo" name="tags"  placeholder="Type Tags Here">{{ $article_one->tags }}</textarea>
                                
                            </div>
                        </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Featured Image</h5>
                    <div ibox-tools></div>
                </div>
                <div class="ibox-content" >
                    <form class="form-horizontal">

                        <div class="form-group">
                            <div class="col-lg-12">
                            @if($article_one->thumb)
                            <img alt="image" class="img" width="100%" src="{{ url('/uploads/article/'.$article_one->thumb) }}"/>
                            @else
                            <a href="">No Featured Image</a>
                            @endif
                            <div class="fallback">
                                <input name="thumb" type="file" multiple />
                            </div>

                            <div class="dropzone-previews" id="dropzonePreview"></div>
                            <!-- <h4 style="text-align: center;color:#428bca;">Drop images in this area  <span class="glyphicon glyphicon-hand-down"></span></h4> -->
                                @if ($errors->has('thumb'))
                                        <span class="alert-danger">{{ $errors->first('title') }}</span><br>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
    <!-- form end -->
    </div>
</div>
@endsection
