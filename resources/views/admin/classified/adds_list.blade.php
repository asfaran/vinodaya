<?php
define('PAGE_PARENT', 'adds', true);
define('PAGE_CURRENT', 'adds_list', true);
?>
@extends('app')

@section('title', 'Adds Lists')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title">
    <h5>All Advertisment Lists</h5>
    <!-- <div ibox-tools></div> -->
</div>
<div class="ibox-content">
@if ( Session::has('flash_message') )
    <div class="alert alert-danger  {{ Session::get('flash_type') }}">
            <button class="close" data-dismiss="alert"></button>
            {{ Session::get('flash_message') }}
    </div>
@endif
@if ( Session::has('flash_success') )
    <div class="alert alert-success  {{ Session::get('flash_type') }}">
        <button class="close" data-dismiss="alert"></button>
        {{ Session::get('flash_success') }}
    </div>
@endif
<table datatable="" class="table table-striped table-bordered table-hover dataTables-example">
<thead>

<tr>
    <th>Title</th>
    <th>Image</th>
    <th>User Name</th>
    <th>Add Type</th>
    <th>Price</th>
    <th>Created At</th>
    <th>Approve</th>
    <th >Action</th>
</tr>
</thead>
<tbody>


@if(count($adds_list) > 0 )
    @foreach($adds_list  as $adds)
     <tr class="gradeX">
        
        <?php  /*<td><a target="_blank" href="{{ URL::to('/admin/classifboostsied/edit/'.$boosts->id) }}">{{ $classified->title }}</a></td> */?>
        <td>{{ $adds->adds_title }}</td>
        <td><a target="_blank"  href="{{ url('/uploads/classifieds/adds/real_image/'.$adds->thumb_real) }}">
            <img alt="image" class="img" width="100px" height="50px" src="{{ url('/uploads/classifieds/adds/real_image/'.$adds->thumb_real) }}"/>
            </a></td>

        <td>{{ $adds->username }}</td>
        <td>{!! $adds_types_array[$adds->adds_type]   !!}</td>
        <td>{{ $adds->price }}</td>
        <td>{{ $adds->created_at }}</td>
        <td>
            @if($adds->approve == 0)
            <a class="btn btn-sm btn-warning" href="{{ URL::to('/admin/classified/adds/change_status/'.$adds->id) }}">
              Not Approved
            </a> 
            @else
            <a class="btn btn-sm btn-success" href="{{ URL::to('/admin/classified/adds/change_status/'.$adds->id) }}">
              Approved
            </a> 
            @endif
        </td>
        <?php /*<td class="center"><a href="{{ URL::to('/admin/classified/edit/'.$boosts->id) }}" class="btn btn-sm btn-warning" type="submit">Edit</a></td>*/ ?>
        <td class="center"><a href="{{ URL::to('/admin/classified/adds/delete/'.$adds->id) }}" class="btn btn-sm btn-danger" type="submit">Remove</a></td>
    </tr>
    @endforeach 

@else
    <tr>
        <th colspan="10">No advertisment found, Once addvertisment done listing will shows</th>
    </tr>
@endif
  
<!-- <tr class="gradeX">
    <td>Trident</td>
    <td>Internet</td>
    <td>Win 95+</td>
    <td class="center">4</td>
    <td class="center">X</td>
</tr> -->

</tbody>
<tfoot>
<!-- <tr>
    <th>Rendering engine</th>
    <th>Browser</th>
    <th>Platform(s)</th>
    <th>Engine version</th>
    <th>CSS grade</th>
</tr> -->
</tfoot>
</table>

</div>
</div>
</div>
</div>
</div>
@endsection
