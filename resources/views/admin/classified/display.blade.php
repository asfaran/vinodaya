<?php
define('PAGE_PARENT', 'classified', true);
define('PAGE_CURRENT', 'classified_display', true);
?>
@extends('app')

@section('title', 'Front View Configuration')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
        {!! Form::open(array('url'=>'/admin/classified/display_store','role'=>'form', 'class'=>'form-horizontal')) !!}
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Classified Front View Configuration</h5>

                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                    

                    @if ( Session::has('flash_message') )
                        <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                                <button class="close" data-dismiss="alert"></button>
                                {{ Session::get('flash_message') }}
                        </div>
                    @endif
                    @if ( Session::has('flash_success') )
                        <div class="alert alert-success  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {{ Session::get('flash_success') }}
                        </div>
                    @endif          
                </div>
            </div>



            <div class="ibox float-e-margins">
                <div class="ibox-content col-sm-12">
                     <div class="form-group">
                            <label class="col-lg-3 control-label">Number of Posts in listing page</label>
                            <div class="col-lg-9">
                                <input type="text"  
                                value="{{ (isset($option_list['classified_count_in_listing']))? $option_list['classified_count_in_listing'] : '' }}" placeholder="Enter Count number"  class="form-control numeric"  name="classified_count_in_listing">
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-lg-3 control-label">Number of Posts in Front page</label>
                            <div class="col-lg-9">
                                <input type="text"  value="{{ (isset($option_list['classified_count_in_front']))? $option_list['classified_count_in_front'] : ''  }}" 
                                placeholder="Enter Count number" class="form-control  numeric" name="classified_count_in_front">
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-lg-3 control-label">Featured Posts Count</label>
                            <div class="col-lg-9">
                                <input type="text"  value="{{ (isset($option_list['classified_featured_post_in_listing']))? $option_list['classified_featured_post_in_listing'] : ''  }}" placeholder="Enter amount Here" class="form-control numeric" name="classified_featured_post_in_listing">
                            </div>
                    </div>

                    <div class="form-group">
                            <label class="col-lg-3 control-label">Top Adds  Count In listing</label>
                            <div class="col-lg-9">
                                <input type="text"  value="{{ (isset($option_list['classified_top_ads_in_listing']))? $option_list['classified_top_ads_in_listing'] : ''  }}" placeholder="Enter amount Here" class="form-control numeric" name="classified_top_ads_in_listing">
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-lg-3 control-label">Featured ads  Count In slider</label>
                            <div class="col-lg-9">
                                <input type="text"  value="{{ (isset($option_list['classified_featured_ads_in_slider']))? $option_list['classified_featured_ads_in_slider'] : ''  }}" placeholder="Enter amount Here" class="form-control numeric" name="classified_featured_ads_in_slider">
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-lg-3 control-label">Related Product Count</label>
                            <div class="col-lg-9">
                                <input type="text"  value="{{ (isset($option_list['classified_related_products_count']))? $option_list['classified_related_products_count'] : ''  }}" placeholder="Enter amount Here" class="form-control numeric" name="classified_related_products_count">
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-lg-3 control-label">Top ads  Count In slider</label>
                            <div class="col-lg-9">
                                <input type="text"  value="{{ (isset($option_list['classified_top_ads_in_slider']))? $option_list['classified_top_ads_in_slider'] : ''  }}" placeholder="Enter amount Here" class="form-control numeric" name="classified_top_ads_in_slider">
                            </div>
                    </div>
                </div>
            </div>


            <div class="ibox">
                <div class="ibox-content col-sm-12">
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-8">
                            <input class="btn btn-sm btn-primary" type="submit" value="Update View Configuration">
                        </div>
                    </div>
                </div>
            </div>


            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
