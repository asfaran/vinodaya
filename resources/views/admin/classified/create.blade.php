<?php
define('PAGE_PARENT', 'classified', true);
define('PAGE_CURRENT', 'classified_add_new', true);
?>
@extends('app')

@section('title', 'Post New Classified')

@section('content')
<style type="text/css">
.ibox {
    padding: 10px;
}
</style>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    {!! Form::open(array('url'=>'/admin/classified/store','role'=>'form', 'files' => true, 'class'=>'form-horizontal')) !!}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
       @if ( Session::has('flash_message') )
            <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                    <button class="close" data-dismiss="alert"></button>
                    {{ Session::get('flash_message') }}
            </div>
        @endif
        @if ( Session::has('flash_success') )
            <div class="alert alert-success  {{ Session::get('flash_type') }}">
                <button class="close" data-dismiss="alert"></button>
                {{ Session::get('flash_success') }}
            </div>
        @endif
        <div class="col-lg-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Post New Classified</h5>
                </div>
                <div class="ibox-content col-sm-12">

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Title</label>
                        <div class="col-lg-9">
                        {!! Form::text('title', null , ['id' => 'title', 'class' => 'form-control']) !!}
                        @if ($errors->has('title'))
                                    <span class="alert-danger">{{ $errors->first('title') }}</span><br>
                        @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Province</label>
                        <div class="col-lg-9">
                        {!! Form::select('location_province', $location['province'], null, ['id' => 'location_province', 'class' => 'classified_location_selection form-control']) !!}
                        @if ($errors->has('location_province'))
                                    <span class="alert-danger">{{ $errors->first('location_province') }}</span><br>
                        @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label">City</label>
                        <div class="col-lg-9">
                            <div id="div_location_city">
                                {!! Form::select('location_city', $location['city'], null, ['id' => 'location_city', 'class' => 'classified_location_selection form-control']) !!}
                            </div>
                            @if ($errors->has('location_city'))
                                        <span class="alert-danger">{{ $errors->first('location_city') }}</span><br>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Town</label>
                        <div class="col-lg-9">
                            <div id="div_location_town">
                                {!! Form::select('location_town', $location['town'], null, ['id' => 'location_town', 'class' => 'classified_location_selection form-control']) !!}
                            </div>
                            @if ($errors->has('location_town'))
                                        <span class="alert-danger">{{ $errors->first('location_town') }}</span><br>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Building</label>
                        <div class="col-lg-9">
                            <div id="div_location_building">
                                {!! Form::select('location_building', $location['building'], null, ['id' => 'location_building', 'class' => 'form-control']) !!}
                            </div>
                            @if ($errors->has('location_building'))
                                        <span class="alert-danger">{{ $errors->first('location_building') }}</span><br>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Price</label>
                        <div class="col-lg-9">
                        {!! Form::text('price', null , ['id' => 'price', 'class' => 'form-control']) !!}
                        @if ($errors->has('price'))
                                    <span class="alert-danger">{{ $errors->first('price') }}</span><br>
                        @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Categories</h5>
                    <div ibox-tools></div>
                </div>
                <div class="ibox-content" >
                    @if($category_list)
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Main</label>
                            <div class="col-lg-9">
                                {!! Form::select('parentid', $category_list, null, 
                                ['id' => 'classified_parentid', 'class' => 'classified_add_form_selection form-control']) !!}
                                @if ($errors->has('parentid'))
                                        <span class="alert-danger">{{ $errors->first('parentid') }}</span><br>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Sub Categories</label>
                            <div class="col-lg-9" id="classified_category">
                               {!! Form::select('category', $category_list, null, ['id' => 'category', 'class' => 'classified_add_form_selection form-control']) !!}
                                @if ($errors->has('category'))
                                            <span class="alert-danger">{{ $errors->first('category') }}</span><br>
                                @endif
                            </div>
                        </div>
                    @else
                    <div class="form-group">
                            <div class="col-lg-12">
                                No categories added.
                            </div>
                    </div>
                    <div class="form-group">
                            <div class="col-lg-2">
                               <i class="fa fa-eyedropper"></i>
                            </div>
                            <div class="col-lg-10">
                                <a href="{{ URL::to('/admin/general/category/list') }}">Create New Category</a>
                            </div>
                    </div>
                    @endif          
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Extra Form Fields</h5>
                </div>
                <div class="ibox-content col-sm-12">
                    <div id="classified_add_form_fields_rows"></div>
                </div>
            </div>

            <div class="ibox">
                <div class="ibox-title">
                    <h5>Product Description</h5>
                </div>
                <div class="ibox-content col-sm-12">
                    <div class="ibox-content no-padding">
                    @if ($errors->has('description'))
                                        <span class="alert-danger">{{ $errors->first('description') }}</span><br>
                    @endif
                    <center>
                        <textarea class="input-block-level" id="description" name="description" rows="6"  cols="70">{{ Input::old('description')}}</textarea>
                    </center>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Publish & Config</h5>
                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                     <input type="hidden" name="post_status" id="post_status_value" value="Draft" >
                     <input type="hidden" name="post_featured" id="post_featured_value" value="Yes" >
                     <input type="hidden" name="post_visibility" id="post_visibility_value" value="Public" >
                     <input type="hidden" name="post_publish" id="post_publish_value" value="Immediately" >

                        <div class="form-group">
                            <div class="col-lg-6">
                                <input class="btn  btn-sm btn-outline btn-default"  name="submit_draft" type="submit" value="Save Draft">
                            </div>
                            <!--<div class="col-lg-6">
                                <button class="btn btn-sm btn-outline btn-default" type="button" 
                                alt="Button Will Work Once Front End Done">Preview</button>
                            </div>-->
                        </div>
                        <div class="form-group">
                            <div class="col-lg-2">
                                <i class="fa fa-key"></i>
                            </div>
                            <div class="col-lg-10">
                                Status: &nbsp;<span  style="font-weight: bold;">Draft</span> &nbsp;
                                <a href=""  id="post_status" class="post_edit">Edit</a>
                            </div>
                        </div>
                        <div class="form-group" id="div_post_status" style="display: none;">
                            <div class="col-lg-2">
                            </div>
                            <div class="col-lg-10" data-id="select">
                                <select class=" form-control  ">
                                    <option value="Draft">Draft</option>
                                    <option value="Pending Review">Pending Review</option>
                                </select>
                                &nbsp;
                                <button class="btn  btn-sm  btn-default post_selection" type="button">OK</button>
                                &nbsp;<a href="" class="post_edit_close">Cancel</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-2">
                                <i class="fa fa-star"></i>
                            </div>
                            <div class="col-lg-10">
                                Featured: &nbsp;<span  style="font-weight: bold;">Yes</span> &nbsp;
                                <a href="" id="post_featured" class="post_edit">Edit</a>
                            </div>
                        </div>
                        <div class="form-group" id="div_post_featured" style="display: none;">
                            <div class="col-lg-2">
                            </div>
                            <div class="col-lg-10"  data-id="select">
                                <select class=" form-control ">
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                                &nbsp;
                                <button class="btn  btn-sm  btn-default post_selection" type="button">OK</button>
                                &nbsp;<a href="" class="post_edit_close">Cancel</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-2">
                                <i class="fa fa-eye"></i>
                            </div>
                            <div class="col-lg-10">
                                Visibility: &nbsp;<span  style="font-weight: bold;">Public</span> &nbsp;
                                <a href="" id="post_visibility"  class="post_edit">Edit</a>
                            </div>
                        </div>
                        <div class="form-group"  style="display: none;">
                            <div class="col-lg-2">
                            </div>
                            <div class="col-lg-10 radio" id="div_post_visibility" >
                                <div class="radio" data-id="radio">
                                            <input type="radio" name="radio1" id="radio1" value="Public" checked="">
                                            <label for="radio1">
                                                Public
                                            </label>
                                            <br>
                                            <input type="radio" name="radio1"   id="radio2" value="Private">
                                            <label for="radio2">
                                                Private
                                            </label>
                                            <br>
                                            &nbsp;
                                            <button class="btn  btn-sm  btn-default post_selection" type="button">OK</button>
                                            &nbsp;<a href="" class="post_edit_close">Cancel</a>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <div class="col-lg-2">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <div class="col-lg-10">
                                Publish: &nbsp;<span  style="font-weight: bold;">Immediately</span> &nbsp;
                                <a href="" id="post_publish" class="post_edit">Edit</a>
                            </div>
                        </div> -->
                        <div class="form-group" style="display: none;">
                            <div class="col-lg-2">
                            </div>
                            <div class="col-lg-10"  id="div_post_publish">
                                <div class="input-group date" data-id="input">
                                    <input type="datetime" class="form-control " date-time ng-model="sampleDate" view="date" auto-close="true">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <br>
                                    &nbsp;
                                    <button class="btn  btn-sm  btn-default post_selection" type="button">OK</button>
                                    &nbsp;<a href=""  class="post_edit_close">Cancel</a>
                                </div>
                                
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-lg-6">
                            </div>
                            <div class="col-lg-6">
                                <input class="btn btn-sm btn-w-m btn-success"  name="submit_button" type="submit" value="Publish">
                            </div>
                        </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Tags</h5>
                    <div ibox-tools></div>
                </div>
                <div class="ibox-content" >

                        <div class="form-group">
                            <div class="col-lg-12">
                             <textarea id="hero-demo" name="tags" placeholder="Type Tags Here"></textarea>                                
                            </div>
                        </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Featured Image</h5>
                    <div ibox-tools></div>
                </div>
                <div class="ibox-content" >
                    <form class="form-horizontal">

                        <div class="form-group">
                            <div class="col-lg-12">
                            <div class="fallback">
                                <input name="thumb" type="file" multiple />
                            </div>

                            <!-- <div class="dropzone-previews" id="dropzonePreview"></div>
                            <h4 style="text-align: center;color:#428bca;">Drop images in this area  <span class="glyphicon glyphicon-hand-down"></span></h4>
                                <a href="">Set featured image</a> -->
                                @if ($errors->has('thumb'))
                                        <span class="alert-danger">{{ $errors->first('title') }}</span><br>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
    <!-- form end -->

    
    </div>
</div>
@endsection

