<?php
define('PAGE_PARENT', 'adds', true);
define('PAGE_CURRENT', 'adds_settings', true);
?>
@extends('app')

@section('title', 'Addvertisment Types')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-5">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Addvertisment Types</h5>

                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                    {!! Form::open(array('url'=>'/admin/classified/adds/store_settings', 'files' => true,'role'=>'form', 'class'=>'form-horizontal')) !!}

                    @if ( Session::has('flash_message') )
                        <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                                <button class="close" data-dismiss="alert"></button>
                                {{ Session::get('flash_message') }}
                        </div>
                    @endif
                    @if ( Session::has('flash_success') )
                        <div class="alert alert-success  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {{ Session::get('flash_success') }}
                        </div>
                    @endif

                        <div class="form-group"><label class="col-lg-3 control-label">Title</label>

                            <div class="col-lg-9">
                                <input type="text" placeholder="title" class="form-control"  value="{{ Input::old('title')}}" id="title" name="title">
                                @if ($errors->has('title'))
                                        <span class="alert-danger">{{ $errors->first('title') }}</span><br>
                                @endif
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Position Name</label>

                            <div class="col-lg-9">
                                <input type="text" placeholder="position name" class="form-control" value="{{ Input::old('position_name')}}" id="position_name"  name="position_name">
                                @if ($errors->has('position_name'))
                                        <span class="alert-danger">{{ $errors->first('position_name') }}</span><br>
                                @endif
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Price Type</label>

                            <div class="col-lg-9">
                            {!! Form::select('price_type', array(''=>'Select One', '1'=>'Hour', '2'=>'Day'), Input::old('price_type') , ['id' => 'price_type', 'class' => 'form-control']) !!}
                            @if ($errors->has('price_type'))
                                        <span class="alert-danger">{{ $errors->first('price_type') }}</span><br>
                            @endif
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Price</label>

                            <div class="col-lg-9">
                                <input type="text" placeholder="Enter Price" class="form-control" value="{{ Input::old('price')}}" id="price"  name="price">
                                @if ($errors->has('price'))
                                        <span class="alert-danger">{{ $errors->first('price') }}</span><br>
                                @endif
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Addvertisment Width Size(Px)</label>

                            <div class="col-lg-9">
                                <input type="text" placeholder="Enter Addvertisment Width Size" class="form-control" value="{{ Input::old('width')}}" id="width"  name="width">
                                @if ($errors->has('width'))
                                        <span class="alert-danger">{{ $errors->first('width') }}</span><br>
                                @endif
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Addvertisment Height Size(Px)</label>

                            <div class="col-lg-9">
                                <input type="text" placeholder="Enter Addvertisment Height Size" class="form-control" value="{{ Input::old('height')}}" id="height"  name="height">
                                @if ($errors->has('height'))
                                        <span class="alert-danger">{{ $errors->first('height') }}</span><br>
                                @endif
                            </div>
                        </div>
                        
                        
                        <div class="form-group"><label class="col-lg-3 control-label">Guide Image For End User</label>

                            <div class="col-lg-9">
                            <div class="fallback">
                                <input name="thumb" type="file" multiple />
                            </div>
                            @if ($errors->has('thumb'))
                                        <span class="alert-danger">{{ $errors->first('thumb') }}</span><br>
                            @endif
                            <div class="dropzone-previews" id="dropzonePreview"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-8">
                                <input class="btn btn-sm btn-primary" type="submit" value="Add Addvertisment Type">
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-lg-7">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Addvertisment Types List</h5>
                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <table datatable="" class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Position Name</th>
                                <th>Price Type</th>
                                <th>Price</th>
                                <th colspan="2"><center>Action</center></th>
                            </tr>
                            </thead>
                            <tbody id="table_body_category_type">
                            @if(count($adds_types) > 0 )
                            <tr class="gradeX">
                              <td colspan="7"><span class="alert-warning">You don't have permission to  Delete Primary Data's.</span></td>
                            </tr>
                             @foreach($adds_types  as $types)
                             <tr class="gradeX">
                                <td>{{ $types->id }}</td>
                                <td>
                                    <a href="{{ URL::to('/admin/classified/adds/edit_setting/'.$types->id) }}">{{ $types->title }}</a>
                                </td>
                                <td>{{ $types->position_name }}</td>
                                <td>{{ $price_type_array[$types->price_type] }}</td>
                                <td>{{ $types->price }}</td>
                                <td class="center"><a href="{{ URL::to('/admin/classified/adds/edit_setting/'.$types->id) }}" class="btn btn-sm btn-warning" type="submit">Edit</a></td>
                                @if($types->status !== 1) 
                                <td class="center"><a href="{{ URL::to('/admin/classified/adds/delete_setting/'.$types->id) }}" class="btn btn-sm btn-danger" type="submit">Remove</a></td>
                                @else
                                <td class="center"><span class="btn btn-sm btn-default">Remove</span></td>
                                @endif
                            </tr>
                            @endforeach 
                            @else
                            <tr class="gradeX">
                                <td colspan="5">No Records Added</td>
                            </tr>
                            @endif                         
                            </tbody>
                           <!--  <tfoot>
                            <tr>
                                <th>Rendering engine</th>
                                <th>Browser</th>
                                <th>Platform(s)</th>
                                <th>Engine version</th>
                                <th>CSS grade</th>
                            </tr>
                            </tfoot> -->
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
