<?php
define('PAGE_PARENT', 'classified', true);
define('PAGE_CURRENT', 'classified_list', true);
?>
@extends('app')

@section('title', 'Classified List')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title">
    <h5>All Classified Posts</h5>
    <!-- <div ibox-tools></div> -->
    <div class="pull-right add_new_link">
        <a class="btn btn-sm btn-w-m btn-default" href="{{ URL::to('/admin/classified/add_new/') }}">Post New Classified</a>
    </div>
</div>
<div class="ibox-content">
@if ( Session::has('flash_message') )
    <div class="alert alert-danger  {{ Session::get('flash_type') }}">
            <button class="close" data-dismiss="alert"></button>
            {{ Session::get('flash_message') }}
    </div>
@endif
@if ( Session::has('flash_success') )
    <div class="alert alert-success  {{ Session::get('flash_type') }}">
        <button class="close" data-dismiss="alert"></button>
        {{ Session::get('flash_success') }}
    </div>
@endif
<table datatable="" class="table table-striped table-bordered table-hover dataTables-example">
<thead>

<tr>
    <th width="10%">Title</th>
    <th width="10%">Type</th>
    <th width="10%">Category</th>
    <th width="10%">Listing</th>
    <th width="15%">Location</th>
    <th width="10%">Status</th>
    <th width="20%">Boost Details</th>
    <th width="15%" colspan="2">Action</th>
</tr>
</thead>
<tbody>


@if(count($classifieds_list) > 0 )
    @foreach($classifieds_list  as $classified)
     <tr class="gradeX">
        <td><a href="{{ URL::to('/admin/classified/edit/'.$classified->id) }}">{{ $classified->title }}</a></td>
        <td>{{ $classified->parent_name }}</td>
        <td>{{ $classified->category_name }}</td>
        <td>{{ $classified->description }}</td>
        <td>
        {!! $classified->province_name."<br>".$classified->city_name."<br>".$classified->town_name."<br>".$classified->building_name !!}
        </td>
        <td>
        @if($classified->status !== 2)
        <a class="btn btn-sm btn-warning" href="{{ URL::to('/admin/classified/classified_status/'.$classified->id) }}">
          {!! $status_array[$classified->status]   !!}
        </a> 
        @else
        <a class="btn btn-sm btn-success" href="{{ URL::to('/admin/classified/classified_status/'.$classified->id) }}">
          {!! $status_array[$classified->status]   !!}
        </a> 
        @endif

        
        </td>
        <td @if($classified->featured == 1 && $classified->boost_status == 2) style="background-color: #ccc;" @endif>
        @if($classified->featured)
         @if($classified->boost_status == 2) Boost Expired @endif
         Featured/Top : 
         @if($classified->boost_type == "1" ) 
            Top <br>Target Listing Count:  {{ ceil($classified->budget / $top_add_option) }}<br>
         @else 
            Featured <br>Target Listing Count:  {{ ceil($classified->budget / $featured_add_option) }}<br>
        @endif 
         
         Listing Count: {{ Counter::show('classified_featured', $classified->id) }}<br>
         Duration : {{ $classified->duration }} <br>
         Start : {{ $classified->boost_Start }} <br>
         Expire : {{ $classified->expire }} <br>
        @else
         Not Boosted
        @endif
        </td>
        <td class="center"><a href="{{ URL::to('/admin/classified/edit/'.$classified->id) }}" class="btn btn-sm btn-warning" type="submit">Edit</a></td>
        <td class="center"><a href="{{ URL::to('/admin/classified/delete/'.$classified->id) }}" class="btn btn-sm btn-danger" type="submit">Remove</a></td>
    </tr>
    @endforeach 

@else
    <tr>
        <th colspan="10">No Classifieds found, Once clasified added listing will shows</th>
    </tr>
@endif
  
<!-- <tr class="gradeX">
    <td>Trident</td>
    <td>Internet</td>
    <td>Win 95+</td>
    <td class="center">4</td>
    <td class="center">X</td>
</tr> -->

</tbody>
<tfoot>
<!-- <tr>
    <th>Rendering engine</th>
    <th>Browser</th>
    <th>Platform(s)</th>
    <th>Engine version</th>
    <th>CSS grade</th>
</tr> -->
</tfoot>
</table>

</div>
</div>
</div>
</div>
</div>
@endsection
