<?php
define('PAGE_PARENT', 'classified', true);
define('PAGE_CURRENT', 'classified_fields_list', true);
?>
@extends('app')

@section('title', 'Classified Form Fields List')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-5">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Add New Field Link</h5>

                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                    {!! Form::open(array('url'=>'/admin/general/form/classified_store','role'=>'form', 'class'=>'form-horizontal')) !!}
                    {!! Form::hidden('id', $field_one->id, array('id' => 'invisible_id')) !!}

                    @if ( Session::has('flash_message') )
                        <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                                <button class="close" data-dismiss="alert"></button>
                                {{ Session::get('flash_message') }}
                        </div>
                    @endif
                    @if ( Session::has('flash_success') )
                        <div class="alert alert-success  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {{ Session::get('flash_success') }}
                        </div>
                    @endif

                        <div class="form-group"><label class="col-lg-3 control-label">Category</label>
                            <div class="col-lg-9">
                            {!! Form::select('parentid', $category_list, 
                            $field_one->parentid, ['id' => 'classified_parentid', 'class' => 'form-control']) !!}
                            @if ($errors->has('parentid'))
                                        <span class="alert-danger">{{ $errors->first('parentid') }}</span><br>
                            @endif
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Sub Category</label>

                            <div class="col-lg-9"  id="classified_category">
                            {!! Form::select('category', $category_list, 
                            $field_one->category, ['id' => 'category', 'class' => 'form-control']) !!}
                            @if ($errors->has('category'))
                                        <span class="alert-danger">{{ $errors->first('category') }}</span><br>
                            @endif
                            </div>
                        </div>

                        <?php $i = 0; ?>

                        @foreach(json_decode($field_one->fields, TRUE)  as $key => $value)

                            @if($i == "0")

                            <div class="form-group"><label class="col-lg-3 control-label">Field</label>

                                <div class="col-lg-8">
                                    {!! Form::select('fields[]', $fields_array, $value, ['id' => 'fields', 'class' => 'form-control']) !!}
                                    @if ($errors->has('fields'))
                                            <span class="alert-danger">{{ $errors->first('fields') }}</span><br>
                                    @endif
                                </div>
                                <label class="col-lg-1 control-label" >
                                <a href="#" alt="Add another Field" id="field_selection_icon" >
                                    <i class="fa fa-plus-square" ></i>
                                </a>
                                </label>
                            </div>

                            @else

                            <div class="form-group" style="display: none" id="field_selection_row">
                                <label class="col-lg-3 control-label"></label>
                                <div class="col-lg-8">
                                    {!! Form::select('fields[]', $fields_array, $value, ['class' => 'form-control']) !!}
                                </div>
                                <label class="col-lg-1 control-label">
                                    <a href="#" alt="Add another Field" class="field_close_icon" >
                                        <i class="fa fa-times" ></i>
                                    </a>
                                </label>
                            </div>

                            @endif

                        <?php  $i++; ?>

                        @endforeach
                        <div class="form-group" style="display: none" id="field_selection_row">
                                <label class="col-lg-3 control-label"></label>
                                <div class="col-lg-8">
                                    {!! Form::select('fields[]', $fields_array, $value, ['class' => 'form-control']) !!}
                                </div>
                                <label class="col-lg-1 control-label">
                                    <a href="#" alt="Add another Field" class="field_close_icon" >
                                        <i class="fa fa-times" ></i>
                                    </a>
                                </label>
                            </div>
                        

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-8">
                                <input class="btn btn-sm btn-primary" type="submit" value="Update Field">
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-lg-7">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Classified Form Fields List</h5>
                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <table datatable="" class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Category</th>
                                <th>Subcategory</th>
                                <th>Linked Fields</th>
                                <th colspan="2"><center>Action</center></th>
                            </tr>
                            </thead>
                            <tbody >
                            @if(count($classified_fields_list) > 0 )
                             @foreach($classified_fields_list  as $field)
                             <tr class="gradeX">
                                <td>{{ $field->id }}</td>
                                <td>{{ $field->parent_name }}</td>
                                <td><a href="{{ URL::to('/admin/general/form/classified_edit/'.$field->id) }}">{{ $field->category_name }}</a></td>
                                <td>{!! $field->fields !!}</td>
                                <td class="center"><a href="{{ URL::to('/admin/general/form/classified_edit/'.$field->id) }}" class="btn btn-sm btn-warning" type="submit">Edit</a></td>
                                <td class="center"><a href="{{ URL::to('/admin/general/form/classified_delete/'.$field->id) }}" class="btn btn-sm btn-danger" type="submit">Remove</a></td>
                            </tr>
                            @endforeach 
                            @else
                            <tr class="gradeX">
                                <td colspan="5">No Records Added</td>
                            </tr>
                            @endif                         
                            </tbody>
                           <!--  <tfoot>
                            <tr>
                                <th>Rendering engine</th>
                                <th>Browser</th>
                                <th>Platform(s)</th>
                                <th>Engine version</th>
                                <th>CSS grade</th>
                            </tr>
                            </tfoot> -->
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
