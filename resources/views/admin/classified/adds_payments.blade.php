<?php
define('PAGE_PARENT', 'classified', true);
define('PAGE_CURRENT', 'adds_payments', true);
?>
@extends('app')

@section('title', 'Adds Payments')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title">
    <h5>All Advertisment Payments</h5>
    <!-- <div ibox-tools></div> -->
</div>
<div class="ibox-content">
@if ( Session::has('flash_message') )
    <div class="alert alert-danger  {{ Session::get('flash_type') }}">
            <button class="close" data-dismiss="alert"></button>
            {{ Session::get('flash_message') }}
    </div>
@endif
@if ( Session::has('flash_success') )
    <div class="alert alert-success  {{ Session::get('flash_type') }}">
        <button class="close" data-dismiss="alert"></button>
        {{ Session::get('flash_success') }}
    </div>
@endif
<table datatable="" class="table table-striped table-bordered table-hover dataTables-example">
<thead>

<tr>
    <th>Title</th>
    <th>User</th>
    <th>Add Type</th>
    <th>Duration(Days)</th>
    <th>Budget</th>
    <th>Paypal Id</th>
    <th>Paypal Reference</th>
    <th>Paid Amount</th>
    <th>Created At</th>
    <!-- <th colspan="2">Action</th> -->
</tr>
</thead>
<tbody>

@if(count($boosts_list) > 0 )
    @foreach($boosts_list  as $boosts)
     <tr class="gradeX">
        
        <?php  /*<td><a target="_blank" href="{{ URL::to('/admin/classifboostsied/edit/'.$boosts->id) }}">{{ $classified->title }}</a></td> */?>
        <td width="15%">{{ $boosts->classifieds_title }}</td>
        <td width="15%">Name:{{ $boosts->username }}<br>Email:{{ $boosts->useremail }}</td>
        <td width="5%">{!! $price_type_array[$boosts->boost_type]   !!}</td>
        <td width="5%">{!! $duration_type[$boosts->duration]   !!}</td>
        <td width="5%">{{ $boosts->budget }}</td>
        <td width="10%">{{ $boosts->paypal_id }}</td>
        <td width="10%">{{ $boosts->paypal_reference }}</td>
        <td width="5%">{{ $boosts->paid_amount }}</td>
        <td width="15%">{{ $boosts->created_at }}</td>
        <?php /*<td class="center"><a href="{{ URL::to('/admin/classified/edit/'.$boosts->id) }}" class="btn btn-sm btn-warning" type="submit">Edit</a></td>
        <td class="center"><a href="{{ URL::to('/admin/classified/delete/'.$boosts->id) }}" class="btn btn-sm btn-danger" type="submit">Remove</a></td>*/ ?>
    </tr>
    @endforeach 

@else
    <tr>
        <th colspan="10">No advertisment payment found, Once payments done listing will shows</th>
    </tr>
@endif
  
<!-- <tr class="gradeX">
    <td>Trident</td>
    <td>Internet</td>
    <td>Win 95+</td>
    <td class="center">4</td>
    <td class="center">X</td>
</tr> -->

</tbody>
<tfoot>
<!-- <tr>
    <th>Rendering engine</th>
    <th>Browser</th>
    <th>Platform(s)</th>
    <th>Engine version</th>
    <th>CSS grade</th>
</tr> -->
</tfoot>
</table>

</div>
</div>
</div>
</div>
</div>
@endsection
