@if(count($fields_array_set) > 0 )
     @foreach($fields_array_set  as $fields_array)
     <div class="form-group">
        <div class="col-lg-3 control-label">{{ $fields_array->label }}</div>
        <div class="col-lg-9">
            @if(count(array_filter(explode(",", $fields_array->content))) > 1)
            <?php $content_array = array_filter(explode(",", $fields_array->content)); ?>

            @foreach($content_array  as $key => $value)
              <?php  $value = trim($value);  ?>
              <div class="row">
                <div class="col-lg-3 control-label" id="{{ $fields_array->frontend_type }}">{{ $value }}</div>
                <div class="col-lg-6">

                  @if($fields_array->frontend_type == "checkbox")

                  <input type="checkbox" name="extra_fields[{{ $fields_array->slug }}][{{ $value }}]" class="form-control">
                       

                  @elseif($fields_array->frontend_type == "text")

                  <input type="text" name="extra_fields[{{ $fields_array->slug }}][{{ $value }}]" class="form-control">


                  @elseif($fields_array->frontend_type == "radio")
                  <input type="radio" name="extra_fields[{{ $fields_array->slug }}][{{ $value }}]" class="form-control">


                  @elseif($fields_array->frontend_type == "number")
                  <input type="text" name="extra_fields[{{ $fields_array->slug }}][{{ $value }}]" class="form-control">

                  @elseif($fields_array->frontend_type == "select")
                  <select name="extra_fields[{{ $fields_array->slug }}][{{ $value }}]" class="form-control">
                      <option>nothing</option>
                  </select>

                  @elseif($fields_array->frontend_type == "date")
                   <input type="text" name="extra_fields[{{ $fields_array->slug }}][{{ $value }}]" class="form-control">

                  @elseif($fields_array->frontend_type == "file")
                   <input type="file" name="extra_fields[{{ $fields_array->slug }}][{{ $value }}]"   class="form-control">

                  @elseif($fields_array->frontend_type == "textarea")
                   <textarea type="text" name="extra_fields[{{ $fields_array->slug }}][{{ $value }}]" class="form-control"></textarea>

                  @endif
                </div>
              </div>

            @endforeach

             
            @else
              @if($fields_array->frontend_type == "checkbox")

              <input type="checkbox" name="extra_fields[{{ $fields_array->slug }}]" class="form-control">
                   

              @elseif($fields_array->frontend_type == "text")

              <input type="text" name="extra_fields[{{ $fields_array->slug }}]" class="form-control">


              @elseif($fields_array->frontend_type == "radio")
              <input type="radio" name="extra_fields[{{ $fields_array->slug }}]" class="form-control">


              @elseif($fields_array->frontend_type == "number")
              <input type="text" name="extra_fields[{{ $fields_array->slug }}]" class="form-control">

              @elseif($fields_array->frontend_type == "select")
              <select name="extra_fields[{{ $fields_array->slug }}]"  class="form-control">
                  <option>nothing</option>
              </select>

              @elseif($fields_array->frontend_type == "date")
               <input type="text" name="extra_fields[{{ $fields_array->slug }}]" class="form-control">
              @elseif($fields_array->frontend_type == "file")
                <input type="file" name="extra_fields[{{ $fields_array->slug }}]" class="form-control" >

              @elseif($fields_array->frontend_type == "textarea")
               <textarea type="text" name="extra_fields[{{ $fields_array->slug }}]" class="form-control"></textarea>

              @endif

            @endif

            
        </div>
      </div>
    @endforeach 
 
@else
<div class="form-group">
    <div class="col-lg-12 alert-warning">No Extra Fields</div>
</div>
@endif 