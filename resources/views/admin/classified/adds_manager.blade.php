<?php
define('PAGE_PARENT', 'adds', true);
define('PAGE_CURRENT', 'adds_manager', true);
?>
@extends('app')

@section('title', 'Adds Manager')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
        {!! Form::open(array('url'=>'/admin/classified/adds/store','role'=>'form', 'class'=>'form-horizontal')) !!}
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Adds Manager</h5>

                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                    

                    @if ( Session::has('flash_message') )
                        <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                                <button class="close" data-dismiss="alert"></button>
                                {{ Session::get('flash_message') }}
                        </div>
                    @endif
                    @if ( Session::has('flash_success') )
                        <div class="alert alert-success  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {{ Session::get('flash_success') }}
                        </div>
                    @endif          
                </div>
            </div>



            <div class="ibox float-e-margins">

                <div class="ibox-content col-sm-12">

                     <div class="form-group">
                            <label class="col-lg-3 control-label">Adds duration option Days</label>
                            <div class="col-lg-9">
                                <input type="text"  
                                value="{{ (isset($option_list['adds_duration_days']))? $option_list['adds_duration_days'] : '' }}" placeholder="Adds duration option Days"  class="form-control"  name="adds_duration_days">
                                <span class="help-block m-b-none">duration option should be days and separated by comma's.</span>
                            </div>
                     </div>
                     <div class="form-group">
                            <label class="col-lg-3 control-label">"Top Add" rate in rupees per day</label>
                            <label class="col-lg-1 control-label">Rs.</label>
                            <div class="col-lg-8">
                                <input type="text"  
                                value="{{ (isset($option_list['adds_top_add_per_day']))? $option_list['adds_top_add_per_day'] : '' }}" placeholder="Enter Price per day"  class="form-control numeric"  name="adds_top_add_per_day">
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-lg-3 control-label">"Top Add" rate in rupees per Click</label>
                            <label class="col-lg-1 control-label">Rs.</label>
                            <div class="col-lg-8">
                               <input type="text"  
                                value="{{ (isset($option_list['adds_top_add_per_click']))? $option_list['adds_top_add_per_click'] : '' }}" placeholder="Enter Price per Click"  class="form-control numeric"  name="adds_top_add_per_click">
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-lg-3 control-label">"Featured Add" rate in rupees per day</label>
                            <label class="col-lg-1 control-label">Rs.</label>
                            <div class="col-lg-8">
                                <input type="text"  
                                value="{{ (isset($option_list['adds_featured_add_per_day']))? $option_list['adds_featured_add_per_day'] : '' }}" placeholder="Enter Price per day"  class="form-control numeric"  name="adds_featured_add_per_day">
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-lg-3 control-label">"Featured Add" rate in rupees per Click</label>
                            <label class="col-lg-1 control-label">Rs.</label>
                            <div class="col-lg-8">
                               <input type="text"  
                                value="{{ (isset($option_list['adds_featured_add_per_click']))? $option_list['adds_featured_add_per_click'] : '' }}" placeholder="Enter Price per Click"  class="form-control numeric"  name="adds_featured_add_per_click">
                            </div>
                    </div>
                </div>
            </div>


            <div class="ibox">
                <div class="ibox-content col-sm-12">
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-8">
                            <input class="btn btn-sm btn-primary" type="submit" value="Update Adds Configurations">
                        </div>
                    </div>
                </div>
            </div>


            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
