<?php
define('PAGE_PARENT', 'classified', true);
define('PAGE_CURRENT', 'form_fields_list', true);
?>
@extends('app')

@section('title', 'Form Fields List')

@section('content')
<div class="wrapper wrapper-content ng-scope animated fadeInRight">
    <div class="row">
        <div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Add New Form Field</h5>

                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                    {!! Form::open(array('url'=>'/admin/general/form/store','role'=>'form', 'class'=>'form-horizontal')) !!}

                    @if ( Session::has('flash_message') )
                        <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                                <button class="close" data-dismiss="alert"></button>
                                {{ Session::get('flash_message') }}
                        </div>
                    @endif
                    @if ( Session::has('flash_success') )
                        <div class="alert alert-success  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {{ Session::get('flash_success') }}
                        </div>
                    @endif

                        <div class="form-group"><label class="col-lg-3 control-label">Title</label>

                            <div class="col-lg-9">
                                <input type="text" placeholder="title" class="form-control" id="form_title" name="title">
                                @if ($errors->has('title'))
                                        <span class="alert-danger">{{ $errors->first('title') }}</span><br>
                                @endif
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Label</label>

                            <div class="col-lg-9">
                                <input type="text" placeholder="Label" class="form-control" id="label" name="label">
                                @if ($errors->has('label'))
                                        <span class="alert-danger">{{ $errors->first('label') }}</span><br>
                                @endif
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Slug</label>

                            <div class="col-lg-9">
                                <input type="text" placeholder="slug" class="form-control" id="form_slug"  name="slug">
                                @if ($errors->has('slug'))
                                        <span class="alert-danger">{{ $errors->first('slug') }}</span><br>
                                @endif
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Data Types</label>

                            <div class="col-lg-9">
                            {!! Form::select('frontend_type', 
                            array('0'=>'Select a Field', 'text'=>'text', 'file'=>'file', 'checkbox'=>'Tick box', 'radio'=>'radio', 'number'=>'number', 'select'=>'select', 'date'=>'date', 'textarea'=>'textarea' ), 
                            null, ['id' => 'frontend_type', 'class' => 'form-control']) !!}
                            @if ($errors->has('frontend_type'))
                                        <span class="alert-danger">{{ $errors->first('frontend_type') }}</span><br>
                            @endif
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Display Method</label>

                            <div class="col-lg-9">
                            {!! Form::select('backend_type', 
                            array('0'=>'Select a Field', 'li'=>'Bullet', 'comma'=>'comma', 'row'=>'row', 'image'=>'image', 'checkmark'=>'checkmark', 'Punctuation'=>'Colon'), 
                            null, ['id' => 'backend_type', 'class' => 'form-control']) !!}
                            @if ($errors->has('backend_type'))
                                        <span class="alert-danger">{{ $errors->first('backend_type') }}</span><br>
                            @endif
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">list order</label>

                            <div class="col-lg-9">
                                <input type="text" placeholder="list order" class="form-control" id="listorder" name="listorder">
                                @if ($errors->has('listorder'))
                                        <span class="alert-danger">{{ $errors->first('listorder') }}</span><br>
                                @endif
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Parent</label>

                            <div class="col-lg-9">
                            {!! Form::select('parentid', $fields_array, 
                            null, ['id' => 'parentid', 'class' => 'form-control']) !!}
                            @if ($errors->has('parentid'))
                                        <span class="alert-danger">{{ $errors->first('parentid') }}</span><br>
                            @endif
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">icon</label>

                            <div class="col-lg-9">
                                <input type="text" placeholder="icon" class="form-control" id="icon" name="icon">
                                @if ($errors->has('icon'))
                                        <span class="alert-danger">{{ $errors->first('icon') }}</span><br>
                                @endif
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Content</label>

                            <div class="col-lg-9">
                                <textarea placeholder="Type Content" class="form-control" name="content" ></textarea>
                                @if ($errors->has('content'))
                                        <span class="alert-danger">{{ $errors->first('content') }}</span><br>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-8">
                                <input class="btn btn-sm btn-primary" type="submit" value="Add New Field">
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Form Fields List</h5>
                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <table datatable="" class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Label</th>
                                <th>Slug</th>
                                <th>Frontend Type</th>
                                <th>Backend Type</th>
                                <th colspan="2"><center>Action</center></th>
                            </tr>
                            </thead>
                            <tbody >
                            @if(count($form_fields_list) > 0 )
                             @foreach($form_fields_list  as $form_field)
                             <tr class="gradeX">
                                <td>{{ $form_field->id }}</td>
                                <td><a href="{{ URL::to('/admin/general/form/edit/'.$form_field->id) }}">{{ $form_field->title }}</a></td>
                                <td>{{ $form_field->label }}</td>
                                <td>{{ $form_field->slug }}</td>
                                <td>{{ $field_types[$form_field->frontend_type] }}</td>
                                <td>{{ $field_types[$form_field->backend_type] }}</td>
                                <td class="center"><a href="{{ URL::to('/admin/general/form/edit/'.$form_field->id) }}" class="btn btn-sm btn-warning" type="submit">Edit</a></td>
                                <td class="center"><a href="{{ URL::to('/admin/general/form/delete/'.$form_field->id) }}" class="btn btn-sm btn-danger" type="submit">Remove</a></td>
                            </tr>
                            @endforeach 
                            @else
                            <tr class="gradeX">
                                <td colspan="5">No Records Added</td>
                            </tr>
                            @endif                         
                            </tbody>
                           <!--  <tfoot>
                            <tr>
                                <th>Rendering engine</th>
                                <th>Browser</th>
                                <th>Platform(s)</th>
                                <th>Engine version</th>
                                <th>CSS grade</th>
                            </tr>
                            </tfoot> -->
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
