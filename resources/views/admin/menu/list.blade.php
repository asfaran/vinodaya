<?php
define('PAGE_PARENT', 'general', true);
define('PAGE_CURRENT', 'menu_list', true);
?>
@extends('app')

@section('title', 'Menu Manager')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-5">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Add New Menu Item</h5>

                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                    {!! Form::open(array('url'=>'/admin/general/menu/store','role'=>'form', 'class'=>'form-horizontal')) !!}

                    @if ( Session::has('flash_message') )
                        <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                                <button class="close" data-dismiss="alert"></button>
                                {{ Session::get('flash_message') }}
                        </div>
                    @endif
                    @if ( Session::has('flash_success') )
                        <div class="alert alert-success  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {{ Session::get('flash_success') }}
                        </div>
                    @endif

                        <div class="form-group"><label class="col-lg-3 control-label">Navigation Label</label>

                            <div class="col-lg-9">
                                <input type="text" placeholder="Navigation Label" value="{{ Input::old('label')}}" id="form_title" class="form-control" name="label">
                                @if ($errors->has('label'))
                                        <span class="alert-danger">{{ $errors->first('label') }}</span><br>
                                @endif

                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Title Attribute</label>

                            <div class="col-lg-9">
                                <input type="text" placeholder="Title Attribute" id="form_slug"  class="form-control" value="{{ Input::old('title') }}" name="title">
                                @if ($errors->has('title'))
                                        <span class="alert-danger">{{ $errors->first('title') }}</span><br>
                                @endif
                                <span class="help-block m-b-none">The “title” is the URL-friendly version of the name.</span>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Menu Type</label>

                            <div class="col-lg-9">
                            {!! Form::select('type', array('0'=>'Select One', '1'=>'Category', '2'=>'Custom Links', '3'=>'Pages'),  Input::old('type') , ['id' => 'menu_type', 'class' => 'form-control']) !!}
                            @if ($errors->has('type'))
                                        <span class="alert-danger">{{ $errors->first('type') }}</span><br>
                            @endif
                            </div>
                        </div>
                        <div class="form-group content_div" id="content_div_1" style="display: none">
                            <div class="col-lg-2"></div>
                            <label class="col-lg-3 control-label">category</label>
                            <div class="col-lg-7">
                            {!! Form::select('categoryid', $category_list, Input::old('categoryid') , ['id' => 'categoryid', 'class' => 'form-control']) !!}
                            @if ($errors->has('content'))
                                        <span class="alert-danger">{{ $errors->first('content') }}</span><br>
                            @endif
                            </div>
                        </div>
                        <div class="form-group content_div" id="content_div_2" style="display: none">
                            <div class="col-lg-2"></div>
                            <label class="col-lg-3 control-label">Url</label>
                            <div class="col-lg-7">
                            {!! Form::text('url', Input::old('url') ,array('id'=>'url','class'=>'form-control','placeholder'=>'Type Custom url')) !!}
                            @if ($errors->has('content'))
                                        <span class="alert-danger">{{ $errors->first('content') }}</span><br>
                            @endif
                            </div>
                        </div>
                        <div class="form-group content_div" id="content_div_3" style="display: none">
                            <div class="col-lg-2"></div>
                            <label class="col-lg-3 control-label">Pages</label>
                            <div class="col-lg-7">
                            {!! Form::select('pageid', $page_list,  Input::old('pageid') , ['id' => 'pageid', 'class' => 'form-control']) !!}
                            @if ($errors->has('content'))
                                        <span class="alert-danger">{{ $errors->first('content') }}</span><br>
                            @endif
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Description</label>

                            <div class="col-lg-9">
                                <textarea placeholder="Type Description" class="form-control" name="description"> {{ Input::old('description') }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-8">
                                <input class="btn btn-sm btn-primary" type="submit" value="Add New Menu Item">
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-lg-7">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Menu List</h5>
                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <table datatable="" class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Label</th>
                                <th>Type</th>
                                <th colspan="2"><center>Action</center></th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr class="gradeX">
                                  <td colspan="5"><span class="alert-warning">You don't have permission to  Delete Primary Data's.</span></td>
                                </tr>

                            @if(count($menu_full) > 0)
                             @foreach($menu_full  as $menu)
                             <tr class="gradeX">
                                <td>{{ $menu->id }}</td>
                                <td>{{ $menu->label }}</td>
                                <td>{{ $type_array[$menu->type] }}</td>
                                <td class="center"><a href="{{ URL::to('/admin/general/menu/edit/'.$menu->id) }}" class="btn btn-sm btn-warning" type="submit">Edit</a></td>
                                @if($menu->access !== 1) 
                                <td class="center"><a href="{{ URL::to('/admin/general/menu/delete/'.$menu->id) }}" class="btn btn-sm btn-danger" type="submit">Remove</a></td>
                                @else
                                <td class="center"><span class="btn btn-sm btn-default">Remove</span></td>
                                @endif

                                
                             </tr>
                             @endforeach
                            @else
                            <tr class="gradeX">
                                <td colspan="5">No Records Added</td>
                            </tr>
                            @endif                         
                            </tbody>
                           <!--  <tfoot>
                            <tr>
                                <th>Rendering engine</th>
                                <th>Browser</th>
                                <th>Platform(s)</th>
                                <th>Engine version</th>
                                <th>CSS grade</th>
                            </tr>
                            </tfoot> -->
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
