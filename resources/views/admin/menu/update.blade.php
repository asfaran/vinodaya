<?php
define('PAGE_PARENT', 'general', true);
define('PAGE_CURRENT', 'menu_list', true);
?>
@extends('app')

@section('title', 'Menu Update')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-5">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Update Menu Item</h5>

                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                    {!! Form::open(array('url'=>'/admin/general/menu/store','role'=>'form', 'class'=>'form-horizontal')) !!}
                    {!! Form::hidden('id', $menu->id, array('id' => 'invisible_id')) !!}

                    @if ( Session::has('flash_message') )
                        <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                                <button class="close" data-dismiss="alert"></button>
                                {{ Session::get('flash_message') }}
                        </div>
                    @endif
                    @if ( Session::has('flash_success') )
                        <div class="alert alert-success  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {{ Session::get('flash_success') }}
                        </div>
                    @endif

                        <div class="form-group"><label class="col-lg-3 control-label">Navigation Label</label>

                            <div class="col-lg-9">
                            
                                 {!! Form::text('label',$menu->label,array('id'=>'form_title','class'=>'form-control','placeholder'=>'Navigation Label')) !!}
                                @if ($errors->has('label'))
                                        <span class="alert-danger">{{ $errors->first('label') }}</span><br>
                                @endif

                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Title Attribute</label>

                            <div class="col-lg-9">
                                {!! Form::text('title',$menu->title,array('id'=>'form_slug','class'=>'form-control','placeholder'=>'Title Attribute')) !!}
                                @if ($errors->has('title'))
                                        <span class="alert-danger">{{ $errors->first('title') }}</span><br>
                                @endif
                                <span class="help-block m-b-none">The “title” is the URL-friendly version of the name.</span>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Menu Type</label>

                            <div class="col-lg-9">
                            <?php $menu_type_array_list =  array('0'=>'Select One', '1'=>'Category', '2'=>'Custom Links', '3'=>'Pages' ); ?>
                            
                            @if($menu->access !== 1) 
                            {!! Form::select('type', array('0'=>'Select One', '1'=>'Category', '2'=>'Custom Links', '3'=>'Pages' ), $menu->type, ['id' => 'menu_type', 'class' => 'form-control']) !!}
                            @else
                            {!! Form::select('type_display', array('0'=>'Select One', '1'=>'Category', '2'=>'Custom Links', '3'=>'Pages' ), $menu->type, ['id' => 'type_display', 'class' => 'form-control', 'disabled' => 'disabled']) !!}

                            {!! Form::hidden('type', $menu->type, array('id' => 'menu_type')) !!}
                            @endif
                            @if ($errors->has('type'))
                                        <span class="alert-danger">{{ $errors->first('type') }}</span><br>
                            @endif
                            </div>
                        </div>
                        <div class="form-group content_div" id="content_div_1" 
                          @if($menu->type !== 1) style="display: none" @endif>
                            <div class="col-lg-2"></div>
                            <label class="col-lg-3 control-label">category</label>
                            <div class="col-lg-7">
                            {!! Form::select('categoryid', $category_list, (int)$menu->content, ['id' => 'categoryid', 'class' => 'form-control']) !!}
                            @if ($errors->has('content'))
                                        <span class="alert-danger">{{ $errors->first('content') }}</span><br>
                            @endif
                            </div>
                        </div>
                        <div class="form-group content_div" id="content_div_2" @if($menu->type !== 2) style="display: none" @endif>
                            <div class="col-lg-2"></div>
                            <label class="col-lg-3 control-label">Url</label>
                            <div class="col-lg-7">
                            @if($menu->access !== 1) 
                            {!! Form::text('url',$menu->content,array('id'=>'url','class'=>'form-control','placeholder'=>'Type Custom url')) !!}
                            @else
                            {!! Form::text('url_display',$menu->content,array('id'=>'url_display','class'=>'form-control','placeholder'=>'Type Custom url', 'disabled' => 'disabled')) !!}

                            {!! Form::hidden('url', $menu->content, array('id' => 'url')) !!}
                            @endif
                            @if ($errors->has('content'))
                                        <span class="alert-danger">{{ $errors->first('content') }}</span><br>
                            @endif
                            </div>
                        </div>
                        <div class="form-group content_div" id="content_div_3" @if($menu->type !== 3) style="display: none" @endif>
                            <div class="col-lg-2"></div>
                            <label class="col-lg-3 control-label">Pages</label>
                            <div class="col-lg-7">
                            {!! Form::select('pageid', $page_list, (int)$menu->content, ['id' => 'pageid', 'class' => 'form-control']) !!}
                            @if ($errors->has('content'))
                                        <span class="alert-danger">{{ $errors->first('content') }}</span><br>
                            @endif
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Description</label>

                            <div class="col-lg-9">
                                {!! Form::textarea('description',$menu->description,array('id'=>'description','class'=>'form-control','placeholder'=>'Type Description')) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-8">
                                <input class="btn btn-sm btn-primary" type="submit" value="Update Menu">
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-lg-7">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Menu List</h5>
                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <table datatable="" class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Label</th>
                                <th>Type</th>
                                <th colspan="2"><center>Action</center></th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr class="gradeX">
                                  <td colspan="5"><span class="alert-warning">You don't have permission to  Delete Primary Data's.</span></td>
                                </tr>
                             @foreach($menu_full  as $menu)
                             <tr class="gradeX">
                                <td>{{ $menu->id }}</td>
                                <td>{{ $menu->label }}</td>
                                <td>{{ $type_array[$menu->type] }}</td>
                                <td class="center"><a href="{{ URL::to('/admin/general/menu/edit/'.$menu->id) }}" class="btn btn-sm btn-warning" type="submit">Edit</a></td>
                                @if($menu->access !== 1) 
                                <td class="center"><a href="{{ URL::to('/admin/general/menu/delete/'.$menu->id) }}" class="btn btn-sm btn-danger" type="submit">Remove</a></td>
                                @else
                                <td class="center"><span class="btn btn-sm btn-default">Remove</span></td>
                                @endif
                            </tr>
                            @endforeach                          
                            </tbody>
                           <!--  <tfoot>
                            <tr>
                                <th>Rendering engine</th>
                                <th>Browser</th>
                                <th>Platform(s)</th>
                                <th>Engine version</th>
                                <th>CSS grade</th>
                            </tr>
                            </tfoot> -->
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
