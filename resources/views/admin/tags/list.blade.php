<?php
define('PAGE_PARENT', 'general', true);
define('PAGE_CURRENT', 'tags_list', true);
?>
@extends('app')

@section('title', 'Tags List')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-5">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Add New Tags</h5>

                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                    {!! Form::open(array('url'=>'/admin/general/tags/store','role'=>'form', 'class'=>'form-horizontal')) !!}

                    @if ( Session::has('flash_message') )
                        <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                                <button class="close" data-dismiss="alert"></button>
                                {{ Session::get('flash_message') }}
                        </div>
                    @endif
                    @if ( Session::has('flash_success') )
                        <div class="alert alert-success  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {{ Session::get('flash_success') }}
                        </div>
                    @endif

                        <div class="form-group"><label class="col-lg-3 control-label">Title</label>

                            <div class="col-lg-9">
                                <input type="text" placeholder="title" id="form_title" class="form-control" value="{{ Input::old('title')}}" name="title">
                                @if ($errors->has('title'))
                                        <span class="alert-danger">{{ $errors->first('title') }}</span><br>
                                @endif
                                <span class="help-block m-b-none">The name is how it appears on your site.</span>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Slug</label>

                            <div class="col-lg-9">
                                <input type="text" placeholder="slug" id="form_slug" class="form-control" value="{{ Input::old('slug')}}" name="slug">
                                @if ($errors->has('slug'))
                                        <span class="alert-danger">{{ $errors->first('slug') }}</span><br>
                                @endif
                                <span class="help-block m-b-none">The “slug” is the URL-friendly version of the name.</span>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Description</label>

                            <div class="col-lg-9">
                                <textarea placeholder="Type Description" class="form-control" name="description">{{ Input::old('description') }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-8">
                                <input class="btn btn-sm btn-primary" type="submit" value="Add New Tag">
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-lg-7">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Tags List</h5>
                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <table datatable="" class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>title</th>
                                <th>Slug</th>
                                <th colspan="2"><center>Action</center></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($tags_list) > 0)
                                 @foreach($tags_list  as $tags)
                                 <tr class="gradeX">
                                    <td>{{ $tags->id }}</td>
                                    <td><a href="{{ URL::to('/admin/general/tags/edit/'.$tags->id) }}">{{ $tags->title }}</a></td>
                                    <td>{{ $tags->slug }}</td>
                                    <td class="center"><a href="{{ URL::to('/admin/general/tags/edit/'.$tags->id) }}" class="btn btn-sm btn-warning" type="submit">Edit</a></td>
                                    <td class="center"><a href="{{ URL::to('/admin/general/tags/delete/'.$tags->id) }}" class="btn btn-sm btn-danger" type="submit">Remove</a></td>
                                </tr>
                                @endforeach 

                            @else
                                <tr>
                                    <th colspan="5">No Records In database</th>
                                </tr>
                            @endif                      
                            </tbody>
                           <!--  <tfoot>
                            <tr>
                                <th>Rendering engine</th>
                                <th>Browser</th>
                                <th>Platform(s)</th>
                                <th>Engine version</th>
                                <th>CSS grade</th>
                            </tr>
                            </tfoot> -->
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
