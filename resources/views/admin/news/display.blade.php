<?php
define('PAGE_PARENT', 'news', true);
define('PAGE_CURRENT', 'news_display', true);
?>
@extends('app')

@section('title', 'Front View Configuration')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
        {!! Form::open(array('url'=>'/admin/news/display_store','role'=>'form', 'class'=>'form-horizontal')) !!}
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>News Front View Configuration</h5>

                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                    

                    @if ( Session::has('flash_message') )
                        <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                                <button class="close" data-dismiss="alert"></button>
                                {{ Session::get('flash_message') }}
                        </div>
                    @endif
                    @if ( Session::has('flash_success') )
                        <div class="alert alert-success  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {{ Session::get('flash_success') }}
                        </div>
                    @endif          
                </div>
            </div>



            <div class="ibox float-e-margins">
                <div class="ibox-content col-sm-12">
                     <div class="form-group">
                            <label class="col-lg-3 control-label">Number of News in listing page</label>
                            <div class="col-lg-9">
                                <input type="text"  
                                value="{{ (isset($option_list['news_count_in_listing']))? $option_list['news_count_in_listing'] : '' }}" placeholder="Enter Count number"  class="form-control numeric"  name="news_count_in_listing">
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-lg-3 control-label">Number of News in Front page</label>
                            <div class="col-lg-9">
                                <input type="text"  value="{{ (isset($option_list['news_count_in_front']))? $option_list['news_count_in_front'] : ''  }}" 
                                placeholder="Enter Count number" class="form-control numeric" name="news_count_in_front">
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-lg-3 control-label">Featured News Count</label>
                            <div class="col-lg-9">
                                <input type="text"  value="{{ (isset($option_list['featured_news_count']))? $option_list['featured_news_count'] : ''  }}" placeholder="Enter amount Here" class="form-control numeric" name="featured_news_count">
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-lg-3 control-label">Breaking News Count</label>
                            <div class="col-lg-9">
                                <input type="text"  value="{{ (isset($option_list['breaking_news_count']))? $option_list['breaking_news_count'] : ''  }}" placeholder="Enter amount Here" class="form-control numeric" name="breaking_news_count">
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-lg-3 control-label">Latest News Count in Sidebar</label>
                            <div class="col-lg-9">
                                <input type="text"  value="{{ (isset($option_list['latest_news_count_sidebar']))? $option_list['latest_news_count_sidebar'] : ''  }}" placeholder="Enter amount Here" class="form-control numeric" name="latest_news_count_sidebar">
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-lg-3 control-label">News  Category Listing Count</label>
                            <div class="col-lg-9">
                                <input type="text"  value="{{ (isset($option_list['news_category_list_count']))? $option_list['news_category_list_count'] : ''  }}" placeholder="Enter amount Here" class="form-control numeric" name="news_category_list_count">
                            </div>
                    </div>
                </div>
            </div>


            <div class="ibox">
                <div class="ibox-content col-sm-12">
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-8">
                            <input class="btn btn-sm btn-primary" type="submit" value="Update View Configuration">
                        </div>
                    </div>
                </div>
            </div>


            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
