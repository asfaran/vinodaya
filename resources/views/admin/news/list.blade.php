<?php
define('PAGE_PARENT', 'news', true);
define('PAGE_CURRENT', 'news_list', true);
?>
@extends('app')

@section('title', 'News List')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title">
    <h5>All News</h5>
    <!-- <div ibox-tools></div> -->
    <div class="pull-right add_new_link">
        <a class="btn btn-sm btn-w-m btn-default" href="{{ URL::to('/admin/news/add_new/') }}">Post New News</a>
    </div>
</div>
<div class="ibox-content">
@if ( Session::has('flash_message') )
    <div class="alert alert-danger  {{ Session::get('flash_type') }}">
            <button class="close" data-dismiss="alert"></button>
            {{ Session::get('flash_message') }}
    </div>
@endif
@if ( Session::has('flash_success') )
    <div class="alert alert-success  {{ Session::get('flash_type') }}">
        <button class="close" data-dismiss="alert"></button>
        {{ Session::get('flash_success') }}
    </div>
@endif
<table datatable="" class="table table-striped table-bordered table-hover dataTables-example">
<thead>

<tr>
    <th>Title</th>
    <th>Categories</th>
    <th>status</th>
    <th>Comments</th>
    <th>Click Count</th>
    <th>Date</th>
    <th colspan="2">Action</th>
</tr>
</thead>
<tbody>
@if(count($news_list) > 0 )
    @foreach($news_list  as $news)
     <tr class="gradeX">
        <td><a href="{{ URL::to('/admin/news/edit/'.$news->id) }}">{{ $news->title }}</a></td>
        <td>{{ $news->category_id }}</td>
        <td>{{ $status_array[$news->status]   }}</td>
        <td>{!! ($news->comments_count == 0 )? '0 Comments' : "<a href='#'>$news->comments_count</a>"    !!}</td>
        <td>{{ Counter::show('news', $news->id) }}
        <?php  /*{{ ($news->click_count == '0' )? '0 Clicks' : '<a href="#">$news->click_count</a>'    }}*/ ?></td>
        <td>{{ $news->created_at }}</td>
        <td class="center"><a href="{{ URL::to('/admin/news/edit/'.$news->id) }}" class="btn btn-sm btn-warning" type="submit">Edit</a></td>
        <td class="center"><a href="{{ URL::to('/admin/news/delete/'.$news->id) }}" class="btn btn-sm btn-danger" type="submit">Remove</a></td>
    </tr>
    @endforeach 

@else
    <tr>
        <th colspan="8">No Records In database</th>
    </tr>
@endif
  
<!-- <tr class="gradeX">
    <td>Trident</td>
    <td>Internet</td>
    <td>Win 95+</td>
    <td class="center">4</td>
    <td class="center">X</td>
</tr> -->

</tbody>
<tfoot>
<!-- <tr>
    <th>Rendering engine</th>
    <th>Browser</th>
    <th>Platform(s)</th>
    <th>Engine version</th>
    <th>CSS grade</th>
</tr> -->
</tfoot>
</table>

</div>
</div>
</div>
</div>
</div>
@endsection
