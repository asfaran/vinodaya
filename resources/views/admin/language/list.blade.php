<?php
define('PAGE_PARENT', 'settings', true);
define('PAGE_CURRENT', 'language_list', true);
?>
@extends('app')

@section('title', 'Language Setting')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
        {!! Form::open(array('url'=>'/admin/settings/language/store','role'=>'form', 'class'=>'form-horizontal')) !!}
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Language Settings</h5>

                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                    

                    @if ( Session::has('flash_message') )
                        <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                                <button class="close" data-dismiss="alert"></button>
                                {{ Session::get('flash_message') }}
                        </div>
                    @endif
                    @if ( Session::has('flash_success') )
                        <div class="alert alert-success  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {{ Session::get('flash_success') }}
                        </div>
                    @endif          
                </div>
            </div>



            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>English Language</h5>
                </div>
                <div class="ibox-content col-sm-12">
                     <div class="form-group">
                            <label class="col-lg-3 control-label">Label</label>
                            <div class="col-lg-9">
                            {!! Form::text('language[english][label]', 
                            (isset($language_list['english']['label']))? $language_list['english']['label'] :"" ,array('id'=>'title','class'=>'form-control','placeholder'=>'Type  English Language Label Here')) !!}
                                
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-lg-3 control-label">Folder</label>
                            <div class="col-lg-9">
                            {!! Form::text('language[english][folder]', 
                            (isset($language_list['english']['folder']))? $language_list['english']['folder'] :"" ,array('id'=>'title','class'=>'form-control','placeholder'=>'Type Language Folder Here')) !!}
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-lg-3 control-label">Bootstrap Icon</label>
                            <div class="col-lg-9">
                            {!! Form::text('language[english][icon]', 
                            (isset($language_list['english']['icon']))? $language_list['english']['icon'] :""
                            ,array('id'=>'title','class'=>'form-control','placeholder'=>'Type  Bootstrap Icon Here')) !!}
                            </div>
                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Sinhala Language</h5>
                </div>
                <div class="ibox-content col-sm-12">
                     <div class="form-group">
                            <label class="col-lg-3 control-label">Label</label>
                            <div class="col-lg-9">
                            {!! Form::text('language[sinhala][label]',
                            (isset($language_list['sinhala']['label']))? $language_list['sinhala']['label'] :""
                            ,array('id'=>'title','class'=>'form-control','placeholder'=>'Type Sinhala Label Here')) !!}
                                
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-lg-3 control-label">Folder</label>
                            <div class="col-lg-9">
                            {!! Form::text('language[sinhala][folder]', 
                            (isset($language_list['sinhala']['folder']))? $language_list['sinhala']['folder'] :""
                            ,array('id'=>'title','class'=>'form-control','placeholder'=>'Type Language Folder Here')) !!}
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-lg-3 control-label">Bootstrap Icon</label>
                            <div class="col-lg-9">
                            {!! Form::text('language[sinhala][icon]', 
                            (isset($language_list['sinhala']['icon']))? $language_list['sinhala']['icon'] :""
                            ,array('id'=>'title','class'=>'form-control','placeholder'=>'Type  Bootstrap Icon Here')) !!}
                            </div>
                    </div>
                </div>
            </div>



            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Tamil Language</h5>
                </div>
                <div class="ibox-content col-sm-12">
                     <div class="form-group">
                            <label class="col-lg-3 control-label">Label</label>
                            <div class="col-lg-9">
                            {!! Form::text('language[tamil][label]',
                            (isset($language_list['tamil']['label']))? $language_list['tamil']['label'] :""  
                            ,array('id'=>'title','class'=>'form-control','placeholder'=>'Type  Tamil Label Here')) !!}
                                
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-lg-3 control-label">Folder</label>
                            <div class="col-lg-9">
                            {!! Form::text('language[tamil][folder]',
                            (isset($language_list['tamil']['folder']))? $language_list['tamil']['folder'] :"" 
                            ,array('id'=>'title','class'=>'form-control','placeholder'=>'Type Language Folder Here')) !!}
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-lg-3 control-label">Bootstrap Icon</label>
                            <div class="col-lg-9">
                            {!! Form::text('language[tamil][icon]', 
                            (isset($language_list['tamil']['icon']))? $language_list['tamil']['icon'] :""  
                            ,array('id'=>'title','class'=>'form-control','placeholder'=>'Type  Bootstrap Icon Here')) !!}
                            </div>
                    </div>
                </div>
            </div>


            <div class="ibox">
                <div class="ibox-content col-sm-12">
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-8">
                            <input class="btn btn-sm btn-primary" type="submit" value="Update Language Setting">
                        </div>
                    </div>
                </div>
            </div>


            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
