<?php
define('PAGE_PARENT', 'image', true);
define('PAGE_CURRENT', 'image_list', true);
?>
@extends('app')

@section('title', 'Image List')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title">
    <h5>All Images</h5>
    <!-- <div ibox-tools></div> -->
    <div class="pull-right add_new_link">
        <a class="btn btn-sm btn-w-m btn-default" href="{{ URL::to('/admin/image/add_new/') }}">Post New Image</a>
    </div>
</div>
<div class="ibox-content">
@if ( Session::has('flash_message') )
    <div class="alert alert-danger  {{ Session::get('flash_type') }}">
            <button class="close" data-dismiss="alert"></button>
            {{ Session::get('flash_message') }}
    </div>
@endif
@if ( Session::has('flash_success') )
    <div class="alert alert-success  {{ Session::get('flash_type') }}">
        <button class="close" data-dismiss="alert"></button>
        {{ Session::get('flash_success') }}
    </div>
@endif
<table datatable="" class="table table-striped table-bordered table-hover dataTables-example">
<thead>

<tr>
    <th>Title</th>
    <th>Image Url</th>
    <th>Author</th>
    <th>Categories</th>
    <th>status</th>
    <th>Comments</th>
    <th>Click Count</th>
    <th>Date</th>
    <th colspan="2">Action</th>
</tr>
</thead>
<tbody>
@if(count($image_list) > 0 )
    @foreach($image_list  as $image)
     <tr class="gradeX">
        <td><a href="{{ URL::to('/admin/image/edit/'.$image->id) }}">{{ $image->title }}</a></td>
        <td>
            <a target="_blank"  href="{{ url('/uploads/images/'.$image->thumb) }}">
            <img alt="image" class="img" width="100px" height="50px" src="{{ url('/uploads/images/'.$image->thumb) }}"/>
            </a>
        </td>
        <td>{{ $image->author }}</td>
        <td>{{ $image->category_id }}</td>
        <td>{{ $status_array[$image->status]   }}</td>
        <td>{!! ($image->comments_count == 0 )? '0 Comments' : "<a href='#'>$image->comments_count</a>"    !!}</td>
        <td>{{ Counter::show('images', $image->id) }} 
        <?php  /*{{ ($image->click_count == '0' )? '0 Clicks' : '<a href="#">$image->click_count</a>'    }}*/ ?></td>
        <td>{{ $image->created_at }}</td>
        <td class="center"><a href="{{ URL::to('/admin/image/edit/'.$image->id) }}" class="btn btn-sm btn-warning" type="submit">Edit</a></td>
        <td class="center"><a href="{{ URL::to('/admin/image/delete/'.$image->id) }}" class="btn btn-sm btn-danger" type="submit">Remove</a></td>
    </tr>
    @endforeach 

@else
    <tr>
        <th colspan="8">No Records In database</th>
    </tr>
@endif
  
<!-- <tr class="gradeX">
    <td>Trident</td>
    <td>Internet</td>
    <td>Win 95+</td>
    <td class="center">4</td>
    <td class="center">X</td>
</tr> -->

</tbody>
<tfoot>
<!-- <tr>
    <th>Rendering engine</th>
    <th>Browser</th>
    <th>Platform(s)</th>
    <th>Engine version</th>
    <th>CSS grade</th>
</tr> -->
</tfoot>
</table>

</div>
</div>
</div>
</div>
</div>
@endsection
