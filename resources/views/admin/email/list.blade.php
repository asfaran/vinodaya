<?php
define('PAGE_PARENT', 'classified', true);
define('PAGE_CURRENT', 'emails', true);
?>
@extends('app')

@section('title', 'Mail Template List')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-5">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Add New Email Template</h5>

                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                    {!! Form::open(array('url'=>'/admin/classified/mails/store','role'=>'form', 'files' => true , 'class'=>'form-horizontal')) !!}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    @if ( Session::has('flash_message') )
                        <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                                <button class="close" data-dismiss="alert"></button>
                                {{ Session::get('flash_message') }}
                        </div>
                    @endif
                    @if ( Session::has('flash_success') )
                        <div class="alert alert-success  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {{ Session::get('flash_success') }}
                        </div>
                    @endif

                        <div class="form-group"><label class="col-lg-3 control-label">Title</label>

                            <div class="col-lg-9">
                                <input type="text" placeholder="title" class="form-control" value="{{ Input::old('title')}}" id="title" name="title">
                                @if ($errors->has('title'))
                                        <span class="alert-danger">{{ $errors->first('title') }}</span><br>
                                @endif

                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Sender</label>

                            <div class="col-lg-9">
                                <input type="text" placeholder="Sender" class="form-control" value="{{ Input::old('sender')}}" id="sender"  name="sender">
                                @if ($errors->has('sender'))
                                        <span class="alert-danger">{{ $errors->first('sender') }}</span><br>
                                @endif
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Sender Email</label>

                            <div class="col-lg-9">
                                <input type="text" placeholder="Sender Email" class="form-control" id="sender_email" value="{{ Input::old('sender_email')}}" name="sender_email">
                                @if ($errors->has('sender_email'))
                                        <span class="alert-danger">{{ $errors->first('sender_email') }}</span><br>
                                @endif
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-3 control-label">Content</label>

                            <div class="col-lg-9">
                            <textarea class="form-control input-block-level" id="summernote" name="content" rows="18">{{ Input::old('content') }}</textarea>
                            @if ($errors->has('content'))
                                        <span class="alert-danger">{{ $errors->first('content') }}</span><br>
                            @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-8">
                                <input class="btn btn-sm btn-primary" type="submit" value="Add Mail Template">
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-lg-7">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Mail Template List</h5>
                    <div ibox-tools></div>
                </div>

                <div class="ibox-content">
                    <div class="row">
                        <table datatable="" class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Mail Title</th>
                                <th>Sender</th>
                                <th>Sender Email</th>
                                <th>Content</th>
                                <th colspan="2"><center>Action</center></th>
                            </tr>
                            </thead>
                            <tbody id="table_body_category_type">
                            @if(count($mail_list) > 0 )
                             @foreach($mail_list  as $mail)
                             <tr class="gradeX">
                                <td>{{ $mail->id }}</td>
                                <td><a href="{{ URL::to('/admin/classified/mails/edit/'.$mail->id) }}">{{ $mail->title }}</a></td>
                                <td>{{ $mail->sender }}</td>
                                <td>{{ $mail->sender_email }}</td>
                                <td>{!! $mail->content !!}</td>
                                <td class="center"><a href="{{ URL::to('/admin/classified/mails/edit/'.$mail->id) }}" class="btn btn-sm btn-warning" type="submit">Edit</a></td>
                                <td class="center"><a href="{{ URL::to('/admin/classified/mails/delete/'.$mail->id) }}" class="btn btn-sm btn-danger" type="submit">Remove</a></td>
                            </tr>
                            @endforeach 
                            @else
                            <tr class="gradeX">
                                <td colspan="7">No Records Added</td>
                            </tr>
                            @endif                         
                            </tbody>
                           <!--  <tfoot>
                            <tr>
                                <th>Rendering engine</th>
                                <th>Browser</th>
                                <th>Platform(s)</th>
                                <th>Engine version</th>
                                <th>CSS grade</th>
                            </tr>
                            </tfoot> -->
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
