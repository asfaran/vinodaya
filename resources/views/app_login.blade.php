<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" >
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>@yield('title') | Vinodaya </title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="{{ url('/assets/css/bootstrap.min.css') }}" rel="stylesheet">

<!-- Font awesome -->
<link href="{{ url('/assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

<!-- Main Inspinia CSS files -->
<link href="{{ url('/assets/css/animate.css') }}" rel="stylesheet">
<link id="loadBefore" href="{{ url('/assets/css/style.css') }}" rel="stylesheet">
<style>
	.LBD_CaptchaImageDiv{

		float: left;
	}

	.captcha-form-control{

		background-color: #FFFBD3;
		border: 1px solid #6ba3c8;
		color: #000000;
		height: 43px;
		width: 100%;
	}

	.LBD_CaptchaImageDiv a{

		display: none;
	}

	.LBD_CaptchaIconsDiv{
		float: left;
	}
</style>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN LOGO -->

<div class="logo" style="align-content: center;">
	<center><a href="{{ url('/') }}" style="width:310px;">
		<img src="{{ url('/assets/img/vinodaya_logo.png') }}" alt="logo" class="logo-default"/>
	</a></center>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
	<div class="container-fluid">
    	@yield('content')
	</div>
</div>
<div class="copyright hide">
	 <?php echo date('Y') ?> &copy; Vinodaya.lk.
</div>
<!-- END LOGIN -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- jQuery and Bootstrap -->
<script src="{{ url('/assets/js/jquery/jquery-2.1.1.min.js') }}"></script>
<script src="{{ url('/assets/js/plugins/jquery-ui/jquery-ui.js') }}"></script>
<script src="{{ url('/assets/js/bootstrap/bootstrap.min.js') }}"></script>

<!-- MetsiMenu -->
<script src="{{ url('/assets/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>

<!-- SlimScroll -->
<script src="{{ url('/assets/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

<!-- Peace JS -->
<script src="{{ url('/assets/js/plugins/pace/pace.min.js') }}"></script>

<!-- Custom and plugin javascript -->
<script src="{{ url('/assets/js/inspinia.js') }}"></script>

<!-- Main Angular scripts-->
<script src="{{ url('/assets/js/angular/angular.min.js') }}"></script>
<script src="{{ url('/assets/js/angular/angular-sanitize.js') }}"></script>
<script src="{{ url('/assets/js/plugins/oclazyload/dist/ocLazyLoad.min.js') }}"></script>
<script src="{{ url('/assets/js/angular-translate/angular-translate.min.js') }}"></script>
<script src="{{ url('/assets/js/ui-router/angular-ui-router.min.js') }}"></script>
<script src="{{ url('/assets/js/bootstrap/ui-bootstrap-tpls-0.12.0.min.js') }}"></script>
<script src="{{ url('/assets/js/plugins/angular-idle/angular-idle.js') }}"></script>

<!--
 You need to include this script on any page that has a Google Map.
 When using Google Maps on your own site you MUST signup for your own API key at:
 https://developers.google.com/maps/documentation/javascript/tutorial#api_key
 After your sign up replace the key in the URL below..
-->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQTpXj82d8UpCi97wzo_nKXL7nYrd4G70"></script>

<!-- Anglar App Script -->
<script src="{{ url('/assets/js/app.js') }}"></script>
<script src="{{ url('/assets/js/config.js') }}"></script>
<script src="{{ url('/assets/js/translations.js') }}"></script>
<script src="{{ url('/assets/js/directives.js') }}"></script>
<script src="{{ url('/assets/js/controllers.js') }}"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- END JAVASCRIPTS -->
</body>

<!-- END BODY -->
</html>