<?php
define('PAGE_PARENT', 'home', true);
define('PAGE_CURRENT', 'home', true);
?>
@extends('app')

@section('title', 'Home')

@section('content')
<link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <!-- <span class="label label-success pull-right">Monthly</span> -->
                    <h5>Total Users</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">{{ $users_count }}</h1>

                    <!-- <div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div> -->
                    <small>Registered End Users</small>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <!-- <span class="label label-primary pull-right">Today</span> -->
                    <h5>Article Post Counts</h5>
                </div>
                <div class="ibox-content">
                    @if(count($article_count_array) > 0 )
                         @foreach($article_count_array  as $key => $value)
                         <ul class="list-group">
                            <li class="list-group-item">
                                <span class="badge badge-primary">{{ $value }}</span>
                                {{ $key }}
                            </li>
                        </ul>
                        @endforeach 
                    @else
                        <ul class="list-group">
                            <li class="list-group-item">
                                <span class="badge badge-primary">0</span>
                                Records
                            </li>
                        </ul>
                    @endif

                    <!-- <div class="stat-percent font-bold text-navy">44% <i class="fa fa-level-up"></i></div>
                    <small>New visits</small> -->
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <!-- <span class="label label-primary pull-right">Today</span> -->
                    <h5>Video Post Counts</h5>
                </div>
                <div class="ibox-content">
                    <!-- <h1 class="no-margins">106,120</h1> -->
                    @if(count($video_count_array) > 0 )
                         @foreach($video_count_array  as $key => $value)
                         <ul class="list-group">
                            <li class="list-group-item">
                                <span class="badge badge-primary">{{ $value }}</span>
                                {{ $key }}
                            </li>
                        </ul>
                        @endforeach 
                    @else
                        <ul class="list-group">
                            <li class="list-group-item">
                                <span class="badge badge-primary">0</span>
                                Records
                            </li>
                        </ul>
                    @endif

                    <!-- <div class="stat-percent font-bold text-navy">44% <i class="fa fa-level-up"></i></div>
                    <small>New visits</small> -->
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <!-- <span class="label label-info pull-right">Annual</span> -->
                    <h5>News Post Counts</h5>
                </div>
                <div class="ibox-content">
                @if(count($news_count_array) > 0 )
                     @foreach($news_count_array  as $key => $value)
                     <ul class="list-group">
                        <li class="list-group-item">
                            <span class="badge badge-primary">{{ $value }}</span>
                            {{ $key }}
                        </li>
                    </ul>
                    @endforeach 
                @else
                    <ul class="list-group">
                        <li class="list-group-item">
                            <span class="badge badge-primary">0</span>
                            Records
                        </li>
                    </ul>
                @endif  
                    

                    <!-- <div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div>
                    <small>New orders</small> -->
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <!-- <span class="label label-danger pull-right">Low value</span> -->
                    <h5>Image Post Counts</h5>
                </div>
                <div class="ibox-content">
                    @if(count($image_count_array) > 0 )
                         @foreach($image_count_array  as $key => $value)
                         <ul class="list-group">
                            <li class="list-group-item">
                                <span class="badge badge-primary">{{ $value }}</span>
                                {{ $key }}
                            </li>
                        </ul>
                        @endforeach 
                    @else
                        <ul class="list-group">
                            <li class="list-group-item">
                                <span class="badge badge-primary">0</span>
                                Records
                            </li>
                        </ul>
                    @endif

                    <!-- <div class="stat-percent font-bold text-danger">38% <i class="fa fa-level-down"></i></div>
                    <small>In first month</small> -->
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <!-- <span class="label label-danger pull-right">Low value</span> -->
                    <h5>Classified Post Couts</h5>
                </div>
                <div class="ibox-content">
                    @if(count($classified_count_array) > 0 )
                         @foreach($classified_count_array  as $key => $value)
                         <ul class="list-group">
                            <li class="list-group-item">
                                <span class="badge badge-primary">{{ $value }}</span>
                                {{ $key }}
                            </li>
                        </ul>
                        @endforeach 
                    @else
                        <ul class="list-group">
                            <li class="list-group-item">
                                <span class="badge badge-primary">0</span>
                                Records
                            </li>
                        </ul>
                    @endif

                    <!-- <div class="stat-percent font-bold text-danger">38% <i class="fa fa-level-down"></i></div>
                    <small>In first month</small> -->
                </div>
            </div>
        </div>
    </div>
    
</div>
@endsection
