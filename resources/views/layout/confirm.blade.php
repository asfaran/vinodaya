<?php
define('PAGE_PARENT', $view_data['page_parent'], true);
define('PAGE_CURRENT', $view_data['current_page'], true);
?>
@extends('app')

@section('title', $view_data['page_title'])

@section('content')

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5> {{ $view_data['section_title'] }}</h5>

                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                    <br />
                    <div class="note note-danger">
                        <h4 class="block">{{ $view_data['confirm_title'] }}</h4>
                        <p>
                            {{ $view_data['confirm_message'] }}
                        </p>
                    </div>

                    {!! Form::open() !!}
                    <input type="submit" class="btn btn-danger" name="submit_confirm" value="{{ $view_data['confirm_button'] or 'Confirm' }}" />
                    <a href="{{ $view_data['cancel_url'] }}" class="btn btn-default">{{ $view_data['cancel_button'] or 'Cancel' }}</a>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection