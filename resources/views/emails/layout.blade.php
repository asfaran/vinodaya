<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <title>
        @section('title')
        @show
        </title>
    </head>
    <body style="margin: 0px;background-color: whitesmoke;font-family: Arial;font-size: 13px;">
        <table  style="width:100%;border-collapse: collapse;">
            <tr style="border-bottom: 2px solid #7A3D96;background-color: rgb(255, 255, 255);">
                <td style="width: 10%"></td>
                <td style="padding-top: 10px;padding-bottom: 10px;">
                    <a href="{!! route('home') !!}">
                        <img width="100" alt="Vinodaya" src="{!! asset('assets/img/login.png') !!}" />
                    </a>
                </td>
                <td style="width: 10%"></td>
            </tr>
            <tr><td style="height: 20px;"></td></tr>
            <tr>
                <td style="width: 10%;background-color: whitesmoke;"></td>
                <td style="background-color: rgb(255, 255, 255);padding: 20px;">
                    @yield('content')
                </td>
                <td style="width: 10%;background-color: whitesmoke;"></td>
            </tr>
            <tr style="border-top: 1px solid rgb(234, 234, 234);background-color: rgb(250, 250, 250);">
                <td style="width: 10%;background-color: whitesmoke;"></td>
                <td style="padding: 10px;">
                    <p style="font-size: 14px;font-family: Arial;">Best Regards</p>
                    <p style="font-size: 14px;font-family: Arial;">Vinodaya Web Team</p>
                    <p style="font-size: 14px;font-family: Arial;">Web - <a href="www.vinodaya.org/" style="text-decoration: none;color: green;font-family: Arial;font-size: 14px;">www.vinodaya.org</a></p>
                    <p style="font-size: 14px;font-family: Arial;">Email - <a href="mailto:contact@vinodaya.org?Subject=Vinodaya Reeply" target="_top" style="text-decoration: none;color: green;font-family: Arial;font-size: 14px;">
                        contact@vinodaya.org
                    </a></p>
                </td>
                <td style="width: 10%;background-color: whitesmoke;"></td>
            </tr>
        </table>
    </body>
</html>