@extends('emails.layout')
@section('content')
    <p style="font-size: 14px;font-family: Arial;">Dear {!! $data['full_name'] !!},</p>
    <p style="font-size: 14px;font-family: Arial;">Welcome to Vinodaya!</p>
    <p style="font-size: 14px;font-family: Arial;">We're happy you're here. you can login to the system via <a href="{!! url('/user/login') !!}">this link.</a></p>

    <p style="font-size: 14px;font-family: Arial;"></p>
    <p style="font-size: 14px;font-family: Arial;text-align: center;">Your Email : {!! $data['email'] !!}</p>
    <p style="font-size: 14px;font-family: Arial;text-align: center;">Your Password : {!! $data['password'] !!}</p>
    @stop