@extends('emails.layout')
@section('content')
    <p style="font-size: 14px;font-family: Arial;">Dear {!! $data['full_name'] !!},</p>
    <p style="font-size: 14px;font-family: Arial;">The password for your Vinodaya account, was recently changed.</p>
    <p style="font-size: 14px;font-family: Arial;">If you made this change, then we're all set! Else reset password from  <a href="{!! URL::to('user/reset')  !!}">Reset Password</a> and secure your account!</p>

    @stop

