
$(document).ready(function() {
    $('input.select2').select2({
        allowClear: true,
        query: function(query) {
            var serch_key = (query.term)? query.term :'';
            var data = {results: []};
            var form = query.element.closest('form');           
            var cascade = query.element.attr('data-cascade');
            var foreign_field_name = query.element.attr('data-foreign-field');
            var post_data = {};
            if (typeof cascade !== 'undefined') {
                var cascaded_element = form.find('input[data-entity="' + cascade + '"]');                
                var cascaded_element_value = cascaded_element.val();
                var cascaded_element_name = cascaded_element.attr('name');                
                if (cascaded_element_value) {
                   if(cascaded_element_value.indexOf(',')) {
                        cc_e_vs = cascaded_element_value.split(',');                        
                        $.each(cc_e_vs, function(a, b) {                          
                           post_data['' + cascaded_element_name + '[' + a + ']'] = b;
                        });
                    } else {
                        post_data['' + cascaded_element_name + '[0]'] = cascaded_element_value;
                    }
                }
            }
            post_data['select_key'] = serch_key;

            $.ajax({
                url: base_url + '/' + query.element.attr('data-entity'),
                type: 'GET',
                data: post_data,
                dataType: 'json',
                success: function(response) { 
                    console.log(data);

                    $.each(response, function(index, item) {
                        data.results.push({
                            id: item.id,
                            text: item[foreign_field_name].toString()
                        });
                    });
                    query.callback(data);
                }
            });
        },
        initSelection: function(element, callback) {
          var id_post = {};            
            var id_arr = $(element).val();
            id_split = id_arr.split(',');
            $.each(id_split, function (a, b) {
                id_post['id[' + a + ']'] = b;
            });
            var foreign_field_name = $(element).attr('data-foreign-field');         
            $.ajax({
                url: base_url + '/' + element.attr('data-entity'),
                type: 'GET',
                data: id_post,
                dataType: 'json',
                success: function(response) {                    
                    callback({id: response[0].id, text: response[0][foreign_field_name].toString()});
                }
            });
        }
    }).on('change', function(e) {
    });
});