// jQuery UI autocomplete extension - suggest labels may contain HTML tags
        // github.com/scottgonzalez/jquery-ui-extensions/blob/master/src/autocomplete/jquery.ui.autocomplete.html.js
        (function($){var proto=$.ui.autocomplete.prototype,initSource=proto._initSource;function filter(array,term){var matcher=new RegExp($.ui.autocomplete.escapeRegex(term),"i");return $.grep(array,function(value){return matcher.test($("<div>").html(value.label||value.value||value).text());});}$.extend(proto,{_initSource:function(){if(this.options.html&&$.isArray(this.options.source)){this.source=function(request,response){response(filter(this.options.source,request.term));};}else{initSource.call(this);}},_renderItem:function(ul,item){return $("<li></li>").data("item.autocomplete",item).append($("<a></a>")[this.options.html?"html":"text"](item.label)).appendTo(ul);}});})(jQuery);




  



jQuery(document).ready(function() {

    /*$.ajaxSetup(
        {
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            }
        });*/

	  var cache = {};
	function googleSuggest(request, response) {
	    var term = request.term;
	    if (term in cache) { response(cache[term]); return; }
	    $.ajax({
	    	type: "POST",
	        url: base_url+'/admin/general/tags/suggestions',
	        dataType: 'JSON',
	        data: { 'term': term },
	        success: function(data) {
	        	
	            var suggestions = [];
	            try { var results = data; } catch(e) { var results = []; }
	            $.each(results, function() {
	                try {
	                    var s = this.toLowerCase();
	                    console.log(s);
	                    suggestions.push({label: s.replace(term, '<b>'+term+'</b>'), value: s});
	                } catch(e){}
	            });
	            cache[term] = suggestions;
	            response(suggestions);
	        }
	    });
	}



/*   $(".numeric").numeric();*/

    


       /* $(document).on('submit', '#form_post_comments', function(e) {

              e.preventDefault();

            $('#form_post_comments').validate({
            

                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {
                    author: {
                        required: true
                    },
                    content: {
                        required: true
                    },
                    author_email: {
                        required: true,
                        email: true
                    }
                },

                messages: {
                    author: {
                        required: "Username is required1."
                    },
                    content: {
                        required: "Password is required2."
                    },
                    author_email: {
                        required: "Password is required2."
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    $('.alert-danger', $('.login-form')).show();
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },

                errorPlacement: function (error, element) {
                    error.insertAfter(element.closest('.input-icon'));
                },

                submitHandler: function (e) {
                     alert("alert");
                    

                 
                    var formdata = $('#form_post_comments').serialize();
                    alert(formdata);  

                    $.ajax({

                    type: "POST",
                    url: base_url+"/user/postcomments",
                    data: {formdata, '_token': $('input[name=_token]').val()},
                    success: function (data) {

                        alert('works');
                        console.log(data);
                    },
                    error: function(e) {
                        alert('not works');
                        console.log(e.responseText);
                    }
                      
                    });
                }
        });



        });*/



    /*$(document).on('submit', '#form_post_comments', function(e) { 
                e.preventDefault();
                 
                var formdata = $('#form_post_comments').serialize();
                alert(formdata);  

                    $.ajax({

                    type: "POST",
                    url: base_url+"/user/postcomments",
                    data: {formdata, '_token': $('input[name=_token]').val()},
                    success: function (data) {

                        alert('works');
                        console.log(data);
                    },
                    error: function(e) {
                        alert('not works');
                        console.log(e.responseText);
                    }
                      
                    });
    }); */ 


        $( ".cmnet" ).each(function( index ) {
         $(this).children(".po-cmnt").slice(-5).show();
        });
    
        $(".shw-cmt").click(function(e){ // click event for load more
            e.preventDefault();
            var done = $('<div class="shw-cmt"><a href="">All Comments Loaded</a></div>');
            $(this).siblings(".shw-cmt:hidden").slice(-5).show(); // select next 5 hidden divs and show them
            if($(this).siblings(".shw-cmt:hidden").length == 0){ // check if any hidden divs
                $(this).replaceWith(done); // if there are none left
            }
        });
     


    $('body').on('click', '#cldp_clbutton', function() {

        var selected_class = $(this).attr("class");
        replace_text = $("#cldp_cl_value").val();
        
        $(this).text(replace_text);
        $(this).removeAttr("class");
        $(this).width('auto');
      
    });


   $('body').on('click', '#cldp_mailbutton', function() {
        var selected_class = $(this).attr("class");
        replace_text = $("#cldp_mail_value").val();

        $(this).text(replace_text);
        $(this).removeAttr("class");
        $(this).width('auto');

      
    });

    


    $('body').on('change', '#postadds_type', function() {

        var adds_type = $(this).val();


        $.ajax({
            type: "POST",
            url: base_url+"/user/postads/image_records",
            data: {'adds_type' : adds_type},
            dataType: 'HTML',
            success: function (data) {

                $("#image_records").html(data);

                console.log(data);
            },
            error: function(e) {
                console.log(e.responseText);
            }
        });



    });


 	$('body').on('change', '#select_category_type', function() {

        var category_type = $(this).val();


        $.ajax({
            type: "POST",
            url: base_url+"/admin/general/category/filter",
            data: {'category_type' : category_type},
            dataType: 'HTML',
            success: function (data) {

            	$("#table_body_category_type").html(data);

            	console.log(data);
            },
            error: function(e) {
                console.log(e.responseText);
            }
        });



    });




  $('body').on('click', '.comments_status', function() {

        var selected_span = $(this).attr("data-status");
        var data_value = $(this).attr("data-value");
        var button_class = $(this).attr("class");
            

        if (confirm('Are you sure you want to change status?')) {


             $.ajax({
                type: "POST",
                url: base_url+"/admin/comments/status_change",
                data: {'selected_span' : selected_span, 'data_value' : data_value, '_token': $('input[name=_token]').val()},
                dataType: 'HTML',
                success: function (data) {
                    if(data != null){
                        $("#comments_status_div"+data_value).html(data);
                    }


                    console.log(data);
                },
                error: function(e) {
                    console.log(e.responseText);
                }
            });



        }

    });




    $('body').on('change', '.classified_add_form_selection', function() {

        var selected_value = $(this).val();
        var selected_field = $(this).attr("id");



        if($(this).attr("id") == "classified_parentid"){

            var parent_id = selected_value;
            var category_id = 0;

        }else{

            var parent_id =  $('#classified_parentid').val();;
            var category_id = selected_value;
        } 



        $.ajax({
                type: "GET",
                url: base_url+"/user/extrafieldsfilter",
                data: {'parent_id' : parent_id, 'category_id' : category_id},
                dataType: 'HTML',
                success: function (data) {

                    $("#classified_add_form_fields_rows").html(data);

                    console.log(data);
                },
                error: function(e) {
                    console.log(e.responseText);
                }
            });



    });



    $('body').on('change', '.classified_location_selection', function() {


        
        var type = $(this).attr("id");
        var value = $(this).val();


        $.ajax({
            type: "GET",
            url: base_url+"/admin/classified/location/classified_filter",
            data: {'location_type' : type, 'value' : value},
            dataType: 'JSON',
            success: function (data) {
                var div_id = data.field_id;
                $("#div_"+div_id).html(data.result);

                console.log(data);
            },
            error: function(e) {
                console.log(e.responseText);
            }
        });



    });






    $('body').on('change', '#select_location_type', function() {
        var location_type = $(this).val();
        if(location_type){

            $.ajax({
                type: "POST",
                url: base_url+"/admin/classified/location/filter",
                data: {'location_type' : location_type},
                dataType: 'HTML',
                success: function (data) {

                    $("#table_body_location_list").html(data);

                    console.log(data);
                },
                error: function(e) {
                    console.log(e.responseText);
                }
            });

        }


    });


    $('body').on('click', '#company_detail_selection', function() {
        var span_class = $(this).attr("class");
        if(span_class == "regi"){
            $(this).attr( "class", "regi reg-don" );
            $('#company_selection').prop('checked', true);
            $("#register_company_name").removeAttr('disabled');
            $("#register_company_addr").removeAttr('disabled');
            $("#register_company_contact").removeAttr('disabled');

        }else{
            $(this).attr( "class", "regi" );
            $('#company_selection').prop('checked', false);
            $("#register_company_name").attr('disabled','disabled');
            $("#register_company_addr").attr('disabled','disabled');
            $("#register_company_contact").attr('disabled','disabled');

        }

     });



    $('body').on('change', '#classified_parentid', function() {

        var category_id = $(this).val();
           $.ajax({
                type: "GET",
                url: base_url+"/user/classified_filter",
                data: {'category_id' : category_id},
                dataType: 'HTML',
                success: function (data) {

                    $("#classified_category").html(data);

                    console.log(data);
                },
                error: function(e) {
                    console.log(e.responseText);
                }
            });


    });








 


	


})
