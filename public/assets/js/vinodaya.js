// jQuery UI autocomplete extension - suggest labels may contain HTML tags
        // github.com/scottgonzalez/jquery-ui-extensions/blob/master/src/autocomplete/jquery.ui.autocomplete.html.js
        (function($){var proto=$.ui.autocomplete.prototype,initSource=proto._initSource;function filter(array,term){var matcher=new RegExp($.ui.autocomplete.escapeRegex(term),"i");return $.grep(array,function(value){return matcher.test($("<div>").html(value.label||value.value||value).text());});}$.extend(proto,{_initSource:function(){if(this.options.html&&$.isArray(this.options.source)){this.source=function(request,response){response(filter(this.options.source,request.term));};}else{initSource.call(this);}},_renderItem:function(ul,item){return $("<li></li>").data("item.autocomplete",item).append($("<a></a>")[this.options.html?"html":"text"](item.label)).appendTo(ul);}});})(jQuery);




  



jQuery(document).ready(function() {

    $.ajaxSetup(
        {
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            }
        });

	  var cache = {};
	function googleSuggest(request, response) {
	    var term = request.term;
	    if (term in cache) { response(cache[term]); return; }
	    $.ajax({
	    	type: "POST",
	        url: base_url+'/admin/general/tags/suggestions',
	        dataType: 'JSON',
	        data: { 'term': term },
	        success: function(data) {
	        	
	            var suggestions = [];
	            try { var results = data; } catch(e) { var results = []; }
	            $.each(results, function() {
	                try {
	                    var s = this.toLowerCase();
	                    console.log(s);
	                    suggestions.push({label: s.replace(term, '<b>'+term+'</b>'), value: s});
	                } catch(e){}
	            });
	            cache[term] = suggestions;
	            response(suggestions);
	        }
	    });
	}



/*   $(".numeric").numeric();*/


 	$('body').on('change', '#select_category_type', function() {

        var category_type = $(this).val();


        $.ajax({
            type: "POST",
            url: base_url+"/admin/general/category/filter",
            data: {'category_type' : category_type},
            dataType: 'HTML',
            success: function (data) {

            	$("#table_body_category_type").html(data);

            	console.log(data);
            },
            error: function(e) {
                console.log(e.responseText);
            }
        });



    });




  $('body').on('click', '.comments_status', function() {

        var selected_span = $(this).attr("data-status");
        var data_value = $(this).attr("data-value");
        var button_class = $(this).attr("class");
            

        if (confirm('Are you sure you want to change status?')) {


             $.ajax({
                type: "POST",
                url: base_url+"/admin/comments/status_change",
                data: {'selected_span' : selected_span, 'data_value' : data_value, '_token': $('input[name=_token]').val()},
                dataType: 'HTML',
                success: function (data) {
                    if(data != null){
                        $("#comments_status_div"+data_value).html(data);
                    }


                    console.log(data);
                },
                error: function(e) {
                    console.log(e.responseText);
                }
            });



        }

    });




    $('body').on('change', '.classified_add_form_selection', function() {

        var selected_value = $(this).val();
        var selected_field = $(this).attr("id");


        if($(this).attr("id") == "classified_parentid"){

            var parent_id = selected_value;
            var category_id = 0;

        }else{

            var parent_id =  $('#classified_parentid').val();;
            var category_id = selected_value;
        } 


        $.ajax({
                type: "GET",
                url: base_url+"/admin/classified/extrafieldsfilter",
                data: {'parent_id' : parent_id, 'category_id' : category_id},
                dataType: 'HTML',
                success: function (data) {

                    $("#classified_add_form_fields_rows").html(data);

                    console.log(data);
                },
                error: function(e) {
                    console.log(e.responseText);
                }
            });



    });



    $('body').on('change', '.classified_location_selection', function() {


        
        var type = $(this).attr("id");
        var value = $(this).val();


        $.ajax({
            type: "GET",
            url: base_url+"/admin/classified/location/classified_filter",
            data: {'location_type' : type, 'value' : value},
            dataType: 'JSON',
            success: function (data) {
                var div_id = data.field_id;
                $("#div_"+div_id).html(data.result);

                console.log(data);
            },
            error: function(e) {
                console.log(e.responseText);
            }
        });



    });






    $('body').on('change', '#select_location_type', function() {
        var location_type = $(this).val();
        if(location_type){

            $.ajax({
                type: "POST",
                url: base_url+"/admin/classified/location/filter",
                data: {'location_type' : location_type},
                dataType: 'HTML',
                success: function (data) {

                    $("#table_body_location_list").html(data);

                    console.log(data);
                },
                error: function(e) {
                    console.log(e.responseText);
                }
            });

        }


    });



    $('body').on('change', '#classified_parentid', function() {

        var category_id = $(this).val();
           $.ajax({
                type: "GET",
                url: base_url+"/admin/general/form/classified_filter",
                data: {'category_id' : category_id},
                dataType: 'HTML',
                success: function (data) {

                    $("#classified_category").html(data);

                    console.log(data);
                },
                error: function(e) {
                    console.log(e.responseText);
                }
            });


    });






	$('body').on('click', '.post_edit', function() {

		var span_id = $(this).attr("id");
        $("#div_"+span_id).show();
        $("#div_"+span_id).parents('div').show();
    });


    $('body').on('click', '.post_slug_edit', function() {

		$("#div_post_slug").show();
        $("#post_slug_div").show();
        $("#post_slug_span").hide();
        $(".post_slug_edit").hide();
    });


     $('body').on('click', '.post_slug_close', function() {

        $("#div_post_slug").hide();
        $("#post_slug_div").hide();
        $("#post_slug_span").show();
        $(".post_slug_edit").show();
    });

     $('body').on('click', '.post_slug_selection', function(event) {

     	var change_value =  $('#post_slug_value').val();
     	$("#post_slug_span").html(change_value);
     	$("#post_slug_span").show();
     	$("#post_slug_div").hide();
     	$(".post_slug_edit").show();
     	$("#div_post_slug").hide();


 	  });


     $('body').on('click', '#field_selection_icon', function() {

       if($('#field_selection_row').is(":visible")){

        var html_data = $('#field_selection_row').html;

          $( "#field_selection_row" ).after($('#field_selection_row').clone());
       }else{
          $( "#field_selection_row" ).show();
       }
    });


     $('body').on('click', '.field_close_icon', function() {

        $(this).closest('div').hide();
    });




    $('body').on('click', '.post_edit_close', function() {

		var div_id = $(this).parents('div').parents('div').attr("id");
        $("#"+div_id).hide();
    });


	$('body').on('click', '.post_selection', function(event) {

		
		var target_type = $(this).parents('div').attr("data-id");

		

		if(!target_type || target_type  === undefined || target_type === null){
			var target_type = $(this).parents('div').parents('div').attr("data-id");
			
		}
		
		if(target_type === 'select'){

			var change_value =  $(this).closest('div').find('select').val();

		}else if(target_type === 'input'){

			var change_value =  $(this).closest('div').find('input').val();
			
		}else if(target_type === 'radio'){

			var change_value =  $(this).closest('div').find('input:checked').val();
		}

		var div_id = $(this).parents('div').parents('div').attr("id");

		
		var record_div = div_id.substring(4);

		$("#" + record_div).closest('div').find('span').html(change_value);
		var rowData = JSON.stringify(change_value);

		$("#" + record_div + "_value").val(change_value);

		$("#"+div_id).hide();

        
    });


    $('body').on('change', '#menu_type', function() {
        var menu_type = $(this).val();

        $(".content_div").hide();
        $("#content_div_"+menu_type).show();



    });


    $('body').on('keyup', '#form_title', function() {
        var title = $(this).val();
        var slug_lower = title.toLowerCase(); 
        var slug_lower = slug_lower.replace(/\s+/g,"-");
        
        $("#form_slug").val(slug_lower);
        
    });


    $('#hero-demo').tagEditor({
                placeholder: 'Enter tags ...',
                autocomplete: { source: googleSuggest, minLength: 3, delay: 250, html: true, position: { collision: 'flip' } }
            });


	


})
