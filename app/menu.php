<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class menu extends Model {

	protected $table = 'menus';

    protected $fillable = ['label', 'title', 'type', 'content', 'icon', 'parentid', 'display', 'access', 'listorder', 'modified_by', 'last_modified'];



    public function scopeByType($query, $type)
    {
        return $query->where("type", $type);
    }

}
