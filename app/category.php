<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class category extends Model {

	protected $table = 'categories';

    protected $fillable = ['title', 'slug', 'description', 'type', 'parentid', 'display', 'icon', 'access', 'listorder', 'modified_by', 'last_modified'];


    public function post()
    {
        return $this->hasMany('App\Posts', 'category_id');
    }


    public function scopeByType($query, $type)
    {
        return $query->where("type", $type);
    }

}
