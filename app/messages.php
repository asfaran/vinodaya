<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class messages extends Model {

	protected $table = 'messages';

    protected $fillable = ['user_id', 'id', 'name', 'email', 'mobile', 'subject', 'message', 'featured_at', 'published_at', 'deleted_at', 'updated_at'];

}
