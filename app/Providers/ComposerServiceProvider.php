<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		view()->composer(
            'front.topnavbar',
            'App\Http\ViewComposers\HomeComposer'
        );

        view()->composer(
            'front.quick_links',
            'App\Http\ViewComposers\QuicklinkComposer'
        );
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

}
