<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class emails extends Model {

	use SoftDeletes;

    protected $table = 'emails';

    protected $fillable = ['id','title', 'sender', 'sender_email', 'content','published_at', 'deleted_at', 'updated_at'];

    protected $dates = ['published_at','deleted_at'];

    protected $softDelete = true;

}
