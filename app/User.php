<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
//use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
//use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    CanResetPasswordContract
{

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';


	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['usertype', 'username', 'username_slug', 'location', 'name', 'surname', 'genre', 'about', 'facebookurl', 'twitterurl', 'weburl', 'email', 'icon', 'password', 'f_name', 'l_name', 'mobile', 'company_status', 'company_name', 'company_addr', 'company_contact'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];


    public function posts(){

        return $this->hasMany('App\Posts', 'user_id');

    }


    public static function findByUsernameOrFail($username,$columns = array('*')) {
        if ( ! is_null($user = static::where('username_slug', $username)->first($columns))) {
            return $user;
        }

        abort(404);
    }


    /**
     * Check user owns related model
     *
     * @param $related
     * @return bool
     */
    public function userifowns($related){

        return $this->id == $related->user_id;

    }

    public function userifhaveyourowns($relateduser){

        return $this->id == $relateduser->id;

    }



    public function setUsername_slugAttribute($username)
    {
        return $this->attributes['username_slug'] = str_slug($username, '-');
    }
}
