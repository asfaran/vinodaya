<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use DB;

class QuicklinkComposer
{
    public $movieList = [];
    /**
     * Create a movie composer.
     *
     * @return void
     */
    public function __construct()
    {
        $this->social_links = [
            'Shawshank redemption',
            'Forrest Gump',
            'The Matrix',
            'Pirates of the Carribean',
            'Back to the future',
        ];
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
       	$quick_category_array = array();

		$quick_categories = DB::table('categories')->where('display', '=', 1)->get();
		foreach($quick_categories  as $key => $category){
			$quick_category_array[$category->type][$category->id] = $category->title ;
		}

        $view->with('quick_category_array', $quick_category_array);
    }
}