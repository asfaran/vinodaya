<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use DB;

class HomeComposer
{
    public $movieList = [];
    /**
     * Create a movie composer.
     *
     * @return void
     */
    public function __construct()
    {
        $this->social_links = [
            'Shawshank redemption',
            'Forrest Gump',
            'The Matrix',
            'Pirates of the Carribean',
            'Back to the future',
        ];
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $social_links = DB::table('socials')->get();
        $menu_list = DB::table('menus')->groupBy('id')->get();
        $breaking_news_count = DB::table('options')->where('name', '=', 'breaking_news_count')->first();
        $news_list = DB::table('news')->where('featured', '=', 1)->orderBy('id', 'desc')->take((int)$breaking_news_count->options)->get();

        $view->with('social_links', $social_links);
        $view->with('menu_list', $menu_list);
        $view->with('news_list', $news_list);
    }
}