<?php namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminMiddleware {





	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if (!($request->user()) || $request->user()->usertype === 3)
        {
            return redirect('home');
        }

		return $next($request);
	}






	/*
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	/*public function handle($request, Closure $next)
	{
		foreach( $this->except as $route )
        {
            if( $request->is( $route ) ) return $next($request);
        }

		return parent::handle($request, $next);
	}*/



}
