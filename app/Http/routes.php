<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::get('/', 'front\HomeController@index');
Route::get('home', ['as' => 'home', 'uses' => 'front\HomeController@index']);
Route::post('search', ['as' => 'search', 'uses' => 'front\FrontController@homeSearch']);

Route::get('articles-lp', ['as' => 'articles-lp', 'uses' => 'front\FrontController@articlesLP']);
Route::get('article/{id}', ['as' => 'article', 'uses' => 'front\FrontController@articlesDP']);
Route::get('articles-lp/category/{id}', ['as' => 'articlecat', 'uses' => 'front\FrontController@articlesCategoryLP']);

Route::get('images-lp', ['as' => 'images-lp', 'uses' => 'front\FrontController@imagesLP']);
Route::get('images-category/{id}', ['as' => 'images-category', 'uses' => 'front\FrontController@imagesCategoryDP']);
Route::get('images/{id}', ['as' => 'images-dp', 'uses' => 'front\FrontController@imagesDP']);
Route::get('images-lp/category/{id}', ['as' => 'imagecat', 'uses' => 'front\FrontController@imageCategoryLP']);


Route::get('news-lp', ['as' => 'news-lp', 'uses' => 'front\FrontController@newsLP']);
Route::get('news-category/{id}', ['as' => 'news-category', 'uses' => 'front\FrontController@newsCategoryLP']);
Route::get('news/{id}', ['as' => 'news', 'uses' => 'front\FrontController@newsDP']);
Route::get('news-lp/category/{id}', ['as' => 'newscat', 'uses' => 'front\FrontController@newsCategoryLP']);


Route::get('video-lp', ['as' => 'video-lp', 'uses' => 'front\FrontController@videoLP']);
Route::get('video-dp/{id}', ['as' => 'video-dp', 'uses' => 'front\FrontController@videoDP']);
Route::get('video-lp/category/{id}', ['as' => 'videocat', 'uses' => 'front\FrontController@videoCategoryLP']);


Route::get('classified-lp', ['as' => 'classified-lp', 'uses' => 'front\FrontController@classifiedLP']);
Route::get('classified/{id}', ['as' => 'classified', 'uses' => 'front\FrontController@classifiedDP']);
Route::get('classifieds/location/{id}', ['as' => 'classifieds.location', 'uses' => 'front\FrontController@classifiedLocation']);
Route::get('classifieds/category/{id}', ['as' => 'classifieds.category', 'uses' => 'front\FrontController@classifiedCategory']);
Route::post('classifieds/search', ['as' => 'classifieds.search', 'uses' => 'front\FrontController@classifiedSearch']);


Route::get('informative/{id}', ['as' => 'informative', 'uses' => 'front\FrontController@informativePage']);
Route::get('contact-us', ['as' => 'contact-us', 'uses' => 'front\FrontController@contactUsPage']);

Route::group([ 'prefix' => 'user'], function() {
    Route::any('/', 'front\ClassifiedController@myads');
    Route::get('login', ['as' => 'user.login', 'uses' => 'front\UserController@login']);
    Route::get('logout', ['as' => 'auth.logout', 'uses' => 'front\UserController@logout']);
    Route::post('login', 'front\UserController@postLogin');
    Route::get('register', ['middleware' => 'cors', 'as' => 'user.register', 'uses' => 'front\UserController@register'] );
    Route::post('register', 'front\UserController@postRegister');
    Route::get('reset', ['as' => 'user.reset', 'uses' => 'front\UserController@reset']);
    Route::post('reset', ['as' => 'user.reset', 'uses' => 'front\UserController@postPassword']);


    Route::post('messages', ['as' => 'user.messages', 'uses' => 'front\UserController@postMessages']);


    Route::get('forgot', ['as' => 'user.forgot', 'uses' => 'front\UserController@forgot']);
    Route::post('forgot', ['as' => 'user.forgot', 'uses' => 'front\UserController@forgotPassword']);


    Route::any('settings', 'front\UserController@settings');

    Route::get('upgrade', ['as' => 'user.upgrade', 'uses' => 'front\UserController@upgrade']);
    Route::post('upgrade', ['as' => 'user.upgrade', 'uses' => 'front\UserController@postUpgrade']);

    
    Route::get('myads', ['as' => 'user.myads', 'uses' => 'front\ClassifiedController@myads']);
    Route::get('myposts', ['as' => 'user.myposts', 'uses' => 'front\ClassifiedController@myPosts']);

    Route::post('postcomments', ['as' => 'user.comments', 'uses' => 'front\FrontController@postComments']);
    Route::any('extrafieldsfilter', ['as' => 'user.extrafieldsfilter', 'uses' => 'front\ClassifiedController@extrafieldsfilter'] );
    Route::any('classified_filter', 'front\ClassifiedController@classifiedFilter');

    Route::post('postclassified', 'front\ClassifiedController@classifiedStore');
    Route::get('postclassified', 'front\ClassifiedController@postclassified');

    Route::post('postads', 'front\ClassifiedController@store');
    Route::get('postads', 'front\ClassifiedController@postads');
    Route::post('postads/image_records', 'front\ClassifiedController@imageRecords');
    Route::any('editads/{id}', 'front\ClassifiedController@editads');
    Route::any('deleteads/{id}', 'front\ClassifiedController@destroy');

    Route::any('editposts/{id}', 'front\ClassifiedController@editPosts');
    Route::any('deleteposts/{id}', 'front\ClassifiedController@destroyPosts');
    Route::any('changestatus/{id}', 'front\ClassifiedController@changestatus');
    Route::any('adds_status/{id}', 'front\ClassifiedController@addsStatus');
    Route::get('paypal_return', ['as' => 'paypal_return', 'uses' => 'front\ClassifiedController@paypal_return']);
    Route::post('paypal_return', ['as' => 'paypal_return', 'uses' => 'front\ClassifiedController@paypal_return']);
    Route::get('paypal_cancel', ['as' => 'paypal_cancel', 'uses' => 'front\ClassifiedController@paypal_cancel']);
});




Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

/*$router->get('auth/login', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@login']);
$router->post('auth/login', ['as' => 'auth.authenticate', 'uses' => 'Auth\AAuthController@authenticate']);
$router->get('auth/logout', ['as' => 'auth.logout', 'uses' => 'Auth\AAuthController@logout']);

//register
$router->get('auth/register', ['as' => 'auth.register', 'uses' => 'Auth\AAuthController@getRegister']);
$router->post('auth/register', ['as' => 'auth.register', 'uses' => 'Auth\AAuthController@postRegister']);
*/
Route::get('addnewform',  'FormController@addnewform');

Route::get('/admin', 'WelcomeController@index');

Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware', 'prefix' => 'admin'], function() {

	

    Route::get('dashboard', 'HomeController@index');

    Route::get('/layout/ibox_tools', 'HomeController@IboxTools');
    Route::get('/layout/ibox_tools_full_screen', 'HomeController@IboxToolsFull');

    Route::any('/general/category/list', 'CategoryController@index');
    Route::any('/general/category/add_new', 'CategoryController@create');  ///not used
    Route::any('/general/category/edit/{id}', 'CategoryController@edit');
    Route::any('/general/category/delete/{id}', 'CategoryController@destroy');
    Route::any('/general/category/store', 'CategoryController@store');
    Route::any('/general/category/filter', 'CategoryController@filter');

    Route::any('/general/tags/list', 'TagsController@index');
    Route::any('/general/tags/add_new', 'TagsController@create');   ///not used
    Route::any('/general/tags/edit/{id}', 'TagsController@edit');
    Route::any('/general/tags/delete/{id}', 'TagsController@destroy');
    Route::any('/general/tags/store', 'TagsController@store');
    Route::any('/general/tags/suggestions', 'TagsController@suggestions');

    Route::any('/general/page/list', 'PagesController@index');
    Route::any('/general/page/add_new', 'PagesController@create');
    Route::any('/general/page/edit/{id}', 'PagesController@edit');
    Route::any('/general/page/store', 'PagesController@store');
    Route::any('/general/page/delete/{id}', 'PagesController@destroy');
    Route::any('/general/page/contact', 'PagesController@contact');
    Route::any('/general/page/contact_store', 'PagesController@contactStore');


    Route::any('/general/menu/list', 'MenuController@index');
    Route::any('/general/menu/add_new', 'MenuController@create');
    Route::any('/general/menu/edit/{id}', 'MenuController@edit');
    Route::any('/general/menu/delete/{id}', 'MenuController@destroy');
    Route::any('/general/menu/store', 'MenuController@store');


    Route::any('/general/image_upload', 'FormController@imageUpload');
    Route::any('/general/form/list', 'FormController@index');
    Route::any('/general/form/edit/{id}', 'FormController@edit');
    Route::any('/general/form/delete/{id}', 'FormController@destroy');
    Route::any('/general/form/store', 'FormController@store');
    Route::any('/general/form/classified_fields', 'FormController@classifiedFields');
    Route::any('/general/form/classified_store', 'FormController@classifiedStore');
    Route::any('/general/form/classified_filter', 'FormController@classifiedFilter');
    Route::any('/general/form/classified_delete/{id}', 'FormController@classifieddestroy');
    Route::any('/general/form/classified_edit/{id}', 'FormController@classifiededit');

    Route::any('/settings/language', 'LanguageController@index');
    Route::any('/settings/language/list', 'LanguageController@index');
    Route::any('/settings/language/store', 'LanguageController@store');

    Route::any('/settings/social/list', 'SocialController@index');
    Route::any('/settings/social/store', 'SocialController@store');
    Route::any('/settings/social/delete', 'SocialController@destroy');


    Route::any('/article/list', 'ArticleController@index');
    Route::any('/article/add_new', 'ArticleController@create');
    Route::any('/article/edit/{id}', 'ArticleController@edit');
    Route::any('/article/delete/{id}', 'ArticleController@destroy');
    Route::any('/article/display', 'ArticleController@display');
    Route::any('/article/configuration', 'ArticleController@configuration');
    Route::any('/article/display_store', 'ArticleController@display_store');
    Route::any('/article/store', 'ArticleController@store');
    Route::any('/article/upload', 'ArticleController@upload');


    Route::any('/news/list', 'NewsController@index');
    Route::any('/news/add_new', 'NewsController@create');
    Route::any('/news/edit/{id}', 'NewsController@edit');
    Route::any('/news/delete/{id}', 'NewsController@destroy');
    Route::any('/news/display', 'NewsController@display');
    Route::any('/news/configuration', 'NewsController@configuration');
    Route::any('/news/display_store', 'NewsController@display_store');
    Route::any('/news/store', 'NewsController@store');
    Route::any('/news/comments', 'NewsController@comments');
    Route::any('/news/upload', 'NewsController@upload');


    Route::any('/classified/list', 'ClassifiedController@index');
    Route::any('/classified/add_new', 'ClassifiedController@create');
    Route::any('/classified/edit/{id}', 'ClassifiedController@edit');
    Route::any('/classified/delete/{id}', 'ClassifiedController@destroy');
    Route::any('/classified/display', 'ClassifiedController@display');
    Route::any('/classified/display_store', 'ClassifiedController@display_store');
    Route::any('/classified/configuration', 'ClassifiedController@configuration');
    Route::any('/classified/upload', 'ClassifiedController@upload');
    Route::any('/classified/store', 'ClassifiedController@store');
    Route::any('/classified/extrafieldsfilter', 'ClassifiedController@extrafieldsfilter');
    Route::any('/classified/classified_status/{id}', 'ClassifiedController@classifiedStatus');


    Route::any('/category/settings', 'CategoryController@index');

    Route::any('/classified/location', 'LocationController@index');
    Route::any('/classified/location/list', 'LocationController@index');
    Route::any('/classified/location/add_new', 'LocationController@create');
    Route::any('/classified/location/edit/{id}', 'LocationController@edit');
    Route::any('/classified/location/delete/{id}', 'LocationController@destroy');
    Route::any('/classified/location/store', 'LocationController@store');
    Route::any('/classified/location/filter', 'LocationController@filter');
    Route::any('/classified/location/classified_filter', 'LocationController@classifiedfilter');
    



    Route::any('/classified/adds', 'AddsController@index');
    Route::any('/classified/adds/manager', 'AddsController@index');
    Route::any('/classified/adds/list', 'AddsController@show');
    Route::any('/classified/adds/add_new', 'AddsController@create');
    Route::any('/classified/adds/edit/{id}', 'AddsController@edit');
    Route::any('/classified/adds/delete/{id}', 'AddsController@destroy');
    Route::any('/classified/adds/store', 'AddsController@store');
    Route::any('/classified/adds/store_new', 'AddsController@storeNew');
    Route::any('/classified/adds/settings', 'AddsController@settings');
    Route::any('/classified/adds/edit_setting/{id}', 'AddsController@editSettings');
    Route::any('/classified/adds/store_settings', 'AddsController@storeSettings');
    Route::any('/classified/adds/delete_setting/{id}', 'AddsController@destroySettings');
    Route::any('/classified/adds/payments', 'AddsController@payments');
    Route::any('/classified/adds/change_status/{id}', 'AddsController@statusChange');


    Route::any('/classified/mails', 'EmailsController@index');
    Route::any('/classified/mails/list', 'EmailsController@index');
    Route::any('/classified/mails/add_new', 'EmailsController@create');
    Route::any('/classified/mails/edit/{id}', 'EmailsController@edit');
    Route::any('/classified/mails/delete/{id}', 'EmailsController@destroy');
    Route::any('/classified/mails/store', 'EmailsController@store');

    Route::any('/image/list', 'ImageController@index');
    Route::any('/image/add_new', 'ImageController@create');
    Route::any('/image/edit/{id}', 'ImageController@edit');
    Route::any('/image/delete/{id}', 'ImageController@destroy');
    Route::any('/image/display', 'ImageController@display');
    Route::any('/image/configuration', 'ImageController@configuration');
    Route::any('/image/display_store', 'ImageController@display_store');
    Route::any('/image/upload', 'ImageController@upload');
    Route::any('/image/store', 'ImageController@store');


    Route::any('/video/list', 'VideoController@index');
    Route::any('/video/add_new', 'VideoController@create');
    Route::any('/video/edit/{id}', 'VideoController@edit');
    Route::any('/video/delete/{id}', 'VideoController@destroy');
    Route::any('/video/display_store', 'VideoController@display_store');
    Route::any('/video/display', 'VideoController@display');
    Route::any('/video/configuration', 'VideoController@configuration');
    Route::any('/video/store', 'VideoController@store');

    Route::any('/comments/list', 'CommentsController@index');
    Route::any('/comments/add_new', 'CommentsController@create');
    Route::any('/comments/edit/{id}', 'CommentsController@edit');
    Route::any('/comments/delete/{id}', 'CommentsController@destroy');
    Route::any('/comments/display/{type}', 'CommentsController@display');
    Route::any('/comments/configuration', 'CommentsController@configuration');
    Route::any('/comments/store', 'CommentsController@store');
    Route::any('/comments/status_change', 'CommentsController@statusChange');


    Route::any('/messages/list', 'HomeController@messagesList');


    Route::any('/users/list', 'Auth\AuthController@index');
    Route::any('/users/add_new', 'Auth\AuthController@create');
    Route::any('/users/edit/{id}', 'Auth\AuthController@edit');
    Route::any('/users/delete/{id}', 'Auth\AuthController@destroy');

    
});




