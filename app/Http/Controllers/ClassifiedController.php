<?php namespace App\Http\Controllers;

use App\classified;
use App\options;
use App\tags;
use DB;
use Session;
use DateTime;
Use Validator;
use Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;

class ClassifiedController extends Controller {

	public function __construct()
    {
        $this->middleware('auth');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$top_add_option_row =  DB::table('options')->where('name', '=', 'adds_top_add_per_click')->first();
		$top_add_option = $top_add_option_row->options;
		$featured_add_option_row =  DB::table('options')->where('name', '=', 'adds_featured_add_per_click')->first();
		$featured_add_option = $featured_add_option_row->options;


		$option_row =  DB::table('options')->where('name', '=', 'adds_duration_days')->first();
		$duration_type = array_map('trim', explode(',', $option_row->options));
		$duration_type_key = 1;
			$new_duration_type[0]= "Select One";
		foreach($duration_type  as $key => $duration){
			$new_duration_type[$duration_type_key]= $duration;
			$duration_type_key++;
		}



		$classifieds_list = DB::table('classifieds')
		->select('classifieds.*','parent.title As parent_name',
		 'category.title AS category_name', 'province.title AS province_name',
		  'city.title AS city_name', 'town.title AS town_name', 'building.title AS building_name')
		->leftJoin('categories AS parent','classifieds.parentid','=','parent.id')
		->leftJoin('categories AS category','classifieds.category','=','category.id')
		->leftJoin('locations AS province','classifieds.location_province','=','province.id')
		->leftJoin('locations AS city','classifieds.location_city','=','city.id')
		->leftJoin('locations AS town','classifieds.location_town','=','town.id')
		->leftJoin('locations AS building','classifieds.location_building','=','building.id')
		->groupBy('classifieds.id')
		->get();

		foreach($classifieds_list  as $classified){
			if($classified->featured === 1){
				$boost_row = DB::table('post_boosts')->where('classifieds_id','=' ,$classified->id)->first();
				if($boost_row){
					$classified->boost_type = $boost_row->boost_type;
					$classified->boost_Start = $boost_row->created_at;
					$classified->budget = $boost_row->budget;
					$classified->boost_status = $boost_row->status;
					$classified->duration  = $new_duration_type[$boost_row->duration];
					$classified->expire = \Carbon\Carbon::createFromTimeStamp(strtotime($boost_row->created_at))->addDays($new_duration_type[$boost_row->duration]);

				}else{
					$classified->boost_Start = "";
					$classified->budget = 0;
					$classified->duration  = 0;
					$classified->expire = "";
					$classified->boost_type = 0;
					$classified->boost_status = 0;
				}

			}else{
				$classified->boost_Start = "";
				$classified->budget = 0;
				$classified->duration  = 0;
				$classified->expire = "";
				$classified->boost_type = 0;
				$classified->boost_status = 0;

			}

		}

		$status_array = array('0'=>'Pending Review', '1'=>'Draft', '2'=>'Published' );
		return view('admin.classified.list', 
			['classifieds_list' => $classifieds_list, 
			'status_array' => $status_array,
			'top_add_option' => $top_add_option,
			'featured_add_option' => $featured_add_option]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$category_list = DB::table('categories')
		->select('categories.id','categories.title','parent.id As parent_id', 'parent.title AS parent_title')
		->leftJoin('categories AS parent','categories.parentid','=','parent.id')
		->where('categories.type','=',5)
		->get();

		if($category_list){
			$category_array[0] ="[Select a Category]";
			foreach($category_list  as $category){
				$category_array[$category->id]= $category->title;
				if($category->parent_id !== 0){
					$subcategory_array[$category->id]= $category->title;
				}

			}
		}else{
			$category_array[0] ="[No Category For Select]";
			$subcategory_array[0] ="[No Category For Select]";
		}

		$locations_result = DB::table('locations')->get();

		if($locations_result){
			$location_array['province'][0] ="[Select a Province]";
			$location_array['city'][0] ="[Select a City]";
			$location_array['town'][0] ="[Select a Town]";
			$location_array['building'][0] ="[Select a Building]";

			foreach($locations_result  as $locations){
				if($locations->type == 1){
					$location_array['province'][$locations->id]= $locations->title;

				}else if($locations->type == 2){
					$location_array['city'][$locations->id]= $locations->title;

				}else if($locations->type == 3){
					$location_array['town'][$locations->id]= $locations->title;
					
				}else{
					$location_array['building'][$locations->id]= $locations->title;
				}
				
			}
		}else{
			$location_array['province'][0] ="[No Province For Select]";
			$location_array['city'][0] ="[No City For Select]";
			$location_array['town'][0] ="[No Town For Select]";
			$location_array['building'][0] ="[No Building For Select]";
		}


		$status_array = array('0'=>'Pending Review', '1'=>'Draft', '2'=>'Published' );
		
		return view('admin.classified.create', 
			['category_list' => $category_array,
			'subcategory_array' => $subcategory_array,
			'location' => $location_array,
			'status_array' => $status_array]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{

		$status_array = array('0'=>'Pending Review', '1'=>'Draft', '2'=>'Published' );

		$v = Validator::make($request->all(), [
			'id' => 'integer',
			'title' => 'required',
			'parentid' => 'required|integer',
			'category' => 'required|integer',
			'post_status' => 'required',
			'post_featured' => 'required',
			'post_publish' => 'required',
			'post_visibility' => 'required',
			'description' => 'required',
			'thumb' => 'mimes:jpg,jpeg,gif,png',
			'location_province' => 'required|integer',
			'location_city' => 'required|integer',
			'location_town' => 'integer',
			'location_building' => 'integer',
			

		]);

		/*'category' => 'required',*/
		if ($v->fails())
		{
			return redirect()->back()->withInput()->withErrors($v->errors());
		}else{

			$tags_array = array_filter(explode(",",$request->input('tags')));
			if($tags_array){
				foreach($tags_array  as $key=>$tag){
					$line_row = DB::table('tags')->where('title','=' ,$tag)->first();
					if(!$line_row || empty($line_row) || $line_row == null){

						$slug = preg_replace('/\s+/', '-', strtolower($tag));

						$new_tags = new tags();
						$new_tags->title = $tag;
						$new_tags->slug = $slug;
						$new_tags->save();

					}
				}
			}

			$parent_cat_row = DB::table('categories')->where('id','=' ,$request->input('parentid'))->first();

			if($request->input('id')){


				$title_lowercase = strtolower($request->input('post_slug'));
				$slug = preg_replace('/\s+/', '-', $title_lowercase);

				$line_row = DB::table('classifieds')->where('slug','=' ,$slug)->where('id','!=' ,$request->input('id'))->first();

				

				if($line_row){
					for ($i = 1; $i <= 100; ++$i) {
						$search_slug = $slug."-".$i;
						$sub_row = DB::table('classifieds')->where('slug','=' ,$search_slug)->where('id', '!=', $request->input('id'))->first();
						if($sub_row){
							continue;
						}else{

							$slug = $search_slug;
							 break;
						}

					}
				}


				if($request->file('thumb')){

					$image_path = $this->upload($request->file('thumb'), $request->input('parentid'));
				}



				if($request->input('submit_draft')) {
					$post_status = ($request->input('post_status') == 'Draft')? 1 : 0  ;

				}else{


					$line_row = DB::table('classifieds')->where('id','=' ,$request->input('id'))->first();

					if($line_row->status == 2){

						$post_status = array_search($request->input('post_status'), $status_array) ;
					}else{
						$post_status =  2;
					}

					

				}
	

				$classified = new classified();
				$classified->exists = true;
				$classified->id = (int)$request->input('id');
				$classified->title = $request->input('title');
				if(isset($image_path)){
					$classified->thumb = $image_path;
				}
				
				/*$classified->user_id = Auth::user()->id;*/
				$classified->user_id = 1;
				$classified->tags = $request->input('tags');
				$classified->status = $post_status;
				$classified->slug = $slug;
				$classified->featured  = ($request->input('post_featured') == 'Yes')? 1 : 0  ;
				$classified->visibility = ($request->input('post_visibility') == 'Public')? 1 : 0  ;
				$classified->parentid = $request->input('parentid');
				$classified->category = $request->input('category');
				$classified->extra_fields = json_encode( $request->input('extra_fields') );
				$classified->description = $request->input('description');
				$classified->location_province = $request->input('location_province');
				$classified->location_city = $request->input('location_city');
				$classified->location_town = $request->input('location_town');
				$classified->location_building = $request->input('location_building');
				$classified->price = $request->input('price');
				$classified->save();

				if($classified){
					

					return redirect('/admin/classified/list/')->with('flash_success', 'Record Updated successfully!.');

				}else{

					return redirect('/admin/classified/list/')->with('flash_warning', 'Record Not Updated!.');

				}
				

				



			}else{

				
				$title_lowercase = strtolower($request->input('title'));
				$slug = preg_replace('/\s+/', '-', $title_lowercase);

				$line_row = DB::table('classifieds')->where('slug','=' ,$slug)->first();

				if($line_row){
					for ($i = 1; $i <= 100; ++$i) {
						$search_slug = $slug."-".$i;
						$sub_row = DB::table('classifieds')->where('slug','=' ,$search_slug)->first();
						if($sub_row){
							continue;
						}else{

							$slug = $search_slug;
							 break;
						}

					}
				}


				if($request->file('thumb')){

					$image_path = $this->upload($request->file('thumb'), $request->input('parentid'));
				}

				if($request->input('submit_draft')) {
					$post_status = ($request->input('post_status') == 'Draft')? 1 : 0  ;

				}else{

					$post_status = 2;

				}


				$classified = new classified();
				$classified->title = $request->input('title');
				if(isset($image_path)){
					$classified->thumb = $image_path;
				}
				
				/*$classified->user_id = Auth::user()->id;*/
				$classified->user_id = 1;
				$classified->tags = $request->input('tags');
				$classified->status = $post_status;
				$classified->slug = $slug;
				$classified->featured  = ($request->input('post_featured') == 'Yes')? 1 : 0  ;
				$classified->visibility = ($request->input('post_visibility') == 'Public')? 1 : 0  ;
				$classified->parentid = $request->input('parentid');
				$classified->category = $request->input('category');
				$classified->extra_fields = json_encode( $request->input('extra_fields') );
				$classified->description = $request->input('description');
				$classified->location_province = $request->input('location_province');
				$classified->location_city = $request->input('location_city');
				$classified->location_town = $request->input('location_town');
				$classified->location_building = $request->input('location_building');
				$classified->price = $request->input('price');
				$classified->save();

				return redirect('/admin/classified/list')->with('flash_success', 'Record Inserted successfully!.');

			}

		
		}
	}




	public function upload($file, $category)
    {
    	/*if($request->hasFile('file')) {
            $file = $request->file('file');

           // $tmpUserName = Auth::user()->id .'-' ;

            $tmpTimeToken = time(); //for if same image was uploaded.

            $tmpFileName = $tmpTimeToken.'-'.$file->getClientOriginalName();

            $tmpFilePath = 'upload/tmp/';

            $hardPath =   date('Y-m') .'/'.date('d') .'/';


            if (!file_exists(public_path() .'/'.$tmpFilePath.$hardPath )) {
                $oldmask = umask(0);
                mkdir(public_path() .'/'. $tmpFilePath.$hardPath , 0777, true);
                umask($oldmask);
            }

            $img = Image::make($file);

            $imgmj = $img->mime();

            if($type!=='preview' and $imgmj=='image/gif'){

                $file->move(public_path() . '/'. $tmpFilePath. $hardPath, $tmpUserName.$tmpFileName);

                $path = '/'.$tmpFilePath .$hardPath . $tmpUserName. $tmpFileName;

            }else{

                    if($type=='entry'){

                        $img->resize(640, null, function ($constraint) {
                         $constraint->aspectRatio();
                         $constraint->upsize();
                        });

                    }else if($type=='preview'){

                        $img->fit(650, 370);

                    }


                $path = $tmpFilePath .$hardPath . $tmpUserName. md5($tmpFileName). '.jpg';

                $img->save($path);

                $path='/'.$path;
            }

        }*/
        $input = array('image' => $file);
        $rules = array(
            'image' => 'image'
        );
        $validator = Validator::make($input, $rules);
        if ( $validator->fails()) {
            return Response::json(array('success' => false, 'errors' => $validator->getMessageBag()->toArray()));
        }
 
        $fileName = time() . '-' . $file->getClientOriginalName();
        $destination = public_path() . '/uploads/classifieds/'.$category.'/';
        $file->move($destination, $fileName);
 
        return $fileName;
    }




    public function extrafieldsfilter(Request $request)
    {

        $location_type_array = array('1'=>'province', '2'=>'city', '3'=>'town', '4'=>'building' );


        $parent_id  = Input::get('parent_id');
        $category_id  = Input::get('category_id');



        $extra_fields_list = DB::table('classified_fields')
						->select('id','fields')
						->where('category','=',$category_id)
						->where('parentid','=', $parent_id)
						->get();


		$fields_array_set = array();



		foreach($extra_fields_list  as $id => $fields){
			if($fields->fields){

				$fields_array = json_decode($fields->fields, TRUE);
					foreach($fields_array  as $id => $field_row){

						$extra_fields_row = DB::table('form_fields')
								->where('id','=',$field_row)
								->first();
						$fields_array_set[] =  $extra_fields_row;


					}
			}
				
		}

		$fields_array_set = array_filter($fields_array_set);


         return view('admin.classified.extra_fields_filter', ['fields_array_set' => $fields_array_set]);


    }




	public function configuration()
	{
		//
	}


	public function display()
	{
		$options_result = DB::table('options')->get();

		$option_list = array();

		if($options_result){
			foreach($options_result  as $key => $value){

					$option_list[$value->name] = $value->options;
							
			}

		}
		return view('admin.classified.display', ['option_list' => $option_list]);
	}




public function classifiedStatus($id, Request $request)
	{
		$classi_row = DB::table('classifieds')->where('id', '=', $id)->first();
		if($classi_row->status !== 2){
			$new_status = 2;
			$status_title = "Publish";
		}else{
			$new_status = 0; 
			$status_title = "Un Publish";
		}
		
		if ($request->input('submit_confirm')) {
            try {
                $affectedRows = DB::table('classifieds')->where('id', '=', $id)->update(array('status' => $new_status));

				if($affectedRows){
					
					/* action log insertion */
					
					/* action log insertion */
					return redirect('/admin/classified/list')->with('flash_success', 'Classified Status Changed Successfully!.');
				}else{
					return redirect('/admin/classified/list')->with('flash_message', 'Classified Status not Changed,Try again...');
				}
            }
            catch (\Exception $ex) {
                Session::flash('flash_message', $ex->getMessage());
                Session::flash('flash_type', 'error');
            }
        }

        $data = [
            'view_data' => [
                'page_parent' => 'classified',
                'current_page' => 'classified_list',
                'page_title' => 'Change Classified Display Status',
                'section_title' => 'Change Classified Display Status',
                'confirm_title' => "Are you sure you want to ".$status_title." the selected Classified?",
                'confirm_message' => 'Please confirm.',
                'confirm_button' => 'Update',
                'cancel_url' => url('/admin/classified/list'),
            ],
        ];
        return view('layout.confirm', $data);
	}




	public function AddStatus($id, Request $request)
	{
		$classi_row = DB::table('classifieds')->where('id', '=', $id)->first();
		if($classi_row->status !== 2){
			$new_status = 2;
			$status_title = "Publish";
		}else{
			$new_status = 0; 
			$status_title = "Un Publish";
		}
		
		if ($request->input('submit_confirm')) {
            try {
                $affectedRows = DB::table('classifieds')->where('id', '=', $id)->update(array('status' => $new_status));

				if($affectedRows){
					
					/* action log insertion */
					
					/* action log insertion */
					return redirect('/admin/classified/list')->with('flash_success', 'Classified Status Changed Successfully!.');
				}else{
					return redirect('/admin/classified/list')->with('flash_message', 'Classified Status not Changed,Try again...');
				}
            }
            catch (\Exception $ex) {
                Session::flash('flash_message', $ex->getMessage());
                Session::flash('flash_type', 'error');
            }
        }

        $data = [
            'view_data' => [
                'page_parent' => 'classified',
                'current_page' => 'classified_list',
                'page_title' => 'Change Classified Display Status',
                'section_title' => 'Change Classified Display Status',
                'confirm_title' => "Are you sure you want to ".$status_title." the selected Classified?",
                'confirm_message' => 'Please confirm.',
                'confirm_button' => 'Update',
                'cancel_url' => url('/admin/classified/list'),
            ],
        ];
        return view('layout.confirm', $data);
	}




	public function display_store(Request $request)
	{
		$data  = Input::all();

		$return_result = array();

		foreach($data as $key => $value) {
			if($key  != "_token"){

				$option_one = DB::table('options')
						->where('name', '=', $key)
						->first();
				
				if($option_one){
					$option_row = DB::table('options')->where('name', '=', $key)->update(array('options' => $value));
				}else{

					$new_option = new options();
					$new_option->name = $key;
					$new_option->options =  $value;
					$new_option->save();


				}
			}
	
		}

		return redirect('/admin/classified/display')->with('flash_success', 'Record Updated successfully!.');
	}



	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$category_list = DB::table('categories')
		->select('categories.id','categories.title','parent.id As parent_id', 'parent.title AS parent_title')
		->leftJoin('categories AS parent','categories.parentid','=','parent.id')
		->where('categories.type','=',5)
		->get();



		$classified_one = DB::table('classifieds')
					->where('id', '=', $id)
					->first();
		$parent_cat_row = DB::table('categories')->where('id','=' ,$classified_one->parentid)->first();

		if($category_list){
			$category_array[0] ="[Select a Category]";
			foreach($category_list  as $category){
				$category_array[$category->id]= $category->title;
				if($category->parent_id !== 0){
					$subcategory_array[$category->id]= $category->title;
				}

			}
		}else{
			$category_array[0] ="[No Category For Select]";
			$subcategory_array[0] ="[No Category For Select]";
		}

		$locations_result = DB::table('locations')->get();

		if($locations_result){
			$location_array['province'][0] ="[Select a Province]";
			$location_array['city'][0] ="[Select a City]";
			$location_array['town'][0] ="[Select a Town]";
			$location_array['building'][0] ="[Select a Building]";

			foreach($locations_result  as $locations){
				if($locations->type == 1){
					$location_array['province'][$locations->id]= $locations->title;

				}else if($locations->type == 2){
					$location_array['city'][$locations->id]= $locations->title;

				}else if($locations->type == 3){
					$location_array['town'][$locations->id]= $locations->title;
					
				}else{
					$location_array['building'][$locations->id]= $locations->title;
				}
				
			}
		}else{
			$location_array['province'][0] ="[No Province For Select]";
			$location_array['city'][0] ="[No City For Select]";
			$location_array['town'][0] ="[No Town For Select]";
			$location_array['building'][0] ="[No Building For Select]";
		}


		$status_array = array('0'=>'Pending Review', '1'=>'Draft', '2'=>'Published' , '3'=>'Published' );
		
		return view('admin.classified.update', 
			['category_list' => $category_array,
			'subcategory_array' => $subcategory_array,
			'location' => $location_array,
			'classified_one' => $classified_one,
			'category_name' => $parent_cat_row->slug,
			'status_array' => $status_array]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id, Request $request)
	{
		
		if ($request->input('submit_confirm')) {
            try {
                $affectedRows = DB::table('classifieds')->where('id', '=', $id)->delete();

				if($affectedRows){
					
					/* action log insertion */
					
					/* action log insertion */
					return redirect('/admin/classified/list')->with('flash_success', 'Selected Classified Deleted Successfully!.');
				}else{
					return redirect('/admin/classified/list')->with('flash_message', 'Selected Classified not deleted,Try again...');
				}
            }
            catch (\Exception $ex) {
                Session::flash('flash_message', $ex->getMessage());
                Session::flash('flash_type', 'error');
            }
        }

        $data = [
            'view_data' => [
                'page_parent' => 'classified',
                'current_page' => 'classified_list',
                'page_title' => 'Delete Classified',
                'section_title' => 'Confirm Delete Selection',
                'confirm_title' => 'Are you sure you want to delete the selected Classified?',
                'confirm_message' => 'This action can not be undone. Please confirm.',
                'confirm_button' => 'Delete',
                'cancel_url' => url('/admin/classified/list'),
            ],
        ];
        return view('layout.confirm', $data);
	}


}
