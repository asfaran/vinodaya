<?php namespace App\Http\Controllers;

use App\images;
use App\options;
use App\tags;
use DB;
use Session;
use DateTime;
Use Validator;
use Auth;
use Image;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;


class ImageController extends Controller {


	public function __construct()
    {
        $this->middleware('auth');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$category_array = array();

		$category_list = DB::table('categories')
		->select('categories.id','categories.title','parent.id As parent_id', 'parent.title AS parent_title')
		->leftJoin('categories AS parent','categories.parentid','=','parent.id')
		->where('categories.type','=',3)
		->get();



		/*$category_list = DB::table('categories')->select('id', 'title')->get();*/

		if($category_list){
			foreach($category_list  as $category){
				$category_array[$category->id]= $category->title;

			}
		}

		$status_array = array('0'=>'Pending Review', '1'=>'Draft', '2'=>'Published' );


		$image_list = DB::table('images')
					->leftJoin('comments', 'comments.post_id', '=', 'images.id')
					->leftJoin('users', 'users.id', '=', 'images.user_id')
					->select(array('images.*' , DB::raw('COUNT(comments.id) as comments_count') , 'users.name as author'))
					->groupBy('images.id')
					->get();

			/*->where('comments.post_type','=',1)*/
			
			foreach($image_list  as $image){
				$category_names = "";
				foreach(json_decode($image->category_id, TRUE)  as $key => $value){

					if (array_key_exists((int)$value, $category_array)) {
						$category_names .=  $category_array[(int)$value];
						$category_names .= " | ";
					}
							
				}

			
				if($image->category_id = $category_names){

				}
				
			}


		return view('admin.image.list', [
			'image_list' => $image_list, 
			'category_array' => $category_array, 
			'status_array' => $status_array]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$category_list = DB::table('categories')
		->select('categories.id','categories.title','parent.id As parent_id', 'parent.title AS parent_title')
		->leftJoin('categories AS parent','categories.parentid','=','parent.id')
		->where('categories.type','=',3)
		->get();

		$status_array = array('0'=>'Pending Review', '1'=>'Draft', '2'=>'Published' );
	

		return view('admin.image.create', ['category_list' => $category_list, 'status_array' => $status_array]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$status_array = array('0'=>'Pending Review', '1'=>'Draft', '2'=>'Published' );

		$v = Validator::make($request->all(), [
			'id' => 'integer',
			'title' => 'required',
			'post_status' => 'required',
			'post_featured' => 'required',
			'category' => 'required|min:1',
			'post_publish' => 'required',
			'post_visibility' => 'required',
			'body' => 'required',
			'thumb' => 'mimes:jpg,jpeg,gif,png',

		]);

		/*'category' => 'required',*/
		if ($v->fails())
		{
			return redirect()->back()->withInput()->withErrors($v->errors());
		}else{

			$tags_array = array_filter(explode(",",$request->input('tags')));
			if($tags_array){
				foreach($tags_array  as $key=>$tag){
					$line_row = DB::table('tags')->where('title','=' ,$tag)->first();
					if(!$line_row || empty($line_row) || $line_row == null){

						$slug = preg_replace('/\s+/', '-', strtolower($tag));

						$new_tags = new tags();
						$new_tags->title = $tag;
						$new_tags->slug = $slug;
						$new_tags->save();

					}
				}
			}

			if($request->input('id')){


				$title_lowercase = strtolower($request->input('post_slug'));
				$slug = preg_replace('/\s+/', '-', $title_lowercase);

				$line_row = DB::table('images')->where('slug','=' ,$slug)->where('id', '!=' ,$request->input('id'))->first();

				if($line_row){
					for ($i = 1; $i <= 100; ++$i) {
						$search_slug = $slug."-".$i;
						$sub_row = DB::table('images')->where('slug','=' ,$search_slug)->where('id', '!=', $request->input('id'))->first();
						if($sub_row){
							continue;
						}else{

							$slug = $search_slug;
							 break;
						}

					}
				}


				if($request->file('thumb')){

					$image_path = $this->upload($request->file('thumb'));
				}



				if($request->input('submit_draft')) {
					$post_status = ($request->input('post_status') == 'Draft')? 1 : 0  ;

				}else{

					$line_row = DB::table('images')->where('id','=' ,$request->input('id'))->first();

					if($line_row->status == 2){

						$post_status = array_search($request->input('post_status'), $status_array) ;
					}else{
						$post_status =  2;
					}

					

				}
	

				$new_image = new images();
				$new_image->exists = true;
				$new_image->id = (int)$request->input('id');
				$new_image->title = $request->input('title');
				if(isset($image_path)){
					$new_image->thumb = $image_path;
				}
				
				/*$new_image->user_id = Auth::user()->id;*/
				$new_image->user_id = 1;
				$new_image->tags = $request->input('tags');
				$new_image->status = $post_status;
				$new_image->slug = $slug;
				$new_image->featured  = ($request->input('post_featured') == 'Yes')? 1 : 0  ;
				/*$new_image->publish  = ($request->input('post_publish') == 'Public')? 1 : 0  ;*/
				$new_image->visibility = ($request->input('post_visibility') == 'Public')? 1 : 0  ;
				$new_image->category_id = json_encode( $request->input('category') );
				$new_image->body = $this->postSummernote($request->input('body'));
				$new_image->save();

				if($new_image){
					

					return redirect('/admin/image/edit/'.$request->input('id'))->with('flash_success', 'Record Updated successfully!.');

				}else{

					return redirect('/admin/image/edit/'.$request->input('id'))->with('flash_success', 'Record Not Updated!.');

				}
				

				



			}else{

				
				$title_lowercase = strtolower($request->input('title'));
				$slug = preg_replace('/\s+/', '-', $title_lowercase);

				$line_row = DB::table('images')->where('slug','=' ,$slug)->first();

				if($line_row){
					for ($i = 1; $i <= 100; ++$i) {
						$search_slug = $slug."-".$i;
						$sub_row = DB::table('images')->where('slug','=' ,$search_slug)->first();
						if($sub_row){
							continue;
						}else{

							$slug = $search_slug;
							 break;
						}

					}
				}


				if($request->file('thumb')){

					$image_path = $this->upload($request->file('thumb'));
				}

				if($request->input('submit_draft')) {
					$post_status = ($request->input('post_status') == 'Draft')? 1 : 0  ;

				}else{

					$post_status = 2;

				}


				$new_image = new images();
				$new_image->title = $request->input('title');
				if(isset($image_path)){
					$new_image->thumb = $image_path;
				}
				/*$new_image->user_id = Auth::user()->id;*/
				$new_image->user_id = 1;
				$new_image->slug = $slug;
				$new_image->tags = $request->input('tags');
				$new_image->status = $post_status  ;
				$new_image->featured  = ($request->input('post_featured') == 'Yes')? 1 : 0  ;
				/*$new_image->publish  = ($request->input('post_publish') == 'Public')? 1 : 0  ;*/
				$new_image->visibility = ($request->input('post_visibility') == 'Public')? 1 : 0  ;
				$new_image->category_id = json_encode( $request->input('category') );
				$new_image->body = $this->postSummernote($request->input('body'));
				$new_image->save();

				return redirect('/admin/image/list')->with('flash_success', 'Record Inserted successfully!.');

			}

		
		}
	}



	public function postSummernote($message)
	{

        $dom = new \DomDocument('1.0', 'UTF-8');
        /*@$dom->loadHtml($message, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);*/
        @$dom->loadHtml( mb_convert_encoding($message, 'HTML-ENTITIES', "UTF-8"), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
        $images = $dom->getElementsByTagName('img');
       // foreach <img> in the submited message
        foreach($images as $img){
            $src = $img->getAttribute('src');
            
            // if the img source is 'data-url'
            if(preg_match('/data:image/', $src)){                
                // get the mimetype
                preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                $mimetype = $groups['mime'];                
                // Generating a random filename
                $filename = uniqid();
                $filepath = "/uploads/images/summernoteimages/$filename"."."."$mimetype";    
                // @see http://image.intervention.io/api/
                $image = Image::make($src)
                  // resize if required
                  /* ->resize(300, 200) */
                  ->encode($mimetype, 100)  // encode file to the specified mimetype
                  ->save(public_path($filepath));                
                $new_src = asset($filepath);
                $img->removeAttribute('src');
                $img->setAttribute('src', $new_src);
            } // <!--endif
        } // <!-
        $message = $dom->saveHTML();
        return $message;

	}








	public function upload($file)
    {
        $input = array('image' => $file);
        $rules = array(
            'image' => 'image'
        );
        $validator = Validator::make($input, $rules);
        if ( $validator->fails()) {
            return Response::json(array('success' => false, 'errors' => $validator->getMessageBag()->toArray()));
        }
 
        $fileName = time() . '-' . $file->getClientOriginalName();
        $destination = public_path() . '/uploads/images/';
        $file->move($destination, $fileName);
 
        return $fileName;
    }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function display()
	{
		$options_result = DB::table('options')->get();

		$option_list = array();

		if($options_result){
			foreach($options_result  as $key => $value){

					$option_list[$value->name] = $value->options;
							
			}

		}
		return view('admin.image.display', ['option_list' => $option_list]);
	}



	public function display_store(Request $request)
	{
		$data  = Input::all();

		foreach($data as $key => $value) {
			if($key  != "_token"){

				$option_one = DB::table('options')
						->where('name', '=', $key)
						->first();
				if($option_one){
					$option_row = options::where('name', $key)->update(array('options' => $value));
				}else{

					$new_option = new options();
					$new_option->name = $key;
					$new_option->options =  $value;
					$new_option->save();


				}
			}
	
		}

		return redirect('/admin/image/display')->with('flash_success', 'Record Updated successfully!.');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$category_list = DB::table('categories')
		->select('categories.id','categories.title','parent.id As parent_id', 'parent.title AS parent_title')
		->leftJoin('categories AS parent','categories.parentid','=','parent.id')
		->where('categories.type','=',3)
		->get();


		$image_one = DB::table('images')
					->where('id', '=', $id)
					->first();

		$status_array = array('0'=>'Pending Review', '1'=>'Draft', '2'=>'Published' );

		$selected_category = array_values(json_decode($image_one->category_id, TRUE));


		return view('admin.image.update', ['category_list' => $category_list, 
			'image_one' => $image_one, 
			'selected_category' => $selected_category, 
			'status_array' => $status_array]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id, Request $request)
    {
        
        if ($request->input('submit_confirm')) {
            try {
                $affectedRows = DB::table('images')->where('id', '=', $id)->delete();

				if($affectedRows){
					
					/* action log insertion */
					
					/* action log insertion */
					return redirect('/admin/image/list')->with('flash_success', 'Selected Image Deleted Successfully!.');
				}else{
					return redirect('/admin/image/list')->with('flash_message', 'Selected Image not deleted,Try again...');
				}
            }
            catch (\Exception $ex) {
                Session::flash('flash_message', $ex->getMessage());
                Session::flash('flash_type', 'error');
            }
        }

        $data = [
            'view_data' => [
                'page_parent' => 'image',
                'current_page' => 'image_list',
                'page_title' => 'Delete Image Post ',
                'section_title' => 'Confirm Delete Selection',
                'confirm_title' => 'Are you sure you want to delete the selected Image Post?',
                'confirm_message' => 'This action can not be undone. Please confirm.',
                'confirm_button' => 'Delete',
                'cancel_url' => url('/admin/image/list/'),
            ],
        ];
        return view('layout.confirm', $data);

    }







}
