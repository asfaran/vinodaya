<?php namespace App\Http\Controllers;

use App\comments;
use DB;
use Session;
use DateTime;
Use Validator;
use Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;

class CommentsController extends Controller {


	public function __construct()
    {
        $this->middleware('auth');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		 $type_array = array(
		 	'1'=>'News',
		 	'2'=>'Article',
		 	'3'=>'Image',
		 	'4'=>'Video',
		 	'5'=>'Classified' );

		$comments_list = DB::table('comments')->orderBy('id', 'desc')->get();

		$comments_list = array_filter($comments_list);



		return view('admin.comments.list', ['comments_list' => $comments_list, 'type_array' => $type_array]);
	}



	public function display($post_type)
	{
		 $type_array = array(
		 	'1'=>'News',
		 	'2'=>'Article',
		 	'3'=>'Image',
		 	'4'=>'Video',
		 	'5'=>'Classified' );

		 $post_type_name = $type_array[$post_type];

			$comments_list = DB::table('comments')->where('post_type', '=', $post_type)->orderBy('id', 'desc')->get();

			$comments_list = array_filter($comments_list);


			return view('admin.comments.display', 
				['comments_list' => $comments_list, 
				'type_array' => $type_array, 
				'post_type' => $post_type,
				'post_type_name' => $post_type_name]);
	}




	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$v = Validator::make($request->all(), [
			'id' => 'integer',
			'author' => 'required',
			'author_email' => 'required|email',
			'content' => 'required',
			'approved' => 'required',

		]);

		if ($v->fails())
		{
			return redirect()->back()->withInput()->withErrors($v->errors());
		}else{

			$approved_value = ($request->input('approved') != null)? 1 : 0;

			if($request->input('id')){

				$comments = comments::find($request->input('id'));
				$comments->author = $request->input('author');
				$comments->author_email = $request->input('author_email');
				$comments->content = $request->input('content');
				$comments->approved = $approved_value;
				$comments->parentid = $request->input('parentid');
				$comments->description = $request->input('description');
				$comments->save();


				return redirect('/admin/comments/list')->with('flash_success', 'Record Updated successfully!.');



			}else{

				$new_comment = new comments();
				$new_comment->author = $request->input('author');
				$new_comment->author_email = $request->input('author_email');
				$new_comment->content = $request->input('content');
				$new_comment->approved = $approved_value;
				$new_comment->save();

				return redirect('/admin/comments/list')->with('flash_success', 'Record Inserted successfully!.');

			}

		
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}



	public function statusChange(Request $request)
    {

        $selected_span  = Input::get('selected_span');
        $data_value  = Input::get('data_value');


        if($selected_span == "comments_approved"){

        	$comment_row =  DB::table('comments')->where('id', '=', $data_value)->update(array('approved' => 0));

        }else{

        	$comment_row =  DB::table('comments')->where('id', '=', $data_value)->update(array('approved' => 1));
        }
        


		if(isset($comment_row)){
			return view('admin.comments.status_change', ['selected_span' => $selected_span, 'comment_id' => $data_value ]);
		}else{
			return null;
		}

         


    }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

		$type_array = array(
		 	'1'=>'News',
		 	'2'=>'Article',
		 	'3'=>'Image',
		 	'4'=>'Video',
		 	'5'=>'Classified' );


		$comment_one = DB::table('comments')->where('id', '=', $id)->first();


		return view('admin.comments.update', 
			['comment_one' => $comment_one, 
			'type_array' => $type_array]);

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id, Request $request)
	{
		
		if ($request->input('submit_confirm')) {
            try {
                $affectedRows = DB::table('comments')->where('id', '=', $id)->delete();

				if($affectedRows){
					
					/* action log insertion */
					
					/* action log insertion */
					return redirect('/admin/comments/list/')->with('flash_success', ' Comments Deleted Successfully!.');
				}else{
					return redirect('/admin/comments/list/')->with('flash_message', 'Comments not deleted,Try again...');
				}
            }
            catch (\Exception $ex) {
                Session::flash('flash_message', $ex->getMessage());
                Session::flash('flash_type', 'error');
            }
        }

        $data = [
            'view_data' => [
                'page_parent' => 'general',
                'current_page' => 'comments_list',
                'page_title' => 'Delete Comments',
                'section_title' => 'Confirm Delete Selection',
                'confirm_title' => 'Are you sure you want to delete the selected Comments?',
                'confirm_message' => 'This action can not be undone. Please confirm.',
                'confirm_button' => 'Delete',
                'cancel_url' => url('/admin/comments/list/'),
            ],
        ];
        return view('layout.confirm', $data);




	}

}
