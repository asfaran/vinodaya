<?php

namespace App\Http\Controllers;


use App\form_fields;
use App\classified_fields;
use DB;
use Session;
use DateTime;
Use Validator;
use Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {

        $form_fields_list = DB::table('form_fields')->get();

        if($form_fields_list){
            $fields_array[0] ="[Select a fields]";
            foreach($form_fields_list  as $fields_list){
                $fields_array[$fields_list->id]= $fields_list->title;

            }
        }else{
            $fields_array[0] ="[No Category For Select]";
        }

        $field_types = array('text' => 'text', 'file'=>'file', 'checkbox'=>'Tick box', 'radio'=>'radio', 'number'=>'number', 'select'=>'select', 'date'=>'date', 'textarea'=>'textarea','li'=>'Bullet', 'comma'=>'comma', 'row'=>'row', 'image'=>'image', 'checkmark'=>'checkmark', 'Punctuation'=>'Colon' );

        return view('admin.classified.form_fields_list', ['form_fields_list' => $form_fields_list, 
            'fields_array' => $fields_array,'field_types' => $field_types]);
    }



    public function imageUpload()
    {
        $data  = Input::all();

        $files = Input::file('file');

        if($files)
        {
            // Iterating over the array
            // "file" should be an instance of UploadedFile
            if(is_object($files)) {
               dd('ssdddsdsd');
               
            }
        }
        
        /*$filename = uniqid();
        $dir_name = "uploads/";
        move_uploaded_file($_FILES['file']['tmp_name'],$dir_name.$_FILES['file']['name']);
        echo $dir_name.$_FILES['file']['name'];*/

    }





    public function classifiedFields()
    {

        $category_list = DB::table('categories')
        ->select('categories.id','categories.title','parent.id As parent_id', 'parent.title AS parent_title')
        ->leftJoin('categories AS parent','categories.parentid','=','parent.id')
        ->where('categories.type','=',5)
        ->get();

        /*$category_list = DB::table('categories')->select('id', 'title')->get();*/

        if($category_list){
            $category_array[0] ="[Select a Category]";
            foreach($category_list  as $category){
                $category_array[$category->id]= $category->title;

            }
        }else{
            $category_array[0] ="[No Category For Select]";
        }


        $form_fields_list = DB::table('form_fields')->get();

        if($form_fields_list){
            $fields_array[0] ="[Select a fields]";
            foreach($form_fields_list  as $fields_list){
                $fields_array[$fields_list->id]= $fields_list->title;

            }
        }else{
            $fields_array[0] ="[No Category For Select]";
        }

        $classified_fields_list = DB::table('classified_fields')
                    ->leftJoin('categories AS parent','classified_fields.parentid','=','parent.id')
                    ->leftJoin('categories AS category','classified_fields.category','=','category.id')
                    ->select(array('classified_fields.*' , 'parent.title as parent_name' , 'category.title as category_name'))
                    ->groupBy('classified_fields.id')
                    ->get();


        foreach($classified_fields_list  as $fields){
                $fields_names = "";
                
                if($fields->fields != null && !empty($fields->fields)){

                    foreach(json_decode($fields->fields, TRUE)  as $key => $value){

                    if (array_key_exists((int)$value, $fields_array) && $value != "0") {
                        $fields_names .=  $fields_array[(int)$value];
                        $fields_names .= " <br>";
                    }
                            
                    }
                }

            
                if($fields->fields = $fields_names){

                }
                
            }


        return view('admin.classified.classified_fields_list', ['category_list' => $category_array, 'classified_fields_list' => $classified_fields_list, 'fields_array' => $fields_array]);
    }




    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'id' => 'integer',
            'title' => 'required',
            'label' => 'required',
            'slug' => 'required',
            'frontend_type' => 'required',
            'backend_type' => 'required',

        ]);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }else{

            if($request->input('id')){

                $fields = new form_fields();
                $fields->exists = true;
                $fields->id = $request->input('id');
                $fields->title = $request->input('title');
                $fields->label = $request->input('label');
                $fields->slug = $request->input('slug');
                $fields->frontend_type = $request->input('frontend_type');
                $fields->backend_type = $request->input('backend_type');
                $fields->listorder = $request->input('listorder');
                $fields->parentid = $request->input('parentid');
                $fields->icon = $request->input('icon');
                $fields->content = $request->input('content');
                $fields->save();


                return redirect('/admin/general/form/edit/'.$request->input('id'))->with('flash_success', 'Record Updated successfully!.');



            }else{

                $new_field = new form_fields();
                $new_field->title = $request->input('title');
                $new_field->label = $request->input('label');
                $new_field->slug = $request->input('slug');
                $new_field->frontend_type = $request->input('frontend_type');
                $new_field->backend_type = $request->input('backend_type');
                $new_field->listorder = $request->input('listorder');
                $new_field->parentid = $request->input('parentid');
                $new_field->icon = $request->input('icon');
                $new_field->content = $request->input('content');;
                $new_field->save();

                return redirect('/admin/general/form/list')->with('flash_success', 'Record Inserted successfully!.');

            }

        
        }
    }



     public function classifiedStore(Request $request)
    {
        $v = Validator::make($request->all(), [
            'id' => 'integer',
            'category' => 'required',
            'parentid' => 'required',
            'fields' => 'required|min:1'

        ]);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }else{

            if($request->input('id')){


                $fields = new classified_fields();
                $fields->exists = true;
                $fields->id = $request->input('id');
                $fields->category = $request->input('category');
                $fields->parentid = $request->input('parentid');
                $fields->fields = json_encode(array_unique($request->input('fields')));
                $fields->save();


                return redirect('/admin/general/form/classified_fields/')->with('flash_success', 'Record Updated successfully!.');



            }else{
                $field_row = DB::table('classified_fields')
                            ->where('category','=' ,$request->input('category'))
                            ->where('parentid','=' ,$request->input('parentid'))
                            ->first();
                if($field_row){


                    $fields_final = array_values( array_merge(json_decode($field_row->fields), $request->input('fields')) );
                    $fields_final = json_encode( array_unique($fields_final) );

                    $update_row = classified_fields::where('id', (int)$field_row->id)
                                ->update(array('fields' => $fields_final));

                    

                     return redirect('/admin/general/form/classified_fields')->with('flash_success', 'Already Records there for same selection, Records updated successfully!.');

                    


                }else{

                    $new_field = new classified_fields();
                    $new_field->category = $request->input('category');
                    $new_field->parentid = $request->input('parentid');
                    $new_field->fields = json_encode(array_unique($request->input('fields')));
                    $new_field->save();

                     return redirect('/admin/general/form/classified_fields')->with('flash_success', 'Record Inserted successfully!.');

                }

            

            }

        
        }
    }



    public function classifiedFilter(Request $request)
    {

        /*if (Request::isMethod('POST')) {*/
        $category_id  = $request->input('category_id');

        $category_list = DB::table('categories')->where('parentid','=',(int)$category_id)->get();

            
        if($category_list){
            $category_array[0] ="[Select a Category]";
            foreach($category_list  as $category){
                $category_array[$category->id]= $category->title;

            }
        }else{
            $category_array[0] ="[No Category For Select]";
        }


            

        return view('admin.classified.classified_fields_filter', ['category_array' => $category_array]);
        /*}*/


    }



    public function addnewform(Request $request)
    {
        if($request->query('addnew')=='text'){

            return view('_forms.__addtextform');

        }else if($request->query('addnew')=='image'){

            return view('_forms.__addimageform');

        }else if($request->query('addnew')=='poll'){

            return view('_forms.__addpollform');

        }else  if($request->query('addnew')=='embed'){

            return view('_forms.__addembedform');

        }else if($request->query('addnew')=='video'){

            return view('_forms.__addvideoform');

        }


    }


    public function edit($id)
    {
        $form_fields_list = DB::table('form_fields')->get();

        $form_fields_one = DB::table('form_fields')->where('id', '=', $id)->first();

        if($form_fields_list){
            $fields_array[0] ="[Select a fields]";
            foreach($form_fields_list  as $fields_list){
                $fields_array[$fields_list->id]= $fields_list->title;

            }
        }else{
            $fields_array[0] ="[No Category For Select]";
        }
    

        return view('admin.classified.form_fields_update', ['form_fields_list' => $form_fields_list, 
            'fields_array' => $fields_array, 
            'field_one' => $form_fields_one]);
    }



    public function classifiededit($id)
    {
        $category_list = DB::table('categories')
        ->select('categories.id','categories.title','parent.id As parent_id', 'parent.title AS parent_title')
        ->leftJoin('categories AS parent','categories.parentid','=','parent.id')
        ->where('categories.type','=',5)
        ->get();

        /*$category_list = DB::table('categories')->select('id', 'title')->get();*/

        if($category_list){
            $category_array[0] ="[Select a Category]";
            foreach($category_list  as $category){
                $category_array[$category->id]= $category->title;

            }
        }else{
            $category_array[0] ="[No Category For Select]";
        }


        $form_fields_list = DB::table('form_fields')->get();

        if($form_fields_list){
            $fields_array[0] ="[Select a fields]";
            foreach($form_fields_list  as $fields_list){
                $fields_array[$fields_list->id]= $fields_list->title;

            }
        }else{
            $fields_array[0] ="[No Category For Select]";
        }

        $classified_fields_list = DB::table('classified_fields')
                    ->leftJoin('categories AS parent','classified_fields.parentid','=','parent.id')
                    ->leftJoin('categories AS category','classified_fields.category','=','category.id')
                    ->select(array('classified_fields.*' , 'parent.title as parent_name' , 'category.title as category_name'))
                    ->groupBy('classified_fields.id')
                    ->get();


        foreach($classified_fields_list  as $fields){
                $fields_names = "";
                
                if($fields->fields != null && !empty($fields->fields)){

                    foreach(json_decode($fields->fields, TRUE)  as $key => $value){

                    if (array_key_exists((int)$value, $fields_array) && $value != "0") {
                        $fields_names .=  $fields_array[(int)$value];
                        $fields_names .= " <br>";
                    }
                            
                    }
                }

            
                if($fields->fields = $fields_names){

                }
                
            }




        $form_fields_one = DB::table('classified_fields')->where('id', '=', $id)->first();
    

        return view('admin.classified.classified_fields_update', 
            ['category_list' => $category_array, 
            'classified_fields_list' => $classified_fields_list, 
            'fields_array' => $fields_array, 
            'field_one' => $form_fields_one]);
    }



    public function destroy($id, Request $request)
    {
        
        if ($request->input('submit_confirm')) {
            try {
                $affectedRows = DB::table('form_fields')->where('id', '=', $id)->delete();

                if($affectedRows){
                    
                    /* action log insertion */
                    
                    /* action log insertion */
                    return redirect('/admin/general/form/list')->with('flash_success', 'Form Fields Deleted Successfully!.');
                }else{
                    return redirect('/admin/general/form/list')->with('flash_message', 'Form Fields not deleted,Try again...');
                }
            }
            catch (\Exception $ex) {
                Session::flash('flash_message', $ex->getMessage());
                Session::flash('flash_type', 'error');
            }
        }

        $data = [
            'view_data' => [
                'page_parent' => 'classified',
                'current_page' => 'form_fields_list',
                'page_title' => 'Delete Form Fields',
                'section_title' => 'Confirm Delete Selection',
                'confirm_title' => 'Are you sure you want to delete the selected Form Fields?',
                'confirm_message' => 'This action can not be undone. Please confirm.',
                'confirm_button' => 'Delete',
                'cancel_url' => url('/admin/general/form/list'),
            ],
        ];
        return view('layout.confirm', $data);




    }







    public function classifieddestroy($id, Request $request)
    {
        
        if ($request->input('submit_confirm')) {
            try {
                $affectedRows = DB::table('classified_fields')->where('id', '=', $id)->delete();

                if($affectedRows){
                    
                    /* action log insertion */
                    
                    /* action log insertion */
                    return redirect('/admin/general/form/classified_fields')->with('flash_success', 'Classified Form Fields Deleted Successfully!.');
                }else{
                    return redirect('/admin/general/form/classified_fields')->with('flash_message', 'Classified Form Fields not deleted,Try again...');
                }
            }
            catch (\Exception $ex) {
                Session::flash('flash_message', $ex->getMessage());
                Session::flash('flash_type', 'error');
            }
        }

        $data = [
            'view_data' => [
                'page_parent' => 'classified',
                'current_page' => 'classified_fields_list',
                'page_title' => 'Delete Classified Field Link',
                'section_title' => 'Confirm Delete Selection',
                'confirm_title' => 'Are you sure you want to delete the selected Classified Field Link?',
                'confirm_message' => 'This action can not be undone. Please confirm.',
                'confirm_button' => 'Delete',
                'cancel_url' => url('/admin/general/form/classified_fields/'),
            ],
        ];
        return view('layout.confirm', $data);




    }


}
