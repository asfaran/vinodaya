<?php namespace App\Http\Controllers;

use App\social;
use DB;
use Session;
use DateTime;
Use Validator;
use Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;

class SocialController extends Controller {

	public function __construct()
    {
        $this->middleware('auth');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$social_list = DB::table('socials')->get();

		if($social_list){
			foreach($social_list  as $social){
				$social_array[$social->title]= $social->url;
			}
		}else{
			$social_array[0] ="[No Category For Select]";
		}

		return view('admin.social.list', ['social_list' => $social_list, 'social_array' => $social_array]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{

		$social_urls =$request->input('social');

		
		foreach($social_urls as $kay => $value) {

			$social = social::where('title', $kay)->update(array('url' => $value));
		}

		return redirect('/admin/settings/social/list')->with('flash_success', 'Record Updated successfully!.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
