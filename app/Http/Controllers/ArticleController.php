<?php namespace App\Http\Controllers;


use App\article;
use App\options;
use DB;
use Session;
use DateTime;
Use Validator;
use Auth;
use Image;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;

class ArticleController extends Controller {


	public function __construct()
    {
        $this->middleware('auth');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$category_array = array();

		$category_list = DB::table('categories')
		->select('categories.id','categories.title','parent.id As parent_id', 'parent.title AS parent_title')
		->leftJoin('categories AS parent','categories.parentid','=','parent.id')
		->where('categories.type','=',2)
		->get();



		/*$category_list = DB::table('categories')->select('id', 'title')->get();*/

		if($category_list){
			foreach($category_list  as $category){
				$category_array[$category->id]= $category->title;

			}
		}


		$article_list = DB::table('articles')
					->leftJoin('comments', 'comments.post_id', '=', 'articles.id')
					->leftJoin('users', 'users.id', '=', 'articles.user_id')
					->select(array('articles.*' , DB::raw('COUNT(comments.id) as comments_count') , 'users.name as author'))
					->groupBy('articles.id')
					->get();

			/*->where('comments.post_type','=',1)*/
			
			foreach($article_list  as $article){
				$category_names = "";
				foreach(json_decode($article->category_id, TRUE)  as $key => $value){

					if (array_key_exists((int)$value, $category_array)) {
						$category_names .=  $category_array[(int)$value];
						$category_names .= " | ";
					}
							
				}

			
				if($article->category_id = $category_names){

				}
				
			}
		$status_array = array('0'=>'Pending Review', '1'=>'Draft', '2'=>'Published' );


		return view('admin.article.list', ['article_list' => $article_list,'category_array' => $category_array, 'status_array' => $status_array]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$category_list = DB::table('categories')
		->select('categories.id','categories.title','parent.id As parent_id', 'parent.title AS parent_title')
		->leftJoin('categories AS parent','categories.parentid','=','parent.id')
		->where('categories.type','=',2)
		->get();

		$status_array = array('0'=>'Pending Review', '1'=>'Draft', '2'=>'Published' );
	

		return view('admin.article.create', ['category_list' => $category_list, 'status_array' => $status_array]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$status_array = array('0'=>'Pending Review', '1'=>'Draft', '2'=>'Published' );

		$v = Validator::make($request->all(), [
			'id' => 'integer',
			'title' => 'required',
			'post_status' => 'required',
			'post_featured' => 'required',
			'post_publish' => 'required',
			'category' => 'required|min:1',
			'post_visibility' => 'required',
			'body' => 'required',
			'thumb' => 'mimes:jpg,jpeg,gif,png',

		]);

		/*'category' => 'required',*/
		if ($v->fails())
		{
			return redirect()->back()->withInput()->withErrors($v->errors());
		}else{

			$tags_array = array_filter(explode(",",$request->input('tags')));
			if($tags_array){
				foreach($tags_array  as $key=>$tag){
					$line_row = DB::table('tags')->where('title','=' ,$tag)->first();
					if(!$line_row || empty($line_row) || $line_row == null){

						$slug = preg_replace('/\s+/', '-', strtolower($tag));

						$new_tags = new tags();
						$new_tags->title = $tag;
						$new_tags->slug = $slug;
						$new_tags->save();

					}
				}
			}
			

			if($request->input('id')){


				$title_lowercase = strtolower($request->input('post_slug'));
				$slug = preg_replace('/\s+/', '-', $title_lowercase);

				$line_row = DB::table('articles')->where('slug','=' ,$slug)->where('id', '!=', $request->input('id'))->first();

				if($line_row){
					for ($i = 1; $i <= 100; ++$i) {
						$search_slug = $slug."-".$i;
						$sub_row = DB::table('articles')->where('slug','=' ,$search_slug)->where('id', '!=', $request->input('id'))->first();
						if($sub_row){
							continue;
						}else{

							$slug = $search_slug;
							 break;
						}

					}
				}


				if($request->file('thumb')){

					$image_path = $this->upload($request->file('thumb'));
				}



				if($request->input('submit_draft')) {
					$post_status = ($request->input('post_status') == 'Draft')? 1 : 0  ;

				}else{

					$line_row = DB::table('articles')->where('id','=' ,$request->input('id'))->first();

					if($line_row->status == 2){
						$post_status = array_search($request->input('post_status'), $status_array) ;
					}else{
						$post_status =  2;
					}

					

				}


				$new_article = new article();
				$new_article->exists = true;
				$new_article->id = (int)$request->input('id');
				$new_article->title = $request->input('title');
				if(isset($image_path)){
					$new_article->thumb = $image_path;
				}
				
				/*$new_article->user_id = Auth::user()->id;*/
				$new_article->user_id = 1;
				$new_article->tags = $request->input('tags');
				$new_article->status = $post_status;
				$new_article->slug = $slug;
				$new_article->featured  = ($request->input('post_featured') == 'Yes')? 1 : 0  ;
				/*$new_article->publish  = ($request->input('post_publish') == 'Public')? 1 : 0  ;*/
				$new_article->visibility = ($request->input('post_visibility') == 'Public')? 1 : 0  ;
				$new_article->category_id = json_encode( $request->input('category') );
				$new_article->body = $this->postSummernote($request->input('body'));
				$new_article->save();

				if($new_article){
					

					return redirect('/admin/article/edit/'.$request->input('id'))->with('flash_success', 'Record Updated successfully!.');

				}else{

					return redirect('/admin/article/edit/'.$request->input('id'))->with('flash_success', 'Record Not Updated!.');

				}
				

			}else{

				
				$title_lowercase = strtolower($request->input('title'));
				$slug = preg_replace('/\s+/', '-', $title_lowercase);

				$line_row = DB::table('articles')->where('slug','=' ,$slug)->first();

				if($line_row){
					for ($i = 1; $i <= 100; ++$i) {
						$search_slug = $slug."-".$i;
						$sub_row = DB::table('articles')->where('slug','=' ,$search_slug)->first();
						if($sub_row){
							continue;
						}else{

							$slug = $search_slug;
							 break;
						}

					}
				}


				if($request->file('thumb')){

					$image_path = $this->upload($request->file('thumb'));
				}

				if($request->input('submit_draft')) {
					$post_status = ($request->input('post_status') == 'Draft')? 1 : 0  ;

				}else{

					$post_status = 2;

				}


				$new_article = new article();
				$new_article->title = $request->input('title');
				if(isset($image_path)){
					$new_article->thumb = $image_path;
				}
				/*$new_article->user_id = Auth::user()->id;*/
				$new_article->user_id = 1;
				$new_article->slug = $slug;
				$new_article->tags = $request->input('tags');
				$new_article->status = $post_status  ;
				$new_article->featured  = ($request->input('post_featured') == 'Yes')? 1 : 0  ;
				/*$new_article->publish  = ($request->input('post_publish') == 'Public')? 1 : 0  ;*/
				$new_article->visibility = ($request->input('post_visibility') == 'Public')? 1 : 0  ;
				$new_article->category_id = json_encode( $request->input('category') );
				$new_article->body = $this->postSummernote($request->input('body'));
				$new_article->save();

				return redirect('/admin/article/list')->with('flash_success', 'Record Inserted successfully!.');

			}

		
		}
	}

	public function postSummernote($message)
	{

        $dom = new \DomDocument('1.0', 'UTF-8');
        /*@$dom->loadHtml($message, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);*/
        @$dom->loadHtml( mb_convert_encoding($message, 'HTML-ENTITIES', "UTF-8"), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getElementsByTagName('img');
       // foreach <img> in the submited message
        foreach($images as $img){
            $src = $img->getAttribute('src');
            
            // if the img source is 'data-url'
            if(preg_match('/data:image/', $src)){                
                // get the mimetype
                preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                $mimetype = $groups['mime'];                
                // Generating a random filename
                $filename = uniqid();
                $filepath = "/uploads/article/summernoteimages/$filename"."."."$mimetype";    
                // @see http://image.intervention.io/api/
                $image = Image::make($src)
                  // resize if required
                  /* ->resize(300, 200) */
                  ->encode($mimetype, 100)  // encode file to the specified mimetype
                  ->save(public_path($filepath));                
                $new_src = asset($filepath);
                $img->removeAttribute('src');
                $img->setAttribute('src', $new_src);
            } // <!--endif
        } // <!-
        $message = $dom->saveHTML();
        return $message;

	}




	public function upload($file)
    {
    	/*if($request->hasFile('file')) {
            $file = $request->file('file');

           // $tmpUserName = Auth::user()->id .'-' ;

            $tmpTimeToken = time(); //for if same image was uploaded.

            $tmpFileName = $tmpTimeToken.'-'.$file->getClientOriginalName();

            $tmpFilePath = 'upload/tmp/';

            $hardPath =   date('Y-m') .'/'.date('d') .'/';


            if (!file_exists(public_path() .'/'.$tmpFilePath.$hardPath )) {
                $oldmask = umask(0);
                mkdir(public_path() .'/'. $tmpFilePath.$hardPath , 0777, true);
                umask($oldmask);
            }

            $img = Image::make($file);

            $imgmj = $img->mime();

            if($type!=='preview' and $imgmj=='image/gif'){

                $file->move(public_path() . '/'. $tmpFilePath. $hardPath, $tmpUserName.$tmpFileName);

                $path = '/'.$tmpFilePath .$hardPath . $tmpUserName. $tmpFileName;

            }else{

                    if($type=='entry'){

                        $img->resize(640, null, function ($constraint) {
                         $constraint->aspectRatio();
                         $constraint->upsize();
                        });

                    }else if($type=='preview'){

                        $img->fit(650, 370);

                    }


                $path = $tmpFilePath .$hardPath . $tmpUserName. md5($tmpFileName). '.jpg';

                $img->save($path);

                $path='/'.$path;
            }

        }*/
        $input = array('image' => $file);
        $rules = array(
            'image' => 'image'
        );
        $validator = Validator::make($input, $rules);
        if ( $validator->fails()) {
            return Response::json(array('success' => false, 'errors' => $validator->getMessageBag()->toArray()));
        }
 
        $fileName = time() . '-' . $file->getClientOriginalName();
        $destination = public_path() . '/uploads/article/';
        $file->move($destination, $fileName);
 
        return $fileName;
    }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function display()
	{
		$options_result = DB::table('options')->get();

		$option_list = array();

		if($options_result){
			foreach($options_result  as $key => $value){

					$option_list[$value->name] = $value->options;
							
			}

		}
		return view('admin.article.display', ['option_list' => $option_list]);
	}




	public function display_store(Request $request)
	{
		$data  = Input::all();

		foreach($data as $key => $value) {
			if($key  != "_token"){

			$option_one = DB::table('options')
					->where('name', '=', $key)
					->first();
			if($option_one){
				$option_row =  options::where('name', $key)->update(array('options' => $value));
			}else{

				$new_option = new options();
				$new_option->name = $key;
				$new_option->options =  $value;
				$new_option->save();


			}



			}			
			
		}

		return redirect('/admin/article/display')->with('flash_success', 'Record Updated successfully!.');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$category_list = DB::table('categories')
		->select('categories.id','categories.title','parent.id As parent_id', 'parent.title AS parent_title')
		->leftJoin('categories AS parent','categories.parentid','=','parent.id')
		->where('categories.type','=',2)
		->get();


		$article_one = DB::table('articles')
					->where('id', '=', $id)
					->first();

		$status_array = array('0'=>'Pending Review', '1'=>'Draft', '2'=>'Published' );

		$selected_category = array_values(json_decode($article_one->category_id, TRUE));


		return view('admin.article.update', ['category_list' => $category_list, 
			'article_one' => $article_one, 
			'selected_category' => $selected_category, 
			'status_array' => $status_array]);

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id, Request $request)
	{
		
		if ($request->input('submit_confirm')) {
            try {
                $affectedRows = DB::table('articles')->where('id', '=', $id)->delete();

				if($affectedRows){
					
					/* action log insertion */
					
					/* action log insertion */
					return redirect('/admin/article/list')->with('flash_success', 'Selected Article Deleted Successfully!.');
				}else{
					return redirect('/admin/article/list')->with('flash_message', 'Selected Article not deleted,Try again...');
				}
            }
            catch (\Exception $ex) {
                Session::flash('flash_message', $ex->getMessage());
                Session::flash('flash_type', 'error');
            }
        }

        $data = [
            'view_data' => [
                'page_parent' => 'article',
                'current_page' => 'article_list',
                'page_title' => 'Delete Article',
                'section_title' => 'Confirm Delete Selection',
                'confirm_title' => 'Are you sure you want to delete the selected Article?',
                'confirm_message' => 'This action can not be undone. Please confirm.',
                'confirm_button' => 'Delete',
                'cancel_url' => url('/admin/article/list'),
            ],
        ];
        return view('layout.confirm', $data);
	}

}
