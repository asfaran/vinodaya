<?php namespace App\Http\Controllers;


use DB;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Session;
use DateTime;
Use Validator;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Auth;
use Illuminate\Http\Request;

class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(Auth::check()){
            if(Auth::user()->usertype == 1){

            	$users_count = DB::table('users')->where('usertype', '=', 3)->count();

				$category_list = DB::table('categories')
				->select('categories.id','categories.title','parent.id As parent_id', 'parent.title AS parent_title')
				->leftJoin('categories AS parent','categories.parentid','=','parent.id')
				->get();



				/*$category_list = DB::table('categories')->select('id', 'title')->get();*/

				if($category_list){
					foreach($category_list  as $category){
						$category_array[$category->id]= $category->title;

					}
				}

				$news_count_array = array();
				$article_count_array = array();
				$image_count_array = array();
				$video_count_array = array();
				$classified_count_array = array();


				$news_list = DB::table('news')
							->groupBy('id')
							->get();
				$article_list = DB::table('articles')
							->groupBy('id')
							->get();

				$image_list = DB::table('images')
							->groupBy('id')
							->get();
				$video_list = DB::table('videos')
							->groupBy('id')
							->get();

				$classified_list = DB::table('classifieds')
							->select(array('categories.id','categories.title' , DB::raw('COUNT(classifieds.id) as classifieds_count') ))
							->leftJoin('categories','classifieds.parentid','=','categories.id')
							->groupBy('categories.id')
							->get();



					foreach($news_list  as $news){
						
						if($news->category_id != null && !empty($news->category_id)){

							foreach(json_decode($news->category_id, TRUE)  as $key => $value){

							if (array_key_exists((int)$value, $category_array)) {

								if(array_key_exists((int)$value, $news_count_array)){
									$old_value = $news_count_array[$value]['count'];
									$news_count_array[$value]['count'] =  (int)$old_value + 1;


								}else{
									$news_count_array[$value]['count'] =  1;
									$news_count_array[$value]['title'] =  $category_array[(int)$value];

								}
								
							}
									
							}
						}

						
					}

					foreach($article_list  as $article){
						
						if($article->category_id != null && !empty($article->category_id)){

							foreach(json_decode($article->category_id, TRUE)  as $key => $value){

							if (array_key_exists((int)$value, $category_array)) {

								if(array_key_exists((int)$value, $article_count_array)){
									$old_value = $article_count_array[$value]['count'];
									$article_count_array[$value]['count'] =  (int)$old_value + 1;


								}else{
									$article_count_array[$value]['count'] =  1;
									$article_count_array[$value]['title'] =  $category_array[(int)$value];

								}
								
							}
									
							}
						}

					}

					foreach($image_list  as $image){
						
						if($image->category_id != null && !empty($image->category_id)){

							foreach(json_decode($image->category_id, TRUE)  as $key => $value){

							if (array_key_exists((int)$value, $category_array)) {

								if(array_key_exists((int)$value, $image_count_array)){
									$old_value = $image_count_array[$value]['count'];
									$image_count_array[$value]['count'] =  (int)$old_value + 1;


								}else{
									$image_count_array[$value]['count'] =  1;
									$image_count_array[$value]['title'] =  $category_array[(int)$value];

								}
								
							}
									
							}
						}
					}


					foreach($video_list  as $video){
						
						if($video->category_id != null && !empty($video->category_id)){

							foreach(json_decode($video->category_id, TRUE)  as $key => $value){

							if (array_key_exists((int)$value, $category_array)) {

								if(array_key_exists((int)$value, $video_count_array)){
									$old_value = $video_count_array[$value]['count'];
									$video_count_array[$value]['count'] =  (int)$old_value + 1;


								}else{
									$video_count_array[$value]['count'] =  1;
									$video_count_array[$value]['title'] =  $category_array[(int)$value];

								}
								
							}
									
							}
						}
					}

				if($classified_list){
					foreach($classified_list  as $classified){
						$classified_count_array[$classified->title]= $classified->classifieds_count;

					}
				}


				return view('welcome', 
					['users_count' => $users_count, 
					'news_count_array' =>  $news_count_array,
					'article_count_array' =>  $article_count_array,
					'image_count_array' =>  $image_count_array,
					'video_count_array' =>  $video_count_array,
					'classified_count_array' =>  $classified_count_array]);
                
            }

            return redirect()->intended('user');
           
        }



		
	}

	public function checkapp()
	{
		return view('welcome');
	}


}
