<?php namespace App\Http\Controllers;

use App\menu;
use DB;
use Session;
use DateTime;
Use Validator;
use Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;

class MenuController extends Controller {

	public function __construct()
    {
        $this->middleware('auth');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$menu_list = DB::table('menus')->groupBy('id')->get();

		$pages_list = DB::table('pages')->get();


		if($pages_list){
			$page_list[0] ="[Select a Page]";
			foreach($pages_list  as $pages){
				$page_list[$pages->id]= $pages->title;

			}
		}else{
			$page_list[0] ="[No Pages For Select]";
		}

		$category_list = DB::table('categories')
		->select('categories.id','categories.title','parent.id As parent_id', 'parent.title AS parent_title')
		->leftJoin('categories AS parent','categories.parentid','=','parent.id')
		->get();



		$category_array = array();

		if($category_list){
			$category_array[0] ="[Select a Category]";
			foreach($category_list  as $category){
				$category_array[$category->id]= $category->title;

			}
		}else{
			$category_array[0] ="[No Category For Select]";
		}

		$type_array = array('0'=>'Nothing Selected', '1'=>'Category', '2'=>'Custom Links', '3'=>'Pages' );



		return view('admin.menu.list', ['category_list' => $category_array,
										 'menu_full'=> $menu_list, 
										 'type_array'=> $type_array,
										 'page_list'=> $page_list]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$v = Validator::make($request->all(), [
			'id' => 'integer',
			'label' => 'required',
			'title' => 'required',
			'type' => 'required|integer',

		]);

		if ($v->fails())
		{
			return redirect()->back()->withInput()->withErrors($v->errors());
		}else{

			if($request->input('id')){

				$menu = menu::find($request->input('id'));
				$menu->title = $request->input('title');
				$menu->label = $request->input('label');
				$menu->type = $request->input('type');
				if($request->input('type') == 1 ){
					$menu->content = $request->input('categoryid');
				}elseif($request->input('type') == 2 ){
					$menu->content = $request->input('url');
				}else{
					$menu->content = $request->input('pageid');
				}
				$menu->description = $request->input('description');;
				$menu->save();


				return redirect('/admin/general/menu/edit/'.$request->input('id'))->with('flash_success', 'Record Updated successfully!.');



			}else{

				$new_menu = new menu();
				$new_menu->title = $request->input('title');
				$new_menu->label = $request->input('label');
				$new_menu->type = $request->input('type');
				if($request->input('type') == 1 ){
					$new_menu->content = $request->input('categoryid');
				}elseif($request->input('type') == 2 ){
					$new_menu->content = $request->input('url');
				}else{
					$new_menu->content = $request->input('pageid');
				}
				$new_menu->description = $request->input('description');;
				$new_menu->save();

				return redirect('/admin/general/menu/list')->with('flash_success', 'Record Inserted successfully!.');

			}

		
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$menu_list = DB::table('menus')->groupBy('id')->get();
		$menu_one = DB::table('menus')
					->where('id', '=', $id)
					->first();


		$pages_list = DB::table('pages')->get();


		if($pages_list){
			$page_list[0] ="[Select a Page]";
			foreach($pages_list  as $pages){
				$page_list[$pages->id]= $pages->title;

			}
		}else{
			$page_list[0] ="[No Pages For Select]";
		}

		$category_list = DB::table('categories')
		->select('categories.id','categories.title','parent.id As parent_id', 'parent.title AS parent_title')
		->leftJoin('categories AS parent','categories.parentid','=','parent.id')
		->get();

		if($category_list){
			$category_array[0] ="[Select a Category]";
			foreach($category_list  as $category){
				$category_array[$category->id]= $category->title;

			}
		}else{
			$category_array[0] ="[No Category For Select]";
		}


		$type_array = array('0'=>'Nothing Selected', '1'=>'Category', '2'=>'Custom Links', '3'=>'Pages');


		return view('admin.menu.update', ['category_list' => $category_array, 'menu_full'=> $menu_list, 'menu'=> $menu_one, 'type_array'=> $type_array, 'page_list'=> $page_list]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id, Request $request)
    {
        
        if ($request->input('submit_confirm')) {
            try {
                $affectedRows = DB::table('menus')->where('id', '=', $id)->delete();

				if($affectedRows){
					
					/* action log insertion */
					
					/* action log insertion */
					return redirect('/admin/general/menu/list')->with('flash_success', 'Selected Menu Deleted Successfully!.');
				}else{
					return redirect('/admin/general/menu/list')->with('flash_message', 'Selected Menu not deleted,Try again...');
				}
            }
            catch (\Exception $ex) {
                Session::flash('flash_message', $ex->getMessage());
                Session::flash('flash_type', 'error');
            }
        }

        $data = [
            'view_data' => [
                'page_parent' => 'general',
                'current_page' => 'menu_list',
                'page_title' => 'Delete Menu Item',
                'section_title' => 'Confirm Delete Selection',
                'confirm_title' => 'Are you sure you want to delete the selected Menu Item?',
                'confirm_message' => 'This action can not be undone. Please confirm.',
                'confirm_button' => 'Delete',
                'cancel_url' => url('/admin/general/menu/list'),
            ],
        ];
        return view('layout.confirm', $data);

    }

}
