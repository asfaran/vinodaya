<?php namespace App\Http\Controllers;


use App\tags;
use DB;
use Session;
use DateTime;
Use Validator;
use Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;

class TagsController extends Controller {

	public function __construct()
    {
        $this->middleware('auth');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$tags_list = DB::table('tags')->get();

		return view('admin.tags.list', ['tags_list' => $tags_list]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$v = Validator::make($request->all(), [
			'id' => 'integer',
			'title' => 'required',
			'slug' => 'required',

		]);

		if ($v->fails())
		{
			return redirect()->back()->withInput()->withErrors($v->errors());
		}else{

			if($request->input('id')){

				$new_tags = tags::find($request->input('id'));
				$new_tags->title = $request->input('title');
				$new_tags->slug = $request->input('slug');
				$new_tags->description = $request->input('description');;
				$new_tags->save();


				return redirect('/admin/general/tags/edit/'.$request->input('id'))->with('flash_success', 'Record Updated successfully!.');



			}else{

				$new_tags = new tags();
				$new_tags->title = $request->input('title');
				$new_tags->slug = $request->input('slug');
				$new_tags->description = $request->input('description');;
				$new_tags->save();

				return redirect('/admin/general/tags/list')->with('flash_success', 'Record Inserted successfully!.');

			}


		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}



	public function suggestions(Request $request)
    {

        /*if (Request::isMethod('POST')) {*/

            $term = Input::get('term');
            
            $tags_list = tags::where('title', 'LIKE', '%'.$term.'%')->get();


            $tags_array = array();

            if($tags_list){
            	foreach($tags_list  as $tags){
            		$tags_array[]= $tags->title;
            	}
            }

            return json_encode($tags_array);
        /*}*/


    }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$tags_list = DB::table('tags')->get();


		$tags_one = DB::table('tags')
					->where('id', '=', $id)
					->first();

		return view('admin.tags.update', ['tags_list' => $tags_list, 'tags_one' => $tags_one]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id, Request $request)
    {
        
        if ($request->input('submit_confirm')) {
            try {
                $affectedRows = DB::table('tags')->where('id', '=', $id)->delete();

				if($affectedRows){
					
					/* action log insertion */
					
					/* action log insertion */
					return redirect('/admin/general/tags/list')->with('flash_success', 'Tag Deleted Successfully!.');
				}else{
					return redirect('/admin/general/tags/list')->with('flash_message', 'Tag not deleted,Try again...');
				}
            }
            catch (\Exception $ex) {
                Session::flash('flash_message', $ex->getMessage());
                Session::flash('flash_type', 'error');
            }
        }

        $data = [
            'view_data' => [
                'page_parent' => 'general',
                'current_page' => 'tags_list',
                'page_title' => 'Delete Tag ',
                'section_title' => 'Confirm Delete Selection',
                'confirm_title' => 'Are you sure you want to delete the selected Tag?',
                'confirm_message' => 'This action can not be undone. Please confirm.',
                'confirm_button' => 'Delete',
                'cancel_url' => url('/admin/general/tags/list'),
            ],
        ];
        return view('layout.confirm', $data);

    }

}
