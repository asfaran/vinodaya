<?php namespace App\Http\Controllers;

use App\emails;
use DB;
use Session;
use DateTime;
Use Validator;
use Auth;
use Image;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;

class EmailsController extends Controller {


	public function __construct()
    {
        $this->middleware('auth');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$mail_list = DB::table('emails')->get();


		return view('admin.email.list', ['mail_list' => $mail_list]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$v = Validator::make($request->all(), [
			'id' => 'integer',
			'title' => 'required',
			'sender' => 'required',
			'sender_email' => 'required|email',

		]);

		if ($v->fails())
		{
			return redirect()->back()->withInput()->withErrors($v->errors());
		}else{

			if($request->input('id')){

				$email = emails::find($request->input('id'));
				$email->title = $request->input('title');
				$email->sender = $request->input('sender');
				$email->sender_email = $request->input('sender_email');
				$email->content =$this->postSummernote($request->input('content'));
				$email->save();


				return redirect('/admin/classified/mails/edit/'.$request->input('id'))->with('flash_success', 'Record Updated successfully!.');



			}else{

				$new_email = new emails();
				$new_email->title = $request->input('title');
				$new_email->sender = $request->input('sender');
				$new_email->sender_email = $request->input('sender_email');
				$new_email->content = $this->postSummernote($request->input('content'));
				$new_email->save();

				return redirect('/admin/classified/mails/list')->with('flash_success', 'Record Inserted successfully!.');

			}

		
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}



	public function postSummernote($message)
	{

        $dom = new \DomDocument('1.0', 'UTF-8');
        /*@$dom->loadHtml($message, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD); */
        @$dom->loadHtml( mb_convert_encoding($message, 'HTML-ENTITIES', "UTF-8"), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);   
        $images = $dom->getElementsByTagName('img');
       // foreach <img> in the submited message
        foreach($images as $img){
            $src = $img->getAttribute('src');
            
            // if the img source is 'data-url'
            if(preg_match('/data:image/', $src)){                
                // get the mimetype
                preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                $mimetype = $groups['mime'];                
                // Generating a random filename
                $filename = uniqid();
                $filepath = "/uploads/email/summernoteimages/$filename"."."."$mimetype";    
                // @see http://image.intervention.io/api/
                $image = Image::make($src)
                  // resize if required
                  /* ->resize(300, 200) */
                  ->encode($mimetype, 100)  // encode file to the specified mimetype
                  ->save(public_path($filepath));                
                $new_src = asset($filepath);
                $img->removeAttribute('src');
                $img->setAttribute('src', $new_src);
            } // <!--endif
        } // <!-
        $message = $dom->saveHTML();
        return $message;

	}



	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$mail_list = DB::table('emails')->get();


		$mail_one = DB::table('emails')
					->where('id', '=', $id)
					->first();
	

		return view('admin.email.update', ['mail_list' => $mail_list, 'mail_one' => $mail_one]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function destroy($id, Request $request)
	{
		
		if ($request->input('submit_confirm')) {
            try {
                $affectedRows = DB::table('emails')->where('id', '=', $id)->delete();

				if($affectedRows){
					
					/* action log insertion */
					
					/* action log insertion */
					return redirect('/admin/classified/mails/list')->with('flash_success', 'Mail Template Deleted Successfully!.');
				}else{
					return redirect('/admin/classified/mails/list')->with('flash_message', 'Mail Template not deleted,Try again...');
				}
            }
            catch (\Exception $ex) {
                Session::flash('flash_message', $ex->getMessage());
                Session::flash('flash_type', 'error');
            }
        }

        $data = [
            'view_data' => [
                'page_parent' => 'classified',
                'current_page' => 'emails',
                'page_title' => 'Delete Email Template',
                'section_title' => 'Confirm Delete Selection',
                'confirm_title' => 'Are you sure you want to delete the selected Email Template?',
                'confirm_message' => 'This action can not be undone. Please confirm.',
                'confirm_button' => 'Delete',
                'cancel_url' => url('/admin/classified/mails/list/'),
            ],
        ];
        return view('layout.confirm', $data);




	}

}
