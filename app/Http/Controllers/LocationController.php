<?php namespace App\Http\Controllers;

use App\location;
use DB;
use Session;
use DateTime;
Use Validator;
use Auth;
use View;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;

class LocationController extends Controller {


	public function __construct()
    {
        $this->middleware('auth');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$locations_list = DB::table('locations')
		->select('locations.id','locations.title','parent.id As parent_id', 'parent.title AS parent_title')
		->leftJoin('locations AS parent','locations.parentid','=','parent.id')
		->get();



		/*$locations_list = DB::table('locations')->select('id', 'title')->get();*/

		if($locations_list){
			$locations_array[0] ="[Select a location]";
			foreach($locations_list  as $locations){
				$locations_array[$locations->id]= $locations->title;

			}
		}else{
			$locations_array[0] ="[No location For Select]";
		}


		return view('admin.location.list', ['locations_list' => $locations_array, 'location_full'=> $locations_list]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$v = Validator::make($request->all(), [
			'id' => 'integer',
			'title' => 'required',
			'slug' => 'required',
			'type' => 'required|integer',
			'parentid' => 'integer',

		]);

		if ($v->fails())
		{
			return redirect()->back()->withInput()->withErrors($v->errors());
		}else{

			if($request->input('id')){

				$location = location::find($request->input('id'));
				$location->title = $request->input('title');
				$location->slug = $request->input('slug');
				$location->type = $request->input('type');
				$location->parentid = $request->input('parentid');
				$location->save();


				return redirect('/admin/classified/location/edit/'.$request->input('id'))->with('flash_success', 'Record Updated successfully!.');



			}else{

				$new_location = new location();
				$new_location->title = $request->input('title');
				$new_location->slug = $request->input('slug');
				$new_location->type = $request->input('type');
				$new_location->parentid = $request->input('parentid');
				$new_location->save();

				return redirect('/admin/classified/location/list')->with('flash_success', 'Record Inserted successfully!.');

			}

		
		}
	}




	public function filter(Request $request)
    {

        /*if (Request::isMethod('POST')) {*/

            $location_type = Input::get('location_type');


            $location_list = DB::table('locations')
							->select('locations.id','locations.title','parent.id As parent_id', 'parent.title AS parent_title')
							->leftJoin('locations AS parent','locations.parentid','=','parent.id')
							->where('locations.type','=',$location_type)
							->get();


            

            return view('admin.location.filter', ['location_list' => $location_list]);
        /*}*/


    }

    public function classifiedfilter(Request $request)
    {

        $location_type_array = array('1'=>'province', '2'=>'city', '3'=>'town', '4'=>'building' );

        
        $location_value  = $request->input('value');


        if($request->input('location_type') == "location_province"){

        	$field_id = "location_city";
        	$location_type_id = array_search('city', $location_type_array);

        }else if($request->input('location_type') == "location_city"){


        	$field_id = "location_town";
        	$location_type_id = array_search('town', $location_type_array);
        }else if($request->input('location_type') == "location_town"){


        	$field_id = "location_building";
        	$location_type_id = array_search('building', $location_type_array);
        }
        
        $location_array = array();


        $location_list = DB::table('locations')
						->select('locations.id','locations.title')
						->where('locations.type','=',$location_type_id)
						->where('locations.parentid','=', $location_value)
						->get();


		if(isset($location_list)){
			$locations_array[0] ="[Select a location]";
			foreach($location_list  as $locations){
				$locations_array[$locations->id]= $locations->title;

			}
		}else{
			$locations_array[0] ="[No location For Select]";
		}

		$classified_filter_array['result'] = (String)View::make('admin.location.selection_filter')
		->with(array('locations_array'  => $locations_array,'field_id'   => $field_id));
		$classified_filter_array['field_id'] = $field_id;


         return json_encode($classified_filter_array); 


    }


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$location_list = DB::table('locations')
		->select('locations.id','locations.title','parent.id As parent_id', 'parent.title AS parent_title')
		->leftJoin('locations AS parent','locations.parentid','=','parent.id')
		->get();


		$location_one = DB::table('locations')
					->where('id', '=', $id)
					->first();

		if($location_list){
			$location_array[0] ="[Select a Location]";
			foreach($location_list  as $location){
				$location_array[$location->id]= $location->title;

			}
		}else{
			$location_array[0] ="[No Location For Select]";
		}
	

		return view('admin.location.update', ['locations_list' => $location_array, 'location_one' => $location_one, 'location_full'=> $location_list]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id, Request $request)
    {
        
        if ($request->input('submit_confirm')) {
            try {
                $affectedRows = DB::table('locations')->where('id', '=', $id)->delete();

				if($affectedRows){
					
					/* action log insertion */
					
					/* action log insertion */
					return redirect('/admin/classified/location/list')->with('flash_success', ' Location Deleted Successfully!.');
				}else{
					return redirect('/admin/classified/location/list')->with('flash_message', 'Location not deleted,Try again...');
				}
            }
            catch (\Exception $ex) {
                Session::flash('flash_message', $ex->getMessage());
                Session::flash('flash_type', 'error');
            }
        }

        $data = [
            'view_data' => [
                'page_parent' => 'classified',
                'current_page' => 'location_list',
                'page_title' => 'Delete Location ',
                'section_title' => 'Confirm Delete Selection',
                'confirm_title' => 'Are you sure you want to delete the selected Location?',
                'confirm_message' => 'This action can not be undone. Please confirm.',
                'confirm_button' => 'Delete',
                'cancel_url' => url('/admin/classified/location/list'),
            ],
        ];
        return view('layout.confirm', $data);

    }

}
