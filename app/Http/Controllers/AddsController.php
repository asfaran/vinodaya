<?php namespace App\Http\Controllers;

use App\options;
use App\adds_type;
use DB;
use Session;
use DateTime;
Use Validator;
use Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;

class AddsController extends Controller {


	public function __construct()
    {
        $this->middleware('auth');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$options_result = DB::table('options')->get();

		$option_list = array();

		if($options_result){
			foreach($options_result  as $key => $value){

					$option_list[$value->name] = $value->options;
							
			}

		}
		return view('admin.classified.adds_manager', ['option_list' => $option_list]);
	}



	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('admin.classified.adds_create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$data  = Input::all();

		foreach($data as $key => $value) {
			if($key  != "_token"){

			$option_one = DB::table('options')
					->where('name', '=', $key)
					->first();
			if($option_one){

				/*$update_option = new options();
				$update_option->exists = true;
				$update_option->name = $key;
				$update_option->options =  $value;
				$update_option->save();*/

				
				$option_row =  DB::table('options')->where('name', '=', $key)->update(array('options' => $value));
				
			}else{

				$new_option = new options();
				$new_option->name = $key;
				$new_option->options =  $value;
				$new_option->save();


			}



			}			
			
		}

		return redirect('/admin/classified/adds/manager')->with('flash_success', 'Record Updated successfully!.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show()
	{
		$price_type_array = array('1'=>'Hour', '2'=>'Day');

		$adds_types = DB::table('adds_types')->get();
		foreach($adds_types  as $key => $types){
			$adds_types_array[$types->id]= $types->title;
		}
		$adds_list = DB::table('adds')
		->select('adds.*','users.username as username','users.email as useremail', 'adds_types.title AS adds_title', 'adds_types.price AS price')
		->leftJoin('users','adds.user_id','=','users.id')
		->leftJoin('adds_types','adds.adds_type','=','adds_types.id')
		->where('adds.status','=', 1)
		->get();
		return view('admin.classified.adds_list', 
			['adds_list' => $adds_list, 
			'price_type_array' => $price_type_array,
			'adds_types_array' => $adds_types_array]);
	}

	public function payments()
	{
		$price_type_array = array('1'=>'Hour', '2'=>'Day');

		$option_row =  DB::table('options')->where('name', '=', 'adds_duration_days')->first();
		$duration_type = array_map('trim', explode(',', $option_row->options));
		$duration_type_key = 1;
		foreach($duration_type  as $key => $duration){
			$new_duration_type[$duration_type_key]= $duration;
			$duration_type_key++;
		}
		$boosts_list = DB::table('post_boosts')
		->select('post_boosts.*','users.username as username','users.email as useremail', 'classifieds.title AS classifieds_title', 'classifieds.slug AS classifieds_slug')
		->leftJoin('users','post_boosts.user_id','=','users.id')
		->leftJoin('classifieds','post_boosts.classifieds_id','=','classifieds.id')
		->get();
		return view('admin.classified.adds_payments', 
			['boosts_list' => $boosts_list, 
			'price_type_array' => $price_type_array,
			'duration_type' => $new_duration_type]);
	}

	public function settings()
	{
		$price_type_array = array('1'=>'Hour', '2'=>'Day');
		$adds_types = DB::table('adds_types')->get();
		return view('admin.classified.adds_settings', 
			['adds_types' => $adds_types, 
			'price_type_array' => $price_type_array]);
	}

	public function storeSettings(Request $request)
	{
		$data  = Input::all();

		$rules = array(
	        'id' => 'integer',
			'title' => 'required',
			'position_name' => 'required',
			'price_type' => 'required|integer',
			'price' => 'required',
			'thumb' => 'mimes:jpg,jpeg,gif,png',
			'width' => 'required|integer',
			'height' => 'required|integer',
	    );

	    $v = Validator::make(Input::all(), $rules);

		/*'category' => 'required',*/
		if ($v->fails())
		{
			return redirect()->back()->withInput()->withErrors($v->errors());
		}else{
			if($request->file('thumb')){

				$image_path = $this->upload($request->file('thumb'));
			}

			if($request->input('id')){

				$adds_type = new adds_type();
				$adds_type->exists = true;
				$adds_type->id = (int)$request->input('id');
				$adds_type->title = $request->input('title');
				if(isset($image_path)){
					$adds_type->thumb = $image_path;
				}
				$adds_type->position_name = $request->input('position_name');
				$adds_type->price_type = $request->input('price_type');
				$adds_type->price = $request->input('price');
				$adds_type->width = $request->input('width');
				$adds_type->height = $request->input('height');
				$adds_type->save();

			}else{
				$adds_type = new adds_type();
				$adds_type->title = $request->input('title');
				if(isset($image_path)){
					$adds_type->thumb = $image_path;
				}
				$adds_type->position_name = $request->input('position_name');
				$adds_type->price_type = $request->input('price_type');
				$adds_type->price = $request->input('price');
				$adds_type->width = $request->input('width');
				$adds_type->height = $request->input('height');
				$adds_type->save();


			}

			return redirect('/admin/classified/adds/settings')->with('flash_success', 'Record Updated successfully!.');


		}

		
	}


	public function editSettings($id)
	{
		$price_type_array = array('1'=>'Hour', '2'=>'Day');
		$add_type_one = DB::table('adds_types')->where('id', '=', $id)->first();
		$adds_types = DB::table('adds_types')->get();
		return view('admin.classified.adds_settings_update', 
			['adds_types' => $adds_types,
			 'add_type_one' => $add_type_one, 
			'price_type_array' => $price_type_array]);
	}



	public function destroySettings($id, Request $request)
	{
		
		if ($request->input('submit_confirm')) {
            try {
                $affectedRows = DB::table('adds_types')->where('id', '=', $id)->delete();

				if($affectedRows){
					
					/* action log insertion */
					
					/* action log insertion */
					return redirect('/admin/classified/adds/settings')->with('flash_success', 'Selected Addvertisment Type Deleted Successfully!.');
				}else{
					return redirect('/admin/classified/adds/settings')->with('flash_message', 'Selected Addvertisment Type not deleted,Try again...');
				}
            }
            catch (\Exception $ex) {
                Session::flash('flash_message', $ex->getMessage());
                Session::flash('flash_type', 'error');
            }
        }

        $data = [
            'view_data' => [
                'page_parent' => 'adds',
                'current_page' => 'adds_settings',
                'page_title' => 'Delete Addvertisment Type',
                'section_title' => 'Confirm Delete Selection',
                'confirm_title' => 'Are you sure you want to delete the selected Addvertisment Type?',
                'confirm_message' => 'This action can not be undone. Please confirm.',
                'confirm_button' => 'Delete',
                'cancel_url' => url('/admin/classified/adds/settings'),
            ],
        ];
        return view('layout.confirm', $data);
	}


	public function storeNew(Request $request)
	{
		return view('admin.classified.adds_payments');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return view('admin.classified.adds_update');
	}


	public function statusChange($id, Request $request)
	{
		$classi_row = DB::table('adds')->where('id', '=', $id)->first();
		if($classi_row->approve == 0){
			$new_status = 1;
			$status_title = "Approve";
		}else{
			$new_status = 0; 
			$status_title = "Un Approve";
		}
		
		if ($request->input('submit_confirm')) {
            try {
                $affectedRows = DB::table('adds')->where('id', '=', $id)->update(array('approve' => $new_status));

				if($affectedRows){
					
					/* action log insertion */
					
					/* action log insertion */
					return redirect('/admin/classified/adds/list')->with('flash_success', 'Advertisment Approved Successfully!.');
				}else{
					return redirect('/admin/classified/adds/list')->with('flash_message', 'Advertisment Approval Status not Changed,Try again...');
				}
            }
            catch (\Exception $ex) {
                Session::flash('flash_message', $ex->getMessage());
                Session::flash('flash_type', 'error');
            }
        }

        $data = [
            'view_data' => [
                'page_parent' => 'adds',
                'current_page' => 'adds_list',
                'page_title' => 'Approve Advertisment',
                'section_title' => 'Approve Advertisment',
                'confirm_title' => "Are you sure you want to ".$status_title." the selected Advertisment?",
                'confirm_message' => 'Please confirm.',
                'confirm_button' => 'Update',
                'cancel_url' => url('/admin/classified/adds/list'),
            ],
        ];
        return view('layout.confirm', $data);
	}



	public function upload($file)
    {
        $input = array('image' => $file);
        $rules = array(
            'image' => 'image'
        );
        $validator = Validator::make($input, $rules);
        if ( $validator->fails()) {
            return Response::json(array('success' => false, 'errors' => $validator->getMessageBag()->toArray()));
        }
 
        $fileName = time() . '-' . $file->getClientOriginalName();
        $destination = public_path() . '/uploads/classifieds/adds/guide/';
        $file->move($destination, $fileName);
 
        return $fileName;
    }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id, Request $request)
	{
		///////////////////
		if ($request->input('submit_confirm')) {
            try {
                $affectedRows = DB::table('adds')->where('id', '=', $id)->delete();

				if($affectedRows){
					
					/* action log insertion */
					
					/* action log insertion */
					return redirect('/admin/classified/adds/list')->with('flash_success', 'Selected Addvertisment Deleted Successfully!.');
				}else{
					return redirect('/admin/classified/adds/list')->with('flash_message', 'Selected Addvertisment not deleted,Try again...');
				}
            }
            catch (\Exception $ex) {
                Session::flash('flash_message', $ex->getMessage());
                Session::flash('flash_type', 'error');
            }
        }

        $data = [
            'view_data' => [
                'page_parent' => 'adds',
                'current_page' => 'adds_list',
                'page_title' => 'Delete Adds',
                'section_title' => 'Confirm Delete Selection',
                'confirm_title' => 'Are you sure you want to delete the selected Add?',
                'confirm_message' => 'This action can not be undone. Please confirm.',
                'confirm_button' => 'Delete',
                'cancel_url' => url('/admin/classified/adds/list'),
            ],
        ];
        return view('layout.confirm', $data);
		/////////////
	}

}
