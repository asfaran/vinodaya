<?php namespace App\Http\Controllers\front;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Response;
use App\classified;
use App\options;
use App\post_boost;
use App\adds;
use App\tags;
use App\User;
use DB;
use Auth;
use Config;
use Image;
use File, Mail, Session;
use Illuminate\Support\Facades\Input;
Use Validator;
use Illuminate\Foundation\Validation\ValidatesRequests;

class ClassifiedController extends Controller {

	protected $request;



	public function __construct(User $user, post_boost $post_boost,Request $request)
    {
    	$this->request = $request;
    	$this->user = $user;
    	$this->post_boost = $post_boost;
        $this->pagination = 5;
        $this->middleware('auth',  ['except' => ['paypal_cancel', 'paypal_return']]);
    }




	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->middleware('auth');
	}



	public function myads()
	{
		if(Auth::user()){
			$adds_types_array = array();

			$my_ads = DB::table('adds')
				->where('user_id','=',Auth::user()->id);
			$per_page = $this->pagination;
			$my_ads = $my_ads->paginate($per_page);

			$adds_types = DB::table('adds_types')->get();

			if($adds_types){
				foreach($adds_types  as $types){
					$adds_types_array[$types->id]= $types->title;
				}
			}

				return view('front.classified.myads',
					['my_ads' => $my_ads, 
					'adds_types_array' => $adds_types_array,
					'per_page' => $per_page]);

		}
		return view('front.user.login');
	}



	public function myPosts()
	{
		if(Auth::user()){
			$my_posts = DB::table('classifieds')
				->where('user_id','=',Auth::user()->id);
			$per_page = $this->pagination;
			$my_posts = $my_posts->paginate($per_page);

			return view('front.classified.myposts',['my_posts' => $my_posts, 'per_page' => $per_page]);

		}
		return view('front.user.login');
	}


	public function postads()
	{
		$adds_types = DB::table('adds_types')->get();

		if($adds_types){
			$adds_types_array[0] ="[Select a Type]";
			foreach($adds_types  as $types){
				$adds_types_array[$types->id]= $types->title;
			}
		}else{
			$adds_types_array[0] ="[No Type For Select]";
		}

		$price_type_array = array('1'=>'Hour', '2'=>'Day');

		$option_row =  DB::table('options')->where('name', '=', 'adds_duration_days')->first();
		$duration_type = array_map('trim', explode(',', $option_row->options));
		$duration_type_key = 1;
			$new_duration_type[0]= "Select One";
		foreach($duration_type  as $key => $duration){
			$new_duration_type[$duration_type_key]= $duration;
			$duration_type_key++;
		}

		
		return view('front.classified.postads', 
			['adds_types' => $adds_types_array,
			'price_type_array' => $price_type_array,
			'duration_type' => $new_duration_type]);
	}



	public function imageRecords()
	{
		$adds_type  = Input::get('adds_type');
		if($adds_type && $adds_type !== 0){
			$my_type = DB::table('adds_types')->where('id','=', $adds_type)->first();

			$return_result = "Image Size Should be Like this!.<br>Else System will resize it.<br>";
			$return_result .= "Height: ".$my_type->height."Px<br>";
			$return_result .= "Width : ".$my_type->width."Px<br>";
			$return_result .= "<a target='_blank' href='".url('/uploads/classifieds/adds/guide/'.$my_type->thumb)."'>Click here</a> check guide image.";

			return $return_result;

		}else{

			return "Please Select One Add Type";			
		}
		
	}



	public function postclassified()
	{
		$option_row =  DB::table('options')->where('name', '=', 'adds_duration_days')->first();
		$duration_type = array_map('trim', explode(',', $option_row->options));
		$duration_type_key = 1;
			$new_duration_type[0]= "Select One";
		foreach($duration_type  as $key => $duration){
			$new_duration_type[$duration_type_key]= $duration;
			$duration_type_key++;
		}


		$category_list = DB::table('categories')
		->select('categories.id','categories.title','parent.id As parent_id', 'parent.title AS parent_title')
		->leftJoin('categories AS parent','categories.parentid','=','parent.id')
		->where('categories.type','=',5)
		->get();

		if($category_list){
			$category_array[0] ="[Select a Category]";
			foreach($category_list  as $category){
				$category_array[$category->id]= $category->title;
				if($category->parent_id !== 0){
					$subcategory_array[$category->id]= $category->title;
				}

			}
		}else{
			$category_array[0] ="[No Category For Select]";
			$subcategory_array[0] ="[No Category For Select]";
		}



		$locations_result = DB::table('locations')->get();

		if($locations_result){
			$location_array['province'][0] ="[Select a Province]";
			$location_array['city'][0] ="[Select a City]";
			$location_array['town'][0] ="[Select a Town]";
			$location_array['building'][0] ="[Select a Building]";

			foreach($locations_result  as $locations){
				if($locations->type == 1){
					$location_array['province'][$locations->id]= $locations->title;

				}else if($locations->type == 2){
					$location_array['city'][$locations->id]= $locations->title;

				}else if($locations->type == 3){
					$location_array['town'][$locations->id]= $locations->title;
					
				}else{
					$location_array['building'][$locations->id]= $locations->title;
				}
				
			}
		}else{
			$location_array['province'][0] ="[No Province For Select]";
			$location_array['city'][0] ="[No City For Select]";
			$location_array['town'][0] ="[No Town For Select]";
			$location_array['building'][0] ="[No Building For Select]";
		}


		$status_array = array('0'=>'Pending Review', '1'=>'Draft', '2'=>'Published' );
		
		return view('front.classified.post', 
			['category_list' => $category_array,
			'subcategory_array' => $subcategory_array,
			'location' => $location_array,
			'status_array' => $status_array,
			'duration_type' => $new_duration_type]);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$messsages = array(
	        'mid_chk.required'=>'You Should Agree to Terms & Condition to Post Ads!',
	    );

	    $rules = array(
	        'id' => 'integer',
			'title' => 'required',
			'adds_type' => 'required|integer',
			'duration_type' => 'required|integer',
			'duration' => 'required|integer',
			'thumb_real' => 'required|mimes:jpg,jpeg,gif,png',
			'extra' => 'required',
			'mid_chk' =>'required',
	    );

	    $v = Validator::make(Input::all(), $rules, $messsages);

		/*'category' => 'required',*/
		if ($v->fails())
		{
			return redirect()->back()->withInput()->withErrors($v->errors());
		}else{

			$adds_type_row = DB::table('adds_types')->where('id','=' ,$request->input('adds_type'))->first();

			

			if($request->file('thumb_real')){
				$thumb_resize = $this->upload_adds($request->file('thumb_real'), $adds_type_row);
				$image_path = $this->upload_adds($request->file('thumb_real'));
				
			}

			if($request->input('id')){				
	

				$adds_post = new adds();
				$adds_post->exists = true;
				$adds_post->id = (int)$request->input('id');
				$adds_post->title = $request->input('title');
				if(isset($image_path)){
					$adds_post->thumb_real = $image_path;
					$adds_post->thumb_resize = $thumb_resize;
				}
				$adds_post->extra = $request->input('extra');
				$adds_post->save();

				$msg = "Record Updated successfully!.";

				$msg_not = "Record Not Updated !";


			
			}else{

				

				$adds_post = new adds();
				$adds_post->title = $request->input('title');
				if(isset($image_path)){
					$adds_post->thumb_real = $image_path;
					$adds_post->thumb_resize = $thumb_resize;
				}
				$adds_post->user_id = Auth::user()->id;
				$adds_post->adds_type = $request->input('adds_type');
				$adds_post->duration_type = $request->input('duration_type');
				$adds_post->duration = $request->input('duration');
				$adds_post->extra = $request->input('extra');
				$adds_post->save();

				$msg = "Record Inserted successfully!.";
				$msg_not = "Record Not inserted !";

				///////////////////////////
				if(Auth::user()->usertype == 3){

					if($request->input('adds_type') && $request->input('duration_type') && $request->input('duration')){

						if($adds_type_row->price_type == 1){

							$amount_per_day = $adds_type_row->price * 24;

						}else{
							$amount_per_day = $adds_type_row->price;
						}
							$payment_amount = $amount_per_day * $request->input('duration');
							$amount_in_usd =  round(($payment_amount / 145), 2);



							////////////////////////////////
		                    $query = array();
		                    $query['notify_url'] = url('/user/myads');
		                    $query['cmd'] = '_xclick';
		                    $query['item_name'] = 'advertisment';
		                    $query['business'] = 'shaqir-buyer@extremewebdesigners.com';
		                    $query['item_number'] =  $adds_post->id;
		                    $query['userid'] = Auth::user()->id;
		                    $query['_token'] = csrf_token();
		                    $query['amount'] = $amount_in_usd;
		                    $query['cpp_header_image'] =  asset('/assets/img/logo.png');
		                    $query['no_shipping'] = 1;
		                    $query['currency_code'] = 'USD';
		                    $query['handling'] = '0';
		                    $query['cancel_return'] = url('/user/paypal_cancel');
		                    $query['return'] = url('/user/paypal_return');
		                    // Prepare query string
		                    $query_string = http_build_query($query);

		                    return redirect('https://www.sandbox.paypal.com/cgi-bin/webscr?'. $query_string);
		                	////////////////////////////////

					}else{

						$msg_not .="<br>Please Add Valid records for boost add.";

					}
				}

				//////////////////////

			}


			

			if($adds_post){
				return redirect('/user/')->with('flash_success', $msg);

			}else{

				return redirect('/user/')->with('flash_warning', $msg_not);
			}




		
		}
	}


	public function classifiedStore(Request $request)
	{

		$status_array = array('0'=>'Pending Review', '1'=>'Draft', '2'=>'Published' );
		$post_status =  0;
		$messsages = array(
	        'mid_chk.required'=>'You Should Agree to Terms & Condition for Ads Posting!',
	    );

	    $rules = array(
	        'id' => 'integer',
			'title' => 'required',
			'parentid' => 'required|integer',
			'category' => 'required|integer',
			'description' => 'required',
			'thumb' => 'required|mimes:jpg,jpeg,gif,png',
			'location_province' => 'required|integer',
			'location_city' => 'required|integer',
			'location_town' => 'integer',
			'price' =>'required',
			'mid_chk' =>'required',
	    );

	    $v = Validator::make(Input::all(), $rules,$messsages);

		/*'category' => 'required',*/
		if ($v->fails())
		{
			return redirect()->back()->withInput()->withErrors($v->errors());
		}else{

			$parent_cat_row = DB::table('categories')->where('id','=' ,$request->input('parentid'))->first();

			if($request->file('thumb')){

				$image_path = $this->upload($request->file('thumb'), $request->input('parentid'));
			}

			$post_featured = ($request->input('featured') != null)? 1 : 0;


			if($request->input('id')){
				$title_lowercase = strtolower($request->input('post_slug'));
				$record_row = DB::table('classifieds')->where('id','=' ,$request->input('id'))->first();
				$slug_before = $record_row->slug;

			}else{

				$title_lowercase = strtolower($request->input('title'));
				$slug_before = "";
				
			}

			$slug = preg_replace('/\s+/', '-', $title_lowercase);
			if($slug  === $slug_before){
				$new_slug = $slug;
			}else{
				$new_slug = $slug;
				
				$line_row = DB::table('classifieds')->where('slug','=' ,$slug)->first();

				if($line_row){
					for ($i = 1; $i <= 100; ++$i) {
						$search_slug = $slug."-".$i;
						$sub_row = DB::table('classifieds')->where('slug','=' ,$search_slug)->first();
						if($sub_row){
							continue;
						}else{

							$new_slug = $search_slug;
							 break;
						}

					}
				}

			}

			if($request->input('id')){


				$classified = new classified();
				$classified->exists = true;
				$classified->id = (int)$request->input('id');
				$classified->title = $request->input('title');
				if(isset($image_path)){
					$classified->thumb = $image_path;
				}
				
				/*$classified->user_id = Auth::user()->id;*/
				$classified->user_id = Auth::user()->id;
				$classified->status = $post_status;
				$classified->slug = $new_slug;
				$classified->featured  = $post_featured;
				$classified->parentid = $request->input('parentid');
				$classified->category = $request->input('category');
				$classified->extra_fields = json_encode( $request->input('extra_fields') );
				$classified->description = $request->input('description');
				$classified->location_province = $request->input('location_province');
				$classified->location_city = $request->input('location_city');
				$classified->location_town = $request->input('location_town');
				$classified->location_building = 0;
				$classified->price = $request->input('price');
				$classified->save();

				$msg = "Record Updated successfully!.";
			}else{

				
				

				$classified = new classified();
				$classified->title = $request->input('title');
				if(isset($image_path)){
					$classified->thumb = $image_path;
				}
				
				/*$classified->user_id = Auth::user()->id;*/
				$classified->user_id = Auth::user()->id;
				$classified->status = $post_status;
				$classified->slug = $new_slug;
				$classified->featured  = $post_featured;
				$classified->parentid = $request->input('parentid');
				$classified->category = $request->input('category');
				$classified->extra_fields = json_encode( $request->input('extra_fields') );
				$classified->description = $request->input('description');
				$classified->location_province = $request->input('location_province');
				$classified->location_city = $request->input('location_city');
				$classified->location_town = $request->input('location_town');
				$classified->location_building = 0;
				$classified->price = $request->input('price');
				$classified->save();


				$msg = "Record Inserted successfully!.";

			}

			if($request->input('featured') !== null){


				if($request->input('duration') && $request->input('budget') && $request->input('boost_type')){

					$amount = round(($request->input('budget') / 145), 2);

					
					$post_boost_row = DB::table('post_boosts')->where('user_id','=', Auth::user()->id)->where('classifieds_id','=', $classified->id)->first();

					$boost_type = ($request->input('boost_type') == "top_ads")? 1 : 2;

					if(!$post_boost_row){

						$post_boost_data = new post_boost();
						$post_boost_data->user_id = Auth::user()->id;
						$post_boost_data->classifieds_id = $classified->id;
						$post_boost_data->boost_type = $boost_type;
						$post_boost_data->duration = $request->input('duration');
						$post_boost_data->budget = $request->input('budget');
						$post_boost_data->save();

						////////////////////////////////
	                    $query = array();
	                    $query['notify_url'] = url('/user/postclassified');
	                    $query['cmd'] = '_xclick';
	                    $query['item_name'] = $request->input('boost_type');
	                    $query['business'] = 'shaqir-buyer@extremewebdesigners.com';
	                    $query['item_number'] =  $classified->id;
	                    $query['userid'] = Auth::user()->id;
	                    $query['_token'] = csrf_token();
	                    $query['amount'] = $amount;
	                    $query['cpp_header_image'] =  asset('/assets/img/logo.png');
	                    $query['no_shipping'] = 1;
	                    $query['currency_code'] = 'USD';
	                    $query['handling'] = '0';
	                    $query['cancel_return'] = url('/user/paypal_cancel');
	                    $query['return'] = url('/user/paypal_return');
	                    // Prepare query string
	                    $query_string = http_build_query($query);

	                    return redirect('https://www.sandbox.paypal.com/cgi-bin/webscr?'. $query_string);
	                	////////////////////////////////

					}

					

				}else{

					$msg .="<br>Please Add Valid records for boost add.";

				}
			}

			if($classified){
				return redirect('/user/myposts')->with('flash_success', $msg);

			}else{

				return redirect('/user/myposts')->with('flash_warning', 'Record Not Updated!.');

			}
		
		}
	}


	public function upload_adds($file, $adds_type_row = null)
    {

    	if($adds_type_row !== null){

                $image_width = $adds_type_row->width;
                $image_height = $adds_type_row->height;
                $input = array('image' => $file);
		        $rules = array(
		            'image' => 'image'
		        );
		        $validator = Validator::make($input, $rules);
		        if ( $validator->fails()) {
		            return Response::json(array('success' => false, 'errors' => $validator->getMessageBag()->toArray()));
		        }
		 
		        $fileName = time() . '-' . $file->getClientOriginalName();
		        $destination = public_path() . '/uploads/classifieds/adds/resize_image/'.$fileName;
		        Image::make($file->getRealPath())->resize($image_width, $image_height)->save($destination);
		        return $fileName;




    	}else{

    		$input = array('image' => $file);
	        $rules = array(
	            'image' => 'image'
	        );
	        $validator = Validator::make($input, $rules);
	        if ( $validator->fails()) {
	            return Response::json(array('success' => false, 'errors' => $validator->getMessageBag()->toArray()));
	        }
	 
	        $fileName = time() . '-' . $file->getClientOriginalName();
	        $destination = public_path() . '/uploads/classifieds/adds/real_image/';
	        $file->move($destination, $fileName);
	 
	        return $fileName;


    	}
        
    }


	public function upload($file, $category)
    {
    	/*if($request->hasFile('file')) {
            $file = $request->file('file');

           // $tmpUserName = Auth::user()->id .'-' ;

            $tmpTimeToken = time(); //for if same image was uploaded.

            $tmpFileName = $tmpTimeToken.'-'.$file->getClientOriginalName();

            $tmpFilePath = 'upload/tmp/';

            $hardPath =   date('Y-m') .'/'.date('d') .'/';


            if (!file_exists(public_path() .'/'.$tmpFilePath.$hardPath )) {
                $oldmask = umask(0);
                mkdir(public_path() .'/'. $tmpFilePath.$hardPath , 0777, true);
                umask($oldmask);
            }

            $img = Image::make($file);

            $imgmj = $img->mime();

            if($type!=='preview' and $imgmj=='image/gif'){

                $file->move(public_path() . '/'. $tmpFilePath. $hardPath, $tmpUserName.$tmpFileName);

                $path = '/'.$tmpFilePath .$hardPath . $tmpUserName. $tmpFileName;

            }else{

                    if($type=='entry'){

                        $img->resize(640, null, function ($constraint) {
                         $constraint->aspectRatio();
                         $constraint->upsize();
                        });

                    }else if($type=='preview'){

                        $img->fit(650, 370);

                    }


                $path = $tmpFilePath .$hardPath . $tmpUserName. md5($tmpFileName). '.jpg';

                $img->save($path);

                $path='/'.$path;
            }

        }*/
        $input = array('image' => $file);
        $rules = array(
            'image' => 'image'
        );
        $validator = Validator::make($input, $rules);
        if ( $validator->fails()) {
            return Response::json(array('success' => false, 'errors' => $validator->getMessageBag()->toArray()));
        }
 
        $fileName = time() . '-' . $file->getClientOriginalName();
        $destination = public_path() . '/uploads/classifieds/'.$category.'/';
        $file->move($destination, $fileName);
 
        return $fileName;
    }


	public function editads($id)
	{
		$adds_types = DB::table('adds_types')->get();


		$advert_one = DB::table('adds')->where('id', '=', $id)->first();

		if($adds_types){
			$adds_types_array[0] ="[Select a Type]";
			foreach($adds_types  as $types){
				$adds_types_array[$types->id]= $types->title;
			}
		}else{
			$adds_types_array[0] ="[No Type For Select]";
		}

		$price_type_array = array('1'=>'Hour', '2'=>'Day');

		$option_row =  DB::table('options')->where('name', '=', 'adds_duration_days')->first();
		$duration_type = array_map('trim', explode(',', $option_row->options));
		$duration_type_key = 1;
			$new_duration_type[0]= "Select One";
		foreach($duration_type  as $key => $duration){
			$new_duration_type[$duration_type_key]= $duration;
			$duration_type_key++;
		}


		
		return view('front.classified.updateads', 
			['advert_one' => $advert_one,
			'adds_types' => $adds_types_array,
			'price_type_array' => $price_type_array,
			'duration_type' => $new_duration_type]);
	}


	public function editPosts($id)
	{
		$post_boost_row =  array('duration'=> null, 'budget'=> null, 'boost_type'=> null);
		$post_boost_row = (object) $post_boost_row;


		$category_list = DB::table('categories')
		->select('categories.id','categories.title','parent.id As parent_id', 'parent.title AS parent_title')
		->leftJoin('categories AS parent','categories.parentid','=','parent.id')
		->where('categories.type','=',5)
		->get();



		$classified_one = DB::table('classifieds')
					->where('id', '=', $id)
					->first();
		$parent_cat_row = DB::table('categories')->where('id','=' ,$classified_one->parentid)->first();

		if($category_list){
			$category_array[0] ="[Select a Category]";
			foreach($category_list  as $category){
				$category_array[$category->id]= $category->title;
				if($category->parent_id !== 0){
					$subcategory_array[$category->id]= $category->title;
				}

			}
		}else{
			$category_array[0] ="[No Category For Select]";
			$subcategory_array[0] ="[No Category For Select]";
		}

		$locations_result = DB::table('locations')->get();

		if($locations_result){
			$location_array['province'][0] ="[Select a Province]";
			$location_array['city'][0] ="[Select a City]";
			$location_array['town'][0] ="[Select a Town]";
			$location_array['building'][0] ="[Select a Building]";

			foreach($locations_result  as $locations){
				if($locations->type == 1){
					$location_array['province'][$locations->id]= $locations->title;

				}else if($locations->type == 2){
					$location_array['city'][$locations->id]= $locations->title;

				}else if($locations->type == 3){
					$location_array['town'][$locations->id]= $locations->title;
					
				}else{
					$location_array['building'][$locations->id]= $locations->title;
				}
				
			}
		}else{
			$location_array['province'][0] ="[No Province For Select]";
			$location_array['city'][0] ="[No City For Select]";
			$location_array['town'][0] ="[No Town For Select]";
			$location_array['building'][0] ="[No Building For Select]";
		}

		$option_row =  DB::table('options')->where('name', '=', 'adds_duration_days')->first();
		$duration_type = array_map('trim', explode(',', $option_row->options));
		$duration_type_key = 1;
			$new_duration_type[0]= "Select One";
		foreach($duration_type  as $key => $duration){
			$new_duration_type[$duration_type_key]= $duration;
			$duration_type_key++;
		}

		if($classified_one->featured == 1){
			$select_row = DB::table('post_boosts')
					->where('user_id','=', Auth::user()->id)
					->where('classifieds_id','=', $id)
					->first();
			if($select_row){
				$post_boost_row = $select_row;
			}

		}



		$status_array = array('0'=>'Pending Review', '1'=>'Draft', '2'=>'Published' );
		
		return view('front.classified.update', 
			['category_list' => $category_array,
			'subcategory_array' => $subcategory_array,
			'location' => $location_array,
			'classified_one' => $classified_one,
			'category_name' => $parent_cat_row->slug,
			'status_array' => $status_array,
			'duration_type' => $new_duration_type,
			'post_boost_row' => $post_boost_row]);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	public function addsStatus($id, Request $request)
	{
		$classi_row = DB::table('adds')->where('id', '=', $id)->first();
		if($classi_row->status == 0){
			$new_status = 1;
			$status_title = "Display";
		}else{
			$new_status = 0; 
			$status_title = "Hide Display";
		}
		
		if ($request->input('submit_confirm')) {
            try {
                $affectedRows = DB::table('adds')->where('id', '=', $id)->update(array('visibility' => $new_status));

				if($affectedRows){
					
					/* action log insertion */
					
					/* action log insertion */
					return redirect('/user/myads')->with('flash_success', 'Advertisment Status Changed Successfully!.');
				}else{
					return redirect('/user/myads')->with('flash_message', 'Advertisment Status not Changed,Try again...');
				}
            }
            catch (\Exception $ex) {
                Session::flash('flash_message', $ex->getMessage());
                Session::flash('flash_type', 'error');
            }
        }

        $data = [
            'view_data' => [
                'page_parent' => 'classified',
                'current_page' => 'postads',
                'page_title' => 'Change Advertisment Display Status',
                'section_title' => 'Change Advertisment Display Status',
                'confirm_title' => "Are you sure you want to ".$status_title." the selected Advertisment?",
                'confirm_message' => 'Please confirm.',
                'confirm_button' => 'Update',
                'cancel_url' => url('/user/myads'),
            ],
        ];
        return view('front.classified.confirm', $data);
	}


	public function changestatus($id, Request $request)
	{
		$classi_row = DB::table('classifieds')->where('id', '=', $id)->first();
		if($classi_row->display == 0){
			$new_status = 1;
			$status_title = "Display";
		}else{
			$new_status = 0; 
			$status_title = "Hide Display";
		}
		
		if ($request->input('submit_confirm')) {
            try {
                $affectedRows = DB::table('classifieds')->where('id', '=', $id)->update(array('visibility' => $new_status));

				if($affectedRows){
					
					/* action log insertion */
					
					/* action log insertion */
					return redirect('/user/myposts')->with('flash_success', 'Classified Status Changed Successfully!.');
				}else{
					return redirect('/user/myposts')->with('flash_message', 'Classified Status not Changed,Try again...');
				}
            }
            catch (\Exception $ex) {
                Session::flash('flash_message', $ex->getMessage());
                Session::flash('flash_type', 'error');
            }
        }

        $data = [
            'view_data' => [
                'page_parent' => 'classified',
                'current_page' => 'postads',
                'page_title' => 'Change Classified Display Status',
                'section_title' => 'Change Classified Display Status',
                'confirm_title' => "Are you sure you want to ".$status_title." the selected Classified?",
                'confirm_message' => 'Please confirm.',
                'confirm_button' => 'Update',
                'cancel_url' => url('/user/myposts'),
            ],
        ];
        return view('front.classified.confirm', $data);
	}



	public function paypal_return(Request $request)
    {
        $input = $this->request->all();

/*
        if(isset($input['item_name'])){
            $program = $this->program->where('name', '=', $input['item_name'])->first();
        }*/

        if(isset($input['item_name'])){

        	if ($input['item_name'] == "company_registration" && $input['payer_status'] == "verified") { 


	            $user = Auth::user();

	            $company_status = $this->user->where('id','=', Auth::user()->id)->update(array('company_status' => 1));

	            $data['full_name'] = $user->first_name . ' ' . $user->last_name;
	            $data['email'] = $user->email;
	            $data['user'] = $user;
	            $data['from'] = Config::get('mail.from.address');
	            $data['from_name'] = Config::get('mail.from.name');
	            $data['admin_email'] = Config::get('mail.admin_email');

	            Mail::send('emails.company_registration', ['data' => $data], function ($m) use ($data) {
	                $m->from($data['from'], $data['from_name']);
	                $m->to($data['email'], $data['full_name'])->subject('Thank you for Registration with Company Details!');
	            });

	            Mail::send('emails.company_registration', ['data' => $data], function ($m) use ($data) {
	                $m->from($data['from'], $data['from_name']);
	                $m->to($data['admin_email'])->subject('Registration with Company Details! | Vinodaya.');
	            });

	            return redirect('/user')->with('flash_success','Payment has been successfully accepted for Registration with Company Details!');
	        }elseif(($input['item_name'] == "top_ads" || $input['item_name'] == "featured_ads" )&& $input['payer_status'] == "verified"){
	        	echo "<script> console.log('".json_encode($input)."'); </script>";
	        	$post_boost_row = DB::table('post_boosts')->where('user_id','=', Auth::user()->id)->where('classifieds_id','=', $input['item_number'])->first();

				$boost_type = ($input['item_name'] == "top_ads")? 1 : 2;
				$amount = $amount = round(($input['mc_gross'] * 145), 2);

				if($post_boost_row){
					$post_boost_data = DB::table('post_boosts')->where('user_id','=', Auth::user()->id)
							->where('classifieds_id','=', $input['item_number'])
							->update(array('boost_type' => $boost_type, 
								'paypal_id' => $input['payer_id'], 
								'paypal_reference' => $input['txn_id'],
								'paid_amount' => $amount,
								'status' => 1));

				}else{

					$post_boost_data = new post_boost();
					$post_boost_data->user_id = Auth::user()->id;
					$post_boost_data->classifieds_id = $input['item_number'];
					$post_boost_data->boost_type = $boost_type;
					$post_boost_data->budget = $amount;
					$post_boost_data->paid_amount = $amount;
					$post_boost_data->paypal_reference = $input['txn_id'];
					$post_boost_data->paypal_id = $input['payer_id'];
					$post_boost_data->status = 1;
					$post_boost_data->save();

				}

				return redirect('/user/myposts')->with('flash_success','Payment has been successfully accepted for Boost Post  as '.$input['item_name'].'!');

	        }elseif($input['item_name'] == "advertisment" && $input['payer_status'] == "verified"){
	        	echo "<script> console.log('".json_encode($input)."'); </script>";

				$amount = $amount = round(($input['mc_gross'] * 145), 2);

				$post_boost_data = DB::table('adds')->where('user_id','=', Auth::user()->id)
							->where('id','=', $input['item_number'])
							->update(array('status' => 1));

				

				return redirect('/user/myads')->with('flash_success','Payment has been successfully accepted for advertisment !');

	        }else {
	            return redirect('/user')->with('flash_message', 'Payment amount is not correct. Please proceed with correct amount.');
	        }



        }else{
        	return redirect('/user')->with('flash_message', 'Issues with return records Please contact admin!');

        }
        return $this->request->all();
    }



    public function paypal_cancel()
    {
    	$input = $this->request->all();
        return redirect('/user')->with('flash_message', 'Your Payment Canceled for Please try again.');
    }




	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id, Request $request)
	{
		
		if ($request->input('submit_confirm')) {
            try {
                $affectedRows = DB::table('adds')->where('id', '=', $id)->delete();

				if($affectedRows){
					
					/* action log insertion */
					
					/* action log insertion */
					return redirect('/user/myads')->with('flash_success', 'Selected Classified Deleted Successfully!.');
				}else{
					return redirect('/user/myads')->with('flash_message', 'Selected Classified not deleted,Try again...');
				}
            }
            catch (\Exception $ex) {
                Session::flash('flash_message', $ex->getMessage());
                Session::flash('flash_type', 'error');
            }
        }

        $data = [
            'view_data' => [
                'page_parent' => 'classified',
                'current_page' => 'postads',
                'page_title' => 'Delete Advertisment',
                'section_title' => 'Confirm Delete Selection',
                'confirm_title' => 'Are you sure you want to delete the selected Advertisment?',
                'confirm_message' => 'This action can not be undone. Please confirm.',
                'confirm_button' => 'Delete',
                'cancel_url' => url('/user/myads'),
            ],
        ];
        return view('front.classified.confirm', $data);
	}


	 public function extrafieldsfilter(Request $request)
    {

        $location_type_array = array('1'=>'province', '2'=>'city', '3'=>'town', '4'=>'building' );


        $parent_id  = Input::get('parent_id');
        $category_id  = Input::get('category_id');



        $extra_fields_list = DB::table('classified_fields')
						->select('id','fields')
						->where('category','=',$category_id)
						->where('parentid','=', $parent_id)
						->get();


		$fields_array_set = array();



		foreach($extra_fields_list  as $id => $fields){
			if($fields->fields){

				$fields_array = json_decode($fields->fields, TRUE);
					foreach($fields_array  as $id => $field_row){

						$extra_fields_row = DB::table('form_fields')
								->where('id','=',$field_row)
								->first();
						$fields_array_set[] =  $extra_fields_row;


					}
			}
				
		}

		$fields_array_set = array_filter($fields_array_set);


         return view('admin.classified.extra_fields_filter', ['fields_array_set' => $fields_array_set]);


    }



    public function classifiedFilter(Request $request)
    {

        /*if (Request::isMethod('POST')) {*/
        $category_id  = $request->input('category_id');

        $category_list = DB::table('categories')->where('parentid','=',(int)$category_id)->get();

            
        if($category_list){
            $category_array[0] ="[Select a Category]";
            foreach($category_list  as $category){
                $category_array[$category->id]= $category->title;

            }
        }else{
            $category_array[0] ="[No Category For Select]";
        }


            

        return view('admin.classified.classified_fields_filter', ['category_array' => $category_array]);
        /*}*/


    }




	public function destroyPosts($id, Request $request)
	{
		
		if ($request->input('submit_confirm')) {
            try {
                $affectedRows = DB::table('classifieds')->where('id', '=', $id)->delete();

				if($affectedRows){
					
					/* action log insertion */
					
					/* action log insertion */
					return redirect('/user/myposts')->with('flash_success', 'Selected Classified Deleted Successfully!.');
				}else{
					return redirect('/user/myposts')->with('flash_message', 'Selected Classified not deleted,Try again...');
				}
            }
            catch (\Exception $ex) {
                Session::flash('flash_message', $ex->getMessage());
                Session::flash('flash_type', 'error');
            }
        }

        $data = [
            'view_data' => [
                'page_parent' => 'classified',
                'current_page' => 'postclassified',
                'page_title' => 'Delete Classified',
                'section_title' => 'Confirm Delete Selection',
                'confirm_title' => 'Are you sure you want to delete the selected Classified?',
                'confirm_message' => 'This action can not be undone. Please confirm.',
                'confirm_button' => 'Delete',
                'cancel_url' => url('/user/myposts'),
            ],
        ];
        return view('front.classified.confirm', $data);
	}

}
