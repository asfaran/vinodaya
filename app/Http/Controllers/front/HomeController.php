<?php namespace App\Http\Controllers\front;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use App\Services\NewsService;
use App\Services\VideoService;
use App\Services\ImageService;
use App\Services\ClassifiedService;
use App\Services\PagesService;
use App\Services\AddsService;
use Counter;

class HomeController extends Controller {

	protected $NewsService;
	protected $VideoService;
	protected $ImageService;
	protected $ClassifiedService;
	protected $PagesService;
	protected $AddsService;


	public function __construct(NewsService $NewsService, VideoService $VideoService, ImageService $ImageService, ClassifiedService $ClassifiedService, PagesService $PagesService, AddsService $AddsService)
    {
        $this->NewsService = $NewsService;
        $this->VideoService = $VideoService;
        $this->ImageService = $ImageService;
        $this->ClassifiedService = $ClassifiedService;
        $this->PagesService = $PagesService;
        $this->AddsService = $AddsService;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$quick_category_array = array();
		$latest_news = $this->NewsService->latest_news();
		$front_latest = $this->NewsService->front_latest();
		$gossip_zone = $this->NewsService->gossip_zone();
		$news_by_category = $this->NewsService->news_by_category();

		$latest_videos = $this->VideoService->latest_videos();
		$patta_videos = $this->VideoService->patta_videos();

		$home_adds = $this->AddsService->home_adds();

		$cartoon_ofthe_day = $this->ImageService->cartoon_ofthe_day();
		$model_ofthe_day = $this->ImageService->model_ofthe_day();

		$classified_featured = $this->ClassifiedService->get_featured();


		$quick_categories = DB::table('categories')->where('display', '=', 1)->get();
		foreach($quick_categories  as $key => $category){
			$quick_category_array[$category->type][$category->id] = $category->title ;
		}

		return view('front.welcome', 
			compact('latest_news', 
					'gossip_zone', 
					'front_latest',
					'latest_videos',
					'patta_videos',
					'news_by_category',
					'cartoon_ofthe_day',
					'model_ofthe_day',
					'classified_featured',
					'home_adds',
					'quick_category_array'));
	}


	public function Schedule()
	{
		$classified_list = DB::table('classifieds')->get();


		$top_add_option_row =  DB::table('options')->where('name', '=', 'adds_top_add_per_click')->first();
		$top_add_option = $top_add_option_row->options;
		$featured_add_option_row =  DB::table('options')->where('name', '=', 'adds_featured_add_per_click')->first();
		$featured_add_option = $featured_add_option_row->options;


		$option_row =  DB::table('options')->where('name', '=', 'adds_duration_days')->first();
		$duration_type = array_map('trim', explode(',', $option_row->options));
		$duration_type_key = 1;
			$new_duration_type[0]= "Select One";
		foreach($duration_type  as $key => $duration){
			$new_duration_type[$duration_type_key]= $duration;
			$duration_type_key++;
		}

		$something = array();


		if($classified_list){

			foreach($classified_list  as $key => $classified){

				if($classified->featured === 1){
				$boost_row = DB::table('post_boosts')
							->where('classifieds_id','=' ,$classified->id)
							->where('status','=' , 1)
							->first();
					if($boost_row){
						$classified->boost_status = $boost_row->status;
						$boost_type = $boost_row->boost_type;
						$classified->boost_type = $boost_type;
						$budget = $boost_row->budget;
						if($boost_type === 1){
							$target_click_count = ceil($budget/$top_add_option);
						}else{
							$target_click_count = ceil($budget/$featured_add_option);
						}
						

						$current_count = Counter::show('classified_featured', $classified->id);
						
						$classified->duration  = $new_duration_type[$boost_row->duration];
						$expire_date = \Carbon\Carbon::createFromTimeStamp(strtotime($boost_row->created_at))->addDays($new_duration_type[$boost_row->duration]);
						
						$something[$classified->id]['target'] = $target_click_count;
						$something[$classified->id]['budget'] = $budget;
						$something[$classified->id]['boost_type'] = $boost_type;
						$something[$classified->id]['now'] =  \Carbon\Carbon::now();
						$something[$classified->id]['expire_date'] = $expire_date;
						$something[$classified->id]['current_count'] = $current_count;

						if(($current_count >= $target_click_count) || (\Carbon\Carbon::now() >= $expire_date)){
							$affectedRows = DB::table('post_boosts')->where('id', '=', $boost_row->id)->update(array('status' => 2));
							$classified->boost_status = 2;
						}
						

					}else{
						$classified->boost_status = 0;
					}
				}
							
			}


		}


	}



	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
