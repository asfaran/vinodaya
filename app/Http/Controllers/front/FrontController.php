<?php namespace App\Http\Controllers\front;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use App\Services\NewsService;
use App\Services\VideoService;
use App\Services\ImageService;
use App\Services\ArticleService;
use App\Services\ClassifiedService;
use App\Services\PagesService;
use App\Services\AddsService;
use App\Services\CategoryService;
use App\comments;
Use Validator;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\URL;
use Counter;

class FrontController extends Controller {

	protected $NewsService;
	protected $VideoService;
	protected $ImageService;
	protected $ArticleService;
	protected $ClassifiedService;
	protected $PagesService;
	protected $AddsService;
	protected $CategoryService;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct(NewsService $NewsService, VideoService $VideoService, ImageService $ImageService, ArticleService $ArticleService, ClassifiedService $ClassifiedService, PagesService $PagesService, AddsService $AddsService, CategoryService $CategoryService)
    {
        $this->NewsService = $NewsService;
        $this->VideoService = $VideoService;
        $this->ImageService = $ImageService;
        $this->ArticleService = $ArticleService;
        $this->ClassifiedService = $ClassifiedService;
        $this->PagesService = $PagesService;
        $this->AddsService = $AddsService;
        $this->CategoryService = $CategoryService;
        $this->pagination = 4;
    }


	public function index()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postComments(Request $request)
	{
		$v = Validator::make($request->all(), [
			'id' => 'integer',
			'author' => 'required',
			'post_type' => 'required',
			'author_email' => 'required|email',
			'content' => 'required',

		]);

		if ($v->fails())
		{
			/*return redirect()->back()->withInput()->withErrors($v->errors());*/

			return redirect()->to(URL::previous() . "#post_comments_div")->withInput()->withErrors($v->errors());
		}else{

			$user_id = (isset(Auth::user()->id))? Auth::user()->id : 0;

			

			if($request->input('id')){

				$comments = comments::find($request->input('id'));
				$comments->post_type = $request->input('post_type');
				$comments->post_id = $request->input('post_id');
				$comments->author = $request->input('author');
				$comments->author_email = $request->input('author_email');
				$comments->content = $request->input('content');
				$comments->user_id = $user_id;
				$comments->save();


			}else{

				/*$input = $request->all();
				
				parse_str($request->input('formdata'), $data_array);*/

				$comments = new comments();
				$comments->post_type = $request->input('post_type');
				$comments->post_id = $request->input('post_id');
				$comments->author = $request->input('author');
				$comments->author_email = $request->input('author_email');
				$comments->content = $request->input('content');
				$comments->user_id = $user_id;
				$comments->save();

				/*$comments->author = $data_array['author'];
				$comments->author_email = $data_array['author_email'];
				$comments->post_type = $data_array['post_type'];
				$comments->content = $data_array['content'];
				$comments->post_id = $data_array['post_id'];
				$comments->user_id = $user_id;
				$comments->save()*/;
				

			}
			echo "<script> console.log('".json_encode($comments)."'); </script>";

			if($comments){
				if($request->input('post_type') == 1){

					return redirect('/news/'.$request->input('post_id').'#post_comments_div')
							->with('flash_success', 'Comments posted successfully');

				}elseif($request->input('post_type') == 2){
					return redirect('/article/'.$request->input('post_id').'#post_comments_div')
							->with('flash_success', 'Commants posted successfully');

				}elseif($request->input('post_type') == 3){
					return redirect('/images/'.$request->input('post_id').'#post_comments_div')
							->with('flash_success', 'Commants posted successfully');
					
				}elseif($request->input('post_type') == 4){
					return redirect('/video-dp/'.$request->input('post_id').'#post_comments_div')
							->with('flash_success', 'Commants posted successfully');
					
				}


			}

		
		}
	}



	public function articlesLP()
	{
		$listing_count = DB::table('options')->where('name', '=', 'article_count_in_listing')->first();
		$per_page = (int)$listing_count->options;
		/*$per_page = $this->pagination;*/
		$latest_news = $this->NewsService->latest_news();
		$gossip_zone = $this->NewsService->gossip_zone();
		$article_list = $this->ArticleService->get_list();
		$article_featured = $this->ArticleService->get_featured($per_page);
		$classified_featured = $this->ClassifiedService->get_featured();

		

		
        $article_list = $article_list->paginate($per_page);
        $page_adds = $this->AddsService->listing_adds($per_page);

		return view('front.articles.lp' , compact('latest_news', 'gossip_zone', 'article_list', 'classified_featured', 'per_page', 'page_adds', 'article_featured'));
	}


	public function articlesCategoryLP($id)
	{
		$listing_count = DB::table('options')->where('name', '=', 'article_count_in_listing')->first();
		$per_page = (int)$listing_count->options;
		/*$per_page = $this->pagination;*/
		$latest_news = $this->NewsService->latest_news();
		$gossip_zone = $this->NewsService->gossip_zone();
		$article_list = $this->ArticleService->getcat_list($id);
		$article_featured = $this->ArticleService->get_featured($per_page);
		$classified_featured = $this->ClassifiedService->get_featured();

		
        $article_list = $article_list->paginate($per_page);
        $page_adds = $this->AddsService->listing_adds($per_page);

		return view('front.articles.lp' , compact('latest_news', 'gossip_zone', 'article_list', 'classified_featured', 'per_page', 'page_adds', 'article_featured'));
	}


	public function articlesDP($id)
	{
		$article_one = $this->ArticleService->get_one($id);
		$related_articles = $this->ArticleService->get_related($id);
		$classified_featured = $this->ClassifiedService->get_featured();


		$Comments_array = $this->CategoryService->Comments_array(2, $id);

		$category_array = $this->CategoryService->category_array(2);
		$catid_array = json_decode($article_one->category_id, TRUE);
		$catid_array_count = 1;
		$category_names = "";

		foreach($catid_array as $key => $value){

			if (array_key_exists((int)$value, $category_array)) {
				$category_names .=  $category_array[(int)$value];
				if(count($catid_array) !== $catid_array_count){
					$category_names .= " , ";
				}
				
			}
		 $catid_array_count++;		
		}


		$page_adds = $this->AddsService->detail_adds();

		
		return view('front.articles.dp', compact('article_one', 'related_articles', 'classified_featured', 'page_adds', 'category_names', 'Comments_array'));
	}


	public function imagesLP()
	{
		$listing_count = DB::table('options')->where('name', '=', 'image_count_in_listing')->first();
		$per_page = (int)$listing_count->options;
		/*$per_page = $this->pagination;*/

		$latest_news = $this->NewsService->latest_news();
		$gossip_zone = $this->NewsService->gossip_zone();
		$image_list = $this->ImageService->get_list();
		$classified_featured = $this->ClassifiedService->get_featured();

        $image_list = $image_list->paginate($per_page);

        $page_adds = $this->AddsService->listing_adds($per_page);

        

		return view('front.images.lp' , compact('latest_news', 'gossip_zone', 'image_list', 'classified_featured', 'per_page', 'page_adds'));
	}



	public function imageCategoryLP($id)
	{
		$listing_count = DB::table('options')->where('name', '=', 'image_count_in_listing')->first();
		$per_page = (int)$listing_count->options;
		/*$per_page = $this->pagination;*/


		$latest_news = $this->NewsService->latest_news();
		$gossip_zone = $this->NewsService->gossip_zone();
		$image_list = $this->ImageService->getcat_list($id);
		$classified_featured = $this->ClassifiedService->get_featured();

        $image_list = $image_list->paginate($per_page);

        $page_adds = $this->AddsService->listing_adds($per_page);


        

		return view('front.images.lp' , compact('latest_news', 'gossip_zone', 'image_list', 'classified_featured', 'per_page', 'page_adds'));
	}


	public function imagesDP($id)
	{
		$image_one = $this->ImageService->get_one($id);
		$related_images = $this->ImageService->get_related($id);
		$classified_featured = $this->ClassifiedService->get_featured();

		$Comments_array = $this->CategoryService->Comments_array(3, $id);

		$page_adds = $this->AddsService->detail_adds();

		$category_array = $this->CategoryService->category_array(3);
		$category_names = "";

		$catid_array = json_decode($image_one->category_id, TRUE);
		$catid_array_count = 1;

		foreach($catid_array as $key => $value){

			if (array_key_exists((int)$value, $category_array)) {
				$category_names .=  $category_array[(int)$value];
				if(count($catid_array) !== $catid_array_count){
					$category_names .= " , ";
				}
				
			}
		 $catid_array_count++;		
		}

		

		return view('front.images.dp', compact('image_one', 'related_images', 'classified_featured', 'page_adds', 'category_names', 'Comments_array'));
	}


	public function newsLP()
	{
		$listing_count = DB::table('options')->where('name', '=', 'news_count_in_listing')->first();
		$per_page = (int)$listing_count->options;
		/*$per_page = $this->pagination;*/
		$featured_count = DB::table('options')->where('name', '=', 'featured_news_count')->first();
		$latest_news = $this->NewsService->latest_news();
		$gossip_zone = $this->NewsService->gossip_zone();
		$news_list = $this->NewsService->get_list();
		$news_featured = $this->NewsService->get_featured((int)$featured_count->options);
		$classified_featured = $this->ClassifiedService->get_featured();
		
        $news_list = $news_list->paginate($per_page);
        $page_adds = $this->AddsService->news_listing($per_page);


        

		return view('front.news.lp' , compact('latest_news', 'gossip_zone', 'news_list', 'classified_featured', 'per_page', 'page_adds', 'news_featured'));
	}



	public function newsCategoryLP($id)
	{
		$listing_count = DB::table('options')->where('name', '=', 'news_count_in_listing')->first();
		$per_page = (int)$listing_count->options;
		/*$per_page = $this->pagination;*/

		$featured_count = DB::table('options')->where('name', '=', 'featured_news_count')->first();

		$latest_news = $this->NewsService->latest_news();
		$gossip_zone = $this->NewsService->gossip_zone();
		$news_list = $this->NewsService->getcat_list($id);
		$news_featured = $this->NewsService->get_featured((int)$featured_count->options);
		$classified_featured = $this->ClassifiedService->get_featured();

        $news_list = $news_list->paginate($per_page);
        $page_adds = $this->AddsService->news_listing($per_page);

        

		return view('front.news.lp' , compact('latest_news', 'gossip_zone', 'news_list', 'classified_featured', 'per_page', 'page_adds', 'news_featured'));
	}


	public function newsDP($id)
	{
		$news_one = $this->NewsService->get_one($id);
		$related_news = $this->NewsService->get_related($id);
		$classified_featured = $this->ClassifiedService->get_featured();
		$Comments_array = $this->CategoryService->Comments_array(1, $id);

		$page_adds = $this->AddsService->detail_adds();


		$category_array = $this->CategoryService->category_array(1);
		$category_names = "";

		$catid_array = json_decode($news_one->category_id, TRUE);
		$catid_array_count = 1;

		foreach($catid_array as $key => $value){

			if (array_key_exists((int)$value, $category_array)) {
				$category_names .=  $category_array[(int)$value];
				if(count($catid_array) !== $catid_array_count){
					$category_names .= " , ";
				}
				
			}
		 $catid_array_count++;		
		}

		

		return view('front.news.dp', compact('news_one', 'related_news', 'classified_featured', 'page_adds', 'category_names', 'Comments_array'));
	}




	public function videoLP()
	{
		$listing_count = DB::table('options')->where('name', '=', 'video_count_in_listing')->first();
		$per_page = (int)$listing_count->options;
		/*$per_page = $this->pagination;*/

		$latest_news = $this->NewsService->latest_news();
		$gossip_zone = $this->NewsService->gossip_zone();
		$video_list = $this->VideoService->get_list($per_page);
		$classified_featured = $this->ClassifiedService->get_featured();

		$page_adds = $this->AddsService->listing_adds($per_page);

		

		return view('front.video.lp' , compact('latest_news', 'gossip_zone', 'video_list', 'classified_featured', 'per_page', 'page_adds'));
	}



	public function videoCategoryLP($id)
	{
		$listing_count = DB::table('options')->where('name', '=', 'video_count_in_listing')->first();
		$per_page = (int)$listing_count->options;
		/*$per_page = $this->pagination;*/


		$latest_news = $this->NewsService->latest_news();
		$gossip_zone = $this->NewsService->gossip_zone();
		$video_list = $this->VideoService->getcat_list($id, $per_page);
		$classified_featured = $this->ClassifiedService->get_featured();

		$page_adds = $this->AddsService->listing_adds($per_page);

		

		return view('front.video.lp' , compact('latest_news', 'gossip_zone', 'video_list', 'classified_featured', 'per_page', 'page_adds'));
	}


	public function videoDP($id)
	{
		$video_one = $this->VideoService->get_one($id);
		$related_videos = $this->VideoService->get_related($id);
		$classified_featured = $this->ClassifiedService->get_featured();
		$Comments_array = $this->CategoryService->Comments_array(4, $id);

		$page_adds = $this->AddsService->detail_adds();


		$category_array = $this->CategoryService->category_array(4);
		$category_names = "";

		$catid_array = json_decode($video_one->category_id, TRUE);
		$catid_array_count = 1;

		foreach($catid_array as $key => $value){

			if (array_key_exists((int)$value, $category_array)) {
				$category_names .=  $category_array[(int)$value];
				if(count($catid_array) !== $catid_array_count){
					$category_names .= " , ";
				}
				
			}
		 $catid_array_count++;		
		}

		

		
		return view('front.video.dp', compact('video_one', 'related_videos', 'classified_featured', 'page_adds', 'category_names', 'Comments_array'));
	}


	public function classifiedLP()
	{
		$listing_count = DB::table('options')->where('name', '=', 'classified_count_in_listing')->first();
		$per_page = (int)$listing_count->options;
		/*$per_page = $this->pagination;*/

		$selected_category = 0;
		$selected_location = 0;
		$latest_news = $this->NewsService->latest_news();
		$gossip_zone = $this->NewsService->gossip_zone();
		$list_category = $this->ClassifiedService->list_category();
		$list_location = $this->ClassifiedService->list_location();
		$classified_list = $this->ClassifiedService->get_list();

		$page_adds = $this->AddsService->classifiedLP();

        $classified_list = $classified_list->paginate($per_page);
        $classified_list = $this->classifiedFilter($classified_list);

        

		return view('front.classified.lp' , compact('latest_news', 'gossip_zone', 'list_category', 'list_location', 'classified_list', 'selected_location', 'selected_category', 'per_page', 'page_adds'));
	}


	public function classifiedLocation($id)
	{
		$listing_count = DB::table('options')->where('name', '=', 'classified_count_in_listing')->first();
		$per_page = (int)$listing_count->options;
		/*$per_page = $this->pagination;*/

		$selected_category = 0;
		$selected_location = $id;
		$latest_news = $this->NewsService->latest_news();
		$gossip_zone = $this->NewsService->gossip_zone();
		$list_category = $this->ClassifiedService->list_category();
		$list_location = $this->ClassifiedService->list_location();
		$classified_list = $this->ClassifiedService->list_bylocation($id);

		$page_adds = $this->AddsService->classifiedLP();

        $classified_list = $classified_list->paginate($per_page);
        $classified_list = $this->classifiedFilter($classified_list);

        

		return view('front.classified.lp' , compact('latest_news', 'gossip_zone', 'list_category', 'list_location', 'classified_list', 'selected_location', 'selected_category', 'per_page', 'page_adds'));
	}


	public function classifiedCategory($id)
	{
		$listing_count = DB::table('options')->where('name', '=', 'classified_count_in_listing')->first();
		$per_page = (int)$listing_count->options;
		/*$per_page = $this->pagination;*/

		$selected_category = $id;
		$selected_location = 0;
		$latest_news = $this->NewsService->latest_news();
		$gossip_zone = $this->NewsService->gossip_zone();
		$list_category = $this->ClassifiedService->list_category();
		$list_location = $this->ClassifiedService->list_location();
		$classified_list = $this->ClassifiedService->list_bycategory($id);

		$page_adds = $this->AddsService->classifiedLP();

        $classified_list = $classified_list->paginate($per_page);
        $classified_list = $this->classifiedFilter($classified_list);


        

		return view('front.classified.lp' , compact('latest_news', 'gossip_zone', 'list_category', 'list_location', 'classified_list', 'selected_location', 'selected_category', 'per_page', 'page_adds'));
	}


	public function classifiedDP($slug)
	{
		$classified_one = $this->ClassifiedService->get_one($slug);
		$related_products = $this->ClassifiedService->related_products($slug);
		$classified_featured = $this->ClassifiedService->get_featured();
		$product_owner = DB::table('users')->where('id', '=', $classified_one->user_id)->first();

		$page_adds = $this->AddsService->classifiedDP();


		

		return view('front.classified.dp', compact('classified_one', 'related_products', 'classified_featured', 'product_owner', 'page_adds'));
	}


	public function informativePage($id)
	{
		$informativePage = $this->PagesService->get_one($id);
		$menu_data = $this->PagesService->get_menu($id);
		$news_by_category = $this->NewsService->news_by_category();

		$page_adds = $this->AddsService->informative();
		return view('front.informative.default', compact('informativePage', 'menu_data', 'news_by_category', 'page_adds'));
	}


	public function contactUsPage()
	{
		$options_result = DB::table('options')->get();

		$option_list = array();

		if($options_result){
			foreach($options_result  as $key => $value){

					$option_list[$value->name] = $value->options;
							
			}

		}
		
		return view('front.informative.contact_us', ['option_list' => $option_list]);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function homeSearch(Request $request)
	{
		$search_text = $request->input('search_text');
		$per_page = $this->pagination;

		$latest_news = $this->NewsService->latest_news();
		$gossip_zone = $this->NewsService->gossip_zone();
		$search_list = $this->ClassifiedService->homesearch_list($search_text);
		$classified_featured = $this->ClassifiedService->get_featured();


        $page_adds = $this->AddsService->news_listing($per_page);

        

		return view('front.search' , compact('latest_news', 'gossip_zone', 'search_list', 'classified_featured', 'per_page', 'page_adds', 'search_text'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function classifiedSearch(Request $request)
	{
		$listing_count = DB::table('options')->where('name', '=', 'classified_count_in_listing')->first();
		$per_page = (int)$listing_count->options;
		/*$per_page = $this->pagination;*/


		$selected_category = 0;
		$selected_location = 0;
		$search_text = $request->input('search_text');
		$latest_news = $this->NewsService->latest_news();
		$gossip_zone = $this->NewsService->gossip_zone();
		$list_category = $this->ClassifiedService->list_category();
		$list_location = $this->ClassifiedService->list_location();
		$classified_list = $this->ClassifiedService->search_list($search_text);

		$page_adds = $this->AddsService->classifiedLP();

        $classified_list = $classified_list->paginate($per_page);
        $classified_list = $this->classifiedFilter($classified_list);

        


		return view('front.classified.search', compact('latest_news', 'gossip_zone', 'list_category', 'list_location', 'classified_list', 'selected_location', 'selected_category', 'per_page', 'page_adds', 'search_text'));
	}


	public function classifiedFilter($classified_list)
	{
		$top_add_option_row =  DB::table('options')->where('name', '=', 'adds_top_add_per_click')->first();
		$top_add_option = $top_add_option_row->options;
		$featured_add_option_row =  DB::table('options')->where('name', '=', 'adds_featured_add_per_click')->first();
		$featured_add_option = $featured_add_option_row->options;


		$option_row =  DB::table('options')->where('name', '=', 'adds_duration_days')->first();
		$duration_type = array_map('trim', explode(',', $option_row->options));
		$duration_type_key = 1;
			$new_duration_type[0]= "Select One";
		foreach($duration_type  as $key => $duration){
			$new_duration_type[$duration_type_key]= $duration;
			$duration_type_key++;
		}

		$something = array();


		if($classified_list){

			foreach($classified_list  as $key => $classified){

				if($classified->featured === 1){
				$boost_row = DB::table('post_boosts')
							->where('classifieds_id','=' ,$classified->id)
							->where('status','=' , 1)
							->first();
					if($boost_row){
						$classified->boost_status = $boost_row->status;
						$boost_type = $boost_row->boost_type;
						$classified->boost_type = $boost_type;
						$budget = $boost_row->budget;
						if($boost_type === 1){
							$target_click_count = ceil($budget/$top_add_option);
						}else{
							$target_click_count = ceil($budget/$featured_add_option);
						}
						

						$current_count = Counter::show('classified_featured', $classified->id);
						
						$classified->duration  = $new_duration_type[$boost_row->duration];
						$expire_date = \Carbon\Carbon::createFromTimeStamp(strtotime($boost_row->created_at))->addDays($new_duration_type[$boost_row->duration]);
						
						$something[$classified->id]['target'] = $target_click_count;
						$something[$classified->id]['budget'] = $budget;
						$something[$classified->id]['boost_type'] = $boost_type;
						$something[$classified->id]['now'] =  \Carbon\Carbon::now();
						$something[$classified->id]['expire_date'] = $expire_date;
						$something[$classified->id]['current_count'] = $current_count;

						if(($current_count >= $target_click_count) || (\Carbon\Carbon::now() >= $expire_date)){
							$affectedRows = DB::table('post_boosts')->where('id', '=', $boost_row->id)->update(array('status' => 2));
							$classified->boost_status = 2;
						}
						

					}else{
						$classified->boost_status = 0;
					}
				}
							
			}


		}

		return $classified_list;
	}





	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
