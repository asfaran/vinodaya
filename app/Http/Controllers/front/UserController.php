<?php namespace App\Http\Controllers\front;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\AuthRequest;
use Illuminate\Http\Request;
use App\User;
use App\messages;
use Hash;
use URL;
use App\Http\Requests\PasswordRequestSet;

use Auth, Input, File, Mail, Session, Validator, DB,Excel,PDF,Config;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;

class UserController extends Controller {

use ResetsPasswords;

	public function __construct(User $user,Request $request, Guard $auth, PasswordBroker $passwords) {
        $this->user = $user;
        $this->request = $request;

        $this->auth = $auth;
        $this->passwords = $passwords;
    }

    

    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function login() {
        if(Auth::check()){
            if(Auth::user()->usertype == 3){
                return redirect()->intended('user');
            }
            return redirect()->intended('admin');
        }
        return view('front.user.login');
    }



    public function authenticate(AuthRequest $request)
    {
        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')], $request->has('remember')))
        {
            if(Auth::user()->role_id == 3){
                return redirect()->intended('admin/program');
            }
            return redirect()->intended('admin');
        }
        return redirect()->back()->withErrors(['email' => 'These credentials does not match.']);
    }

    public function logout() {
        if(Auth::check()) {
            Auth::logout();
        }
        return redirect()->route('user.login');
    }

    public function getRegister() {
        return view('auth.register');
    }


    function decryptIt( $q ) {
        $q = str_replace(array('-', '_'), array('+', '/'), $q);
        $cryptKey  = 'qJB0rGtIn5UB1xG03efyCp';
        $qDecoded      = rtrim( mcrypt_decrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), base64_decode( $q ), MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ), "\0");
        return( $qDecoded );
    }


    public function postPassword(Request $request){
        
        $rules = array(
            'current_password' => 'required',
            'password' => 'required|min:6|confirmed',
        );

        $v = Validator::make(Input::all(), $rules);

        /*'category' => 'required',*/
        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }else{

            $user = $this->user->where('id','=', Auth::user()->id)->first();

            if(Hash::check($request->input('current_password'), $user->password)){ 

                 $user->password = bcrypt($request->input('password'));
                 $user->save();

                $data['full_name'] = $user->f_name . ' ' . $user->l_name;
                $data['email']     = $user->email;
                $data['password']  = $request->input('password');
                $data['from']      = Config::get('mail.from.address');
                $data['from_name'] = Config::get('mail.from.name');

                Mail::send('emails.password_changed', ['data' => $data], function ($m) use ($data) {
                    $m->from($data['from'], $data['from_name']);
                    $m->to(  $data['email'],  $data['full_name'])->subject('Password changed | Vinodaya - Team.');
                });

                return redirect()->route('user.myads')->with('flash_success','Your Password successfully set.');

            }else{
                return back()->with('flash_message','The specified password does not match the database password');


            }


        }
        
    }



    public function upgrade()
    {
        $user_data = Auth::user();


        return view('front.user.upgrade',['user_data' => $user_data]);
    }

    public function postUpgrade(Request $request)
    {
        $messsages = array(
            'company_selection.required'=>'You Should Agree to Payment Terms!',
        );

        $rules = array(
            'id' => 'integer',
            'title' => 'required',
            'adds_type' => 'required|integer',
            'duration_type' => 'required|integer',
            'duration' => 'required|integer',
            'thumb_real' => 'mimes:jpg,jpeg,gif,png',
            'extra' => 'required',
            'company_selection' =>'required',
        );

        $v = Validator::make(Input::all(), $rules, $messsages);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }else{

            $inputs = $this->request->all();

            $check_mail = User::where('email',$inputs['email'])->first();

            if($check_mail){

                return redirect()->back()->withInput()->withErrors(['email' => 'This Email Address Already in use Try another one.']);

            }else{

                $user = new User();
                $user->f_name = $inputs['f_name'];
                $user->l_name = $inputs['l_name'];
                $user->email = $inputs['email'];
                $user->location = $inputs['location'];
                $user->mobile = $inputs['mobile'];
                $user->password = bcrypt($inputs['password']);
                $user->usertype = 3;

                if($request->input('company_selection') != null){
                    $user->company_name = $inputs['company_name'];
                    $user->company_addr = $inputs['company_addr'];
                    $user->company_contact = $inputs['company_contact'];  
                }

                $uniqid = uniqid();
                $username_slug = $inputs['f_name'] . '_' . $inputs['l_name'];
                if ($this->userslug_find($username_slug)) {
                    $username_slug = $inputs['l_name'] . $uniqid;
                }

                $user->name = $inputs['f_name'] . ' ' . $inputs['l_name'];
                $user->username = $inputs['email'];
                $user->username_slug = $username_slug;
                $user->save();

                $data['full_name'] = $user->first_name . ' ' . $user->last_name;
                $data['email'] = $user->email;
                $data['user'] = $user;
                $data['password'] = $inputs['password'];
                $data['from'] = Config::get('mail.from.address');
                $data['from_name'] = Config::get('mail.from.name');

                Mail::send('emails.welcome', ['data' => $data], function ($m) use ($data) {
                    $m->from($data['from'], $data['from_name']);
                    $m->to(  $data['email'],  $data['full_name'])->subject('Welcome to Vinodaya');
                });

                Auth::attempt(['email' => $inputs['email'], 'password' => $inputs['password']], 0);



                if($request->input('company_selection') != null){
                ////////////////////////////////



                    $query = array();
                    $query['notify_url'] = url('/user');
                    $query['cmd'] = '_xclick-subscriptions';
                    $query['item_name'] = 'company_registration';
                    $query['business'] = 'shaqir-buyer@extremewebdesigners.com';
                    $query['item_number'] = '1';
                    $query['userid'] = $user->email;
                    $query['a3'] = 3.44;
                    $query['p3'] = 1;
                    $query['t3'] = 'M';
                    $query['src'] = 1;
                    $query['srt'] = 12;
                    $query['disp_tot'] = 'Y';
                    $query['_token'] = csrf_token();
                    $query['amount'] = 3.44;
                    $query['cpp_header_image'] =  asset('/assets/img/logo.png');
                    $query['no_shipping'] = 1;
                    $query['currency_code'] = 'USD';
                    $query['handling'] = '0';
                    $query['rm'] = 2;
                    $query['cancel_return'] = url('/user/paypal_cancel');
                    $query['return'] = url('/user/paypal_return');
                    // Prepare query string
                    $query_string = http_build_query($query);

                    return redirect('https://www.sandbox.paypal.com/cgi-bin/webscr?'. $query_string);
               /* $curl = curl_init();
 
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_URL, '');
                curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
                    'USER' => 's',
                    'PWD' => 'F2TLQXLXWXU2E6J9',
                    'SIGNATURE' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31AE2gqN5Jwma2ZZFfE-3Jb.iIhBKC',
                 
                    'METHOD' => 'SetExpressCheckout',
                    'VERSION' => '108',
                    'LOCALECODE' => 'pt_BR',
                 
                    'PAYMENTREQUEST_0_AMT' => 100,
                    'PAYMENTREQUEST_0_CURRENCYCODE' => 'BRL',
                    'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',
                    'PAYMENTREQUEST_0_ITEMAMT' => 100,
                 
                    'L_PAYMENTREQUEST_0_NAME0' => 'Exemplo',
                    'L_PAYMENTREQUEST_0_DESC0' => 'Assinatura de exemplo',
                    'L_PAYMENTREQUEST_0_QTY0' => 1,
                    'L_PAYMENTREQUEST_0_AMT0' => 100,

                    'L_BILLINGTYPE0' => 'RecurringPayments',
                    'L_BILLINGAGREEMENTDESCRIPTION0' => 'Exemplo',
                 
                    'CANCELURL' => '',
                    'RETURNURL' => 'http://vinodaya.lk/user'
                )));
                 
                $response =    curl_exec($curl);
                dd($response);
                 
                curl_close($curl);
                */
                //////////////////////////////////
            }else{

                if (Auth::attempt(['email' => $inputs['email'], 'password' => $inputs['password']], 0))
                {
                    return redirect()->intended('user/postads');
                }
                return redirect()->intended('user/settings');
                //return redirect()->route('auth.register')->with('success','Registration has successfully completed, refer email to proceed');

            }

            }   

        }
    }



	



	public function register()
	{
		return view('front.user.register');
	}


	public function postLogin(AuthRequest $request)
    {
        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')], true))
        {
            if(Auth::user()->usertype == 3){
                return redirect()->intended('user/myads');
            }
            return redirect()->intended('admin');
        }
        return redirect()->back()->withErrors(['email' => 'These credentials does not match.']);
    }


	public function postRegister(Request $request){
        $v = Validator::make($request->all(), [
            'f_name' => 'required',
            'l_name' => 'required',
            'location' => 'required',
            'email' => 'required|email',
            'mobile' => 'required|numeric',
            'password' => 'required|min:6|confirmed'

        ]);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }else{

            $inputs = $this->request->all();

            $check_mail = User::where('email',$inputs['email'])->first();

            if($check_mail){

                return redirect()->back()->withInput()->withErrors(['email' => 'This Email Address Already in use Try another one.']);

            }else{

                $user = new User();
                $user->f_name = $inputs['f_name'];
                $user->l_name = $inputs['l_name'];
                $user->email = $inputs['email'];
                $user->location = $inputs['location'];
                $user->mobile = $inputs['mobile'];
                $user->password = bcrypt($inputs['password']);
                $user->usertype = 3;

                if($request->input('company_selection') != null){
                    $user->company_name = $inputs['company_name'];
                    $user->company_addr = $inputs['company_addr'];
                    $user->company_contact = $inputs['company_contact'];  
                }

                $uniqid = uniqid();
                $username_slug = $inputs['f_name'] . '_' . $inputs['l_name'];
                if ($this->userslug_find($username_slug)) {
                    $username_slug = $inputs['l_name'] . $uniqid;
                }

                $user->name = $inputs['f_name'] . ' ' . $inputs['l_name'];
                $user->username = $inputs['email'];
                $user->username_slug = $username_slug;
                $user->save();

                $data['full_name'] = $user->first_name . ' ' . $user->last_name;
                $data['email'] =  $inputs['email'];
                $data['user'] = $user;
                $data['password'] = $inputs['password'];
                $data['from'] = Config::get('mail.from.address');
                $data['from_name'] = Config::get('mail.from.name');

                Mail::send('emails.welcome', ['data' => $data], function ($m) use ($data) {
                    $m->from($data['from'], $data['from_name']);
                    $m->to(  $data['email'],  $data['full_name'])->subject('Welcome to Vinodaya');
                });

                Auth::attempt(['email' => $inputs['email'], 'password' => $inputs['password']], 0);



                if($request->input('company_selection') != null){
                ////////////////////////////////



                    $query = array();
                    $query['notify_url'] = url('/user');
                    $query['cmd'] = '_xclick-subscriptions';
                    $query['item_name'] = 'company_registration';
                    $query['business'] = 'shaqir-buyer@extremewebdesigners.com';
                    $query['item_number'] = '1';
                    $query['userid'] = $user->email;
                    $query['a3'] = 3.44;
                    $query['p3'] = 1;
                    $query['t3'] = 'M';
                    $query['src'] = 1;
                    $query['srt'] = 12;
                    $query['disp_tot'] = 'Y';
                    $query['_token'] = csrf_token();
                    $query['amount'] = 3.44;
                    $query['cpp_header_image'] =  asset('/assets/img/logo.png');
                    $query['no_shipping'] = 1;
                    $query['currency_code'] = 'USD';
                    $query['handling'] = '0';
                    $query['rm'] = 2;
                    $query['cancel_return'] = url('/user/paypal_cancel');
                    $query['return'] = url('/user/paypal_return');
                    // Prepare query string
                    $query_string = http_build_query($query);

                    return redirect('https://www.sandbox.paypal.com/cgi-bin/webscr?'. $query_string);
               /* $curl = curl_init();
 
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_URL, '');
                curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
                    'USER' => 's',
                    'PWD' => 'F2TLQXLXWXU2E6J9',
                    'SIGNATURE' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31AE2gqN5Jwma2ZZFfE-3Jb.iIhBKC',
                 
                    'METHOD' => 'SetExpressCheckout',
                    'VERSION' => '108',
                    'LOCALECODE' => 'pt_BR',
                 
                    'PAYMENTREQUEST_0_AMT' => 100,
                    'PAYMENTREQUEST_0_CURRENCYCODE' => 'BRL',
                    'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',
                    'PAYMENTREQUEST_0_ITEMAMT' => 100,
                 
                    'L_PAYMENTREQUEST_0_NAME0' => 'Exemplo',
                    'L_PAYMENTREQUEST_0_DESC0' => 'Assinatura de exemplo',
                    'L_PAYMENTREQUEST_0_QTY0' => 1,
                    'L_PAYMENTREQUEST_0_AMT0' => 100,

                    'L_BILLINGTYPE0' => 'RecurringPayments',
                    'L_BILLINGAGREEMENTDESCRIPTION0' => 'Exemplo',
                 
                    'CANCELURL' => '',
                    'RETURNURL' => 'http://vinodaya.lk/user'
                )));
                 
                $response =    curl_exec($curl);
                dd($response);
                 
                curl_close($curl);
*/
                //////////////////////////////////
            }else{

                if (Auth::attempt(['email' => $inputs['email'], 'password' => $inputs['password']], 0))
                {
                    return redirect()->intended('user/postads');
                }
                return redirect()->intended('user/settings');
                //return redirect()->route('auth.register')->with('success','Registration has successfully completed, refer email to proceed');

            }

            }   

        }


        
    }


    public function postMessages(Request $request)
    {
        $v = Validator::make($request->all(), [
            'id' => 'integer',
            'name' => 'required',
            'subject' => 'required',
            'mobile' => 'required|numeric',
            'email' => 'required|email',
            'message' => 'required',
            

        ]);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }else{

            $user_id = (isset(Auth::user()->id))? Auth::user()->id : 0;

            

            if($request->input('id')){

                $message = messages::find($request->input('id'));
                $message->name = $request->input('name');
                $message->subject = $request->input('subject');
                $message->mobile = $request->input('mobile');
                $message->email = $request->input('email');
                $message->message = $request->input('message');
                $message->user_id = $user_id;
                $message->save();


            }else{

                /*$input = $request->all();
                
                parse_str($request->input('formdata'), $data_array);*/

                $message = new messages();
                $message->name = $request->input('name');
                $message->subject = $request->input('subject');
                $message->mobile = $request->input('mobile');
                $message->email = $request->input('email');
                $message->message = $request->input('message');
                $message->user_id = $user_id;
                $message->save();
                

            }
            echo "<script> console.log('".json_encode($message)."'); </script>";

            if($message){
                return redirect('contact-us')->with('flash_success', 'Message send successfully');

            }else{
                return redirect('contact-us')->with('flash_success', 'Message Not send');
            }

            return redirect('contact-us');

        
        }
    }


    public function forgot()
    {
        return view('front.user.forgot');
    }


    public function reset()
    {
        return view('front.user.reset');
    }


    public function settings()
    {
        return view('front.user.settings');
    }



    public function forgotPassword(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);

        $response = $this->passwords->sendResetLink($request->only('email'), function($m)
        {
            $m->subject($this->getEmailSubject());
        });

        switch ($response)
        {
            case PasswordBroker::RESET_LINK_SENT:
                return redirect()->back()->with('status', trans($response));

            case PasswordBroker::INVALID_USER:
                return redirect()->back()->withErrors(['email' => trans($response)]);
        }
    }


    /**
     * Get the e-mail subject line to be used for the reset link email.
     *
     * @return string
     */
    protected function getEmailSubject()
    {
        return isset($this->subject) ? $this->subject : 'Your Password Reset Link';
    }

    /**
     * Display the password reset view for the given token.
     *
     * @param  string  $token
     * @return Response
     */
    public function getReset($token = null)
    {
        if (is_null($token))
        {
            throw new NotFoundHttpException;
        }

        return view('auth.reset')->with('token', $token);
    }

    /**
     * Reset the given user's password.
     *
     * @param  Request  $request
     * @return Response
     */
    public function postReset(Request $request)
    {
        $this->validate($request, [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed',
        ]);

        $credentials = $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );

        $response = $this->passwords->reset($credentials, function($user, $password)
        {
            $user->password = bcrypt($password);

            $user->save();

            $this->auth->login($user);
        });

        switch ($response)
        {
            case PasswordBroker::PASSWORD_RESET:
                return redirect($this->redirectPath());

            default:
                return redirect()->back()
                            ->withInput($request->only('email'))
                            ->withErrors(['email' => trans($response)]);
        }
    }



    function userslug_find($number) {
	    // query the database and return a boolean
	    // for instance, it might look like this in Laravel
	    return $this->user->where('username_slug','=',$number)->exists();
	}









	


	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
