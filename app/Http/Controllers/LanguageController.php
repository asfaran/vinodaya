<?php namespace App\Http\Controllers;

use App\language;
use DB;
use Session;
use DateTime;
Use Validator;
use Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;

class LanguageController extends Controller {


	public function __construct()
    {
        $this->middleware('auth');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$language_result = DB::table('languages')->get();

		$language_list = array();

		if($language_result){
			foreach($language_result as $value) {

			$language_list[$value->language][$value->option] = $value->value;		
		}
		}


		return view('admin.language.list', ['language_list' => $language_list]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$language_data =$request->input('language');

		foreach($language_data as $key => $value) {

			foreach($value as $key_last => $value_last) {

				$language_one = DB::table('languages')
					->where('language', '=', $key)
					->where('option', '=', $key_last)
					->first();
				if($language_one){
					$lng_row = language::where('language', $key)->where('option', $key_last)->update(array('value' => $value_last));
				}else{

					$new_lang = new language();
					$new_lang->language = $key;
					$new_lang->option = $key_last;
					$new_lang->value = $value_last;
					$new_lang->save();


				}

			}
			
		}

		return redirect('/admin/settings/language/list')->with('flash_success', 'Record Updated successfully!.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
