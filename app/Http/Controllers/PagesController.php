<?php namespace App\Http\Controllers;

use App\pages;
use App\tags;
use App\options;
use DB;
use Session;
use DateTime;
Use Validator;
use Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

use Illuminate\Http\Request;

class PagesController extends Controller {


	public function __construct()
    {
        $this->middleware('auth');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$page_list = DB::table('pages')->get();


		return view('admin.page.list', ['page_list' => $page_list]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('admin.page.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{

		$status_array = array('0'=>'Pending Review', '1'=>'Draft', '2'=>'Published' );

		$v = Validator::make($request->all(), [
			'id' => 'integer',
			'title' => 'required',
			'body' => 'required'
			

		]);

		/*'category' => 'required',*/
		if ($v->fails())
		{
			return redirect()->back()->withInput()->withErrors($v->errors());
		}else{

			$tags_array = array_filter(explode(",",$request->input('tags')));
			if($tags_array){
				foreach($tags_array  as $key=>$tag){
					$line_row = DB::table('tags')->where('title','=' ,$tag)->first();
					if(!$line_row || empty($line_row) || $line_row == null){

						$slug = preg_replace('/\s+/', '-', strtolower($tag));

						$new_tags = new tags();
						$new_tags->title = $tag;
						$new_tags->slug = $slug;
						$new_tags->save();

					}
				}
			}

			if($request->input('id')){


				$title_lowercase = strtolower($request->input('post_slug'));
				$slug = preg_replace('/\s+/', '-', $title_lowercase);

				$line_row = DB::table('pages')->where('slug','=' ,$slug)->first();

				if($line_row){
					for ($i = 1; $i <= 100; ++$i) {
						$search_slug = $slug."-".$i;
						$sub_row = DB::table('pages')->where('slug','=' ,$search_slug)->first();
						if($sub_row){
							continue;
						}else{

							$slug = $search_slug;
							 break;
						}

					}
				}


				$pages = new pages();
				$pages->exists = true;
				$pages->id = (int)$request->input('id');
				$pages->title = $request->input('title');
				/*$pages->user_id = Auth::user()->id;*/
				$pages->tags = $request->input('tags');
				$pages->body = $request->input('body');
				$pages->save();

				if($pages){
					

					return redirect('/admin/general/page/list')->with('flash_success', 'Record Updated successfully!.');

				}else{

					return redirect('/admin/general/page/list')->with('flash_success', 'Record Not Updated!.');

				}
				

				



			}else{

				
				$title_lowercase = strtolower($request->input('title'));
				$slug = preg_replace('/\s+/', '-', $title_lowercase);

				$line_row = DB::table('pages')->where('slug','=' ,$slug)->first();

				if($line_row){
					for ($i = 1; $i <= 100; ++$i) {
						$search_slug = $slug."-".$i;
						$sub_row = DB::table('pages')->where('slug','=' ,$search_slug)->first();
						if($sub_row){
							continue;
						}else{

							$slug = $search_slug;
							 break;
						}

					}
				}





				$pages = new pages();
				$pages->title = $request->input('title');
				/*$pages->user_id = Auth::user()->id;*/
				$pages->tags = $request->input('tags');
				$pages->body = $request->input('body');
				$pages->save();

				return redirect('/admin/general/page/list')->with('flash_success', 'Record Inserted successfully!.');

			}

		
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$category_list = DB::table('categories')
		->select('categories.id','categories.title','parent.id As parent_id', 'parent.title AS parent_title')
		->leftJoin('categories AS parent','categories.parentid','=','parent.id')
		->where('categories.type','=',1)
		->get();


		$page_one = DB::table('pages')
					->where('id', '=', $id)
					->first();

		return view('admin.page.update',['page_one' => $page_one]);
	}

	public function contact()
	{
		$options_result = DB::table('options')->get();

		$option_list = array();

		if($options_result){
			foreach($options_result  as $key => $value){

					$option_list[$value->name] = $value->options;
							
			}

		}
		return view('admin.page.contact_details', ['option_list' => $option_list]);
	}



	public function contactStore(Request $request)
	{
		$data  = Input::all();

		foreach($data as $key => $value) {
			if($key  != "_token"){

			$option_one = DB::table('options')
					->where('name', '=', $key)
					->first();
			if($option_one){
				$option_row =  DB::table('options')->where('name', $key)->update(array('options' => $value));
			}else{

				$new_option = new options();
				$new_option->name = $key;
				$new_option->options =  $value;
				$new_option->save();


			}



			}			
			
		}

		return redirect('/admin/general/page/contact')->with('flash_success', 'Record Updated successfully!.');
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
