<?php namespace App\Http\Controllers;

use App\category;
use DB;
use Session;
use DateTime;
Use Validator;
use Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;


class CategoryController extends Controller {

	public function __construct()
    {
        $this->middleware('auth');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$category_list = DB::table('categories')
		->select('categories.id', 'categories.access', 'categories.title','parent.id As parent_id', 'parent.title AS parent_title')
		->leftJoin('categories AS parent','categories.parentid','=','parent.id')
		->get();


		$category_full = DB::table('categories')
		->select('categories.id', 'categories.access', 'categories.title','parent.id As parent_id', 'parent.title AS parent_title')
		->leftJoin('categories AS parent','categories.parentid','=','parent.id')
		->where('categories.type','=',1)
		->get();



		/*$category_list = DB::table('categories')->select('id', 'title')->get();*/

		if($category_list){
			$category_array[0] ="[Select a Category]";
			foreach($category_list  as $category){
				$category_array[$category->id]= $category->title;

			}
		}else{
			$category_array[0] ="[No Category For Select]";
		}


		return view('admin.category.list', ['category_list' => $category_array, 'category_full'=> $category_full]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('admin.category.create');
	}




	public function filter(Request $request)
    {

        /*if (Request::isMethod('POST')) {*/

            $category_type = Input::get('category_type');


            $category_list = DB::table('categories')
            	->select('categories.id', 'categories.access','categories.title','parent.id As parent_id', 'parent.title AS parent_title')
				->leftJoin('categories AS parent','categories.parentid','=','parent.id')
				->where('categories.type','=',$category_type)
				->get();
            

            return view('admin.category.filter', ['category_list' => $category_list]);
        /*}*/


    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$v = Validator::make($request->all(), [
			'id' => 'integer',
			'title' => 'required',
			'slug' => 'required',
			'type' => 'required|integer',
			'parentid' => 'integer',

		]);

		if ($v->fails())
		{
			return redirect()->back()->withInput()->withErrors($v->errors());
		}else{

			$quciklink_value = ($request->input('quciklink') != null)? 1 : 0;

			if($request->input('id')){

				$category = category::find($request->input('id'));
				$category->title = $request->input('title');
				$category->slug = $request->input('slug');
				$category->type = $request->input('type');
				$category->display = $quciklink_value;
				$category->parentid = $request->input('parentid');
				$category->description = $request->input('description');
				$category->save();


				return redirect('/admin/general/category/list')->with('flash_success', 'Record Updated successfully!.');



			}else{

				$new_category = new category();
				$new_category->title = $request->input('title');
				$new_category->slug = $request->input('slug');
				$new_category->type = $request->input('type');
				$new_category->display = $quciklink_value;
				$new_category->parentid = $request->input('parentid');
				$new_category->description = $request->input('description');;
				$new_category->save();

				return redirect('/admin/general/category/list')->with('flash_success', 'Record Inserted successfully!.');

			}

		
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$category_list = DB::table('categories')
		->select('categories.id', 'categories.access','categories.title','parent.id As parent_id', 'parent.title AS parent_title')
		->leftJoin('categories AS parent','categories.parentid','=','parent.id')
		->get();

		$category_full = DB::table('categories')
		->select('categories.id', 'categories.access','categories.title','parent.id As parent_id', 'parent.title AS parent_title')
		->leftJoin('categories AS parent','categories.parentid','=','parent.id')
		->where('categories.type','=',1)
		->get();


		$category_one = DB::table('categories')
					->where('id', '=', $id)
					->first();

		if($category_list){
			$category_array[0] ="[Select a Category]";
			foreach($category_list  as $category){
				$category_array[$category->id]= $category->title;

			}
		}else{
			$category_array[0] ="[No Category For Select]";
		}
	

		return view('admin.category.update', ['category_list' => $category_array, 'category' => $category_one, 'category_full'=> $category_full]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id, Request $request)
	{
		
		if ($request->input('submit_confirm')) {
            try {
                $affectedRows = DB::table('categories')->where('id', '=', $id)->delete();

				if($affectedRows){
					
					/* action log insertion */
					
					/* action log insertion */
					return redirect('/admin/general/category/list')->with('flash_success', ' Category Deleted Successfully!.');
				}else{
					return redirect('/admin/general/category/list')->with('flash_message', 'Category not deleted,Try again...');
				}
            }
            catch (\Exception $ex) {
                Session::flash('flash_message', $ex->getMessage());
                Session::flash('flash_type', 'error');
            }
        }

        $data = [
            'view_data' => [
                'page_parent' => 'general',
                'current_page' => 'category_list',
                'page_title' => 'Delete Category',
                'section_title' => 'Confirm Delete Selection',
                'confirm_title' => 'Are you sure you want to delete the selected category?',
                'confirm_message' => 'This action can not be undone. Please confirm.',
                'confirm_button' => 'Delete',
                'cancel_url' => url('/admin/general/category/list'),
            ],
        ];
        return view('layout.confirm', $data);
	}

}
