<?php namespace App\Http\Controllers\Auth;



use App\users;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Session;
use DateTime;
Use Validator;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;

class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;

		$this->middleware('guest',  ['except' => ['index','create','store','destroy','edit', 'getLogout']]);
	}


	public function index()
	{
		$users_list = DB::table('users')->get();

		return view('admin.users.list', ['users_list' => $users_list]);


	}

	public function create()
	{

		return view('admin.users.create');
	}



	public function store(Request $request)
	{

		return view('admin.users.list');
	}


	public function edit($id)
	{
		$users_one = DB::table('users')
					->where('id', '=', $id)
					->first();


		return view('admin.users.update', ['users_one' => $users_one]);
	}


	public function destroy($id, Request $request)
	{
		
		if ($request->input('submit_confirm')) {
            try {
                $affectedRows = DB::table('users')->where('id', '=', $id)->delete();

				if($affectedRows){
					
					/* action log insertion */
					
					/* action log insertion */
					return redirect('/admin/users/list')->with('flash_success', 'Selected User Deleted Successfully!.');
				}else{
					return redirect('/admin/users/list')->with('flash_message', 'Selected User not Removed,Try again...');
				}
            }
            catch (\Exception $ex) {
                Session::flash('flash_message', $ex->getMessage());
                Session::flash('flash_type', 'error');
            }
        }

        $data = [
            'view_data' => [
                'page_parent' => 'adminuser',
                'current_page' => 'users_list',
                'page_title' => 'Delete a User',
                'section_title' => 'Confirm Delete Selection',
                'confirm_title' => 'Are you sure you want to delete the selected User?',
                'confirm_message' => 'This action can not be undone. Please confirm.',
                'confirm_button' => 'Delete',
                'cancel_url' => url('/admin/users/list/'),
            ],
        ];
        return view('layout.confirm', $data);
	}

}
