<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class adds_type extends Model {

	use SoftDeletes;

    protected $table = 'adds_types';

    protected $fillable = ['id', 'title', 'position_name', 'price_type', 'price', 'thumb', 'width','height', 'extra', 'status','updated_at', 'featured_at', 'deleted_at'];

    protected $dates = ['featured_at', 'updated_at','deleted_at'];

    protected $softDelete = true;

	//

}
