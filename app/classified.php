<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class classified extends Model {

	use SoftDeletes;

    protected $table = 'classifieds';

    protected $fillable = ['id','slug', 'title', 'user_id', 'tags','parentid', 'category', 'location_province', 'location_city','location_town', 'location_building', 'thumb', 'extra_fields', 'status', 'featured', 'visibility', 'publish', 'description', 'approve', 'show_in_homepage', 'modified_by', 'display', 'featured_at', 'published_at','deleted_at', 'updated_at'];

    protected $dates = ['published_at','deleted_at', 'updated_at'];

    protected $softDelete = true;


}
