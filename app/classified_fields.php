<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class classified_fields extends Model {

	use SoftDeletes;

    protected $table = 'classified_fields';

    protected $fillable = ['id','category', 'parentid', 'fields', 'featured_at', 'deleted_at', 'updated_at'];

    protected $dates = ['updated_at','deleted_at'];

    protected $softDelete = true;

}
