<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class location extends Model {

	protected $table = 'locations';

    protected $fillable = ['title', 'slug', 'type', 'parentid', 'display', 'icon', 'access', 'listorder', 'modified_by', 'last_modified'];



    public function scopeByType($query, $type)
    {
        return $query->where("type", $type);
    }

}
