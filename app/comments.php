<?php namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class comments extends Model {

	use SoftDeletes;

    protected $table = 'comments';

    protected $fillable = ['id','post_type', 'post_id', 'author', 'author_email', 'author_url', 'author_IP','parent', 'user_id', 'approved', 'content', 'featured_at', 'published_at', 'deleted_at', 'updated_at'];

    protected $dates = ['published_at','deleted_at'];

    protected $softDelete = true;

}
