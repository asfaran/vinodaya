<?php namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class images extends Model {

	use SoftDeletes;

    protected $table = 'images';

    protected $fillable = ['id','slug', 'title', 'body', 'tags', 'user_id', 'category_id', 'type','ordertype', 'thumb', 'excerpt', 'status', 'featured', 'visibility', 'publish', 'approve', 'show_in_homepage', 'published_at', 'featured_at', 'deleted_at'];

    protected $dates = ['published_at','deleted_at'];

    protected $softDelete = true;

}
