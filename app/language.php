<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class language extends Model {

	use SoftDeletes;

	protected $table = 'languages';

    protected $fillable = ['language', 'option', 'value', 'modified_by', 'last_modified'];

    protected $dates = ['modified_by', 'last_modified'];

    protected $softDelete = true;

}
