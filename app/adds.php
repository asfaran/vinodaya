<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class adds extends Model {

	use SoftDeletes;

    protected $table = 'adds';

    protected $fillable = ['id', 'title', 'user_id', 'adds_type', 'duration_type', 'duration', 'thumb_real','thumb_resize', 'extra', 'status', 'visibility', 'approve', 'updated_at', 'featured_at', 'deleted_at'];

    protected $dates = ['featured_at', 'updated_at','deleted_at'];

    protected $softDelete = true;


}
