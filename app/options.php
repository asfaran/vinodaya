<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class options extends Model {

	use SoftDeletes;

    protected $table = 'options';

    protected $fillable = ['id','name', 'options', 'autoload', 'deleted_at', 'updated_at'];

    protected $dates = [ 'updated_at', 'deleted_at'];

    protected $softDelete = true;

}
