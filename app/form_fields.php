<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class form_fields extends Model {

	use SoftDeletes;

    protected $table = 'form_fields';


    protected $fillable = ['id','title', 'label', 'slug','frontend_type', 'backend_type', 'parentid', 'featured_at', 'listorder', 'icon', 'content', 'deleted_at', 'updated_at'];

    protected $dates = ['updated_at','deleted_at'];

    protected $softDelete = true;

}
