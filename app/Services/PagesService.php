<?php
namespace App\Services;

use Auth;
use Crypt;
use DB;


class PagesService
{
    /**
    */

    public function __construct()
    {
    }



    /**
     * @return bool
     */
    public function get_one($id)
    {
        $get_onepage = DB::table('pages')->where('id', '=', $id)->first();
        
        return $get_onepage;
    }


    public function get_menu($id)
    {
        $get_onepage = DB::table('menus')->where('type', '=', 3)->where('content', '=', $id)->first();
        
        return $get_onepage;
    }


    
}

