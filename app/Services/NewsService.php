<?php
namespace App\Services;

use Auth;
use Crypt;
use DB;


class NewsService
{
    


    /**
    */

    public function __construct()
    {
    }


    /**
     * @return bool
     */
    public function latest_news()
    {
    	$latest_news_count_sidebar = DB::table('options')->where('name', '=', 'latest_news_count_sidebar')->first();

    	$latest_news_list = DB::table('news')->orderBy('id', 'desc')->take((int)$latest_news_count_sidebar->options)->get();
        
        return $latest_news_list;
    }


    public function front_latest()
    {
    	$latest_news_count_front = DB::table('options')->where('name', '=', 'news_count_in_front')->first();

    	$latest_news_list = DB::table('news')->orderBy('id', 'desc')->take((int)$latest_news_count_front->options)->get();
        
        return $latest_news_list;
    }



    public function news_by_category()
    {
        $category_list = DB::table('categories')->where('type','=',1)->get();
        $category_array = array();

        if($category_list){
            foreach($category_list  as $category){
                $category_array[$category->id]['id']= $category->id;
                $category_array[$category->id]['title']= $category->title;
                $category_array[$category->id]['slug']= $category->slug;
                $category_array[$category->id]['news_list']= array();

            }
        }

        $news_list = DB::table('news')->orderBy('id', 'desc')->get();

        foreach($news_list  as $news){
            if($news->category_id != null && !empty($news->category_id)){
                foreach(json_decode($news->category_id, TRUE)  as $key => $value){
                    if(isset($category_array[$value])){
                      $category_array[$value]['news_list'][] = $news;   
                    }
                           
                }
            }


        }

        /*$category_list = DB::table('categories')
        ->select('categories.id','categories.title','parent.id As parent_id', 'parent.title AS parent_title')
        ->leftJoin('categories AS parent','categories.parentid','=','parent.id')
        ->where('categories.type','=',1)
        ->get();


        $latest_news_count_front = DB::table('options')->where('name', '=', 'news_count_in_front')->first();*/

       
        
        return $category_array;
    }



    public function gossip_zone()
    {
    	$news_category_list_count = DB::table('options')->where('name', '=', 'news_category_list_count')->first();

    	$gossip_row = DB::table('categories')->where('slug', '=', 'gossip-news')->first();

        $gossip_news_list = DB::table('news')
                    ->leftJoin('comments', 'comments.post_id', '=', 'news.id')
                    ->select(array('news.*' , DB::raw('COUNT(comments.id) as comments_count')))
                    ->where('news.category_id','like', "%$gossip_row->id%")
                    ->where('comments.approved','=', 1)
                    ->orderBy('news.id', 'desc')
                    ->groupBy('news.id')
                    ->take((int)$news_category_list_count->options)
                    ->get();
        
        return $gossip_news_list ;
    }


    public function get_list()
    {
        $listing_count = DB::table('options')->where('name', '=', 'news_count_in_listing')->first();
        $news_list = DB::table('news')->orderBy('id', 'desc')->take((int)$listing_count->options);
        
        return $news_list;
    }


    public function get_featured($per_page)
    {
        $featured_news_count = DB::table('options')->where('name', '=', 'featured_news_count')->first();
        $news_list = DB::table('news')->where('featured', '=', 1)->orderBy('id', 'desc')->take((int)$featured_news_count->options)->get();
        
        return $news_list;
    }


    public function getcat_list($id)
    {
        $listing_count = DB::table('options')->where('name', '=', 'news_count_in_listing')->first();
        $news_list = DB::table('news')->where('category_id','like', "%$id%")->orderBy('id', 'desc')->take((int)$listing_count->options);
        
        return $news_list;
    }


    public function get_one($id)
    {
        $news_one = DB::table('news')
                    ->where('id', '=', $id)
                    ->first();
        
        return $news_one;
    }


    public function get_related($id)
    {
        $news_one = DB::table('news')
                    ->where('id', '=', $id)
                    ->first();

        $related_list = DB::table('news')->where('category_id','like', "%$news_one->category_id%")->orderBy('id', 'desc')->take(4)->get();
        
        return $related_list;
    }


    
}

