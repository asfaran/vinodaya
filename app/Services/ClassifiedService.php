<?php
namespace App\Services;

use Auth;
use Crypt;
use DB;
use Counter;


class ClassifiedService
{
    


    /**
    */

    public function __construct()
    {
         $this->pagination = 4;
    }


    /**
     * @return bool
     */
    public function list_category()
    {
    	$category_list = DB::table('categories')
        ->select('categories.id','categories.title', DB::raw('COUNT(classifieds.id) as post_count'))
        ->leftJoin('classifieds','classifieds.parentid','=','categories.id')
        ->where('categories.type','=',5)
        ->where('classifieds.visibility', '=', 1)
        ->groupBy('categories.id')
        ->get();
        
        return $category_list;
    }


    public function list_location()
    {
    	$location_list = DB::table('locations')
        ->select('locations.id','locations.title', DB::raw('COUNT(classifieds.id) as post_count'))
        ->leftJoin('classifieds','classifieds.location_city','=','locations.id')
        ->where('locations.type','=',2)
        ->where('classifieds.visibility', '=', 1)
        ->groupBy('locations.id')
        ->get();
        
        return $location_list;
    }



    public function get_featured()
    {
        $list_count = DB::table('options')->where('name', '=', 'classified_featured_post_in_listing')->first();
        $classifieds_list = DB::table('classifieds')
                                ->leftJoin('post_boosts','classifieds.id','=','post_boosts.classifieds_id')
                                ->where('classifieds.featured', '=', 1)
                                ->where('classifieds.visibility', '=', 1)
                                ->where('post_boosts.boost_type', '=', 2)
                                ->where('post_boosts.status', '=', 1)
                                ->orderBy(DB::raw('RAND()'))
                                ->take((int)$list_count->options)
                                ->get();
         $classifieds_list = $this->classifiedFilter($classifieds_list);

        return $classifieds_list;
    }



    public function get_one($slug)
    {
        $classified_one = DB::table('classifieds')
                    ->where('slug', '=', $slug)
                    ->first();
        
        return $classified_one;
    }


    public function get_list()
    {
        $listing_count = DB::table('options')->where('name', '=', 'classified_count_in_listing')->first();
        $classified_list = DB::table('classifieds')
                            ->where('visibility', '=', 1)
                            ->orderBy('id', 'desc')
                            ->take((int)$listing_count->options);
        
        return $classified_list;
    }



    public function homesearch_list($search_text)
    {
        $listing_count = 4;
        $x = 0;

        $return_array = array();

        $classified_list = DB::table('classifieds')
                        ->select('classifieds.*')
                        ->leftJoin('categories as parent','classifieds.parentid','=','parent.id')
                        ->leftJoin('categories as child','classifieds.category','=','child.id')
                        ->leftJoin('locations as province','classifieds.location_province','=','province.id')
                        ->leftJoin('locations as city','classifieds.location_city','=','city.id')
                        ->leftJoin('locations as town','classifieds.location_town','=','town.id')
                        ->leftJoin('locations as building','classifieds.location_building','=','building.id')
                        ->where('classifieds.visibility', '=', 1)
                        ->where(function ($query) use ($search_text){
                            $query->Where('classifieds.title','like',"%$search_text%")
                                ->orWhere('classifieds.tags','like', "%$search_text%")
                                ->orWhere('classifieds.description','like', "%$search_text%")
                                ->orWhere('parent.title','like', "%$search_text%")
                                ->orWhere('child.title','like', "%$search_text%")
                                ->orWhere('province.title','like', "%$search_text%")
                                ->orWhere('city.title','like', "%$search_text%")
                                ->orWhere('town.title','like',$search_text)
                                ->orWhere('building.title','like', "%$search_text%");
                              })
                        ->groupBy('classifieds.id')
                        ->orderBy('classifieds.id', 'desc')
                        ->take((int)$listing_count)
                        ->get();
        $classified_list = $this->classifiedFilter($classified_list);

        foreach($classified_list  as $key => $list){

            $return_array[$x] = $list;
            $return_array[$x]->return_type = 'classified';

            $x++;
        }




        $article_list = DB::table('articles')
                        ->where('title','like',"%$search_text%")
                        ->orWhere('tags','like', "%$search_text%")
                        ->orWhere('body','like', "%$search_text%")
                        ->take((int)$listing_count)
                        ->get();
        foreach($article_list  as $key => $list){

            $return_array[$x] = $list;
            $return_array[$x]->return_type = 'article';

            $x++;
        }

        $news_list = DB::table('news')
                        ->where('title','like',"%$search_text%")
                        ->orWhere('tags','like', "%$search_text%")
                        ->orWhere('body','like', "%$search_text%")
                        ->take((int)$listing_count)
                        ->get();
        foreach($news_list  as $key => $list){

            $return_array[$x] = $list;
            $return_array[$x]->return_type = 'news';

            $x++;
        }

        $images_list = DB::table('images')
                        ->where('title','like',"%$search_text%")
                        ->orWhere('tags','like', "%$search_text%")
                        ->orWhere('body','like', "%$search_text%")
                        ->take((int)$listing_count)
                        ->get();
        foreach($images_list  as $key => $list){

            $return_array[$x] = $list;
            $return_array[$x]->return_type = 'image';

            $x++;
        }

        $videos_list = DB::table('videos')
                        ->where('title','like',"%$search_text%")
                        ->orWhere('tags','like', "%$search_text%")
                        ->orWhere('body','like', "%$search_text%")
                        ->take((int)$listing_count)
                        ->get();
        foreach($videos_list  as $key => $list){

            $return_array[$x] = $list;
            $return_array[$x]->return_type = 'video';

            $x++;
        }


        /*$per_page = $this->pagination;
        $return_array = $return_array->paginate($per_page);*/
        
        return $return_array;
    }


    


    public function search_list($search_text)
    {
        $listing_count = DB::table('options')->where('name', '=', 'classified_count_in_listing')->first();

        $classified_list = DB::table('classifieds')
                        ->select('classifieds.*')
                        ->leftJoin('categories as parent','classifieds.parentid','=','parent.id')
                        ->leftJoin('categories as child','classifieds.category','=','child.id')
                        ->leftJoin('locations as province','classifieds.location_province','=','province.id')
                        ->leftJoin('locations as city','classifieds.location_city','=','city.id')
                        ->leftJoin('locations as town','classifieds.location_town','=','town.id')
                        ->leftJoin('locations as building','classifieds.location_building','=','building.id')
                        ->where('classifieds.visibility', '=', 1)
                        ->where(function ($query) use ($search_text){
                            $query->Where('classifieds.title','like',"%$search_text%")
                                ->orWhere('classifieds.tags','like', "%$search_text%")
                                ->orWhere('classifieds.description','like', "%$search_text%")
                                ->orWhere('parent.title','like', "%$search_text%")
                                ->orWhere('child.title','like', "%$search_text%")
                                ->orWhere('province.title','like', "%$search_text%")
                                ->orWhere('city.title','like', "%$search_text%")
                                ->orWhere('town.title','like',$search_text)
                                ->orWhere('building.title','like', "%$search_text%");
                              })
                        ->groupBy('classifieds.id')
                        ->orderBy('classifieds.id', 'desc')
                        ->take((int)$listing_count->options);
        
        return $classified_list;
    }



    public function list_bylocation($id)
    {
        $listing_count = DB::table('options')->where('name', '=', 'classified_count_in_listing')->first();
        $classified_list = DB::table('classifieds')
                            ->where('visibility', '=', 1)
                            ->where('location_city', '=', $id)
                            ->orderBy('id', 'desc')
                            ->take((int)$listing_count->options);
        
        return $classified_list;
    }



    public function list_bycategory($id)
    {
        $listing_count = DB::table('options')->where('name', '=', 'classified_count_in_listing')->first();
        $classified_list = DB::table('classifieds')
                            ->where('visibility', '=', 1)
                            ->where('parentid', '=', $id)
                            ->orderBy('id', 'desc')
                            ->take((int)$listing_count->options);
        
        return $classified_list;
    }



    public function related_products($slug)
    {
        $classified_one = $this->get_one($slug);

        $list_count = DB::table('options')->where('name', '=', 'classified_related_products_count')->first();

        $related_products = DB::table('classifieds')
                        ->where('visibility', '=', 1)
                        ->where('id','!=', $classified_one->id)
                        ->where('category','like', $classified_one->category)
                        ->orderBy(DB::raw('RAND()'))
                        ->take((int)$list_count->options)
                        ->get();
        
        return $related_products ;
    }



    public function classifiedFilter($classified_list)
    {
        $top_add_option_row =  DB::table('options')->where('name', '=', 'adds_top_add_per_click')->first();
        $top_add_option = $top_add_option_row->options;
        $featured_add_option_row =  DB::table('options')->where('name', '=', 'adds_featured_add_per_click')->first();
        $featured_add_option = $featured_add_option_row->options;


        $option_row =  DB::table('options')->where('name', '=', 'adds_duration_days')->first();
        $duration_type = array_map('trim', explode(',', $option_row->options));
        $duration_type_key = 1;
            $new_duration_type[0]= "Select One";
        foreach($duration_type  as $key => $duration){
            $new_duration_type[$duration_type_key]= $duration;
            $duration_type_key++;
        }

        $something = array();


        if($classified_list){

            foreach($classified_list  as $key => $classified){

                if($classified->featured === 1){
                $boost_row = DB::table('post_boosts')
                            ->where('classifieds_id','=' ,$classified->id)
                            ->where('status','=' , 1)
                            ->first();
                    if($boost_row){
                        $classified->boost_status = $boost_row->status;
                        $boost_type = $boost_row->boost_type;
                        $classified->boost_type = $boost_type;
                        $budget = $boost_row->budget;
                        if($boost_type === 1){
                            $target_click_count = ceil($budget/$top_add_option);
                        }else{
                            $target_click_count = ceil($budget/$featured_add_option);
                        }
                        

                        $current_count = Counter::show('classified_featured', $classified->id);
                        
                        $classified->duration  = $new_duration_type[$boost_row->duration];
                        $expire_date = \Carbon\Carbon::createFromTimeStamp(strtotime($boost_row->created_at))->addDays($new_duration_type[$boost_row->duration]);
                        
                        $something[$classified->id]['target'] = $target_click_count;
                        $something[$classified->id]['budget'] = $budget;
                        $something[$classified->id]['boost_type'] = $boost_type;
                        $something[$classified->id]['now'] =  \Carbon\Carbon::now();
                        $something[$classified->id]['expire_date'] = $expire_date;
                        $something[$classified->id]['current_count'] = $current_count;

                        if(($current_count >= $target_click_count) || (\Carbon\Carbon::now() >= $expire_date)){
                            $affectedRows = DB::table('post_boosts')->where('id', '=', $boost_row->id)->update(array('status' => 2));
                            $classified->boost_status = 2;
                        }
                        

                    }else{
                        $classified->boost_status = 0;
                    }
                }
                            
            }


        }

        return $classified_list;
    }



    public function news_by_category()
    {
        $category_list = DB::table('categories')->where('type','=',1)->get();
        $category_array = array();

        if($category_list){
            foreach($category_list  as $category){
                $category_array[$category->id]['id']= $category->id;
                $category_array[$category->id]['title']= $category->title;
                $category_array[$category->id]['slug']= $category->slug;
                $category_array[$category->id]['news_list']= array();

            }
        }

        $news_list = DB::table('news')->orderBy('id', 'desc')->get();

        foreach($news_list  as $news){
            if($news->category_id != null && !empty($news->category_id)){
                foreach(json_decode($news->category_id, TRUE)  as $key => $value){
                    $category_array[$value]['news_list'][] = $news;        
                }
            }


        }

        /*$category_list = DB::table('categories')
        ->select('categories.id','categories.title','parent.id As parent_id', 'parent.title AS parent_title')
        ->leftJoin('categories AS parent','categories.parentid','=','parent.id')
        ->where('categories.type','=',1)
        ->get();


        $latest_news_count_front = DB::table('options')->where('name', '=', 'news_count_in_front')->first();*/

       
        
        return $category_array;
    }






    public function gossip_zone()
    {
    	$news_category_list_count = DB::table('options')->where('name', '=', 'news_category_list_count')->first();

    	$gossip_row = DB::table('categories')->where('slug', '=', 'gossip-news')->first();

    	$gossip_news_list = DB::table('news')->where('category_id','like', "%$gossip_row->id%")->orderBy('id', 'desc')->take((int)$news_category_list_count->options)->get();
        
        return $gossip_news_list ;
    }


    


    



    
}

