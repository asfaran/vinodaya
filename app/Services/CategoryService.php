<?php
namespace App\Services;

use Auth;
use Crypt;
use DB;


class CategoryService
{
    /**
    */

    public function __construct()
    {
    }


    /**
     * @return bool
     */
    public function category_array($type)
    {

    	$category_array = array();

		$category_list = DB::table('categories')
		->select('categories.id','categories.title','parent.id As parent_id', 'parent.title AS parent_title')
		->leftJoin('categories AS parent','categories.parentid','=','parent.id')
		->where('categories.type','=', $type)
		->get();



		/*$category_list = DB::table('categories')->select('id', 'title')->get();*/

		if($category_list){
			foreach($category_list  as $category){
				$category_array[$category->id]= $category->title;

			}
		}

		 return $category_array;
    }



    public function Comments_array($type, $id)
    {

    	$Comments_array = DB::table('comments')
		->where('post_type','=', $type)
		->where('post_id','=', $id)
		->where('approved','=', 1)
		->get();

		 return $Comments_array;
    }






}