<?php
namespace App\Services;

use Auth;
use Crypt;
use DB;


class ArticleService
{
    /**
    */

    public function __construct()
    {
    }



    /**
     * @return bool
     */
    public function get_list()
    {
        $listing_count = DB::table('options')->where('name', '=', 'article_count_in_listing')->first();
    	$article_list = DB::table('articles')->orderBy('id', 'desc')->take((int)$listing_count->options);
        
        return $article_list;
    }


    public function get_featured($per_page)
    {
        $featured_count = DB::table('options')->where('name', '=', 'featured_article_count')->first();
        $article_list = DB::table('articles')->where('featured', '=', 1)->orderBy('id', 'desc')->take((int)$featured_count->options)->get();
        
        return $article_list;
    }



     /**
     * @return bool
     */
    public function getcat_list($id)
    {
        $listing_count = DB::table('options')->where('name', '=', 'article_count_in_listing')->first();
        $article_list = DB::table('articles')->where('category_id','like', "%$id%")->orderBy('id', 'desc')->take((int)$listing_count->options);
        
        return $article_list;
    }


    public function get_one($id)
    {
        $article_one = DB::table('articles')
                    ->where('id', '=', $id)
                    ->first();
        
        return $article_one;
    }

    public function get_related($id)
    {
        $article_one = DB::table('articles')
                    ->where('id', '=', $id)
                    ->first();

        $related_list = DB::table('articles')->where('category_id','like', "%$article_one->category_id%")->orderBy('id', 'desc')->take(4)->get();
        
        return $related_list;
    }

    
}

