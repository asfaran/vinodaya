<?php
namespace App\Services;

use Auth;
use Crypt;
use DB;


class ImageService
{
    /**
    */

    public function __construct()
    {
    }



    /**
     * @return bool
     */
    public function cartoon_ofthe_day()
    {
        $cartoon_row = DB::table('categories')->where('slug', '=', 'cartoon')->where('type', '=', 3)->first();
    	$cartoon_ofthe_day = DB::table('images')->where('category_id','like', "%$cartoon_row->id%")->orderBy('id', 'desc')->first();
        
        return $cartoon_ofthe_day;
    }



    public function model_ofthe_day()
    {
        $model_row = DB::table('categories')->where('slug', '=', 'model')->where('type', '=', 3)->first();
        $model_ofthe_day = DB::table('images')->where('category_id','like', "%$model_row->id%")->orderBy('id', 'desc')->first();
        
        return $model_ofthe_day;
    }

    public function get_list()
    {
        $listing_count = DB::table('options')->where('name', '=', 'image_count_in_listing')->first();
        $image_list = DB::table('images')->orderBy('id', 'desc')->take((int)$listing_count->options);
        
        return $image_list;
    }


    public function getcat_list($id)
    {
        $listing_count = DB::table('options')->where('name', '=', 'image_count_in_listing')->first();
        $image_list = DB::table('images')->where('category_id','like', "%$id%")->orderBy('id', 'desc')->take((int)$listing_count->options);
        
        return $image_list;
    }



    public function get_one($id)
    {
        $image_one = DB::table('images')
                    ->where('id', '=', $id)
                    ->first();
        
        return $image_one;
    }


    public function get_related($id)
    {
        $image_one = DB::table('images')
                    ->where('id', '=', $id)
                    ->first();

        $related_list = DB::table('images')->where('category_id','like', "%$image_one->category_id%")->orderBy('id', 'desc')->take(3)->get();
        
        return $related_list;
    }

    
}

