<?php
namespace App\Services;

use Auth;
use Crypt;
use DB;


class AddsService
{
    /**
    */

    public function __construct()
    {
    }



    /**
     * @return bool
     */
    public function home_adds()
    {
        $home_adds = array();
        $adds_position_list = array('Add-1','Add-2','Add-3','Add-4');
        $adds_type_rows = DB::table('adds_types')->whereIn('position_name',  $adds_position_list)->get();
        foreach($adds_type_rows as $key => $value) {
            if($value->position_name == "Add-1"){
                $home_adds['Add-1'] = DB::table('adds')
                    ->where('adds_type', '=', $value->id)
                    ->where('status', '=', 1)
                    ->where('visibility', '=', 1)
                    ->where('approve', '=', 1)
                    ->orderBy(DB::raw('RAND()'))
                    ->take(1)
                    ->first();

            }elseif($value->position_name == "Add-2"){
                $home_adds['Add-2'] = DB::table('adds')
                    ->where('adds_type', '=', $value->id)
                    ->where('status', '=', 1)
                    ->where('visibility', '=', 1)
                    ->where('approve', '=', 1)
                    ->orderBy(DB::raw('RAND()'))
                    ->take(1)
                    ->first();

            }elseif($value->position_name == "Add-3"){
                $home_adds['Add-3'] = DB::table('adds')
                    ->where('adds_type', '=', $value->id)
                    ->where('status', '=', 1)
                    ->where('visibility', '=', 1)
                    ->where('approve', '=', 1)
                    ->orderBy(DB::raw('RAND()'))
                    ->take(2)
                    ->get();

            }elseif($value->position_name == "Add-4"){
                $home_adds['Add-4'] = DB::table('adds')
                    ->where('adds_type', '=', $value->id)
                    ->where('status', '=', 1)
                    ->where('visibility', '=', 1)
                    ->where('approve', '=', 1)
                    ->orderBy(DB::raw('RAND()'))
                    ->take(2)
                    ->get();

            }
            
        }
    	
        
        return $home_adds;
    }



    /**
     * @return bool
     */
    public function classifiedDP()
    {
        $page_adds = array();
        $adds_position_list = array('Add-5','Add-9');
        $adds_type_rows = DB::table('adds_types')->whereIn('position_name',  $adds_position_list)->get();
        foreach($adds_type_rows as $key => $value) {
            if($value->position_name == "Add-5"){
                $page_adds['Add-5'] = DB::table('adds')
                    ->where('adds_type', '=', $value->id)
                    ->where('status', '=', 1)
                    ->where('visibility', '=', 1)
                    ->where('approve', '=', 1)
                    ->orderBy(DB::raw('RAND()'))
                    ->take(1)
                    ->first();

            }elseif($value->position_name == "Add-9"){
                $page_adds['Add-9'] = DB::table('adds')
                    ->where('adds_type', '=', $value->id)
                    ->where('status', '=', 1)
                    ->where('visibility', '=', 1)
                    ->where('approve', '=', 1)
                    ->orderBy(DB::raw('RAND()'))
                    ->take(1)
                    ->first();

            }
            
        }
        
        
        return $page_adds;
    }



    /**
     * @return bool
     */
    public function informative()
    {
        $page_adds = array();
        $adds_position_list = array('Add-6','Add-7','Add-9','Add-10');
        $adds_type_rows = DB::table('adds_types')->whereIn('position_name',  $adds_position_list)->get();
        foreach($adds_type_rows as $key => $value) {
                $page_adds[] = DB::table('adds')
                    ->where('adds_type', '=', $value->id)
                    ->where('status', '=', 1)
                    ->where('visibility', '=', 1)
                    ->where('approve', '=', 1)
                    ->orderBy(DB::raw('RAND()'))
                    ->take(1)
                    ->first();
            
        }
        
        
        return $page_adds;
    }



    /**
     * @return bool
     */
    public function detail_adds()
    {
        $page_adds = array();
        $adds_position_list = array('Add-5', 'Add-6', 'Add-7');
        $adds_type_rows = DB::table('adds_types')->whereIn('position_name',  $adds_position_list)->get();
        foreach($adds_type_rows as $key => $value) {
            if($value->position_name == "Add-5"){
                $page_adds['Add-5'] = DB::table('adds')
                    ->where('adds_type', '=', $value->id)
                    ->where('status', '=', 1)
                    ->where('visibility', '=', 1)
                    ->where('approve', '=', 1)
                    ->orderBy(DB::raw('RAND()'))
                    ->take(1)
                    ->first();

            }elseif($value->position_name == "Add-6"){
                $page_adds['Add-6'] = DB::table('adds')
                    ->where('adds_type', '=', $value->id)
                    ->where('status', '=', 1)
                    ->where('visibility', '=', 1)
                    ->where('approve', '=', 1)
                    ->orderBy(DB::raw('RAND()'))
                    ->take(1)
                    ->first();

            }elseif($value->position_name == "Add-7"){
                $page_adds['Add-7'] = DB::table('adds')
                    ->where('adds_type', '=', $value->id)
                    ->where('status', '=', 1)
                    ->where('visibility', '=', 1)
                    ->where('approve', '=', 1)
                    ->orderBy(DB::raw('RAND()'))
                    ->take(1)
                    ->first();

            }
            
        }
        
        
        return $page_adds;
    }


    /**
     * @return bool
     */
    public function listing_adds($per_page, $adds_type_rows = null)
    {
        $page_adds = array();
        $take_count = ceil($per_page/2);
        $adds_type_rows = DB::table('adds_types')->where('position_name', '=', 'Add-8')->first();
        $page_adds = DB::table('adds')
                    ->where('adds_type', '=', $adds_type_rows->id)
                    ->where('status', '=', 1)
                    ->where('visibility', '=', 1)
                    ->where('approve', '=', 1)
                    ->orderBy(DB::raw('RAND()'))
                    ->take($take_count)
                    ->get();
        
        
        return $page_adds;
    }


    /**
     * @return bool
     */
    public function news_listing($per_page)
    {
        $page_adds = array();
        $adds_position_list = array('Add-8', 'Add-11', 'Add-12');
        $adds_type_rows = DB::table('adds_types')->whereIn('position_name',  $adds_position_list)->get();
        foreach($adds_type_rows as $key => $value) {
            if($value->position_name == "Add-8"){
                $page_adds['Add-8'] = DB::table('adds')
                    ->where('adds_type', '=', $value->id)
                    ->where('status', '=', 1)
                    ->where('visibility', '=', 1)
                    ->where('approve', '=', 1)
                    ->orderBy(DB::raw('RAND()'))
                    ->take($per_page)
                    ->get();

            }elseif($value->position_name == "Add-11"){
                $page_adds['Add-11'] = DB::table('adds')
                    ->where('adds_type', '=', $value->id)
                    ->where('status', '=', 1)
                    ->where('visibility', '=', 1)
                    ->where('approve', '=', 1)
                    ->orderBy(DB::raw('RAND()'))
                    ->take(1)
                    ->first();

            }elseif($value->position_name == "Add-12"){
                $page_adds['Add-12'] = DB::table('adds')
                    ->where('adds_type', '=', $value->id)
                    ->where('status', '=', 1)
                    ->where('visibility', '=', 1)
                    ->where('approve', '=', 1)
                    ->orderBy(DB::raw('RAND()'))
                    ->take(1)
                    ->first();

            }
            
        }
        
        
        return $page_adds;
    }




    /**
     * @return bool
     */
    public function classifiedLP()
    {
        $page_adds = array();
        $adds_type_rows = DB::table('adds_types')->where('position_name', '=', 'Add-10')->first();
        $page_adds = DB::table('adds')
                    ->where('adds_type', '=', $adds_type_rows->id)
                    ->where('status', '=', 1)
                    ->where('visibility', '=', 1)
                    ->where('approve', '=', 1)
                    ->orderBy(DB::raw('RAND()'))
                    ->take(3)
                    ->get();
        
        
        return $page_adds;
    }

    
}

