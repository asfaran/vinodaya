<?php
namespace App\Services;

use Auth;
use Crypt;
use DB;


class VideoService
{
    /**
    */

    public function __construct()
    {
    }



    /**
     * @return bool
     */
    public function latest_videos()
    {
    	$latest_video_count = DB::table('options')->where('name', '=', 'video_count_in_front')->first();

    	$latest_videos_list = DB::table('videos')->orderBy('id', 'desc')->take((int)$latest_video_count->options)->get();

        foreach($latest_videos_list  as $video){
            if($video->thumb){
                $video->thumbnail_type = "image";
                $video->thumbnail = null;
            }else{
                $video->thumbnail_type = "thumbnail";
                $video->thumbnail = $this->get_video_thumbnail($video->video_url);
            }
            $video->embed_url = $this->get_embed($video->video_url);
        }
        
        return $latest_videos_list;
    }



    public function patta_videos()
    {
    	$featured_video_count = DB::table('options')->where('name', '=', 'featured_video_count')->first();

    	$featured_video = DB::table('videos')->where('featured', '=', 1)->orderBy('id', 'desc')->take((int)$featured_video_count->options)->get();

        foreach($featured_video  as $video){
            if($video->thumb){
                $video->thumbnail_type = "image";
                $video->thumbnail = null;
            }else{
                $video->thumbnail_type = "thumbnail";
                $video->thumbnail = $this->get_video_thumbnail($video->video_url);
            }
            $video->embed_url = $this->get_embed($video->video_url);
        }
        
        return $featured_video ;
    }

    public function get_video_thumbnail( $src )
    {
        $url_pieces = explode('/', $src);
    
        if ( $url_pieces[2] == 'vimeo.com' ) { // If Vimeo
            $id = $url_pieces[3];
            $hash = unserialize(file_get_contents('http://vimeo.com/api/v2/video/' . $id . '.php'));
            $thumbnail = $hash[0]['thumbnail_large'];
        } elseif ( $url_pieces[2] == 'www.youtube.com' ) { // If Youtube
            $extract_id = explode('?', $url_pieces[3]);
            $id_with_v = $extract_id[1];
            $id = str_replace("v=", "", $id_with_v);
            $thumbnail = 'http://img.youtube.com/vi/' . $id . '/mqdefault.jpg';
        }
        return $thumbnail;
    }


    public function get_video_viewscount( $src )
    {
        $url_pieces = explode('/', $src);
    
        if ( $url_pieces[2] == 'vimeo.com' ) { // If Vimeo
            $id = $url_pieces[3];
            $hash = unserialize(file_get_contents('http://vimeo.com/api/v2/video/' . $id . '.php'));
            /*$thumbnail = $hash[0]['thumbnail_large'];
                $thumb_index = $hash['STATS_NUMBER_OF_PLAYS'][0];
                $video_view_count = $vals[$thumb_index]['value'];*/

            $xml = simplexml_load_file("http://vimeo.com/api/v2/video/$video_id.xml");
            foreach ($xml->video as $video) {
                $video_view_count = $video->stats_number_of_plays;
            }
            
        } elseif ( $url_pieces[2] == 'www.youtube.com' ) { 
            $extract_id = explode('?', $url_pieces[3]);
            $id_with_v = $extract_id[1];
            $id = str_replace("v=", "", $id_with_v);

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, 'http://gdata.youtube.com/feeds/api/videos/' . $id.'?v=2&alt=json');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
            $data = curl_exec($curl);
            curl_close($curl);

            
            /*$video_view_count = $data->entry->{'yt$statistics'}->viewCount;*/
            $video_view_count = $data;
        }
        return $video_view_count;
    }



    public function get_embed( $src )
    {
        $url_pieces = explode('/', $src);
    
        if ( $url_pieces[2] == 'vimeo.com' ) { // If Vimeo
            $return_url = $src;
        } elseif ( $url_pieces[2] == 'www.youtube.com' ) { // If Youtube
            $extract_id = explode('?', $url_pieces[3]);
            $id_with_v = $extract_id[1];
            $id = str_replace("v=", "", $id_with_v);
            $return_url = 'https://www.youtube.com/embed/' . $id;
        }
        return $return_url;
    }


    public function get_list($per_page)
    {
        $listing_count = DB::table('options')->where('name', '=', 'video_count_in_listing')->first();

        $videos_list = DB::table('videos')->orderBy('id', 'desc')->take((int)$listing_count->options)->paginate($per_page);;

        foreach($videos_list  as $video){
            if($video->thumb){
                $video->thumbnail_type = "image";
                $video->thumbnail = null;
            }else{
                $video->thumbnail_type = "thumbnail";
                $video->thumbnail = $this->get_video_thumbnail($video->video_url);
            }
            $video->embed_url = $this->get_embed($video->video_url);
           /* $video->views_count = $this->get_video_viewscount($video->video_url);*/
        }
        
        return $videos_list;
    }




    public function getcat_list($id, $per_page)
    {
        $listing_count = DB::table('options')->where('name', '=', 'video_count_in_listing')->first();

        $videos_list = DB::table('videos')->where('category_id','like', "%$id%")->orderBy('id', 'desc')->take((int)$listing_count->options)->paginate($per_page);;

        foreach($videos_list  as $video){
            if($video->thumb){
                $video->thumbnail_type = "image";
                $video->thumbnail = null;
            }else{
                $video->thumbnail_type = "thumbnail";
                $video->thumbnail = $this->get_video_thumbnail($video->video_url);
            }
            $video->embed_url = $this->get_embed($video->video_url);
           /* $video->views_count = $this->get_video_viewscount($video->video_url);*/
        }
        
        return $videos_list;
    }


    public function get_one($id)
    {
        $video_one = DB::table('videos')
                    ->where('id', '=', $id)
                    ->first();
            if($video_one->thumb){
                $video_one->thumbnail_type = "image";
                $video_one->thumbnail = null;
            }else{
                $video_one->thumbnail_type = "thumbnail";
                $video_one->thumbnail = $this->get_video_thumbnail($video_one->video_url);
            }
            $video_one->embed_url = $this->get_embed($video_one->video_url);
        
        return $video_one;
    }


    public function get_related($id)
    {
        $video_one = DB::table('videos')
                    ->where('id', '=', $id)
                    ->first();

        $related_list = DB::table('videos')->where('category_id','like', "%$video_one->category_id%")->orderBy('id', 'desc')->take(3)->get();

        foreach($related_list  as $video){
            if($video->thumb){
                $video->thumbnail_type = "image";
                $video->thumbnail = null;
            }else{
                $video->thumbnail_type = "thumbnail";
                $video->thumbnail = $this->get_video_thumbnail($video->video_url);
            }
            $video->embed_url = $this->get_embed($video->video_url);
           /* $video->views_count = $this->get_video_viewscount($video->video_url);*/
        }
        
        return $related_list;
    }


    
}

