<?php namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class article extends Model {

	use SoftDeletes;

    protected $table = 'articles';

    protected $fillable = ['id','slug', 'title', 'body', 'tags', 'user_id', 'category_id', 'type','ordertype', 'thumb', 'excerpt', 'status', 'featured', 'visibility', 'publish', 'approve', 'show_in_homepage', 'published_at', 'featured_at', 'deleted_at'];

    protected $dates = ['published_at','deleted_at'];

    protected $softDelete = true;

    /**
     * Post belongs to user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

   
    /**
     * Get Post All comments
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany('comments');
    }

}
