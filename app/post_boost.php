<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class post_boost extends Model {

	use SoftDeletes;

    protected $table = 'post_boosts';

    protected $fillable = ['id', 'user_id', 'classifieds_id', 'boost_type', 'duration', 'budget', 'paypal_id','paypal_reference', 'paid_amount', 'status','updated_at', 'featured_at', 'deleted_at'];

    protected $dates = ['featured_at', 'updated_at','deleted_at'];

    protected $softDelete = true;

}
