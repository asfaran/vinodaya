<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class pages extends Model {

	use SoftDeletes;

    protected $table = 'pages';

    protected $fillable = ['id','slug', 'title', 'body', 'tags', 'body', 'published_at', 'updated_at', 'deleted_at'];

    protected $dates = ['published_at', 'updated_at', 'deleted_at'];

    protected $softDelete = true;

}
